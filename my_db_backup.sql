#
# TABLE STRUCTURE FOR: answers
#

DROP TABLE IF EXISTS `answers`;

CREATE TABLE `answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `answer` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `is_correct` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# TABLE STRUCTURE FOR: bus_line
#

DROP TABLE IF EXISTS `bus_line`;

CREATE TABLE `bus_line` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `bus_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `bus_line` (`id`, `title`, `description`, `bus_id`) VALUES (1, 'خط برزة', '<p dir=\"rtl\">\r\n	برزة- شارع الثورة - ساحة الامويين - المزة</p>\r\n', 5);
INSERT INTO `bus_line` (`id`, `title`, `description`, `bus_id`) VALUES (2, 'خط الميدان', '<p dir=\"rtl\">\r\n	ميدان - زاهرة قديمة - زاهية جديدة - مزة</p>\r\n', 5);


#
# TABLE STRUCTURE FOR: bus_stop
#

DROP TABLE IF EXISTS `bus_stop`;

CREATE TABLE `bus_stop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `busline_id` int(11) NOT NULL,
  `stop_name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `lat` varchar(255) NOT NULL,
  `long` varchar(255) NOT NULL,
  `time_arrivals` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO `bus_stop` (`id`, `busline_id`, `stop_name`, `address`, `lat`, `long`, `time_arrivals`) VALUES (1, 1, 'برزة', 'جانب مشفى حاميش', '', '', '<p>\r\n	8:15</p>\r\n<p>\r\n	4:30</p>\r\n');
INSERT INTO `bus_stop` (`id`, `busline_id`, `stop_name`, `address`, `lat`, `long`, `time_arrivals`) VALUES (2, 1, 'مشفى الحياة', 'مشفى الحياة عند الجسر', '', '', '<p>\r\n	8:20</p>\r\n<p>\r\n	4:10</p>\r\n');
INSERT INTO `bus_stop` (`id`, `busline_id`, `stop_name`, `address`, `lat`, `long`, `time_arrivals`) VALUES (3, 1, 'ساحة الامويين', 'عند الساحة', '', '', '<p>\r\n	8:30</p>\r\n<p>\r\n	4:00</p>\r\n');


#
# TABLE STRUCTURE FOR: change_line_request
#

DROP TABLE IF EXISTS `change_line_request`;

CREATE TABLE `change_line_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `requester_id` int(11) NOT NULL,
  `request_text` text NOT NULL,
  `request_date` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: change_stop_request
#

DROP TABLE IF EXISTS `change_stop_request`;

CREATE TABLE `change_stop_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `requester_id` int(11) NOT NULL,
  `requested_stop` varchar(255) NOT NULL,
  `request_text` text NOT NULL,
  `request_date` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO `change_stop_request` (`id`, `requester_id`, `requested_stop`, `request_text`, `request_date`) VALUES (1, 4, 'مشفى الحياة', 'هيك', '2022-05-23 11:18:08');
INSERT INTO `change_stop_request` (`id`, `requester_id`, `requested_stop`, `request_text`, `request_date`) VALUES (2, 4, 'مشفى الحياة', 'هيك', '2022-05-23 11:18:51');
INSERT INTO `change_stop_request` (`id`, `requester_id`, `requested_stop`, `request_text`, `request_date`) VALUES (3, 7, 'مشفى الحياة', 'زيارة بيت اهلي', '2022-05-23 14:22:39');


#
# TABLE STRUCTURE FOR: child_notes
#

DROP TABLE IF EXISTS `child_notes`;

CREATE TABLE `child_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `child_id` int(11) NOT NULL,
  `note_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `child_notes` (`id`, `child_id`, `note_title`, `note`, `created_date`) VALUES (1, 1, 'مشاغب كثير الحركة', 'مشاغبة خلال خصة الرياضيات والتشاجر مع أبو بريص', '2022-06-25 11:11:09');
INSERT INTO `child_notes` (`id`, `child_id`, `note_title`, `note`, `created_date`) VALUES (2, 1, 'علامة اختبار الانكليزي', 'لقد حصل على درجة ممتازة', '2022-06-25 11:11:09');


#
# TABLE STRUCTURE FOR: children
#

DROP TABLE IF EXISTS `children`;

CREATE TABLE `children` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `child_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `children` (`id`, `child_name`, `parent_id`, `section_id`, `is_active`, `created_date`) VALUES (1, 'احسان', 7, 1, 1, '2022-06-24 23:10:15');
INSERT INTO `children` (`id`, `child_name`, `parent_id`, `section_id`, `is_active`, `created_date`) VALUES (2, 'مجد الدين', 7, 1, 1, '2022-06-24 23:10:15');
INSERT INTO `children` (`id`, `child_name`, `parent_id`, `section_id`, `is_active`, `created_date`) VALUES (3, 'صلاح الدين', 7, 2, 1, '2022-06-24 23:10:15');


#
# TABLE STRUCTURE FOR: class_subject
#

DROP TABLE IF EXISTS `class_subject`;

CREATE TABLE `class_subject` (
  `id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `book` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# TABLE STRUCTURE FOR: classes
#

DROP TABLE IF EXISTS `classes`;

CREATE TABLE `classes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `school_id` int(11) NOT NULL,
  `class_name` enum('KJ1','KJ2','KJ3','أول','ثاني','ثالث','رابع','خامس','سادس','سابع','ثامن','تاسع','عاشر','حادي عشر','بكالوريا') COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# TABLE STRUCTURE FOR: customer
#

DROP TABLE IF EXISTS `customer`;

CREATE TABLE `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `nationalImage` varchar(255) NOT NULL,
  `type` enum('admin','school','parent','bus') NOT NULL,
  `related_id` int(11) DEFAULT NULL,
  `stop_id` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `register_date` datetime NOT NULL DEFAULT current_timestamp(),
  `firebase_token` varchar(255) NOT NULL,
  `is_new` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

INSERT INTO `customer` (`id`, `name`, `mobile`, `password`, `email`, `address`, `nationalImage`, `type`, `related_id`, `stop_id`, `created_date`, `is_active`, `register_date`, `firebase_token`, `is_new`) VALUES (1, 'amer alhosary', '0962554805', '$2a$08$lZ8cWWW.dhWZ4fWb4P2Gx.BTqVE4RRHybl3.IYtL3eDouqr14w8.S', 'amer.alhosary1993@gmail.com', 'damascus wjomray', '', 'admin', NULL, NULL, '2020-09-14 23:09:58', 1, '2020-09-14 23:09:58', '', 1);
INSERT INTO `customer` (`id`, `name`, `mobile`, `password`, `email`, `address`, `nationalImage`, `type`, `related_id`, `stop_id`, `created_date`, `is_active`, `register_date`, `firebase_token`, `is_new`) VALUES (4, 'مدرسة الأوائل', '0962554805', '$2a$08$nJSdrjtbE.xzR7OHlOK2We3RzHx1/nfCnVtlFkABn19/s6hq6uwWi', 'awael@gmail.com', '<p>\r\n	تجربة</p>\r\n', 'a6646-f.png', 'school', NULL, 2, '2022-05-09 17:05:43', 1, '2022-05-09 17:05:41', '', 1);
INSERT INTO `customer` (`id`, `name`, `mobile`, `password`, `email`, `address`, `nationalImage`, `type`, `related_id`, `stop_id`, `created_date`, `is_active`, `register_date`, `firebase_token`, `is_new`) VALUES (5, 'باص رقم 1', '0932465348', '$2a$08$Z1JysbfkkoG3UErzotghL.hVE35ehXgUBXDy/NLtpwXRbq4kxDOhm', 'bus1@gmail.com', 'hhhh', 'f1b42-22-8-2021.jpg', 'bus', 4, NULL, '2022-05-09 17:10:39', 1, '2022-05-09 17:10:38', '', 0);
INSERT INTO `customer` (`id`, `name`, `mobile`, `password`, `email`, `address`, `nationalImage`, `type`, `related_id`, `stop_id`, `created_date`, `is_active`, `register_date`, `firebase_token`, `is_new`) VALUES (7, 'عامر الحصري', '0962554805', '$2a$08$u1HRkqnrjnqVPKHUvznBsuvf0ADvsSNFFe2zMIS7QeRb1qXoWAfiu', 'a@a.com', '<p>\r\n	hj</p>\r\n', '', 'parent', 4, 1, '2022-05-09 17:32:17', 1, '2022-05-09 17:32:14', '', 1);
INSERT INTO `customer` (`id`, `name`, `mobile`, `password`, `email`, `address`, `nationalImage`, `type`, `related_id`, `stop_id`, `created_date`, `is_active`, `register_date`, `firebase_token`, `is_new`) VALUES (8, 'الانسة مرح', '0997279193', '$2a$08$eOjEGqYZ59iFZSS7kTxS/eFpKnFGk/VlBZajrpPVrQ0/ZxptEjmri', 'busebdaa@gmail.com', '<p>\r\n	مزة الشيخ سعد </p>\r\n', '', 'bus', 4, NULL, '2022-05-24 07:13:53', 1, '2022-05-24 10:13:47', '', 0);


#
# TABLE STRUCTURE FOR: groups
#

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO `groups` (`id`, `name`, `description`) VALUES (1, 'admin', 'Administrator');
INSERT INTO `groups` (`id`, `name`, `description`) VALUES (2, 'member', 'General User');
INSERT INTO `groups` (`id`, `name`, `description`) VALUES (3, 'store', 'store');


#
# TABLE STRUCTURE FOR: lesson_teacher_section_subject
#

DROP TABLE IF EXISTS `lesson_teacher_section_subject`;

CREATE TABLE `lesson_teacher_section_subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_subject_section_id` int(11) NOT NULL,
  `lesson_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 0,
  `show_date` datetime NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `lesson_teacher_section_subject` (`id`, `teacher_subject_section_id`, `lesson_id`, `is_active`, `show_date`, `created_date`) VALUES (1, 1, 1, 1, '2022-06-25 01:56:48', '2022-06-25 22:57:17');


#
# TABLE STRUCTURE FOR: lessons
#

DROP TABLE IF EXISTS `lessons`;

CREATE TABLE `lessons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lesson_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subject_id` int(11) NOT NULL,
  `lesson_description` text COLLATE utf8_unicode_ci NOT NULL,
  `lesson_video` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lesson_youtube_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lesson_pdf` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `lessons` (`id`, `lesson_name`, `subject_id`, `lesson_description`, `lesson_video`, `lesson_youtube_link`, `lesson_pdf`, `is_active`) VALUES (1, 'الدرس الاول', 1, 'عملية الجمع', '', 'CEdqSAvdbUQ', 'test.pdf', 1);


#
# TABLE STRUCTURE FOR: login_attempts
#

DROP TABLE IF EXISTS `login_attempts`;

CREATE TABLE `login_attempts` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(16) NOT NULL,
  `login` varchar(100) DEFAULT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: movement
#

DROP TABLE IF EXISTS `movement`;

CREATE TABLE `movement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `start` time NOT NULL,
  `end` time NOT NULL,
  `driver_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (2, '2022-05-17', '04:17:31', '00:00:00', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (3, '2022-05-17', '04:21:56', '00:00:00', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (4, '2022-05-17', '04:24:37', '10:14:43', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (5, '2022-05-20', '10:14:46', '17:46:42', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (6, '2022-05-23', '17:46:47', '17:51:17', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (7, '2022-05-23', '17:51:21', '17:59:14', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (8, '2022-05-23', '17:59:21', '17:59:27', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (9, '2022-05-23', '17:59:32', '18:24:12', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (10, '2022-05-23', '18:24:16', '00:00:00', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (11, '2022-05-23', '18:24:17', '18:24:26', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (12, '2022-05-23', '22:57:52', '00:00:00', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (13, '2022-05-23', '22:57:58', '22:59:43', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (14, '2022-05-23', '23:30:15', '00:00:00', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (15, '2022-05-23', '23:30:19', '23:31:20', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (16, '2022-05-24', '00:11:47', '00:17:59', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (17, '2022-05-24', '00:26:27', '00:29:11', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (18, '2022-05-24', '00:29:15', '00:29:53', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (19, '2022-05-24', '00:29:58', '00:38:51', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (20, '2022-05-24', '00:38:55', '00:00:00', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (21, '2022-05-24', '00:39:58', '00:00:00', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (22, '2022-05-24', '00:50:14', '00:00:00', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (23, '2022-05-24', '00:50:15', '00:00:00', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (24, '2022-05-24', '09:16:27', '09:23:07', 1);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (25, '2022-05-24', '09:23:09', '09:24:17', 1);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (26, '2022-05-24', '09:25:37', '09:26:41', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (27, '2022-05-24', '09:28:06', '10:56:30', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (28, '2022-05-24', '10:56:34', '12:15:00', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (29, '2022-05-24', '23:09:43', '00:00:00', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (30, '2022-05-24', '23:09:44', '00:00:00', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (31, '2022-05-24', '23:09:46', '00:00:00', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (32, '2022-05-26', '12:33:20', '12:37:09', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (33, '2022-05-28', '11:59:47', '12:05:34', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (34, '2022-05-28', '12:05:38', '12:05:47', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (35, '2022-05-28', '12:05:53', '12:06:10', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (36, '2022-05-28', '12:06:24', '00:00:00', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (37, '2022-05-28', '12:06:25', '12:06:40', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (38, '2022-05-28', '12:06:52', '12:07:11', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (39, '2022-05-28', '12:19:46', '00:00:00', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (40, '2022-05-28', '12:19:46', '00:00:00', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (41, '2022-05-28', '12:19:54', '19:14:43', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (42, '2022-05-28', '14:27:47', '00:00:00', 8);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (43, '2022-05-28', '14:41:51', '14:48:24', 8);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (44, '2022-05-28', '14:48:24', '00:00:00', 8);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (45, '2022-05-29', '18:32:15', '00:00:00', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (46, '2022-05-30', '15:43:17', '15:43:39', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (47, '2022-05-30', '15:43:46', '00:00:00', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (48, '2022-05-30', '15:43:47', '00:00:00', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (49, '2022-05-30', '15:43:47', '00:00:00', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (50, '2022-05-30', '15:43:49', '00:00:00', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (51, '2022-05-30', '15:43:51', '00:00:00', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (52, '2022-05-30', '15:43:51', '15:44:31', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (53, '2022-05-30', '15:44:32', '15:44:57', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (54, '2022-05-30', '15:45:36', '00:00:00', 5);
INSERT INTO `movement` (`id`, `date`, `start`, `end`, `driver_id`) VALUES (55, '2022-06-07', '16:29:25', '00:00:00', 5);


#
# TABLE STRUCTURE FOR: questions
#

DROP TABLE IF EXISTS `questions`;

CREATE TABLE `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quize_id` int(11) NOT NULL,
  `question` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `questions` (`id`, `quize_id`, `question`) VALUES (1, 1, 'ناتج جمع 1+1');
INSERT INTO `questions` (`id`, `quize_id`, `question`) VALUES (2, 1, 'ناتج طرح 9-6');
INSERT INTO `questions` (`id`, `quize_id`, `question`) VALUES (3, 1, 'ناتج ضرب 5*2');
INSERT INTO `questions` (`id`, `quize_id`, `question`) VALUES (4, 1, 'ليكن لديك تفاحتين وأعطيت واحدة لصديقك كم تفاحة بقي لديك');


#
# TABLE STRUCTURE FOR: quize
#

DROP TABLE IF EXISTS `quize`;

CREATE TABLE `quize` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_subject_section_id` int(11) NOT NULL,
  `quize_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `show_date` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `quize` (`id`, `teacher_subject_section_id`, `quize_title`, `show_date`, `is_active`, `created_date`) VALUES (1, 1, 'الاختبار الأول للرياضيات', '2022-06-30 00:20:13', 1, '2022-06-29 21:21:35');
INSERT INTO `quize` (`id`, `teacher_subject_section_id`, `quize_title`, `show_date`, `is_active`, `created_date`) VALUES (2, 1, 'الاختبار الاول في اللغة العربية', '2022-06-30 00:20:13', 1, '2022-06-29 21:21:35');


#
# TABLE STRUCTURE FOR: related_customers
#

DROP TABLE IF EXISTS `related_customers`;

CREATE TABLE `related_customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer1` int(11) NOT NULL,
  `customer2` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: section
#

DROP TABLE IF EXISTS `section`;

CREATE TABLE `section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `class_id` int(11) NOT NULL,
  `max_number` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `section` (`id`, `section_name`, `class_id`, `max_number`) VALUES (1, 'شعبة أولى', 1, 20);


#
# TABLE STRUCTURE FOR: subject
#

DROP TABLE IF EXISTS `subject`;

CREATE TABLE `subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subject_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'subject.jpg',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `subject` (`id`, `subject_title`, `subject_image`) VALUES (1, 'رياضيات', 'subject.jpg');
INSERT INTO `subject` (`id`, `subject_title`, `subject_image`) VALUES (2, 'عربي', 'subject.jpg');
INSERT INTO `subject` (`id`, `subject_title`, `subject_image`) VALUES (3, 'لغة أجنبية', 'subject.jpg');
INSERT INTO `subject` (`id`, `subject_title`, `subject_image`) VALUES (4, 'حساب ذهني', 'subject.jpg');


#
# TABLE STRUCTURE FOR: teacher
#

DROP TABLE IF EXISTS `teacher`;

CREATE TABLE `teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `school_id` int(11) NOT NULL,
  `teacher_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `gender` enum('male','female') COLLATE utf8_unicode_ci NOT NULL,
  `specilization` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `identity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# TABLE STRUCTURE FOR: teacher_subject_section
#

DROP TABLE IF EXISTS `teacher_subject_section`;

CREATE TABLE `teacher_subject_section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `teacher_subject_section` (`id`, `teacher_id`, `section_id`, `subject_id`) VALUES (1, 1, 1, 1);
INSERT INTO `teacher_subject_section` (`id`, `teacher_id`, `section_id`, `subject_id`) VALUES (2, 1, 1, 2);
INSERT INTO `teacher_subject_section` (`id`, `teacher_id`, `section_id`, `subject_id`) VALUES (3, 1, 1, 3);
INSERT INTO `teacher_subject_section` (`id`, `teacher_id`, `section_id`, `subject_id`) VALUES (4, 1, 1, 4);


#
# TABLE STRUCTURE FOR: token
#

DROP TABLE IF EXISTS `token`;

CREATE TABLE `token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

INSERT INTO `token` (`id`, `user_id`, `token`) VALUES (4, 4, '8911c419861ccd2ce7b095c9d3f9cf90');
INSERT INTO `token` (`id`, `user_id`, `token`) VALUES (5, 5, '47858b26571a55c92e38b68b831c9143');
INSERT INTO `token` (`id`, `user_id`, `token`) VALUES (7, 7, 'bb5c65422ff09c3abdcb7ecf79a5b94e');
INSERT INTO `token` (`id`, `user_id`, `token`) VALUES (8, 8, '0e1589a53d8838f895ffbe2934bd8f74');


#
# TABLE STRUCTURE FOR: users
#

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT 1,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `forgotten_password_selector` varchar(255) NOT NULL,
  `remember_selector` varchar(255) NOT NULL,
  `is_store` tinyint(1) NOT NULL DEFAULT 1,
  `show_inhome` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `image`, `company`, `phone`, `forgotten_password_selector`, `remember_selector`, `is_store`, `show_inhome`) VALUES (1, '127.0.0.1', 'administrator', '$2y$12$6JthPiwJKNEteOexVKGeu./fpxYF2StxjRTEzZQhCUzxAsF9RqNs6', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1656637186, 1, 'Admin', 'istrator', '', 'ADMIN', '0', '', '', 0, 0);
INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `image`, `company`, `phone`, `forgotten_password_selector`, `remember_selector`, `is_store`, `show_inhome`) VALUES (5, '::1', 'الكابتن', '$2y$10$p3jw4v/xc3fmHRPhsWuae.6milHzLyRAqUTkjB5S1nLRDyndG/44i', NULL, '', NULL, NULL, NULL, NULL, 1598427928, NULL, 1, 'متجر', 'الكابتن', '80621-photo_-_-.jpg', 'Healthy Zone', NULL, '', '', 1, 1);
INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `image`, `company`, `phone`, `forgotten_password_selector`, `remember_selector`, `is_store`, `show_inhome`) VALUES (7, '', 'smartbuy', '$2y$10$ZSkeNYy2BdNstR28Km/bHekSLIUnvjtKc4l6zPrQxvL9NnGYDPAsG', NULL, 'samrtbuy@gmail.com', NULL, NULL, NULL, NULL, 0, 1603741164, 1, 'smart', 'buy', 'c0434-smartbuyjpg.jpg', 'SmartBuy', '12345678', '', '', 1, 1);
INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `image`, `company`, `phone`, `forgotten_password_selector`, `remember_selector`, `is_store`, `show_inhome`) VALUES (8, '', 'kareem', '$2y$10$SQS/.oCLAstQMMHOemzqcevDBj0mYnNVIkvlEBzmwVSv7u/wT/AJa', NULL, 'kareem@gmail.com', NULL, NULL, NULL, NULL, 0, NULL, NULL, 'كريم', 'هايبرماركت', 'b4c43-kareem.png', 'Kareem', '123456789', '', '', 1, 1);
INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `image`, `company`, `phone`, `forgotten_password_selector`, `remember_selector`, `is_store`, `show_inhome`) VALUES (9, '', 'safeway', '$2y$10$hcOQvYLmX3vNUBXJTJgvTuPFHYPg1eQmlNf2UUw.JAHcul.mcUi5S', NULL, 'safway@gmail.com', NULL, NULL, NULL, NULL, 0, NULL, NULL, 'سيفوي', 'الأردن', '6aec3-safeway2.png', 'safeway', '1234567889', '', '', 1, 1);
INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `image`, `company`, `phone`, `forgotten_password_selector`, `remember_selector`, `is_store`, `show_inhome`) VALUES (10, '', 'halabazar', '$2y$10$Alq81RYx3HPVBf0jgftNF.CQfdMMW8py8DuWyfPrKzkKQiFcPPLki', NULL, 'halabazar@gmail.com', NULL, NULL, NULL, NULL, 0, NULL, NULL, 'هلا', 'بازار', 'c69ea-halabazar.png', 'halabazar', '123456789', '', '', 1, 1);
INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `image`, `company`, `phone`, `forgotten_password_selector`, `remember_selector`, `is_store`, `show_inhome`) VALUES (11, '', 'carrefourjordan', '$2y$10$a/v7uC.lxHV1BsC05GVoE.TVtKubwRtU3ZZ2ZRCtWOH7/yJx1guQi', NULL, 'carrefourjordan@gmail.com', NULL, NULL, NULL, NULL, 0, NULL, 1, 'كارفور', 'الأردن', '3198c-index.png', 'كارفور الأردن', '0963962554805', '', '', 1, 1);
INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `image`, `company`, `phone`, `forgotten_password_selector`, `remember_selector`, `is_store`, `show_inhome`) VALUES (12, '', 'STYLISH KIDS ', '0', NULL, '', NULL, NULL, NULL, NULL, 0, NULL, 1, 'STYLISH ', 'KIDS ', 'b7c54-photo_-_-.jpg', 'STYLISH KIDS ', NULL, '', '', 1, 1);


#
# TABLE STRUCTURE FOR: users_groups
#

DROP TABLE IF EXISTS `users_groups`;

CREATE TABLE `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=172 DEFAULT CHARSET=utf8;

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES (167, 1, 1);
INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES (168, 1, 3);
INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES (21, 2, 1);
INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES (22, 2, 2);
INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES (166, 3, 2);
INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES (169, 4, 2);
INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES (170, 5, 3);
INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES (171, 6, 3);


#
# TABLE STRUCTURE FOR: vehiacle
#

DROP TABLE IF EXISTS `vehiacle`;

CREATE TABLE `vehiacle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `caddyId` varchar(255) NOT NULL,
  `max_number` int(11) NOT NULL,
  `goingTowards` text NOT NULL,
  `isActive` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `bus_information` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

