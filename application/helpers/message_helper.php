<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function get_error_message ($errorCode) {  //TODO should be in a helper class 
	    if($errorCode == 1)
	    {
	        return "الرقم غير معرف يرجى تعبئة المعلومات أدناه";
	    }
	    if($errorCode == 2)
	    {
	         return "الحساب غير مفعل";
	    }
	     if($errorCode == 3)
	    {
	         return "الحساب موجود مسبقا";
	    }
	    if($errorCode ==4)
	    {
	          return "يجب تسجيل الدخول لإتمام عملية الطلب";
	    }
	}
	if(!function_exists('lang')){
		function lang($line){
			$CI =& get_instance();
			return $CI->lang->line($line);
		}
	}
	
	function language()
	{
	$CI =& get_instance();
	$lang=$CI->session->userdata('lang');
	if(!isset($lang) || $lang=="")
	return "ar";
	else
	return $lang;
	}

	function unparse_url($parsed_url) {
		$scheme   = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
		$host     = isset($parsed_url['host']) ? $parsed_url['host'] : '';
		$port     = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : '';
		$user     = isset($parsed_url['user']) ? $parsed_url['user'] : '';
		$pass     = isset($parsed_url['pass']) ? ':' . $parsed_url['pass']  : '';
		$pass     = ($user || $pass) ? "$pass@" : '';
		$path     = isset($parsed_url['path']) ? $parsed_url['path'] : '';
		$query    = isset($parsed_url['query']) ? '?' . trim($parsed_url['query'], '&') : '';
		$fragment = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : '';
		return "$scheme$user$pass$host$port$path$query$fragment";
	  }
	  
	  function strip_query($url, $query_to_strip) {
		$parsed = parse_url($url);
		$parsed['query'] = preg_replace('/(^|&)'.$query_to_strip.'[^&]*/', '', $parsed['query']);
		return unparse_url($parsed);
	  }
	  
	  function keep_only_query($url, $query_to_keep) {
		$parsed = parse_url($url);
		$parsed['query'] = preg_replace('/(^|&)(?!'.$query_to_keep.')[^&]*/', '', $parsed['query']);
		return unparse_url($parsed);
	  }
	  	function strip_tags_content($string) { 
            // ----- remove HTML TAGs ----- 
            $string = preg_replace ('/<[^>]*>/', ' ', $string); 
            // ----- remove control characters ----- 
            $string = str_replace("\r", '', $string);
            $string = str_replace("\n", ' ', $string);
            $string = str_replace("\t", ' ', $string);
            // ----- remove multiple spaces ----- 
            $string = trim(preg_replace('/ {2,}/', ' ', $string));
            return $string; 
        
        }
?>