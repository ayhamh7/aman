<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    function sendSMS($PHONE,$f_msg)
    {

        //https://services.mtnsyr.com:7443/General/MTNSERVICES/ConcatenatedSender.aspx?User=ALbaraka2013&Pass=Jj2013&From=AL-Baraka&Gsm=963962554805&Msg=test&Lang=1
        $URL="https://services.mtnsyr.com:7443/General/MTNSERVICES/ConcatenatedSender.aspx?User=ALbaraka2013&Pass=Jj2013&From=AL-Baraka&Gsm=".$PHONE."&Msg=".string_to_utf16_hex($f_msg)."&Lang=0"; 
       var_dump($URL);
        $ch = curl_init( $URL );
        $str  = "Accept-Language: en-us,en;q=0.5\r\n";
        $str .= "Accept-Charset: windows-1256;q=0.7,*;q=0.7\r\n";
        $str .= "Keep-Alive: 300\r\n";
        $str .= "Connection: keep-alive\r\n";
       
        curl_setopt($ch, CURLOPT_HTTPHEADER, array($str));
       // curl_setopt( $ch, CURLOPT_HTTPHEADER, array("Content-Type: text/html; charset=UTF-8"));//'Content-Type:application/json'
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        echo $result;
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($httpcode == 200 || $httpcode == 204 ){
            return array('status' => $result);
        }else if ($httpcode == 401){
            return array('status' => false , "ErrorCode"=>401 , "message" => "Username or password");
        }else {
            return array('status' => false , "ErrorCode"=>500,"message" => "Error on server");
        }
    }


    function hotels()
    {
          
        $URL="https://staging.api.bedsclick.com/v1/hotel?page=1&per_page=20"; 
    
        $ch = curl_init( $URL );
       // $str  = "Accept-Language: en-us,en;q=0.5\r\n";
        $str = "Content-Type:application/json;\r\n";
        $str .= "Authentication: d0564566-a6e9-4bbb-acb6-76fa6a13c020";
       //var_dump($str);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array($str));
      //  curl_setopt( $ch, CURLOPT_HTTPHEADER, array("Content-Type:application/json; charset=UTF-8"));//'Content-Type:application/json'
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        //var_dump(json_decode($result,true));
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return json_decode($result,true);
    }


    function bookings()
    {
          
        $URL="https://staging.api.bedsclick.com/v1/booking?sort_by=check_out_date&direction=desc&status=confirmed"; 
    
        $ch = curl_init( $URL );
       // $str  = "Accept-Language: en-us,en;q=0.5\r\n";
        $str = "Content-Type:application/json;\r\n";
        $str .= "Authentication: d0564566-a6e9-4bbb-acb6-76fa6a13c020";
       
        curl_setopt($ch, CURLOPT_HTTPHEADER, array($str));
      //  curl_setopt( $ch, CURLOPT_HTTPHEADER, array("Content-Type:application/json; charset=UTF-8"));//'Content-Type:application/json'
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
     //   echo "<pre>";
        //print_r($result);die();
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
       return json_decode($result,true);
    }
    function booking($booking_id)
    {
          
        $URL="https://staging.api.bedsclick.com/v1/booking/".$booking_id; 
    
        $ch = curl_init( $URL );
       // $str  = "Accept-Language: en-us,en;q=0.5\r\n";
        $str = "Content-Type:application/json;\r\n";
        $str .= "Authentication: d0564566-a6e9-4bbb-acb6-76fa6a13c020";
       
        curl_setopt($ch, CURLOPT_HTTPHEADER, array($str));
      //  curl_setopt( $ch, CURLOPT_HTTPHEADER, array("Content-Type:application/json; charset=UTF-8"));//'Content-Type:application/json'
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
     //   echo "<pre>";
        //print_r($result);die();
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
       return json_decode($result,true);
    }
    function search()
    {
        $URL="https://staging.api.bedsclick.com/v1/hotel/search?page=1&per_page=20&sort_by=rating&direction=desc"; 

        $ch = curl_init();
        $data = array (
            'check_in_date' => '2019-10-29',
            'check_out_date' => '2019-10-30',
            'place' => 5475,
            'residency' => 'TR',
            'language' => 'EN',
            'currency' => 'USD',
            'rooms' => 
            array (
              0 => 
              array (
                'index' => 1,
                'adult_count' => 2,
                'child_ages' => 
                array (
                  0 => 2,
                  1 => 5,
                ),
              ),
            ),
            'options' => 
            array (
              'include_facilities' => true,
              'include_images' => true,
              'include_nearest_hotels' => true,
              'include_nearest_places' => true,
              'return_partial_results' => false,
            )
            );
         
        $payload = json_encode($data);
        //var_dump($payload);
        curl_setopt($ch, CURLOPT_URL,$URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
          // $str  = "Accept-Language: en-us,en;q=0.5\r\n";
          $str = "Content-Type:application/json;\r\n";
          $str .= "Authentication: d0564566-a6e9-4bbb-acb6-76fa6a13c020";
         
          curl_setopt($ch, CURLOPT_HTTPHEADER, array($str));

       // curl_setopt($ch, CURLOPT_HEADER, true); 
        $result = curl_exec($ch);
        
      //  echo 'Curl error: ' . curl_error($ch);
   //   print_r($result);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
       // var_dump("hhtpp code ".$httpcode." error no".curl_errno($ch));
       return json_decode($result,true);
    }

    function hotelAvalibility()
    {
        $URL="https://staging.api.bedsclick.com/v1/hotel/availability"; 

        $ch = curl_init();
        $data = array (
            'check_in_date' => '2019-10-29',
            'check_out_date' => '2019-10-29',
            'hotel' => 155406,
            'residency' => 'TR',
            'language' => 'EN',
            'currency' => 'TRY',
            'rooms' => 
            array (
              0 => 
              array (
                'adult_count' => 2,
                'child_ages' => 
                array (
                ),
              ),
            ),
            'options' => 
            array (
              'include_unavailable_hotels' => true,
              'include_facilities' => true,
              'include_images' => true,
              'include_nearest_places' => true,
              'include_nearest_hotels' => true,
              'return_partial_results' => true,
            ),
            'last_progress' => 0.75
          );
         
        $payload = json_encode($data);
        //var_dump($payload);
        curl_setopt($ch, CURLOPT_URL,$URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
          // $str  = "Accept-Language: en-us,en;q=0.5\r\n";
          $str = "Content-Type:application/json;\r\n";
          $str .= "Authentication: d0564566-a6e9-4bbb-acb6-76fa6a13c020";
         
          curl_setopt($ch, CURLOPT_HTTPHEADER, array($str));

       // curl_setopt($ch, CURLOPT_HEADER, true); 
        $result = curl_exec($ch);
        
      //  echo 'Curl error: ' . curl_error($ch);
   //   print_r($result);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
       // var_dump("hhtpp code ".$httpcode." error no".curl_errno($ch));
       return json_decode($result,true);
    }

    function prepare()
    {
        $URL="https://staging.api.bedsclick.com/v1/booking/prepare"; 

        $ch = curl_init();
        $data =array (
            'currency' => 'TRY',
            'token' => 'CI6-CRIGCICjnc0FGgYIgMaizQUiAggCKgJUUjABOgNUUllCEQoPRG91YmxlIFN0YW5kYXJkShQKCDIwMTcwODI4EggyMDE3MDgzMVHD9Shcj7JQQA==',
            'guest_list' => 
            array (
              0 => 
              array (
                'index' => 1,
                'guests' => 
                array (
                  0 => 
                  array (
                    'type' => 'adult',
                    'title' => 'mr',
                    'name' => 'John',
                    'last_name' => 'Doe',
                  ),
                  1 => 
                  array (
                    'type' => 'adult',
                    'title' => 'ms',
                    'name' => 'Marry',
                    'last_name' => 'Doe',
                  ),
                ),
              ),
            ),
            'contact' => 
            array (
              'name' => 'John Doe',
              'phone' => '+4413124512122',
              'email' => 'john@doe.personal',
            ),
          );
         
        $payload = json_encode($data);
        //var_dump($payload);
        curl_setopt($ch, CURLOPT_URL,$URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
          // $str  = "Accept-Language: en-us,en;q=0.5\r\n";
          $str = "Content-Type:application/json;\r\n";
          $str .= "Authentication: d0564566-a6e9-4bbb-acb6-76fa6a13c020";
         
          curl_setopt($ch, CURLOPT_HTTPHEADER, array($str));

       // curl_setopt($ch, CURLOPT_HEADER, true); 
        $result = curl_exec($ch);
        
      //  echo 'Curl error: ' . curl_error($ch);
   //   print_r($result);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
       // var_dump("hhtpp code ".$httpcode." error no".curl_errno($ch));
       return json_decode($result,true);
    }

    function confirm()
    {
        $URL="https://staging.api.bedsclick.com/v1/booking/confirm"; 

        $ch = curl_init();
        $data =array (
            'payment_type' => "reservation",
            "token"=> "CI6-CRIGCICjnc0FGgY",
          );
         
        $payload = json_encode($data);
        //var_dump($payload);
        curl_setopt($ch, CURLOPT_URL,$URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
          // $str  = "Accept-Language: en-us,en;q=0.5\r\n";
          $str = "Content-Type:application/json;\r\n";
          $str .= "Authentication: d0564566-a6e9-4bbb-acb6-76fa6a13c020";
         
          curl_setopt($ch, CURLOPT_HTTPHEADER, array($str));

       // curl_setopt($ch, CURLOPT_HEADER, true); 
        $result = curl_exec($ch);
        
      //  echo 'Curl error: ' . curl_error($ch);
   //   print_r($result);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
       // var_dump("hhtpp code ".$httpcode." error no".curl_errno($ch));
       return json_decode($result,true);
    }
    function cancel()
    {
        $URL="https://staging.api.bedsclick.com/v1/booking/cancel"; 

        $ch = curl_init();
        $data =array (
            "type"=> "confirm",
            "booking_id"=> 502123,
          );
         
        $payload = json_encode($data);
        //var_dump($payload);
        curl_setopt($ch, CURLOPT_URL,$URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
          // $str  = "Accept-Language: en-us,en;q=0.5\r\n";
          $str = "Content-Type:application/json;\r\n";
          $str .= "Authentication: d0564566-a6e9-4bbb-acb6-76fa6a13c020";
         
          curl_setopt($ch, CURLOPT_HTTPHEADER, array($str));

       // curl_setopt($ch, CURLOPT_HEADER, true); 
        $result = curl_exec($ch);
        
      //  echo 'Curl error: ' . curl_error($ch);
   //   print_r($result);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
       // var_dump("hhtpp code ".$httpcode." error no".curl_errno($ch));
       return json_decode($result,true);
    }
    
//for send arabic message
    function string_to_utf16_hex( $string ) {
        return bin2hex(mb_convert_encoding($string, "UTF-16", "UTF-8"));
      }