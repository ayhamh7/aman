<?php
ini_set('max_execution_time', 6000);
defined('BASEPATH') OR exit('No direct script access allowed');
define( 'API_ACCESS_KEY', 'AAAADnM6PCE:APA91bHEKyfUs34p2nY2OyRpoX4ZDpEJLbbyYgx8ZrpOUcZR_bCBnu8Kyg8SHrxi0UXA7XyMllsO_OAOL83o3lU45uqDolfWL08J_-LK35bCQO6UsldZcHqViyos3OrUZGEFC6GOI5it' );//AIzaSyBLV2ZQQFHy0lR7T-LUFOl-CyoWFiHe9q0
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require(APPPATH.'controllers/BaseController2.php');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @ncetegory        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Delivery_api extends BaseController2 {

   function __construct()
    {
        // Construct the parent class
        parent::__construct();
        //$this->set_timezone();
        //header('content-Type: text/html; charset=utf-8');
      //  header('Content-Type: application/json; charset=utf-8');
      //  $this->load->model('api_model');  // to fix
      $this->load->library("pagination");
        $this->load->model('api_model');
        $this->load->model('auth_model');
    }
    function index_get()
    {
        echo "welocme";
    }

    function approve_rerquest_post()
    {
        $order_id = $this->post("order_id");
        $user_id = ($this->current_user!=null)?$this->current_user["id"]:null;
        $data=0;
        if($user_id)
        {
             $this->db->where("id",$order_id);
            $this->db->where("status","new")->or_where("status","InProgress");
            
            $this->db->update("orders",array("delivery_id"=>$user_id));
            if($this->db->affected_rows())
            {
                $data =1;
            }
           // //echo $this->db->last_query();
        }
        else
        {
            $data = -1;
        }
        $this->response(array("data"=>$data));
       
    }
    
    
     function deliver_rerquest_post()
    {
        $order_id = $this->post("order_id");
        $user_id = ($this->current_user!=null)?$this->current_user["id"]:null;
        $data=0;
        if($user_id)
        {
             $this->db->where("id",$order_id);
            $this->db->where("status","Sent");
            
            $this->db->update("orders",array("delivery_id"=>$user_id,"status"=>"Delivered","finish_date"=>date('Y-m-d H:i:s')));
            if($this->db->affected_rows())
            {
                $data =1;
            }
           // //echo $this->db->last_query();
        }
        else
        {
            $data = -1;
        }
        $this->response(array("data"=>$data));
       
    }
    
     function cancel_rerquest_post()
    {
        $order_id = $this->post("order_id");
        $user_id = ($this->current_user!=null)?$this->current_user["id"]:null;
        $data=0;
        if($user_id)
        {
             $this->db->where("id",$order_id);
            $this->db->where("status","Sent");
            
            $this->db->update("orders",array("delivery_id"=>$user_id,"status"=>"Canceled","note"=> $this->post("note"),"finish_date"=>date('Y-m-d H:i:s')));
            if($this->db->affected_rows())
            {
                $data =1;
            }
           // //echo $this->db->last_query();
        }
        else
        {
            $data = -1;
        }
        $this->response(array("data"=>$data));
       
    }
   function reassign_rerquest_post()
    {
        $order_id = $this->post("order_id");
        $user_id = ($this->current_user!=null)?$this->current_user["id"]:null;
        $data=0;
        if($user_id)
        {
             $this->db->where("id",$order_id);
            $this->db->where("status","InProgress");
            
            $this->db->update("orders",array("delivery_id"=>null));
            if($this->db->affected_rows())
            {
                $data =1;
            }
           // //echo $this->db->last_query();
        }
        else
        {
            $data = -1;
        }
        $this->response(array("data"=>$data));
       
    }

    function checkPassword_post()
    {
          if($user_id == null)
        {
            $result = array("message"=>"not logged in");
             $this->response($result,401); 
        }
        $this->load->model('auth_model2');
        $pass = $this->post('current_password');
        $res =  $this->auth_model->check_password($user_id,$pass);
        $this->response(array("data"=>$res));
    }
       public function userInfo_post()
    {
        $object =  (array) $this->post();
        
        $object["id"]=$user_id=($this->current_user!=null)?$this->current_user["id"]:null;
        if($user_id == null)
        {
            $result = array("data"=>null,"message"=>"not logged in");
             $this->response($result,200); 
        }
        $user = $this->current_user;
        $authResult = $this->auth_model2->updateUser($object);
        $result = array("data"=>$authResult,"ErrorCode"=>200);
        $this->response($result,200); 
    }

    public function checkActive_get()
    {
        if($this->current_user==null)
        {
              $result = array("data"=>false,"ErrorCode"=>399);//token is invalid
            $this->response($result,200); 
        }
        else
        {
             $is_active  = $this->auth_model->checkActive($this->current_user["id"]);
            //var_dump("amer ".$this->current_user["id"]);
            $result = array("data"=>$is_active,"ErrorCode"=>200);
            $this->response($result,200); 
        }
       
    }
   
   function changeStatus_post()
   {
       $user_id = ($this->current_user!=null)?$this->current_user["id"]:null;
       $data = (array)$this->post();
        if($user_id == null)
        {
             $result = array("data"=>null/*$this->utf8ize($data)*/,"ErrorCode"=>0);
            $this->response($result , 200); 
        }
        
        $customer_id = $this->db->where("id",$data["order_id"])->get("orders")->row_array()["customer_id"];
        $order_id = $data["order_id"];
        if($data["status"] == "charged")
        {
             $this->sendNotification("تم شحن  طلبكم, رقم الطلب ".$order_id,$customer_id);
        }
        else if($data["status"] == "canceled")
        {
            $this->sendNotification("تم إلغاء طلبكم".$order_id,$customer_id);
        }
        $this->db->where("status","charged")->where("delivery_id",$user_id)->where("id",$data["order_id"])->update("orders",array("status"=>$data["status"],"note"=>$data["note"],"finish_date"=>date('Y-m-d H:i:s')));
         $this->response(true , 200); 
   }
   

    function newOrders_get()
    {
        $data  =$this->api_model->getNewOrders();
        $result = array("data"=>$data/*$this->utf8ize($data)*/,"ErrorCode"=>200);
        $this->response($data , 200); 
    }
    
    function waitingOrders_get()
    {
         $user_id = ($this->current_user!=null)?$this->current_user["id"]:null;
        $data  =$this->api_model->getWaitingOrders_delivery($user_id);
        $result = array("data"=>$data/*$this->utf8ize($data)*/,"ErrorCode"=>200);
        $this->response($data , 200); 
    }
    
    function allOrders_get()
    {
         $user_id = ($this->current_user!=null)?$this->current_user["id"]:null;
         $data  =$this->api_model->getNewOrders();
        $data3  =$this->api_model->getWaitingOrders_delivery($user_id);
        $data2  =$this->api_model->getInProgressOrders_delivery($user_id);
        $result = array($data,$data3,$data2);
        $this->response($result , 200); 
    }
    
    function deliveredOrders_get()
    {
         $user_id = ($this->current_user!=null)?$this->current_user["id"]:null;
        $data  =$this->api_model->getDeliveredOrders($user_id);
       // echo $this->db->last_query();
        $result = array("data"=>$data/*$this->utf8ize($data)*/,"ErrorCode"=>200);
        $this->response($data , 200); 
    }
    
    function myOrders_get()
    {
        
        
        $user_id = ($this->current_user!=null)?$this->current_user["id"]:null;
        if($user_id == null)
        {
             $result = array("data"=>null/*$this->utf8ize($data)*/,"ErrorCode"=>0);
            $this->response($result , 200); 
        }
     
      $page=0;
      $data  =$this->api_model->getDeliveryOrders($user_id);
      $result = array("data"=>$data/*$this->utf8ize($data)*/,"ErrorCode"=>200);
      $this->response($result , 200); 
        
    }
    
    
    function order_get()
    {
        $order_id = $this->get("order_id");
        $data = $this->api_model->order_details_delivery($order_id);
        $data["products"] =  $this->api_model->getorderItems($order_id);
        $this->response($data , 200); 
    }
    
    function orderItems_get()
    {
        $order_id = $this->get("order_id");
        $data = $this->api_model->getorderItems($order_id);
        $result = array("data"=>$data/*$this->utf8ize($data)*/,"ErrorCode"=>200);
      $this->response($result , 200); 
    }

}