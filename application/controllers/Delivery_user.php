<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/REST_Controller.php');
 
class Delivery_user extends REST_Controller {

	public  function __construct(){
        parent::__construct();
        $this->load->model('auth_model2');
        $this->load->model('api_model');
    } 

    public function login_post(){
        $object =  (array)$this->post();
       
        //var_dump($object);die();
        $authResult = $this->auth_model2->login($object);
        //var_dump($authResult);
        if(isset($authResult["data"]))
            $this->response($authResult["data"],200); 
        else
             $this->response(array("message"=>$authResult["message"]),$authResult["ErrorCode"]); 
    }
}