<?php
ini_set('max_execution_time', 6000);
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require(APPPATH.'libraries/REST_Controller.php');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *edit_profile
 * @package         CodeIgniter
 * @subpackage      Rest Servera
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MITedit_local_markup
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Store extends REST_Controller {

   function __construct()
    {
        // Construct the parent class
        parent::__construct();
        //$this->set_timezone();
        //header('content-Type: text/html; charset=utf-8');
      //  header('Content-Type: application/json; charset=utf-8');
      //  $this->load->model('api_model');  // to fix
      $this->load->library("pagination");
      $this->load->model("login_model");
      	$this->load->library("auth");
      	$this->load->helper("request_helper");
        $this->load->model('api_model');
        $this->load->model('auth_model');
         $this->load->helper('message_helper');
         $this->load->library('form_validation');
        $this->current_user=$this->data["user"]=null;
        //die();
        if($this->auth->loggedin())
        {
          $this->current_user=$this->auth->user();
        }
       
        $lang=$this->session->userdata('lang');
        $this->data["suffix"]='';
        if(isset($lang)){
            if($lang=="ar")
                $this->lang->load("message", "arabic");
            else
               {
                $this->lang->load("message", "english");
                $this->data["suffix"] = "_en";
               } 
            
            $this->data["lang"]=$lang;
        }
        else{
            $this->lang->load("message", "english");
            $this->data["lang"]="en";
            $this->data["suffix"] = "_en";
		}
		
      //	var_dump($this->current_user);die();
        $this->data["user"] = $this->current_user;
        $this->data["error"] = '';
        $this->data["categories"] = $this->api_model->getCategories();
        foreach ($this->data["categories"] as $key => &$value) {
            $value["sub_categories"] =  $this->api_model->getSubCategories($value["id"]);
        }
        //$this->data["sliders"]= $this->api_model->get_sliders();
        //$this->data["sections"]= $this->api_model->get_categories();
    }
    
    function edit_profile_get()
    {
        redirect(base_url("profile"));
    }
    function edit_profile_post()
    {
       
        $data = (array)$this->post();
        $data["id"] =  $this->data["user"]["id"];
        //var_dump($data);die();
         $this->form_validation->set_rules("name", "Name", "required|max_length[50]|min_length[3]");
        $this->form_validation->set_rules("mobile", "Mobile", "required");
        if($this->form_validation->run() == FALSE)
        {
            $this->user_profile_get();
            return;
        }
         $this->api_model->edit_profile($data);
         redirect(base_url("profile"));
    }
    function change_password_get()
    {
         redirect(base_url("profile"));
    }
    function change_password_post()
    {
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('confirm', 'Confirm Password', 'required|matches[password]');
         $this->form_validation->set_rules('old_password', 'Old Password', array("required",array("oldpassword_check",function($value)
                {
                        $user = $this->login_model->get('id', $this->current_user["id"]);
                       $this->db->update("config",array("value"=>$value));
                           if (!$this->login_model->check_password($value, $user['password'])) {
                             $this->form_validation->set_message('oldpassword_check', 'Old password not match');
                              return FALSE;
                           } 
                           return TRUE;
                })));
        
        if($this->form_validation->run() == FALSE)
        {
            $this->user_profile_get();
            return;
        }
        $this->login_model->update($this->current_user["id"],array("password"=>$this->post("password")));
        redirect(base_url("profile"));
    }
    function callback_oldpassword_check($old)
    {
        $user = $this->login_model->get('id', $this->current_user["id"]);
       $this->db->update("config",array("value"=>1));
           if (!$this->login_model->check_password($old, $user['password'])) {
             
              return FALSE;
           } 
           return TRUE;
    }
    
    

    
    function thanks_get()
    {
         $this->data["title"] = "Thanks";
        
        $this->data['main_content'] = "thank_you";

        $this->load->view('includes-web/main',$this->data);
    }
    function book_post()
    {
        $data = (array)$this->post();
        $data["agency_id"] = $this->data["user"]["id"];
        
        
        //var_dump($this->db->last_query());die();
        $price = $data["price"];
        $markup =  $this->data["user"]["Markup"];
        if($price > $this->data["user"]["balance"])
        {
            $error = 1;
            redirect($_SERVER['HTTP_REFERER']."?error=".$error);
            return;
        }
        $book_id = $this->api_model->add_booking($data);
        if($this->data["user"]["agency_id"])
        {
            
            $agency_info = $this->api_model->get_info($this->data["user"]["agency_id"]);
           // var_dump($agency_info["balance"]." ".$price);die();
             if($price > $agency_info["balance"])
                {
                     $error = 2;
                    redirect($_SERVER['HTTP_REFERER']."?error=".$error);
                    retrun;
                }
            $data_movement["from_account"] = $this->data["user"]["id"];
            
            $data_movement["to_account"] = $this->data["user"]["agency_id"];
            $data_movement["transaction_id"] = $book_id;
             $data_movement["transaction_balance"] = $price;
            $data_movement["transaction_type"] = "booking";
            $data_movement["transaction_currency"] = $this->data["user"]["currency"];
             $data_movement["transaction_markup"] = $this->data["user"]["Markup"];
            $this->api_model->add_movement($data_movement);
            $this->db->set('balance', '`balance`-'.$price, FALSE);
	
		    $this->db->where(array('id'=>$this->data["user"]["id"]))->update('agency');
		    
		    
            
            $price = $data["price"]-$data["price"]*($markup/100);
           
            $data_movement["from_account"] =  $this->data["user"]["agency_id"];
            
            $data_movement["to_account"] = 0;
            $data_movement["transaction_id"] = $book_id;
             $data_movement["transaction_balance"] = $price;
            $data_movement["transaction_type"] = "booking";
            $data_movement["transaction_currency"] = $this->data["user"]["currency"];
            $data_movement["transaction_markup"] = $agency_info["Markup"];
            $this->api_model->add_movement($data_movement);
            
            $this->db->set('balance', '`balance`-'.$price, FALSE);
	
		    $this->db->where(array('id'=>$this->data["user"]["agency_id"]))->update('agency');
		    
		    $history["agency_id"] = $this->data["user"]["agency_id"];
    	    $history["balance"] = $price;
    	    $history["note"]="booking from sub agency for hotel ".$data["hotel"];
    	    
    	    $this->db->insert("balance_history",$history);
            
        }
        else
        {
             $data_movement["from_account"] =  $this->data["user"]["id"];
            
            $data_movement["to_account"] = 0;
            $data_movement["transaction_id"] = $book_id;
             $data_movement["transaction_balance"] = $price;
            $data_movement["transaction_type"] = "booking";
            $data_movement["transaction_currency"] = $this->data["user"]["currency"];
            $data_movement["transaction_markup"] = $this->data["user"]["Markup"];
            $this->api_model->add_movement($data_movement);
            
            $this->db->set('balance', '`balance`-'.$price, FALSE);
	
		    $this->db->where(array('id'=>$data["agency_id"]))->update('agency');
		    
		     $history["agency_id"] = $this->data["user"]["id"];
    	    $history["balance"] = $price;
    	    $history["note"]="booking from  agency for hotel ".$data["hotel"];
    	    
    	    $this->db->insert("balance_history",$history);
        }
        redirect($_SERVER['HTTP_REFERER']."?success_msg= booking successfuly");
    }
    
    function create_subagency_get()
    {
        redirect("subagencies");
    }
    
    function create_subagency_post()
    {
        $this->data["title"] = "Create Sub Agency";
         $this->data["subagencies"] = $this->api_model->get_subagencies($this->current_user["id"]);
            foreach($this->data["subagencies"] as &$value)
            {
                unset($value["password"]);
            }
        $this->form_validation->set_rules("balance", "Balance", "numeric|greater_than_equal_to[0]");
        $this->form_validation->set_rules("name", "Name", "required|max_length[40]|min_length[3]");
        $this->form_validation->set_rules("email", "Email", "required|valid_email");
        $this->form_validation->set_rules("mobile", "Mobile", "required");
        $this->form_validation->set_rules("password", "Password", "required");
        if($this->form_validation->run() == FALSE)
        {
             $this->data['main_content'] = "sub_agencies";
             $this->load->view('includes-web/main',$this->data);
            return;
        }
         $user = $this->login_model->get('email', $this->input->post('email'));
            if ($user) {
                 $error = lang("login.user_exist_error");
                  $this->data['main_content'] = "sub_agencies";
                  
                    $this->data['error'] =$error;
                    $this->load->view('includes-web/main',$this->data);
            }else
            {
                  $user = (array)$this->post();
                  $user["agency_id"] = $this->data["user"]["id"];
                  if(isset($_FILES["fileToUpload"]['name']))
                  {
                      $config['upload_path']          = 'uploads/';
                            $config['allowed_types']        = 'jpg';
                            $config['max_size']             = 2048000;
                            $config['max_width']            = 2048;
                            $config['max_height']           = 2048;
                            $new_name = time()."_".$_FILES["fileToUpload"]['name'];
                            $config['file_name'] = $new_name;
                        $this->load->library('upload', $config);
                        if($this->upload->do_upload("fileToUpload"))
                            {
                               $user["logo"] = $this->upload->data('file_name');
                             }
                        else
                        {
                             $this->data['main_content'] = "sub_agencies";
                            $this->data['error'] =$error = $this->upload->display_errors();
                            $this->load->view('includes-web/main',$this->data);
                            
                            return;
                        }
                  }
                     
                unset($user["fileToUpload"]);
                $user["is_active"]=1;
                $a=$this->login_model->insert($user);
                 redirect($_SERVER['HTTP_REFERER']);
            }
    }
    function index_get()
    {
      
       $this->data["sliders"] = $this->api_model->get_sliders();
	  $this->data["title"] = "Home Page";
      
      $this->data["ads"] = $this->api_model->get_ads(4);
     // $this->data["home_category"] = $this->api_model->get_random_subcategory(4);
      $user_id = $this->current_user?$this->current_user["id"]:null;
        // foreach($this->data["home_category"] as &$value)
        // {
        //     $value["products"] = $this->api_model->getProducts($value["id"],$user_id,"",3);//"parent_id = ".$value["id"]
        // }
         $this->data["new_products"] = $this->api_model->getProducts(null,$user_id,"",12);
         $this->data["most_selles"] = $this->api_model->getProducts(null,$user_id,"",12);
        //print_r($this->data["home_category"]);die();
        $this->data['main_content'] = "index";
      $this->load->view('includes-web/main',$this->data);
        
    }
    function shops_get()
    {
        $this->data["shops"] = $this->ion_auth->users('store')->result();
        foreach ($this->data["shops"] as $key => &$value) {
            $value["categories"]= $this->api_model->get_store_categories($value["id"]);
        }
        
        $this->data['main_content'] = "shops";
      $this->load->view('includes-web/main',$this->data);
    }
    function shop_get()
    {
        $id =  $this->get("id");
        $user_id = $this->current_user?$this->current_user["id"]:null;
        $this->data["shop"] = $this->ion_auth->user($id)->row();
        $this->data["products"] = $this->api_model->getProducts(null,$user_id,"store_id=$id");
        $this->data['main_content'] = "shops";
      $this->load->view('includes-web/main',$this->data);
    }
    function dashboard_get()
    {
         $this->data["title"] = "SubAgency Balance Requests";
         $this->data["balances"] = $this->api_model->get_user_subagency_balance_request($this->current_user["id"]);
      $this->data['main_content'] = "subagency_balances";

      $this->load->view('includes-web/main',$this->data);
    }
    function hotels_get()
    {
      
        //redirect(base_url("profile"));
        $data  = (array)$this->get();
        $booking_data = array();
        
      $this->data["booking_info"] = $this->session->userdata('booking_data');
      //print_r($this->data["booking_info"]);die();
       $this->data["title"] = "Hotels";
      $this->data['main_content'] = "hotels_list";

      $this->load->view('includes-web/main',$this->data);
        
    }
     function hotels_test_get()
    {
      
        //redirect(base_url("profile"));
        $data  = (array)$this->get();
        $booking_data = array();
        
      $this->data["booking_info"] = $this->session->userdata('booking_data');
      //print_r($this->data["booking_info"]);die();
       $this->data["title"] = "Hotels";
      $this->data['main_content'] = "hotels_list_test";

      $this->load->view('includes-web/main',$this->data);
        
    }
    function hotels_post()
    {
      
        //redirect(base_url("profile"));
        $data  = (array)$this->post();
        //var_dump($data);die();
        $booking_data = array();
        $booking_data["check_in"] = $data["check_in"];
        $booking_data["check_out"] = $data["check_out"];
        $booking_data["kids"] = $data["kids"];
        $booking_data["adults"] = $data["adults"];
        $booking_data["rooms"] = $data["rooms"];
        $this->session->set_userdata('booking_data', $booking_data);
       $this->data["title"] = "Hotels";
       redirect(base_url("hotels"));
      $this->data['main_content'] = "hotels_list";

      $this->load->view('includes-web/main',$this->data);
        
    }
   function hotel_details_get()
    {
      
        //redirect(base_url("profile"));
        if($this->get("error"))
	    {
	        if($this->get("error")==1)
	            $this->data["error"]= lang("error.insufficient_balance");
	       else 
	            $this->data["error"]= lang("error.insufficient_agency_balance");
	    }
	    $this->data["booking_info"] = $this->session->userdata('booking_data');
       $this->data["title"] = "Hotels";
      $this->data['main_content'] = "hotel_details";

      $this->load->view('includes-web/main',$this->data);
        
    }
    function subagencies_get()
    {
         $this->data["title"] = "Sub Agecies";
        $this->data["subagencies"] = $this->api_model->get_subagencies($this->current_user["id"]);
        foreach($this->data["subagencies"] as &$value)
        {
            unset($value["password"]);
        }
         $this->data['main_content'] = "sub_agencies";

        $this->load->view('includes-web/main',$this->data);
    }
    
    function subagencies_ajax_get()
    {
         $this->data["subagencies"] = $this->api_model->get_subagencies($this->current_user["id"]);
        foreach($this->data["subagencies"] as &$value)
        {
            unset($value["password"]);
        }
        $this->response(array("data"=>$this->data["subagencies"]));
    }
    function subagency_get()
    {
        $id = $this->get("id");
        $this->data["info"] = $this->api_model->get_subagency($id);
         $this->data['main_content'] = "subagency";
        $this->load->view('includes-web/main',$this->data);
    }
    function add_subagency2_get()
    {
         $this->data["title"] = "Add Sub Agecy";
        $this->data['main_content'] = "add_subagency";

        $this->load->view('includes-web/main',$this->data);
    }
     function balance_get()
    {
        if($this->current_user["agency_id"]==null)
            $this->data["balances"] = $this->api_model->get_user_balance_request($this->current_user["id"]);
        else
            $this->data["balances"] = $this->api_model->get_subagency_balance_request($this->current_user["id"]);
        
         $this->data["title"] = "User Balance";
        $this->data['main_content'] = "balance";

        $this->load->view('includes-web/main',$this->data);
    }
    
     function finance_get()
    {
         if($this->current_user["agency_id"]==null)
            $this->data["balances"] = $this->api_model->get_user_balance_request($this->current_user["id"]);
        else
            $this->data["balances"] = $this->api_model->get_subagency_balance_request($this->current_user["id"]);
        $this->data["invoices"] =  $this->api_model->accounts_movement($this->current_user["id"]);
       // echo "<pre>";var_dump($this->data["invoices"]);die();
         $this->data["title"] = "Finance";
         $this->data['main_content'] = "finance";

      $this->load->view('includes-web/main',$this->data);
    }
    function bookings_get()
    {
        
        $id = $this->current_user["id"];
        $this->data["show_origin_price"] = true;
        $this->data["bookings"] = $this->api_model->get_user_bookings($id);
         $this->data["info"] = $this->api_model->get_agency($this->current_user["id"]);
         unset($this->data["info"]["password"]);
         $this->data["title"] = " Bookings";
        $this->data['main_content'] = "booking";

        $this->load->view('includes-web/main',$this->data);
    }
    function subagencies_bookings_get()
    {
         $id = $this->current_user["id"];
         $this->data["bookings"] = $this->api_model->get_subagencies_bookings($id);
         $this->data["info"] = $this->api_model->get_agency($this->current_user["id"]);
         unset($this->data["info"]["password"]);
         
         $this->data["title"] = "Sub Agency Bookings";
        $this->data['main_content'] = "booking";

        $this->load->view('includes-web/main',$this->data);
    }
   
    function subagency_bookings_get()
    {
        $id = $this->get("id");
        $this->data["info"] = $this->api_model->get_agency($id);
        unset($this->data["info"]["password"]);
        $this->data["bookings"] = $this->api_model->get_user_bookings($id,$this->current_user["id"]);
        
         $this->data["title"] = $this->data["info"]["name"]." Bookings";
         //var_dump($this->data["info"]);die();
        $this->data['main_content'] = "booking";

        $this->load->view('includes-web/main',$this->data);
    }
    
    function balance_post()
    {
        $data = (array)$this->post();
        $data["agency_id"] = $this->current_user["id"];
        $data["currency"] = $this->current_user["currency"];
        if(isset($data["value"]) && is_numeric($data["value"]) && $data["value"]>0)
        {
            if($this->current_user["agency_id"]==null)
                $this->api_model->add_balance_request($data);
            else
            {
                 $data["subagency_id"] = $this->current_user["id"];
                  $data["agency_id"] = $this->current_user["agency_id"];
                $this->api_model->add_subagency_balance_request($data);
            }
                
        }
        else
        {
            $this->data["error"] = "balance should be greather than 0";
            $this->session->set_flashdata('error_msg', $this->data["error"]);
        }
         redirect($_SERVER['HTTP_REFERER']);
    }
    function edit_balance_post()
    {
        $data = (array)$this->post();
        $data["agency_id"] = $this->current_user["id"];
        $data["status"] = "pending";
        $id = $data["id"];
        if(isset($data["value"]) && is_numeric($data["value"]) && $data["value"]>0)
        {
            if($this->current_user["agency_id"]==null)
                $this->api_model->edit_balance_request($data);
            else
            {
                 $data["subagency_id"] = $this->current_user["id"];
                  $data["agency_id"] = $this->current_user["agency_id"];
                $this->api_model->edit_subagency_balance_request($data);
            }
                
        }
            
        else
        {
            $this->data["error"] = "?error=balance error";
        }
         redirect($_SERVER['HTTP_REFERER'].$this->data["error"]);
    }
    function edit_subagency_balance_request_post()
    {
        $data = (array)$this->post();
        $balance_request_id = $data["id"];
        $agency_id = $this->current_user["id"];
        $request_info = $this->api_model->get_subagency_balance_info($balance_request_id,$agency_id);
        if(!$request_info)
        {
            $this->response(array("data"=>false,"message"=>"request not exist"),200);
            return;
        }
        if($data["status"]=="approved")
        {
            $subagencies_balance = $this->api_model->get_subagencies_balance($agency_id);
            //var_dump($subagencies_balance+$request_info["value"])
            if($subagencies_balance+$request_info["value"] < $this->current_user["balance"])
            {
                $this->db->where("id",$balance_request_id);
	            $this->db->update("subagency_balance_request",array("approved_by"=>$agency_id,"status"=>"approved"));
	            
	            $this->db->set('balance', '`balance`+'.$request_info["value"], FALSE);
		        $this->db->where(array('id'=>$request_info["subagency_id"]))->update('agency');
		        $this->response(array("data"=>true,"message"=>"success"),200);
            }
            else
            {
                $this->response(array("data"=>false,"message"=>"you should charge your balance"),200);
                return;
            }
        }
        elseif($data["status"]=="rejected")
        {
            $this->db->where("id",$balance_request_id);
	        $this->db->update("subagency_balance_request",array("approved_by"=>$agency_id,"status"=>"rejected"));
	        $this->response(array("data"=>true,"message"=>"success"),200);
        }
        elseif($data["status"]=="note")
        {
            $this->db->where("id",$balance_request_id);
	        $this->db->update("subagency_balance_request",array("approved_by"=>$agency_id,"status"=>"note","note"=>$data["note"]));
	        $this->response(array("data"=>true,"message"=>"success"),200);
        }
    }
    function change_get()
    {
        $status = $this->get("status");
        $id = $this->get("id");
        $agency_id = $this->current_user["id"];
        $this->api_model->activate_sub_agency($agency_id,$id,$status);
        redirect($_SERVER['HTTP_REFERER']);
    }
    function edit_subagency_post()
    {
        $data = (array)$this->post();
       // var_dump($data);die();
       
       if(isset($data["Markup"]))
       {
           $this->form_validation->set_rules("Markup", "Markup", "numeric|greater_than_equal_to[1]|less_than_equal_to[100]");
           if ($this->form_validation->run() == FALSE)
           {
                $error = lang("error.value_beween0and100");
                $this->session->set_flashdata('error_msg', $error);
           }
            redirect($_SERVER['HTTP_REFERER']);
       }
        $data["agency_id"] = $this->current_user["id"];
        $this->login_model->update($data["id"],$data);
        redirect($_SERVER['HTTP_REFERER']);
    }
    
    function add_subagency_get()
    {
        redirect("subagencies");
    }
    function add_subagency_post()
    {
        $user = (array)$this->post();
        $this->form_validation->set_rules("balance", "Balance", "numeric|greater_than_equal_to[0]");
        $this->form_validation->set_rules("name", "Name", "required");
        $this->form_validation->set_rules("email", "Email", "required");
        if($this->form_validation->run() == FALSE)
        {
            $this->subagencies_get();
            return;
        }
        $user["agency_id"] = $this->current_user["id"];
        $user["currency"] = $this->current_user["currency"];
         $this->load->model('login_model');
		$a=$this->login_model->insert_return_user($user);
		redirect("subagencies");
		
    }
    
  
    function profile_get()
    {
         $this->data["info"] = $this->api_model->get_agency($this->current_user["id"]);
         unset($this->data["info"]["password"]);
         $this->data['main_content'] = "profile";
        $this->load->view('includes-web/main',$this->data);
    }
    
    function saveImage_post()
    {
          $this->load->helper("fancy_file_uploader_helper");
          if (isset($_REQUEST["action"]) && $_REQUEST["action"] === "fileuploader")
          {
           
            $allowedexts = array(
              "jpg" => true,
              "png" => true,
            );
        
            $files = FancyFileUploaderHelper::NormalizeFiles("files");
            //print_r($files[0]);
            if (!isset($files[0]))  $result = array("success" => false, "error" => "File data was submitted but is missing.", "errorcode" => "bad_input");
            else if (!$files[0]["success"])  $result = $files[0];
            else if (!isset($allowedexts[strtolower($files[0]["ext"])]))
            {
              $result = array(
                "success" => false,
                "error" => "Invalid file extension.  Must be '.jpg' or '.png'.",
                "errorcode" => "invalid_file_ext"
              );
            }
            else
            {
              // For chunked file uploads, get the current filename and starting position from the incoming headers.
              $name = FancyFileUploaderHelper::GetChunkFilename();
              //var_dump($name);
              if ($name !== false)
              {
                $startpos = FancyFileUploaderHelper::GetFileStartPosition();
        
                // [Do stuff with the file chunk.]
              }
              else
              {
                // [Do stuff with the file here.]
                $t=round(microtime(true) * 1000);
                 copy($files[0]["file"], "uploads/" . $t.".".$files[0]["ext"]);
                 $data_insert["image"]=$t.".".$files[0]["ext"];
                 $data_insert["offer_id"]= $_POST["offer_id"];
                 $this->db->insert("offer_images",$data_insert);
              }
        
              $result = array(
                "success" => true
              );
            }
          }
        //print_r($result);
        $this->response($result,200);
    }
  
    function contact_get()
    {
        	$this->data['main_content'] = "contact";

		 $this->load->view('includes-web/main',$this->data);
    }

    function pag_config($fun,$type)
    {

         $config = array();
        $config["base_url"] = base_url() . "OfferController/".$fun."?"."id=".$type;
        $config["total_rows"] = $this->api_model->record_count($type);
		    $config['page_query_string']=true;
       //var_dump($config["total_rows"] );die();
        $config["per_page"] = 9;
        $config["uri_segment"] = 3;
        $config['cur_tag_open'] = '&nbsp;<a style="background-color: darkgrey;">';

        // Close tag for CURRENT link.
        $config['cur_tag_close'] = '</a>';
        
        // By clicking on performing NEXT pagination.
       // $config['next_link'] = 'Next';
        
        // By clicking on performing PREVIOUS pagination.
        //$config['prev_link'] = 'Previous';

        $this->pagination->initialize($config);
        
         $this->data["links"] = $this->pagination->create_links();
        // var_dump($this->data["links"]);
    }
    function pag_config2($fun,$table,$attr,$type,$where,$val="")
    {

         $config = array();
        $config["base_url"] = base_url() . "OfferController/".$fun."?".$attr."=".$type;
        $config["total_rows"] = $this->api_model->record_count($table,$where,$val);
		    $config['page_query_string']=true;
       //var_dump($config["total_rows"] );die();
        $config["per_page"] = 9;
        $config["uri_segment"] = 3;
        $config['cur_tag_open'] = '&nbsp;<a style="background-color: darkgrey;">';

        // Close tag for CURRENT link.
        $config['cur_tag_close'] = '</a>';
        
        // By clicking on performing NEXT pagination.
       // $config['next_link'] = 'Next';
        
        // By clicking on performing PREVIOUS pagination.
        //$config['prev_link'] = 'Previous';

        $this->pagination->initialize($config);
        $this->data["links"] = $this->pagination->create_links();
         //$this->data["links"] = $this->pagination->create_links();
         //return $config["total_rows"];
    }
    
    

}