<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('memory_limit', '-1');
class Examples	 extends CI_Controller {
    
    
	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
	}
	function import_company()
	{
		$this->db2 = $this->load->database("db2",true);
		$res = $this->db2->where("type =1")->or_where("type=2")->get("strings")->result_array();
		$i=0;
		foreach ($res as $key => $value) {
			echo $value["String"]."<br>";
			$co_data[$i]["company_name_ar"] = $value["String"];
			$co_data[$i]["type"] = $value["Type"];
			$i++;
		}
		$this->db->insert_batch('company', $co_data);
		
		//print_r($res);
	}

	function import_products()
	{
		$this->db2 = $this->load->database("db2",true);
		$res = $this->db2->get("product")->result_array();
		$i=0;
		foreach ($res as $key => $value) {
			$product_data[$i]["name"] = $value["NmA"];
			$product_data[$i]["name_eng"] = $value["Nm"];
			$product_data[$i]["details"] = $value["Gebrauch"]." ".$value["ChFm"];
			$product_data[$i]["details_eng"] = $value["Gebrauch"]." ".$value["ChFm"];
			$product_data[$i]["Siz"] = $value["Siz"];
			$product_data[$i]["price"] = $value["Pr"];
			$product_data[$i]["Dose"] = $value["Dose"];
			$product_data[$i]["origin_countryid"] = 235;
			$product_data[$i]["category_id"] = 1;
			$i++;
		}
		$this->db->insert_batch('products', $product_data);
	}
    public static function convert_to_utf8_recursively($dat)
    {
       if (is_string($dat)) {
          return iconv('WINDOWS-1256', 'UTF-8',$dat);
       } elseif (is_array($dat)) {
          $ret = [];
          foreach ($dat as $i => $d) $ret[ $i ] = self::convert_to_utf8_recursively($d);
 
          return $ret;
       } elseif (is_object($dat)) {
          foreach ($dat as $i => $d) $dat->$i = self::convert_to_utf8_recursively($d);
 
          return $dat;
       } else {
          return $dat;
       }
    }
    
    function import_csv()
	{
    	$file = fopen(base_url().'data/data.csv',"r");
    
    	$this->db->truncate('item');
    	$i = 0 ;
		while(! feof($file))
          {
          //print_r(fgetcsv($file));
            $arr =fgetcsv($file);
            $data[$i]["code"] = $arr[0];
            $data[$i]["category"] = $arr[1];
            	//var_dump(iconv('UTF-8', 'ISO-8859-1//TRANSLIT',  $arr[1]));
            //echo mb_convert_encoding( $data[$i]["category"], 'UTF-16LE', 'UTF-8')."<br>";
            $data[$i]["group"] = $arr[2];
            $data[$i]["article_number"] = $arr[3];
            $data[$i]["id"] = $arr[3];
            $data[$i]["image"] = $arr[3].".jpg";
            if(isset($arr[4]))
            {
                 $data[$i]["packeging"] = $arr[4];
                  $data[$i]["price"] = $arr[5];
            }
            $i++;
          }
          //print_r($data);
          unset($data[0]);
          $res = self::convert_to_utf8_recursively($data);
         // print_r($res);
        $this->db->insert_batch("item",$res);
        fclose($file);
		
		//$this->image_moo->load($file_uploaded)->resize(250,250)->save($target,true);
	 
		return true;
	}
	
    function import_json()
	{
        /*$string = file_get_contents(base_url().'data/data.json');
        //var_dump($string);
        $string = utf8_decode($string);
        $string = preg_replace('/[[:cntrl:]]/', '', $string);*/
        $myfile = fopen(getcwd()."/data/log.txt", "w");
        fwrite($myfile, date("Y-m-d H:i"));
        fclose($myfile);//die();
        $string = file_get_contents(base_url().'data/jsondata.json');
	//	echo $string;
		$string = self::convert_to_utf8_recursively($string);
	//	$result = json_decode($string, true, JSON_UNESCAPED_UNICODE);
        $res_data = json_decode($string,true, JSON_UNESCAPED_UNICODE);
      //  var_dump($res_data);
        var_dump(json_last_error_msg());//die();
    	$this->db->truncate('item');
    	$i = 0 ;
    	
    	foreach($res_data["Output"] as $arr)
    	{
    	    $data[$i]["code"] = $arr["CODE"];
            $data[$i]["category"] = $arr["CAT"];
            	//var_dump(iconv('UTF-8', 'ISO-8859-1//TRANSLIT',  $arr[1]));
            //echo mb_convert_encoding( $data[$i]["category"], 'UTF-16LE', 'UTF-8')."<br>";
            $data[$i]["group"] = $arr["GROUP-ID"];
            $data[$i]["article_number"] = $arr["ART-NUM"];
            $data[$i]["id"] = $arr["CODE"];
            $data[$i]["image"] = $arr["ART-NUM"].".jpg";
          
                 $data[$i]["packeging"] = $arr["PCK"];
                   $data[$i]["store_quantity"] = $arr["QUANTITY"];
                  $data[$i]["price"] = $arr["PRICE"];
            
            $i++;
    	}
        //  $res = self::convert_to_utf8_recursively($data);
         // print_r($res);
        $this->db->insert_batch("item",$data);
        $this->check_exist_image();
		return true;
	}
	
	function check_exist_image()
	{
	    $products = $this->db->where("has_real_image",0)->get("item")->result_array();
	    foreach($products as $value)
	    {
	        if(file_exists(getcwd()."/uploads/".$value["image"]))
	        {
	            $this->db->where("id",$value["id"])->update("item",array("has_real_image"=>1));
	        }
	    }
	}
	public function _example_output($output = null)
	{
		$this->load->view('example.php',$output);
	}

	public function offices()
	{
		$output = $this->grocery_crud->render();

		$this->_example_output($output);
	}

	public function index()
	{
	echo "enter";
	}

	public function offices_management()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('offices');
			$crud->set_subject('Office');
			$crud->required_fields('city');
			$crud->columns('city','country','phone','addressLine1','postalCode');

			$output = $crud->render();

			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function employees_management()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('employees');
			$crud->set_relation('officeCode','offices','city');
			$crud->display_as('officeCode','Office City');
			$crud->set_subject('Employee');

			$crud->required_fields('lastName');

			$crud->set_field_upload('file_url','assets/uploads/files');

			$output = $crud->render();

			$this->_example_output($output);
	}

	public function customers_management()
	{
			$crud = new grocery_CRUD();

			$crud->set_table('customers');
			$crud->columns('customerName','contactLastName','phone','city','country','salesRepEmployeeNumber','creditLimit');
			$crud->display_as('salesRepEmployeeNumber','from Employeer')
				 ->display_as('customerName','Name')
				 ->display_as('contactLastName','Last Name');
			$crud->set_subject('Customer');
			$crud->set_relation('salesRepEmployeeNumber','employees','lastName');

			$output = $crud->render();

			$this->_example_output($output);
	}

	public function orders_management()
	{
			$crud = new grocery_CRUD();

			$crud->set_relation('customerNumber','customers','{contactLastName} {contactFirstName}');
			$crud->display_as('customerNumber','Customer');
			$crud->set_table('orders');
			$crud->set_subject('Order');
			$crud->unset_add();
			$crud->unset_delete();

			$output = $crud->render();

			$this->_example_output($output);
	}

	public function products_management()
	{
			$crud = new grocery_CRUD();

			$crud->set_table('products');
			$crud->set_subject('Product');
			$crud->unset_columns('productDescription');
			$crud->callback_column('buyPrice',array($this,'valueToEuro'));

			$output = $crud->render();

			$this->_example_output($output);
	}

	public function valueToEuro($value, $row)
	{
		return $value.' &euro;';
	}

	public function film_management()
	{
		$crud = new grocery_CRUD();

		$crud->set_table('film');
		$crud->set_relation_n_n('actors', 'film_actor', 'actor', 'film_id', 'actor_id', 'fullname','priority');
		$crud->set_relation_n_n('category', 'film_category', 'category', 'film_id', 'category_id', 'name');
		$crud->unset_columns('special_features','description','actors');

		$crud->fields('title', 'description', 'actors' ,  'category' ,'release_year', 'rental_duration', 'rental_rate', 'length', 'replacement_cost', 'rating', 'special_features');

		$output = $crud->render();

		$this->_example_output($output);
	}

	public function film_management_twitter_bootstrap()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('twitter-bootstrap');
			$crud->set_table('film');
			$crud->set_relation_n_n('actors', 'film_actor', 'actor', 'film_id', 'actor_id', 'fullname','priority');
			$crud->set_relation_n_n('category', 'film_category', 'category', 'film_id', 'category_id', 'name');
			$crud->unset_columns('special_features','description','actors');

			$crud->fields('title', 'description', 'actors' ,  'category' ,'release_year', 'rental_duration', 'rental_rate', 'length', 'replacement_cost', 'rating', 'special_features');

			$output = $crud->render();
			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function multigrids()
	{
		$this->config->load('grocery_crud');
		$this->config->set_item('grocery_crud_dialog_forms',true);
		$this->config->set_item('grocery_crud_default_per_page',10);

		$output1 = $this->offices_management2();

		$output2 = $this->employees_management2();

		$output3 = $this->customers_management2();

		$js_files = $output1->js_files + $output2->js_files + $output3->js_files;
		$css_files = $output1->css_files + $output2->css_files + $output3->css_files;
		$output = "<h1>List 1</h1>".$output1->output."<h1>List 2</h1>".$output2->output."<h1>List 3</h1>".$output3->output;

		$this->_example_output((object)array(
				'js_files' => $js_files,
				'css_files' => $css_files,
				'output'	=> $output
		));
	}

	public function offices_management2()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('offices');
		$crud->set_subject('Office');

		$crud->set_crud_url_path(site_url(strtolower(__CLASS__."/".__FUNCTION__)),site_url(strtolower(__CLASS__."/multigrids")));

		$output = $crud->render();

		if($crud->getState() != 'list') {
			$this->_example_output($output);
		} else {
			return $output;
		}
	}

	public function employees_management2()
	{
		$crud = new grocery_CRUD();

		$crud->set_theme('datatables');
		$crud->set_table('employees');
		$crud->set_relation('officeCode','offices','city');
		$crud->display_as('officeCode','Office City');
		$crud->set_subject('Employee');

		$crud->required_fields('lastName');

		$crud->set_field_upload('file_url','assets/uploads/files');

		$crud->set_crud_url_path(site_url(strtolower(__CLASS__."/".__FUNCTION__)),site_url(strtolower(__CLASS__."/multigrids")));

		$output = $crud->render();

		if($crud->getState() != 'list') {
			$this->_example_output($output);
		} else {
			return $output;
		}
	}

	public function customers_management2()
	{
		$crud = new grocery_CRUD();

		$crud->set_table('customers');
		$crud->columns('customerName','contactLastName','phone','city','country','salesRepEmployeeNumber','creditLimit');
		$crud->display_as('salesRepEmployeeNumber','from Employeer')
			 ->display_as('customerName','Name')
			 ->display_as('contactLastName','Last Name');
		$crud->set_subject('Customer');
		$crud->set_relation('salesRepEmployeeNumber','employees','lastName');

		$crud->set_crud_url_path(site_url(strtolower(__CLASS__."/".__FUNCTION__)),site_url(strtolower(__CLASS__."/multigrids")));

		$output = $crud->render();

		if($crud->getState() != 'list') {
			$this->_example_output($output);
		} else {
			return $output;
		}
	}

}