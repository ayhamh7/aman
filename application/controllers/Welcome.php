<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
define('API_ACCESS_KEY', 'AAAAaC8DVYk:APA91bHtSzJffQPKEhCuOZCk79IgAZV1WRQ9T2dPMy_QcfYOtjAb68G39kOq0nJA06kdX7D0flaqPt-RIOF3zvE18iPl3R6NB104apudBc6Q98uUz1-Mt4c586ANi44bZwYNymc6rWaq'); //AIzaSyBLV2ZQQFHy0lR7T-LUFOl-CyoWFiHe9q0

class Welcome	 extends CI_Controller {
    
    
	public function __construct()
	{
		parent::__construct();
   
	}
    function test_notification()
    {
      $android_token = 'e6Jv4ogySNGmublQntPo1S:APA91bFcbJC6x87Heyi9uQDwg1NwJXomYbDifSAdvdbsWn48rS35DKMFCw1iM9IRVXHgeCz4AdUROe6vrujkx_XajK12IXrXHIrC5vRIDGJckns6I2X6eWKGJjsRuaKwnV5S80eVC5TT';//$user["firebase_token"];
      $message = 'لقد اقترب الباص من الوصول';
      // var_dump(API_ACCESS_KEY);
      // $ios_token = $user["ios_token"];
      $msg = array(
        'body'  => strip_tags($message),
        'title'     => "حافلتي",
      );
      $notification = array(
        'body'  => strip_tags($message),
        'title'     => "حافلتي",
        "click_action" => ''
      );
      
      $fields = array(
        //'to'  =>$android_token,
        "registration_ids" => [$android_token],
        'data'          => $msg,
        'notification' => $notification,
        'priority' => 'high'
      );
  
  
  
      $headers = array(
        'Authorization: key=' . API_ACCESS_KEY,
        'Content-Type: application/json'
      );
      //echo API_ACCESS_KEY;
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
      $result = curl_exec($ch);
      //echo($result);
  
      if ($result === false) {
        echo 'Curl error: ' . curl_error($ch);
      } else {
        //echo 'Operation completed without any errors';
      }
      if ($errno = curl_errno($ch)) {
        $error_message = curl_strerror($errno);
        echo "cURL error ({$errno}):\n {$error_message}";
      }
      curl_close($ch);
      //die();
      /*
          TODO exho result
      */
      echo $result;
  
    }
    public static function convert_to_utf8_recursively($dat)
    {
       if (is_string($dat)) {
          return iconv('WINDOWS-1256', 'UTF-8',$dat);
       } elseif (is_array($dat)) {
          $ret = [];
          foreach ($dat as $i => $d) $ret[ $i ] = self::convert_to_utf8_recursively($d);
 
          return $ret;
       } elseif (is_object($dat)) {
          foreach ($dat as $i => $d) $dat->$i = self::convert_to_utf8_recursively($d);
 
          return $dat;
       } else {
          return $dat;
       }
    }
    
 
   


	
	public function index()
	{
	  
      $this->load->view('index.html');
	}
	
	public function hotel_index()
	{
	    $this->data["sliders"] = $this->api_model->get_sliders();
	  $this->data["title"] = "Home Page";
      $this->data['main_content'] = "hotel_home";

      $this->load->view('includes-web/main',$this->data);
	}
    
    
    function error_page()
    {
         $this->data["title"] = "404";
      $this->data['main_content'] = "error_page";

      $this->load->view('includes-web/main',$this->data);
    }
    
     function contact_us()
    {
      $this->data["active_menue"] = "contact";
         $this->data["title"] = "Contact Us";
      $this->data['main_content'] = "contact_us";

      $this->load->view('includes-web/main',$this->data);
    }
     function about_us()
    {
      $this->data["active_menue"] = "about";
         $this->data["title"] = "About Us";
         $this->data["info"] = $this->db->get("about")->row_array();
      $this->data['main_content'] = "about_us";

      $this->load->view('includes-web/main',$this->data);
    }
    function thank_you()
    {
      $this->load->view('thank_you');
    }
    function faq()
    {
      $this->data["active_menue"] = "faq";
      $this->data["title"] = "About Us";
      $this->data["questions"] = $this->db->get("site_faq")->result_array();
   $this->data['main_content'] = "faq";

   $this->load->view('includes-web/main',$this->data);
    }
    function policy()
    {
      $this->data["active_menue"] = "policy";
         $this->data["title"] = "Policy";
         $this->data["info"] = $this->db->get("policy")->row_array();
      $this->data['main_content'] = "policy";

      $this->load->view('includes-web/main',$this->data);
    }
    function save_contact()
    {
        $data = $this->input->post();
        $this->load->library('form_validation');
        $this->form_validation->set_rules("email", "Email", "required|max_length[50]|min_length[3]|valid_email");
        $this->form_validation->set_rules("name", "Name", "required|max_length[40]|min_length[3]");
        $this->form_validation->set_rules("phone", "Phone", "required|max_length[40]|min_length[9]");
        $this->form_validation->set_rules("subject", "Subject", "required|max_length[40]|min_length[3]");
        $this->form_validation->set_rules("message", "Message", "required|min_length[3]");
        if($this->form_validation->run() == FALSE)
        {
             $this->session->set_flashdata('error_msg', "Error Send Message");
             redirect($_SERVER['HTTP_REFERER']);
        }
        $this->db->insert("contact",$data);
         redirect($_SERVER['HTTP_REFERER']."?success_msg= message send successfuly");
    }
    public function lang()
    {
      $lang=$this->session->userdata('lang');
    //	var_dump($lang);die();
      if(isset($lang)&& $lang!=false){
        if($lang=="ar"){
          $this->session->set_userdata('lang','en');
          $this->lang->load("message", "english");
        }
        else{
          $this->session->set_userdata('lang','ar');
          $this->lang->load("message", "arabic");
        }
      }
      else{
        $this->session->set_userdata('lang','en');
        $this->lang->load("message", "english");
      }
  
      header('Location: ' . $_SERVER['HTTP_REFERER']);
      die;
    }


}