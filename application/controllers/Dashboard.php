<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('API_ACCESS_KEY', 'AAAAaC8DVYk:APA91bHtSzJffQPKEhCuOZCk79IgAZV1WRQ9T2dPMy_QcfYOtjAb68G39kOq0nJA06kdX7D0flaqPt-RIOF3zvE18iPl3R6NB104apudBc6Q98uUz1-Mt4c586ANi44bZwYNymc6rWaq'); //AIzaSyBLV2ZQQFHy0lR7T-LUFOl-CyoWFiHe9q0
class dashboard extends CI_Controller
{

	/**
	 * Index Page for this controller.manage_types
	 *csINVISIBLEstore
	 * Maps to the following URL
	 * 		http://example.com/index.php/dabookinshboard
	 *	- or -iadd_balance
	 * 		http://example.com/index.php/dashboard/index
	 *	- or -pr
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/dashboard/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
	{
		parent::__construct();
		// if(strtotime("2019/04/24") <= time())
		// 	die();
		$this->load->database();
		$this->load->helper('url', 'message');
		$this->load->library('grocery_CRUD');
		$this->load->library("ion_auth");
		$this->load->model("api_model");
		if (!$this->ion_auth->logged_in()) {
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		$lang = $this->session->userdata('lang');

		if (isset($lang)) {
			if ($lang == "ar")
				$this->lang->load("message", "arabic");
			else
				$this->lang->load("message", "english");

			$this->data["lang"] = $lang;
		} else {
			$this->lang->load("message", "arabic");
			$this->data["lang"] = "ar";
		}



		// $unactive_count = $this->db->select("count(*) as visit_count")->from("agency")->where("is_active",0)->get()->result();
		$this->data["unactive_agency"] = 0;

		// $unactive_count = $this->db->select("count(*) as visit_count")->from("agency")->get()->result();
		$this->data["all"] = 0;

		// $unactive_count = $this->db->select("count(*) as visit_count")->where("status",0)->from("balance_request")->get()->result();
		$this->data["pending"] = 0;

		// $unactive_users = $this->db->select("count(*) as visit_count")->from("customer")->where("is_active",0)->get()->result();
		// $this->data["unactive"]=$unactive_users[0]->visit_count;;

		// $active_users = $this->db->select("count(*) as visit_count")->from("customer")->where("is_active",1)->get()->result();
		// $this->data["active"]=$active_users[0]->visit_count;;


		// $items = $this->db->select("count(*) as visit_count")->from("item")->where("has_real_image",0)->get()->result();
		// $this->data["not_image"]=$items[0]->visit_count;;
		//var_dump($this->db->last_query());


	}


	public function index()
	{
		//$this->load->view("dashboard/index.php");
		redirect(base_url('Dashboard/manageCustomers'));
	}
	function manage_ads()
	{
		$this->data["title"] = "إدارة الاعلانات";
		$this->data["ads_active"] = true;
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		$crud->set_table('ads');
		$crud->required_fields("title");
		$crud->required_fields("image");
		$crud->set_field_upload('image', 'uploads');
		$crud->set_relation('product_id', 'products', 'name');
		$crud->unset_fields('date');
		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";

		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}

	function manage_policy()
	{
		$this->data["title"] = "إدارة سياسة الخصوصية";
		$this->data["gm_active"] = true;

		$crud = new grocery_CRUD();
		$crud->where("code", "002");
		$crud->set_language("arabic");
		$crud->set_theme('bootstrap');
		$crud->set_table('app_constant');
		$crud->unset_fields("code", "content_en");
		$crud->unset_columns("content_en");
		$crud->display_as("title", "العنوان");
		$crud->display_as("content_ar", "الشرح");
		$crud->unset_add();
		$crud->unset_delete();
		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";

		$this->load->view($data['main_content'], $this->data);
	}

	function manage_about()
	{
		$this->data["title"] = "إدارة حولنا";
		$this->data["gm_active"] = true;

		$crud = new grocery_CRUD();
		$crud->where("code", "003");
		$crud->set_theme('bootstrap');
		$crud->set_language("arabic");
		$crud->set_table('app_constant');
		$crud->unset_fields("code", "content_en");
		$crud->unset_columns("content_en");
		$crud->display_as("title", "العنوان");
		$crud->display_as("content_ar", "الشرح");
		$crud->unset_add();
		$crud->unset_delete();
		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";
		$this->load->view($data['main_content'], $this->data);
	}
	function manage_contact()
	{
		$this->data["title"] = "إدارة اتصل بنا";
		$this->data["gm_active"] = true;
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		$crud->set_table('contact');
		$crud->unset_add();
		$crud->unset_edit();
		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";

		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}
	function test_notification()
	{
		$message = "تجربة";
		$this->sendNotification($message, 1, 1);
	}

	function test_allnotification()
	{
		$message = "تجربة الجميع";
		$this->sendNotification_all($message);
	}
	function _users_output($output = null)
	{
		$this->load->view('example.php', $output);
	}


	function manage_category()
	{
		$this->data["title"] = lang('manage_categories');
		$this->data["gm_active"] = true;
		$crud = new grocery_CRUD();
		$crud->set_language("arabic");
		$crud->set_theme('bootstrap');
		$crud->set_table('category');
		$crud->where("parent_id is NULL");
		$crud->required_fields("name");
		$crud->unset_columns("name_en", "parent_id");
		$crud->unset_fields("name_en", "parent_id");
		$crud->set_field_upload('image', 'uploads');
		$crud->add_action('المدخلات', '', '', 'ui-icon-image', array($this, 'types_action'));
		$crud->add_action('التصنيفات الفرعية', '', '', 'ui-icon-image', array($this, 'sub_catgories_action'));
		$crud->callback_after_upload(array($this, 'compress_callback_after_upload'));
		$crud->display_as("name", "اسم التصنيف ");
		$crud->display_as("image", "الصورة");
		$crud->display_as("order", "الترتيب");
		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";

		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}
	function sub_catgories_action($primary_key, $row)
	{
		return base_url('/dashboard/manage_subcategories') . '/' . $row->id;
	}
	function manage_subcategories($cat_id)
	{
		$cat = $this->db->where('id', $cat_id)->get("category")->row_array();

		$this->data["title"] = "إدارة التصنيفات الفرعية - " . $cat["name"];
		$this->data["gm_active"] = true;
		$crud = new grocery_CRUD();
		$crud->set_language("arabic");
		$crud->set_theme('bootstrap');
		$crud->set_table('category');
		$crud->unset_columns("name_en", "image", "parent_id");
		$crud->unset_fields("name_en", "image");
		$crud->where("parent_id", $cat_id);
		$crud->field_type('parent_id', 'hidden', $cat_id);
		$crud->display_as("name", "اسم التصنيف ");
		$crud->display_as("image", "الصورة");
		$crud->display_as("order", "الترتيب");
		$crud->add_action('التصنيفات الفرعية', '', '', 'ui-icon-image', array($this, 'sub_catgories_action'));
		$crud->required_fields("name");
		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";

		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}
	function types_action($primary_key, $row)
	{
		return base_url('/dashboard/manage_types') . '/' . $row->id;
	}
	function manage_types($cat_id)
	{
		$cat = $this->db->where('id', $cat_id)->get("category")->row_array();

		$this->data["title"] = "  إدارة واصفات تصنيف -" . $cat["name"];
		$this->data["gm_active"] = true;
		$crud = new grocery_CRUD();
		$crud->set_language("arabic");
		$crud->set_theme('bootstrap');
		$crud->set_table('attrs');
		$crud->where("category_id", $cat_id);
		$crud->display_as("attr_title", "العنوان");
		$crud->display_as("type", "النوع");
		$crud->display_as("value", "القيمة");
		$crud->display_as("category_id", "التصنيف");
		$crud->field_type('category_id', 'hidden', $cat_id);
		$crud->required_fields("attr_title");
		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";

		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}

	function encrypt_password_callback2($post_array, $primary_key = null)
	{
		$this->load->library("auth");
		$this->load->model("ion_auth_model");
		$post_array['password'] = $this->ion_auth_model->hash_password($post_array['password']);
		//$post_array['is_active'] = 1;
		return $post_array;
	}
	function manage_constant()
	{
		$this->data["title"] = lang('manage_charge_method');
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		$crud->set_table('app_constant');
		$crud->unset_add();
		$crud->unset_delete();
		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";

		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}

	function manage_cities()
	{
		$this->data["title"] = "إدارة المدن";
		$this->data["gm_active"] = true;
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		$crud->set_language("arabic");
		$crud->set_table('cities');
		$crud->display_as("city_name", "اسم المدينة");
		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";

		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}
	function manage_locations()
	{
		$this->data["title"] = "إدارة الأحياء";
		$this->data["gm_active"] = true;
		$crud = new grocery_CRUD();
		$crud->set_relation('city_id', 'cities', 'city_name');
		$crud->set_language("arabic");
		$crud->display_as("state_name", "اسم الحي");
		$crud->display_as("city_id", "اسم المدينة");
		$crud->set_theme('bootstrap');
		$crud->set_table('state');
		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";

		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}

	function manage_payment_method()
	{
		$this->data["title"] = lang('manage_payment_method');
		$this->data["gm_active"] = true;
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		$crud->set_table('payment_method');
		$crud->set_field_upload('method_image', 'uploads');
		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";

		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}



	function manage_slider()
	{
		$this->data["title"] = lang('manage_slider');
		$this->data["ads_active"] = true;
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		$crud->set_table('slider');
		$crud->required_fields("title", "image");
		$crud->set_relation('product_id', 'products', 'name');
		$crud->set_field_upload('image', 'uploads');
		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";

		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}

	function manage_changestop_request()
	{
		//change_stop_request
		
		$this->data["title"] = 'طلب تغيير موقف';
		$this->data["ads_active"] = true;
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		$crud->set_language('arabic');
		$crud->set_table('change_stop_request');
		$crud->required_fields("title", "image");
		$crud->set_relation('requester_id', 'customer', '{name}-{mobile}');
		$crud->order_by("request_date","DESC");
		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";

		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
		
	}

	function manage_changebus_request()
	{
		//change_stop_request
		
		$this->data["title"] = 'طلب تغيير خط باص';
		$this->data["ads_active"] = true;
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		$crud->set_language('arabic');
		$crud->set_table('change_line_request');
		$crud->required_fields("title", "image");
		$crud->set_relation('requester_id', 'customer', '{name}-{mobile}');
		$crud->order_by("request_date","DESC");
		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";

		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
		
	}

	function manage_product_stores($primary_key, $row)
	{
		return base_url('/dashboard/product_stores') . '/' . $row->id;
	}
	function manage_store_products($primary_key, $row)
	{
		return base_url('/dashboard/store_products') . '/' . $row->id;
	}


	function store_products($user_id)
	{
		$this->data["gm_active"] = true;
		$this->data["title"] = "إعلانات زبون";
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		$crud->set_table('products');
		$crud->set_language("arabic");
		$crud->set_relation('category_id', 'category', 'name');
		$crud->set_relation('city_id', 'cities', 'city_name');
		$crud->set_relation('state_id', 'state', 'state_name');
		$crud->unset_columns("lat", "lang", "type_id");
		//$crud->add_action('المستودعات', '', '','ui-icon-image',array($this,'manage_product_stores'));
		$crud->set_relation('store_id', 'customer', '{name}-{mobile}');
		$crud->set_field_upload('image', 'uploads');
		$crud->callback_after_upload(array($this, 'compress_callback_after_upload'));
		$crud->where("store_id", $user_id);
		$crud->field_type('store_id', 'hidden', $user_id);
		$crud->display_as("title", "العنوان");
		$crud->display_as("price", "السعر");
		$crud->display_as("city_id", "المدينة");
		$crud->display_as("state_id", "الحي");
		$crud->display_as("address", "العنوان");
		$crud->display_as("store_id", "الزبون");
		$crud->display_as("image", "الصورة");
		$crud->display_as("description", "الوصف");
		$crud->display_as("video", "الفيديو");
		$crud->display_as("category_str", "التصنيفات");
		$crud->display_as("attr_str", "قيم المدخلات");
		$crud->unset_add();
		//$crud->set_relation('item_id','products','{name}');
		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";

		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}
	function manageCustomers()
	{
		$this->data['customer_active'] = true;
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		$this->data['title'] = "إدارة جميع المستخدمين";
		$crud->set_table('customer');
		//$crud->add_action('View Coupons', '', '','ui-icon-image',array($this,'customer_coupon_action'));
		$crud->order_by('id', 'desc');
		$crud->set_field_upload('image', 'uploads');
		$crud->set_language("arabic");
		$crud->display_as("name", "الاسم");
		$crud->display_as("mobile", "الموبايل");
		$crud->display_as("address", "العنوان");
		$crud->display_as("nationalImage","صورة الهوية");
		$crud->set_field_upload('nationalImage', 'uploads');
		$crud->display_as("register_date", "تاريخ التسجيل");
		$crud->display_as("email", "الايميل");
		$crud->callback_before_insert(array($this,'encrypt_password_callback'));
		$crud->unset_columns("reset_code","is_new","android_token", "activation_code", "password", "created_date", "firebase_token");
		$crud->unset_edit_fields("reset_code","is_new","android_token", "activation_code", "password", "created_date", "firebase_token");
		$crud->unset_add_fields("reset_code","is_new","android_token", "activation_code", "created_date", "firebase_token");
		$crud->unset_add();
		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";
		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}

	function manageSchools()
	{
		$this->data['customer_active'] = true;
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		
		$this->data['title'] = "إدارة  المدارس";
		$crud->set_table('customer');
		$crud->where("customer.type","school");
		$crud->add_action('عرض الباصات', '', '','ui-icon-image',array($this,'school_buses_action'));
		$crud->add_action('عرض الأهل', '', '','ui-icon-image',array($this,'school_parents_action'));
		$crud->order_by('id', 'desc');
		$crud->set_field_upload('image', 'uploads');
		$crud->set_language("arabic");
		$crud->display_as("name", "الاسم");
		$crud->display_as("mobile", "الموبايل");
		$crud->display_as("address", "العنوان");
		$crud->display_as("nationalImage","صورة الهوية");
		$crud->set_field_upload('nationalImage', 'uploads');
		$crud->display_as("register_date", "تاريخ التسجيل");
		$crud->display_as("email", "الايميل");
		$crud->callback_before_insert(array($this,'encrypt_password_callback_with_insert_user'));
		$crud->unset_columns("stop_id","reset_code","is_new","related_id","android_token", "activation_code", "password", "created_date", "firebase_token");
		$crud->unset_edit_fields("stop_id","reset_code","is_new","related_id","android_token", "activation_code", "password", "created_date", "firebase_token");
		$crud->unset_add_fields("stop_id","reset_code","is_new","related_id","android_token", "activation_code", "created_date", "firebase_token");
		//$crud->unset_add();
		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";
		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}
	function school_buses_action($primary_key, $row)
	{
		return base_url('/dashboard/manageSchoolBuses') . '/' . $row->id;
	}
	function school_parents_action($primary_key, $row)
	{
		return base_url('/dashboard/manageSchoolParents') . '/' . $row->id;
	}
	function manageSchoolBuses($school_id)
	{
		$this->data['customer_active'] = true;
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		$this->data['title'] = "إدارة  باصات مدرسة";
		$crud->set_table('customer');
		$crud->where("customer.type","bus");
		$crud->where("related_id",$school_id);
		//$crud->set_relation("related_id","customer","{name}-{mobile}");
		//$crud->add_action('View Coupons', '', '','ui-icon-image',array($this,'customer_coupon_action'));
		$crud->order_by('id', 'desc');
		$crud->set_field_upload('image', 'uploads');
		$crud->set_language("arabic");
		$crud->display_as("name", "الاسم");
		$crud->display_as("mobile", "الموبايل");
		$crud->display_as("address", "العنوان");
		$crud->display_as("type","نوع الحساب");
		$crud->display_as("related_id","المدرسة التابع لها");
		$crud->display_as("nationalImage","صورة الهوية");
		$crud->set_field_upload('nationalImage', 'uploads');
		$crud->display_as("register_date", "تاريخ التسجيل");
		$crud->display_as("email", "الايميل");
		$crud->callback_before_insert(array($this,'encrypt_password_callback'));
		$crud->unset_columns("stop_id","related_id","reset_code","is_new","android_token", "activation_code", "password", "created_date", "firebase_token");
		$crud->unset_edit_fields("stop_id","reset_code","is_new","android_token", "activation_code", "password", "created_date", "firebase_token");
		$crud->unset_add_fields("stop_id","reset_code","is_new","android_token", "activation_code", "created_date", "firebase_token");
		if(!$this->ion_auth->is_admin())
			$crud->unset_add();
		$crud->unset_edit();
		$crud->unset_delete();
		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";
		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}
	function manageSchoolParents($school_id)
	{
		$this->data['customer_active'] = true;
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		$this->data['title'] = "إدارة  حسابات الاهل لمدرسة";
		$crud->set_table('customer');
		$crud->where("customer.type","parent");
		$crud->where("related_id",$school_id);
		$crud->field_type('related_id', 'hidden', $school_id);
		//$crud->add_action('View Coupons', '', '','ui-icon-image',array($this,'customer_coupon_action'));
		$crud->order_by('id', 'desc');
		$crud->set_field_upload('image', 'uploads');
		$crud->set_language("arabic");
		$crud->display_as("name", "الاسم");
		$crud->display_as("mobile", "الموبايل");
		$crud->display_as("address", "العنوان");
		$crud->display_as("type","نوع الحساب");
		$crud->display_as("related_id","المدرسة التابع لها");
		$crud->display_as("nationalImage","صورة الهوية");
		$crud->set_field_upload('nationalImage', 'uploads');
		$crud->display_as("register_date", "تاريخ التسجيل");
		$crud->display_as("email", "الايميل");
		$crud->callback_before_insert(array($this,'encrypt_password_callback'));
		$crud->unset_columns("reset_code","is_new","android_token", "activation_code", "password", "created_date", "firebase_token");
		$crud->unset_edit_fields("reset_code","is_new","android_token", "activation_code", "password", "created_date", "firebase_token");
		$crud->unset_add_fields("reset_code","is_new","android_token", "activation_code", "created_date", "firebase_token");
		//$crud->unset_add();
		
		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";
		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}
	function manageBuses()
	{
		$this->data['customer_active'] = true;
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		$this->data['title'] = "إدارة  الباصات";
		$crud->set_table('customer');
		$crud->where("customer.type","bus");
		$crud->set_relation("related_id","customer","{name}-{mobile}","type = 'school'");
		$crud->add_action('إدارة الخطوط', '', '','ui-icon-image',array($this,'bus_line_action'));
		$crud->order_by('id', 'desc');
		$crud->set_field_upload('image', 'uploads');
		$crud->set_language("arabic");
		$crud->display_as("name", "الاسم");
		$crud->display_as("mobile", "الموبايل");
		$crud->display_as("address", "العنوان");
		$crud->display_as("type","نوع الحساب");
		$crud->display_as("related_id","المدرسة التابع لها");
		$crud->display_as("nationalImage","صورة الهوية");
		$crud->set_field_upload('nationalImage', 'uploads');
		$crud->display_as("register_date", "تاريخ التسجيل");
		$crud->display_as("email", "الايميل");
		$crud->callback_before_insert(array($this,'encrypt_password_callback'));
		$crud->unset_columns("reset_code","is_new","android_token", "activation_code", "password", "created_date", "firebase_token");
		$crud->unset_edit_fields("stop_id","reset_code","is_new","android_token", "activation_code", "password", "created_date", "firebase_token");
		$crud->unset_add_fields("stop_id","reset_code","is_new","android_token", "activation_code", "created_date", "firebase_token");
		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";
		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}
	function bus_line_action($primary_key, $row)
	{
		return base_url('/dashboard/manageBusLine') . '/' . $row->id;
	}
	function manageBusLine($bus_id)
	{
		$this->data['customer_active'] = true;
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		$crud->set_language("arabic");
		$this->data['title'] = "إدارة خط باص";
		$crud->set_table('bus_line');
		$crud->add_action('إدارة المواقف', '', '','ui-icon-image',array($this,'bus_stop_action'));
		$crud->where("bus_id",$bus_id);
		$crud->field_type('bus_id', 'hidden', $bus_id);
		$crud->display_as("stop_name", "اسم الموقف");
		$crud->display_as("address", "عنوان الموقف");
		$crud->display_as("time_arrivals", "أوقات الوصول");
		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";
		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}
	function bus_stop_action($primary_key, $row)
	{
		return base_url('/dashboard/manageBusStop') . '/' . $row->id;
	}
	function manageBusStop($busline_id)
	{
		$this->data['customer_active'] = true;
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		$crud->set_language("arabic");
		$this->data['title'] = "إدارة مواقف باص";
		$crud->set_table('bus_stop');
		$crud->where("busline_id",$busline_id);
		$crud->field_type('busline_id', 'hidden', $busline_id);
		$crud->display_as("address", "عنوان الموقف");
		$crud->display_as("time_arrivals", "أوقات الوصول");
		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";
		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}

	
	function manageParents()
	{
		$this->data['customer_active'] = true;
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		$this->data['title'] = "إدارة  حسابات الأهل";
		$crud->set_table('customer');
		$crud->where("customer.type","parent");
		$crud->set_relation("related_id","customer","{name}-{mobile}","type = 'school'");
		$crud->set_relation("stop_id","bus_stop","{stop_name}-{address}");
		//$crud->add_action('View Coupons', '', '','ui-icon-image',array($this,'customer_coupon_action'));
		$crud->order_by('id', 'desc');
		$crud->set_field_upload('image', 'uploads');
		$crud->set_language("arabic");
		$crud->display_as("name", "الاسم");
		$crud->display_as("mobile", "الموبايل");
		$crud->display_as("address", "العنوان");
		$crud->display_as("type","نوع الحساب");
		$crud->display_as("related_id","المدرسة التابع لها");
		$crud->display_as("nationalImage","صورة الهوية");
		$crud->set_field_upload('nationalImage', 'uploads');
		$crud->display_as("register_date", "تاريخ التسجيل");
		$crud->callback_before_insert(array($this,'encrypt_password_callback'));
		$crud->display_as("email", "الايميل");
		$crud->unset_columns("reset_code","is_new","android_token", "activation_code", "password", "created_date", "firebase_token");
		$crud->unset_edit_fields("reset_code","is_new","android_token", "activation_code", "password", "created_date", "firebase_token");
		$crud->unset_add_fields("reset_code","is_new","android_token", "activation_code", "created_date", "firebase_token");
		//$crud->unset_add();
		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";
		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}
	
	function manageUnActiveCustomers()
	{
		$this->data['customer_active'] = true;
		$this->data["customer_active"] = true;
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		$this->data['title'] = "إدارة الزبائن الغير مفعلين";
		$crud->set_language("arabic");
		$crud->set_table('customer');
		$crud->where("is_active", 0);
		$crud->display_as("name", "الاسم");
		$crud->display_as("mobile", "الموبايل");
		$crud->display_as("city", "المدينة");
		$crud->display_as("address", "العنوان");
		$crud->display_as("register_date", "تاريخ التسجيل");
		$crud->display_as("email", "الايميل");
		$crud->unset_columns("reset_code","is_new", "activation_code", "password", "created_date", "firebase_token", "is_active");
		$crud->unset_fields("reset_code","is_new", "activation_code", "password", "created_date", "firebase_token");
		$crud->add_action('الإعلانات', '', '', 'ui-icon-image', array($this, 'customer_products'));
		$crud->set_field_upload('image', 'uploads');
		//$crud->add_action('تفعيل', '', '','ui-icon-image',array($this,'active_user'));
		$crud->order_by('id', 'desc');
		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";
		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}


	function encrypt_password_callback($post_array, $primary_key = null)
	{
		$this->load->library("auth");
		$this->load->model("login_model");
		$post_array['password'] = $this->login_model->hash($post_array['password']);
		$post_array['is_active'] = 1;
		return $post_array;
	}
	function encrypt_password_callback_with_insert_user($post_array, $primary_key = null)
	{
			$this->load->library("auth");
             $username = $post_array['email'];
            $password =  $post_array['password'];
            $email = $post_array['email'];
            $additional_data = array(
                        'first_name' => $post_array['name'],
                        'last_name' => $post_array['name'],
                        'refrence_id' => $primary_key,
                        'phone' =>$post_array['mobile']
                        );
            $group = array('2'); // Sets user to admin.
        
            $this->ion_auth->register($username, $password, $email, $additional_data, $group);
		return $post_array;
	}
	function reset_password_action($primary_key, $row)
	{
		return base_url('/dashboard/reset_password') . '/' . $row->id . "/replace";
	}

	function reset_password($agency_id)
	{
		$this->load->library("auth");
		$this->load->model("login_model");
		$pass = $this->login_model->hash("12345678");
		$this->db->where("id", $agency_id)->update("agency", array("password" => $pass));
		redirect($_SERVER['HTTP_REFERER'] . '/success/1', 'refresh');
	}
	function activate($agency_id)
	{
		$this->db->where("id", $agency_id);
		$this->db->update("agency", array("is_active" => 1));
		redirect($_SERVER['HTTP_REFERER']);
	}
	function deactivate($agency_id)
	{
		$this->db->where("id", $agency_id);
		$this->db->update("agency", array("is_active" => 0));
		redirect($_SERVER['HTTP_REFERER']);
	}

	function page404()
	{
		$this->data['main_content'] = "404";
		$this->load->view('includes/main', $this->data);
	}



	function manageConfig()
	{
		$this->data["title"] = lang("manage_config");
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		$crud->set_table('config');
		$crud->field_type('key', 'readonly');
		$crud->unset_add();
		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";

		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}



	public function sendNotification($message, $user_id, $order_id = null, $allow_insert = true)
	{
		//var_dump($user_id);
		$user = $this->db->where("id", $user_id)->get("customer")->result_array()[0];
		if ($allow_insert)
			$this->db->insert("notifications", array("customer_id" => $user_id, "notification_msg" => $message));
		$android_token = $user["android_token"];
		// var_dump(API_ACCESS_KEY);
		// $ios_token = $user["ios_token"];
		$msg = array(
			'body'  => strip_tags($message),
			'title'     => "One-Deals",
		);
		$notification = array(
			'body'  => strip_tags($message),
			'title'     => "One-Deals",
			"click_action" => ''
		);
		if ($order_id) {
			$msg["order_id"] = $order_id;
		}
		$fields = array(
			//'to'  =>$android_token,
			"registration_ids" => [$android_token],
			'data'          => $msg,
			'notification' => $notification,
			'priority' => 'high'
		);



		$headers = array(
			'Authorization: key=' . API_ACCESS_KEY,
			'Content-Type: application/json'
		);
		//echo API_ACCESS_KEY;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		$result = curl_exec($ch);
		//echo($result);

		if ($result === false) {
			echo 'Curl error: ' . curl_error($ch);
		} else {
			//echo 'Operation completed without any errors';
		}
		if ($errno = curl_errno($ch)) {
			$error_message = curl_strerror($errno);
			echo "cURL error ({$errno}):\n {$error_message}";
		}
		curl_close($ch);
		//die();
		/*
		    TODO exho result
		*/
		//echo $result;

	}

	public function sendNotification_all($message, $topic = "all")
	{
		//var_dump($user_id);


		$msg = array(
			'body'  => strip_tags($message),
			'title'     => "Dllaal-app",
		);

		$notification = array(
			'body'  => strip_tags($message),
			'title'     => "Dllaal-app",
			"click_action" => ''
		);
		$fields = array(
			'to' 	=> '/topics/' . $topic,
			'data'			=> $msg,
			'notification' => $notification,
		);


		$headers = array(
			'Authorization: key=' . API_ACCESS_KEY,
			'Content-Type: application/json'
		);
		//echo API_ACCESS_KEY;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		$result = curl_exec($ch);
		//	echo($result);

		if ($result === false) {
			echo 'Curl error: ' . curl_error($ch);
		} else {
			//echo 'Operation completed without any errors';
		}
		if ($errno = curl_errno($ch)) {
			$error_message = curl_strerror($errno);
			echo "cURL error ({$errno}):\n {$error_message}";
		}
		curl_close($ch);
		//		die();
		/*
		    TODO exho result
		*/
		//	echo $result;

	}


	function manageActiveCustomers()
	{
		$this->data['customer_active'] = true;
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		$this->data['title'] = "إدارة الزبائن المفعلين";
		$crud->set_table('customer');
		$crud->set_language("arabic");
		$crud->set_field_upload('image', 'uploads');
		$crud->add_action('الإعلانات', '', '', 'ui-icon-image', array($this, 'customer_products'));
		$crud->add_action('الاشعارات', '', '', 'ui-icon-image', array($this, 'customer_notifications'));
		//	$crud->set_relation('category_id','category','category_name');
		$crud->where("is_active", 1);
		$crud->display_as("name", "الاسم");
		$crud->display_as("mobile", "الموبايل");
		$crud->display_as("city", "المدينة");
		$crud->display_as("address", "العنوان");
		$crud->display_as("register_date", "تاريخ التسجيل");
		$crud->display_as("email", "الايميل");
		$crud->unset_columns("reset_code","is_new", "activation_code", "password", "created_date", "firebase_token", "is_active");
		$crud->unset_fields("reset_code","is_new", "activation_code", "password", "created_date", "firebase_token");
		$crud->order_by('id', 'desc');
		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";
		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}


	function customer_products($primary_key, $row)
	{
		return base_url('/dashboard/store_products') . '/' . $row->id;
	}
	function customer_notifications($primary_key, $row)
	{
		return base_url('/dashboard/manage_customer_notifications') . '/' . $row->id;
	}


	function customerorders_notifications($primary_key, $row)
	{
		return base_url('/dashboard/manage_customer_notifications') . '/' . $row->customer_id;
	}

	function manage_customer_notifications($user_id)
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		$this->data['title'] = "Customer Notification";
		$crud->set_table('notifications');
		$crud->order_by('id', 'desc');
		$crud->where("customer_id", $user_id);
		$crud->field_type('customer_id', 'hidden', $user_id);
		$crud->fields('customer_id', "notification_msg");
		$crud->callback_after_insert(array($this, 'sendNotification_user'));
		//$crud->callback_after_update(array($this, 'sendNotification_user'));

		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";
		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}
	function sendNotification_user($post_array, $primary_key)
	{
		//print_r($post_array);
		$this->sendNotification($post_array["notification_msg"], $post_array["customer_id"], null, false);
		return true;
	}



	function manage_general_notifications()
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		$this->data['title'] = "General Notifications";
		$crud->set_table('general_notifications');
		$crud->order_by('id', 'desc');

		$crud->fields("message");
		$crud->callback_after_insert(array($this, 'sendNotification_alluser'));
		//$crud->callback_after_update(array($this, 'sendNotification_user'));

		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";
		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}
	function sendNotification_alluser($post_array, $primary_key)
	{
		//print_r($post_array);
		$this->sendNotification_all($post_array["message"]);
		return true;
	}

	function sendAcivateNotification($post_array, $primary_key)
	{
		//print_r($post_array);
		$this->sendNotification("لقد تم تفعيل حسابكم..يرجى استكمال المعلومات الخاصة بكم", $primary_key);

		return true;
	}


	function album_products($primary_key, $row)
	{
		return base_url('/dashboard/manage_album_products') . '/' . $row->id;
	}

	function manage_album_products($album_id)
	{

		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
		$this->data['title'] = "product gallery management";
		$crud->set_table('kt_ms_art');
		$crud->columns('ArticleCodeId', 'ArticleName1Id', 'Color', 'Size', 'Remarks');
		$crud->fields('ArticleCodeId', 'ArticleName1Id', 'Color', 'Size', 'Remarks');
		$crud->where("Remarks", $album_id);
		$crud->set_relation('Remarks', 'album', 'name');
		$crud->unset_add();
		//$crud->set_relation('city_id','cities','name');
		$crud->unset_columns(array('ip', 'password'));
		$crud->order_by('id', 'desc');
		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";
		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}
	function setAlbum($post_array, $primary_key)
	{
		$this->db->where("ArticleCodeId Between '" . $post_array["from_id"] . "' and '" . $post_array["to_id"] . "'")->update("kt_ms_art", array("Remarks" => $primary_key));

		return true;
	}


	function album($primary_key, $row)
	{
		return base_url('/dashboard/manage_album') . '/' . $row->id;
	}

	function manage_album($product_id)
	{
		$this->load->library('image_CRUD');
		$image_crud = new image_CRUD();
		$this->data['title'] = "إدارة الألبوم";
		$image_crud->set_table('images');

		$this->data['is_images'] = true;
		$image_crud->set_url_field('image');

		$image_crud->set_relation_field('product_id')
			->set_image_path('uploads');

		$output = $image_crud->render();
		// var_dump($output);die();
		$this->data['output'] = $output;
		$data['main_content'] = 'management';
		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}

	function add_user_product()
	{
		$data = (array) $this->input->post();
		$data["user_id"] = $this->ion_auth->get_user_id();
		//var_dump($data);
		$this->api_model->add_user_product($data);
		redirect($_SERVER['HTTP_REFERER']);
	}
	function edit_user_product()
	{
		$data = (array) $this->input->post();
		$data["user_id"] = $this->ion_auth->get_user_id();
		//var_dump($data);
		$this->api_model->update_user_product($data);
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function manage_users()
	{
		//var_dump($this->auth->user());die();
		if (!$this->ion_auth->is_admin()) {
			redirect(base_url() . "dashboard/manage_connection");
		}
		$this->data['title'] = "إدارة المستخدمين";
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		$crud->set_table('users');
		$crud->set_subject('User');
		$crud->required_fields('username');

		$crud->columns('username', 'first_name', 'last_name', 'active');
		$crud->fields('username', 'first_name', 'password', 'last_name', 'active');
		$crud->change_field_type('password', 'password');
		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = 'management';
		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}



	function decrypt_password_callback($value)
	{
		$this->load->library('encrypt');
		$key = 'bcb04b4a8b6a0cffe54763945cef08bc88abe000fdebae5e1d417e2ffb2a12a3';
		$decrypted_password = $this->encrypt->decode($value, $key);
		//var_dump($decrypted_password);die();
		return "<input type='password' name='password' value='$decrypted_password' />";
	}
	function compress_callback_after_upload($uploader_response, $field_info, $files_to_upload)
	{
		$this->load->library('image_moo');
		//var_dump($field_info->upload_path);die();
		//Is only one file uploaded so it ok to use it with $uploader_response[0].
		$file_uploaded = $field_info->upload_path . '/' . $uploader_response[0]->name;
		$target = $field_info->upload_path . '/thumb/' . $uploader_response[0]->name;
		$this->image_moo->load($file_uploaded)->resize(250, 250)->save($target, true);

		//$this->image_moo->load($file_uploaded)->resize(250,250)->save($target,true);

		return true;
	}



	function import_callback_after_upload($uploader_response, $field_info, $files_to_upload)
	{
		$file = fopen($field_info->upload_path . '/' . $uploader_response[0]->name, "r");
		$i = 0;
		while (!feof($file)) {
			//print_r(fgetcsv($file));
			$arr = fgetcsv($file);
			$data[$i]["number"] = $arr[0];
			$data[$i]["card_value"] = $arr[1];
			$i++;
		}
		$this->db->insert_batch("card", $data);
		fclose($file);

		//$this->image_moo->load($file_uploaded)->resize(250,250)->save($target,true);

		return true;
	}


	public static function convert_to_utf8_recursively($dat)
	{
		if (is_string($dat)) {
			return iconv('WINDOWS-1256', 'UTF-8', $dat);
		} elseif (is_array($dat)) {
			$ret = [];
			foreach ($dat as $i => $d) $ret[$i] = self::convert_to_utf8_recursively($d);

			return $ret;
		} elseif (is_object($dat)) {
			foreach ($dat as $i => $d) $dat->$i = self::convert_to_utf8_recursively($d);

			return $dat;
		} else {
			return $dat;
		}
	}
}
