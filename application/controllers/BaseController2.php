<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/REST_Controller.php');
 
class BaseController2 extends REST_Controller {

	public  function __construct()
    {
        parent::__construct();

        // if(strtotime("2019/04/24") <= time())
        //     die();
        $this->load->model('Auth_model2');
        $headers = $this->input->request_headers();
        $this->current_user = null; 
        //var_dump($headers["Authorization"]);
        //authorization : for angular
        //Authorization : for test from postman
        //var_dump(apache_request_headers());
        //var_dump("authorization = ".$headers["authorization"]);
        if (isset($headers["Authorization"])  || isset($headers["authorization"])){
            $result = array();
            if (isset($headers["Authorization"])){
                $result = $this->Auth_model2->checkToken($headers["Authorization"]);
            }else {
                $result = $this->Auth_model2->checkToken($headers["authorization"]);
            }
        	if (count($result) > 0 ){
                $this->current_user =$result[0];
        	}else {
        		$toSend =  array('status' => false , "ErrorCode"=>401);//not logged in
           		// $this->response($toSend,200);
        	}
        }else if (isset($headers["Token"])  || isset($headers["token"])){
            $result = array();
            if (isset($headers["Token"])){
                $result = $this->Auth_model2->checkToken($headers["Token"]);
            }else {
                $result = $this->Auth_model2->checkToken($headers["token"]);
            }
        	if (count($result) > 0 ){
                $this->current_user =$result[0];
        	}else {
        		$toSend =  array('status' => false , "ErrorCode"=>401);//not logged in
           		$this->response(array(),401);
        	}
        }else {
        	$toSend =  array('status' => false , "ErrorCode"=>401);//not logged in
           // $this->response($toSend,200);
        }
    } 
}