<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, PUT, POST, OPTIONS');
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type");
header("Access-Control-Allow-Methods: GET, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
if ("OPTIONS" === $_SERVER['REQUEST_METHOD']) {
    die();
}
// header('Access-Control-Allow-Headers: Special-Request-Header');
// header('Access-Control-Allow-Credentials: true');
// header('Access-Control-Max-Age: 240');

// if (isset($_SERVER['HTTP_ORIGIN'])) {
//     // should do a check here to match $_SERVER['HTTP_ORIGIN'] to a
//     // whitelist of safe domains
//     header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
//     header('Access-Control-Allow-Credentials: true');
//     header('Access-Control-Max-Age: 86400');    // cache for 1 day
// }
// // Access-Control headers are received during OPTIONS requests
// if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

//     if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
//         header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

//     if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
//         header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

// }
ini_set('max_execution_time', 6000);
defined('BASEPATH') or exit('No direct script access allowed');
define('API_ACCESS_KEY', 'AAAAaC8DVYk:APA91bHtSzJffQPKEhCuOZCk79IgAZV1WRQ9T2dPMy_QcfYOtjAb68G39kOq0nJA06kdX7D0flaqPt-RIOF3zvE18iPl3R6NB104apudBc6Q98uUz1-Mt4c586ANi44bZwYNymc6rWaq'); //AIzaSyBLV2ZQQFHy0lR7T-LUFOl-CyoWFiHe9q044444444444444444444444444444444444444444444444
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require(APPPATH . 'controllers/BaseController.php');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class DashboardApi extends BaseController
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->helper("message");
        $this->load->library("pagination");
        $this->load->model('dashboard_model');
        $this->load->model('auth_model');
        $this->load->library('ion_auth');
        $this->load->model("login_model");

    }


    // function locationByDeputy_get()
    // {
    // }
    // function schoolsByLocation_get()
    // {
    // }
    // function busBySchool_get()
    // {
    // }


    function getTokenFromHeader()
    {
        header("Access-Control-Allow-Methods: GET, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
        if ("OPTIONS" === $_SERVER['REQUEST_METHOD']) {
            die();
        }
        $headers = getallheaders();


        if (isset($headers['Authorization'])) {
            $authorizationHeader = $headers['Authorization'];

            // فصل نوع التوكن عن القيمة
            list($tokenType, $tokenValue) = explode(' ', $authorizationHeader);

            // التحقق من نوع التوكن (Bearer)
            if ($tokenType === 'Bearer') {
                // إرجاع قيمة التوكن
                return $tokenValue;
            }
        }
    }
    function allUser_get()
    {
        $token = $this->getInputData('token');
        // $token = $this->getTokenFromHeader();
        $checkUser = $this->dashboard_model->checkToken($token);
        if (!$checkUser) {
            // التوكن غير متطابق أو غير موجود، يمكنك اتخاذ الإجراء المناسب هنا
            $error = array(
                'status' => false,
                'message' => 'التوكن غير صالح.'
            );
            $this->response($error, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
        // $data = $this->api_model->getAllUsers();
        $data['users'] = $this->ion_auth->users()->result();
        $this->response($data, 200);
    }
    function allUserGroup_get()
    {
        $users = $this->ion_auth->users()->result();
        $data['users'] = [];

        foreach ($users as $user) {
            $groups = $this->ion_auth->get_users_groups($user->id)->result();

            $group = array_column($groups, 'id');
            $user->groups = implode(',', $group);
            $data['users'][] = $user;
            // $data['users']['group'] = $group;
        }

        $this->response($data, 200);
    }
    function createAccount_post()
    {
        $token = $this->getInputData('token');
        // $token = $this->getTokenFromHeader();
        $checkUser = $this->dashboard_model->checkToken($token);
        if (!$checkUser) {
            // التوكن غير متطابق أو غير موجود، يمكنك اتخاذ الإجراء المناسب هنا
            $error = array(
                'status' => false,
                'message' => 'التوكن غير صالح.'
            );
            $this->response($error, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }


        if ($checkUser->group_name != 'admin') {
            if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
                // redirect('auth', 'refresh');

                $error = array(
                    'status' => false,
                    'message' => $this->ion_auth->errors(),
                    'message' => 'لا يمكنك انشاء مستخدم لأنك لست مدير أو لأنك لست مسجل '
                );
                $this->response($error, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            }
        }

        $username = $this->getInputData('username', true);
        $email = $this->getInputData('email', true);
        $password = $this->getInputData('password', true);
        $group = $this->getInputData('group', true);

        $first_name = $this->getInputData('first_name');
        $last_name = $this->getInputData('last_name');
        $company = $this->getInputData('company');
        $phone = $this->getInputData('phone');
        // إنشاء الحساب باستخدام مكتبة ion_auth
        $additional_data = array(
            'first_name' => $first_name,
            'last_name' => $last_name,
            'company' => $company,
            'phone' => $phone,

            // يمكنك إضافة معلومات إضافية أخرى هنا
        );
        // معرّف المجموعة التي ينتمي إليها المستخدم

        $result = $this->dashboard_model->register($username, $password, $email, $additional_data, $group);

        if ($result['status'] == 'Success') {
            // تم إنشاء الحساب بنجاح
            $message = array(
                'status' => true,
                'message' => 'Account created successfully.'
            );
            $this->response($message, REST_Controller::HTTP_OK);
        } else {
            // فشل في إنشاء الحساب
            $error = array(
                'status' => false,
                'message' => $result['message'],
                'ayham' => 'hi yasin'
            );
            $this->response($error, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function updateAccount_post()
    {
        $token = $this->getInputData('token');
        // $token = $this->getTokenFromHeader();
        $checkUser = $this->dashboard_model->checkToken($token);
        if (!$checkUser) {
            // التوكن غير متطابق أو غير موجود، يمكنك اتخاذ الإجراء المناسب هنا
            $error = array(
                'status' => false,
                'message' => 'التوكن غير صالح.'
            );
            $this->response($error, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }

        $user_id = $this->getInputData('user_id', true); // تلقائيًا مطلوبة

        $update_data = array(
            'username' => $this->getInputData('username'),
            'password' => $this->getInputData('password'),
            'email' => $this->getInputData('email'),
            'first_name' => $this->getInputData('first_name'),
            'last_name' => $this->getInputData('last_name'),
            'company' => $this->getInputData('company'),
            'phone' => $this->getInputData('phone'),
            'active' => $this->getInputData('active'),
            // يمكنك إضافة حقول إضافية هنا
        );

        if (is_null($update_data['active'])) {
            $update_data['active'] = 1;
        }
        $result = $this->dashboard_model->updateUser($user_id, $update_data);

        if ($result['status'] == 'Success') {
            // تم تحديث بيانات المستخدم بنجاح
            $message = array(
                'status' => true,
                'message' => 'User updated successfully.'
            );
            $this->response($message, REST_Controller::HTTP_OK);
        } else {
            // فشل في تحديث بيانات المستخدم
            $error = array(
                'status' => false,
                'message' => $result['message']
            );
            $this->response($error, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    public function deleteAccount_post()
    {
        $token = $this->getInputData('token');

        // $token = $this->getTokenFromHeader();
        $checkUser = $this->dashboard_model->checkToken($token);
        if (!$checkUser) {
            // التوكن غير متطابق أو غير موجود، يمكنك اتخاذ الإجراء المناسب هنا
            $error = array(
                'status' => false,
                'message' => 'التوكن غير صالح.',
                'ahmaad kayaliii' => $checkUser,
                'token' => $token
            );
            $this->response($error, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }

        $user_id = $this->getInputData('user_id', true); // تلقائيًا مطلوب
        $result = $this->dashboard_model->deleteUser($user_id);

        if ($result['status'] == 'Success') {
            // تم حذف المستخدم بنجاح
            $message = array(
                'status' => true,
                'message' => 'User deleted successfully.'
            );
            $this->response($message, REST_Controller::HTTP_OK);
        } else {
            // فشل في حذف المستخدم
            $error = array(
                'status' => false,
                'message' => $result['message']
            );
            $this->response($error, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }


    function generateToken($userId)
    {
        $token = bin2hex(random_bytes(32));

        return $token;
    }
    // function login_option(){
    //     header("Access-Control-Allow-Origin: *");
    //     header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    //     header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

    //     // إرجاع استجابة فارغة
    //     http_response_code(204);
    //     exit;
    // }


    function login_post()
    {
        $identity = $this->input->post('username');
        $password = $this->input->post('password');
        $remember = (bool) $this->input->post('remember');

        if (empty($identity) || empty($password)) {
            $requestBody = $this->input->raw_input_stream;
            $data = json_decode($requestBody, true); // تحويل النص إلى صيغة البيانات المطلوبة
            $identity = $data['username'];
            $password = $data['password'];
            $remember = isset($data['remember']) ? (bool) $data['remember'] : false;
        }
        $user = $this->ion_auth->where('username', $identity)->users()->row();
        if ($user && $user->active == 0) {
            $error = array(
                'status' => false,
                'message' => 'Account is not active. Please contact the administrator.',
                'username' => $identity
            );
            $this->response($error, REST_Controller::HTTP_UNAUTHORIZED);
        }

        if ($this->ion_auth->login($identity, $password, $remember)) {
            // تم تسجيل الدخول بنجاح
            $user = $this->ion_auth->user()->row();
            $groups = $this->ion_auth->get_users_groups($user->id)->result();

            $permissions = array();
            foreach ($groups as $group) {
                $permissions = array();

                if ($group->name === 'admin') {
                    $permissions = array(
                        'role' => 'admin',
                        'cities' => 'All'
                    );
                } elseif ($group->name === 'agent') {
                    $agentCities = $this->dashboard_model->getAgentCities($user->id); // استرداد المدن التي يعمل فيها الوكيل
                    // $permissions='agent';
                    $permissions = array(
                        'role' => 'agent',
                        'cities' => $agentCities
                    );

                } elseif ($group->name === 'school') {
                    $permissions = array(
                        'role' => 'school',
                        'cities' => ''
                    );
                }
            }

            // قم بتوليد توكن جديدة
            $token = $this->generateToken($user->id);

            // قم بتخزين التوكن في جدول المستخدمين
            $this->dashboard_model->storeToken($user->id, $token);

            $message = array(
                'status' => true,
                'message' => 'Logged in successfully.',
                'user_id' => $user->id,
                'username' => $user->username,
                'permissions' => $permissions,
                'token' => $token
            );
            $this->response($message, REST_Controller::HTTP_OK);
        } else {
            // فشل في تسجيل الدخول
            $error = array(
                'status' => false,
                'message' => $this->ion_auth->errors(),
                'this data you send' => ['$identity', $identity]
            );
            $this->response($error, REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
    function logout_post()
    {
        $user_id = $this->ion_auth->get_user_id();

        $this->dashboard_model->storeToken($user_id, null);
        $this->ion_auth->logout();
        $message = array(
            'status' => true,
            'message' => 'Logged out successfully.'
        );
        $this->response($message, REST_Controller::HTTP_OK);
    }
    function getAllCities_get()
    {

        $cities = $this->dashboard_model->getAllCities();

        if (!empty($cities)) {
            $message = array(
                'status' => true,
                'message' => 'تمت استرداد المدن بنجاح.',
                'cities' => $cities
            );
            $this->response($message, REST_Controller::HTTP_OK);
        } else {
            $message = array(
                'status' => false,
                'message' => 'لا توجد مدن متاحة.'
            );
            $this->response($message, REST_Controller::HTTP_NOT_FOUND);
        }
    }
    function addCities_post()
    {
        // استقبال المدن من الطلب الوارد
        $cities = $this->input->post('cities');

        // التأكد من وجود مصفوفة المدن
        if (!empty($cities) && is_array($cities)) {
            // إذا تم إرسال المدن كـ array عبر الـ POST فلا حاجة لتحويل البيانات
            $cities = $cities;
        } else {
            // استقبال البيانات كـ raw input stream وتحويلها إلى صيغة البيانات المطلوبة
            $requestBody = $this->input->raw_input_stream;
            $data = json_decode($requestBody, true);

            // استخراج المدن من البيانات
            $cities = isset($data['cities']) ? $data['cities'] : [];
        }


        $successCities = array();
        $failedCities = array();

        if (!empty($cities) && is_array($cities)) {
            foreach ($cities as $city) {
                $result = $this->dashboard_model->addCity($city);
                if ($result) {
                    $successCities[] = $city;
                } else {
                    $failedCities[] = $city;
                }
            }
        }

        $response = array(
            'success_cities' => $successCities,
            'failed_cities' => $failedCities
        );

        $this->response($response, 200);

    }

    public function editCity_put()
    {
        // check for city is find 
        if ($this->input->post('city_id') && $this->input->post('city_name_arabic') && $this->input->post('city_name_english')) {
            // استخدم البيانات المستلمة عبر $this->input->post()
            $cityId = $this->input->post('city_id');
            $cityNameArabic = $this->input->post('city_name_arabic');
            $cityNameEnglish = $this->input->post('city_name_english');
        } else {
            // استخدم البيانات المستلمة بالطريقة الأخرى ($this->input->raw_input_stream)
            $requestBody = $this->input->raw_input_stream;
            $data = json_decode($requestBody, true); // تحويل النص إلى صيغة البيانات المطلوبة

            $cityId = isset($data['city_id']) ? $data['city_id'] : null;
            $cityNameArabic = isset($data['city_name_arabic']) ? $data['city_name_arabic'] : null;
            $cityNameEnglish = isset($data['city_name_english']) ? $data['city_name_english'] : null;
        }

        $result = $this->dashboard_model->updateCity($cityId, $cityNameArabic, $cityNameEnglish);

        if ($result) {
            $response = array(
                'status' => 'success',
                'message' => 'City updated successfully.'
            );
        } else {
            $response = array(
                'status' => 'error',
                'message' => 'Failed to update city.'
            );
        }

        $this->response($response, 200);
    }


    function addCitiesToUser_post()
    {
        $token = $this->getInputData('token');
        // $token = $this->getTokenFromHeader();
        $checkUser = $this->dashboard_model->checkToken($token);
        if (!$checkUser) {
            // التوكن غير متطابق أو غير موجود، يمكنك اتخاذ الإجراء المناسب هنا
            $error = array(
                'status' => false,
                'message' => 'التوكن غير صالح.'
            );
            $this->response($error, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }

        // $isAuthorized = $this->dashboard_model->checkAdminPermission();

        if ($checkUser->group_name == 'admin') {
            $user_id = $this->input->post('user_id');
            $cities = $this->input->post('cities');

            // if (empty($user_id) || empty($cities)) {
            //     $requestBody = $this->input->raw_input_stream;
            //     $data = json_decode($requestBody, true); // تحويل النص إلى صيغة البيانات المطلوبة
            //     $user_id = $data['user_id'];
            //     $cities = $data['cities'];
            // }

            if (empty($user_id) || empty($cities)) {
                $requestBody = $this->input->raw_input_stream;
                $data = json_decode($requestBody, true);
                if (isset($data['user_id'])) {
                    $user_id = $data['user_id'];
                } else {
                    $error = array(
                        'status' => false,
                        'message' => 'فشل في إضافة المدن. لم تختر الوكيل '
                    );
                    $this->response($error, REST_Controller::HTTP_BAD_REQUEST);
                }
                if (isset($data['cities'])) {
                    $cities = $data['cities'];
                } else {
                    $error = array(
                        'status' => false,
                        'message' => 'فشل في إضافة المدن. لم تختر مدن'
                    );
                    $this->response($error, REST_Controller::HTTP_BAD_REQUEST);
                }
            }
            $successCount = 0;
            $errorCount = 0;
            // $error = array(
            //     'status' => false,
            //     'message' => 'هيك عم تصل المددن',
            //     'data'=>$cities
            // );
            // $this->response($error, REST_Controller::HTTP_BAD_REQUEST);
            if (!is_array($cities)) {
                $cities = array($cities);
            }
            foreach ($cities as $city_id) {
                $isAdded = $this->dashboard_model->addCitiesToUser($user_id, $city_id);

                if ($isAdded) {
                    $successCount++;
                } else {
                    $errorCount++;
                }
            }

            if ($successCount > 0) {
                $message = array(
                    'status' => true,
                    'message' => 'تمت إضافة ' . $successCount . ' مدينة بنجاح.'
                );
                $this->response($message, REST_Controller::HTTP_OK);
            } else {
                $error = array(
                    'status' => false,
                    'message' => 'فشل في إضافة المدن.'
                );
                $this->response($error, REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            $error = array(
                'status' => false,
                'message' => 'غير مسموح لك بإضافة المدن.'
            );
            $this->response($error, REST_Controller::HTTP_UNAUTHORIZED);
        }
    }


    public function editCitiesForUser_put()
    {
        $token = $this->getInputData('token');
        // $token = $this->getTokenFromHeader();
        $checkUser = $this->dashboard_model->checkToken($token);
        if (!$checkUser) {
            // التوكن غير متطابق أو غير موجود، يمكنك اتخاذ الإجراء المناسب هنا
            $error = array(
                'status' => false,
                'message' => 'التوكن غير صالح.'
            );
            $this->response($error, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }

        // $isAuthorized = $this->dashboard_model->checkAdminPermission();

        if ($checkUser->group_name == 'admin') {
            $user_id = $this->input->post('user_id');
            if (empty($user_id)) {
                $requestBody = $this->input->raw_input_stream;
                $data = json_decode($requestBody, true);
                $user_id = $data['user_id'];
            }

            $cities = $this->input->post('cities');
            if (empty($cities)) {
                $requestBody = $this->input->raw_input_stream;
                $data = json_decode($requestBody, true);
                $cities = $data['cities'];
            }

            $successCount = $this->dashboard_model->updateCitiesForUser($user_id, $cities);

            if ($successCount > 0) {
                $message = array(
                    'status' => true,
                    'message' => 'تم تحديث ' . $successCount . ' مدينة بنجاح.'
                );
                $this->response($message, REST_Controller::HTTP_OK);
            } else {
                $error = array(
                    'status' => false,
                    'message' => 'فشل في تحديث المدن.'
                );
                $this->response($error, REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            $error = array(
                'status' => false,
                'message' => 'غير مسموح لك بتحديث المدن.'
            );
            $this->response($error, REST_Controller::HTTP_UNAUTHORIZED);
        }
    }

    // function getInputData($field)
    // {
    //     $value = $this->input->post($field);
    //     if (empty($value)) {
    //         $requestBody = $this->input->raw_input_stream;
    //         $data = json_decode($requestBody, true);
    //         if (isset($data[$field])) {
    //             $value = $data[$field];
    //         } else {
    //             $value = null; // تعيين القيمة إلى `null` في حالة عدم وجود الحقل في المصفوفة
    //         }
    //     }
    //     return $value;
    // }
    function getInputData($field, $isRequired = false)
    {
        $value = $this->input->post($field);

        if (empty($value)) {
            $value = $this->input->get($field);
        }

        if (empty($value)) {
            $requestBody = $this->input->raw_input_stream;
            $data = json_decode($requestBody, true);
            if (isset($data[$field])) {
                $value = $data[$field];
            } else {
                if ($isRequired) {
                    $message = 'يرجى إضافة العنصر: ' . $field;
                    $error = array(
                        'status' => false,
                        'message' => $message
                    );
                    $this->response($error, REST_Controller::HTTP_BAD_REQUEST);
                }
            }
        }

        return $value;
    }
    // بهذا التعديل، يمكنك تمرير true كقيمة ثانوية للمعامل $isRequired إذا كنت ترغب في جعل الحقل مطلوبًا. وإذا كانت القيمة null ولم يتم العثور على الحقل في المصفوفة $data وكانت القيمة $isRequired محددة على true، سيتم إرجاع رسالة الخطأ واستدعاء response() لإرسال رمز الاستجابة HTTP_BAD_REQUEST والرسالة إلى العميل.

    public function addSchool_post()
    {
        // $nationalImage = $_FILES['nationalImage'];

        $school_data = array(
            'name' => $this->getInputData('name'),
            'name_ar' => $this->getInputData('name_ar'),
            'name_en' => $this->getInputData('name_en'),
            'username' => $this->getInputData('username', true),
            'mobile' => $this->getInputData('mobile'),
            'password' => password_hash($this->getInputData('password', true), PASSWORD_DEFAULT),
            'email' => $this->getInputData('email', true),
            'address' => $this->getInputData('address'),
            'nationalImage' => $this->getInputData('nationalImage'),
            // 'is_active'=>$this->getInputData('is_active'),
            // 'stop_id' => $this->getInputData('stop_id'),
            // 'firebase_token' => $this->getInputData('firebase_token'),
            'city_id' => $this->getInputData('city_id', true),
            'type' => 'school'
        );

        $token = $this->getInputData('token');
        // $token = $this->getTokenFromHeader();
        $checkUser = $this->dashboard_model->checkToken($token);
        if (!$checkUser) {
            // التوكن غير متطابق أو غير موجود، يمكنك اتخاذ الإجراء المناسب هنا
            $error = array(
                'status' => false,
                'message' => 'التوكن غير صالح.'
            );
            $this->response($error, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }

        if ($checkUser->group_name != 'admin' && $checkUser->group_name != 'agent') {
            $error = array(
                'status' => false,
                'message' => 'غير مسموح لك بإضافة المدرسة.',
                'your user' => $checkUser->group_name
            );
            $this->response($error, REST_Controller::HTTP_UNAUTHORIZED);
        }

        // التحقق من صحة معرف المدينة المرتبطة بالوكيل
        if ($checkUser->group_name == 'agent') {
            $agent_city_ids = $this->dashboard_model->getAgentCityIds($checkUser->id);
            if (!in_array($school_data['city_id'], $agent_city_ids)) {
                $error = array(
                    'status' => false,
                    'message' => 'غير مسموح لك بإضافة مدرسة خارج المدن المسؤول عنها.'
                );
                $this->response($error, REST_Controller::HTTP_UNAUTHORIZED);
            }
        }

        $isAddedSchoolId = $this->dashboard_model->addSchool($school_data);

        if ($isAddedSchoolId) {
            $username = $school_data['username'];
            $email = $school_data['email'];
            $password = $this->getInputData('password');
            $group = 3; // مجموعة المدرسة
            $last_name = $school_data['name_ar'];
            $first_name = $school_data['name_en'];
            $additional_data = array(
                'first_name' => $first_name,
                'last_name' => $last_name,
                // 'is_active' => $school_data['is_active'] ? 1 : 0,
                // يمكنك إضافة معلومات إضافية أخرى هنا
            );
            // معرّف المجموعة التي ينتمي إليها المستخدم

            $user_id = $this->dashboard_model->register($username, $password, $email, $additional_data, $group);
            if ($user_id['status'] == 'Success') {
                $user_id = $user_id['user_id'];
                // تحصل هنا على قيمة user_id المعادة من الدالة
                // يمكنك استخدامها كما تحتاج في السياق اللاحق للكود
            } else {
                $user_id = false;
                // إدارة حالة فشل إنشاء الحساب هنا
            }
            // print_r($user_id->user_id);
            $isUpdated = $this->dashboard_model->updateCustomer($isAddedSchoolId, $user_id);
            if ($user_id !== false) {
                $message = array(
                    'status' => true,
                    'message' => 'تمت إضافة المدرسة بنجاح.'
                );
                $this->response($message, REST_Controller::HTTP_OK);
            } else {
                $error = array(
                    'status' => false,
                    'message' => 'فشل في إضافة المدرسة.'
                );
                $this->response($error, REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            $error = array(
                'status' => false,
                'message' => 'فشل في إضافة المدرسة.'
            );
            $this->response($error, REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    function addBusToSchool_post()
    {
        $token = $this->getInputData('token');
        // $token = $this->getTokenFromHeader();
        $checkUser = $this->dashboard_model->checkToken($token);
        if (!$checkUser) {
            // التوكن غير متطابق أو غير موجود، يمكنك اتخاذ الإجراء المناسب هنا
            $error = array(
                'status' => false,
                'message' => 'التوكن غير صالح.'
            );
            $this->response($error, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
        // $nationalImage = $_FILES['nationalImage'];

        $data = array(
            'name' => $this->getInputData('name'),
            'username' => $this->getInputData('username', true),
            'mobile' => $this->getInputData('mobile'),
            'password' => password_hash($this->getInputData('password', true), PASSWORD_DEFAULT),
            'email' => $this->getInputData('email', true),
            'address' => $this->getInputData('address'),
            'nationalImage' => $this->getInputData('nationalImage'),
            'type' => 'bus',
            'related_id' => $this->getInputData('school_id', true),
            'created_date' => date('Y-m-d H:i:s'),
            'is_active' => 1,
            'register_date' => date('Y-m-d H:i:s'),
            'is_new' => 1
        );
        $schoolCityId = $this->dashboard_model->getSchoolCityId($data['related_id']);

        // $user_id = $this->ion_auth->get_user_id();
        // // التحقق من صلاحيات المستخدم
        // $is_admin = $this->ion_auth->is_admin($user_id);
        // $is_agent = $this->ion_auth->in_group('agent', $user_id);
        // $is_school = $this->ion_auth->in_group('school', $user_id);



        if ($checkUser->group_name != 'admin' && $checkUser->group_name != 'agent' && $checkUser->group_name != 'school') {
            $error = array(
                'status' => false,
                'message' => 'غير مسموح لك بإضافة باص.'
            );
            $this->response($error, REST_Controller::HTTP_UNAUTHORIZED);
        }

        // التحقق من صحة معرف المدينة المرتبطة بالوكيل
        if ($checkUser->group_name == 'agent') {
            $agent_city_ids = $this->dashboard_model->getAgentCityIds($checkUser->id);
            if (!in_array($schoolCityId, $agent_city_ids)) {
                $error = array(
                    'status' => false,
                    'message' => 'غير مسموح لك بإضافة باص خارج المدن المسؤول عنها.'
                );
                $this->response($error, REST_Controller::HTTP_UNAUTHORIZED);
            }
        }
        if ($checkUser == "school") {
            // $loginSchool = $this->ion_auth->user()->row();
            $newSchoolEmail = $this->dashboard_model->getEmailById($data['related_id']);
            if ($checkUser->email !== $newSchoolEmail) {
                $error = array(
                    'status' => false,
                    'message' => 'غير مسموح لك بإضافة باص خارج المدارس المسؤول عنها.'
                );
            }
        }


        $yassin = $this->dashboard_model->addBusToCustomer($data);

        $message = array(
            'status' => true,
            'message' => 'تمت إضافة الباص بنجاح.',
            'data' => $yassin
        );
        $this->response($message, REST_Controller::HTTP_OK);
    }

    public function getAgentCities_get($user_id = 0)
    {
        $user_id = $this->getInputData('user_id', true);
        $data = $this->dashboard_model->getAgentCities($user_id);
        if (!empty($data)) {
            // تم العثور على مدن تابعة للوكيل
            $message = array(
                'status' => true,
                'message' => 'تم العثور على مدن تابعة للوكيل.',
                'data' => $data
            );
            $this->response($message, REST_Controller::HTTP_OK);
        } else {
            // لا يوجد مدن تابعة للوكيل
            $message = array(
                'status' => false,
                'message' => 'لا يوجد مدن تابعة لهذا الوكيل.',
                'user_id' => $user_id
            );
            $this->response($message, REST_Controller::HTTP_OK);
        }
    }


    public function deleteSchool_delete($school_id = 0)
    {
        // $parent_id = $this->getInputData('parent_id', true);

        $token = $this->getInputData('token');
        // $token = $this->getTokenFromHeader();
        // قم بتنفيذ التحقق من التوكن والتحقق من المستخدم هنا

        if ($school_id == 0) {
            $error = array(
                'status' => false,
                'message' => 'يرجى ارسال معرف المدرسة '
            );
            $this->response($error, REST_Controller::HTTP_BAD_REQUEST);
        }
        $isDeleted = $this->dashboard_model->deleteParent($school_id);

        if ($isDeleted) {
            $message = array(
                'status' => true,
                'message' => 'تم حذف المدرسة من جدول المدارس بنجاح.'
            );
            $this->response($message, REST_Controller::HTTP_OK);
        } else {
            $error = array(
                'status' => false,
                'message' => 'فشل في حذف المدرسة.'
            );
            $this->response($error, REST_Controller::HTTP_BAD_REQUEST);
        }
    }
    public function updateSchool_put()
    {
        $school_id = $this->getInputData('School_id', true);
        $school_data = array(
            'name' => $this->getInputData('name'),
            'username' => $this->getInputData('username'),
            'mobile' => $this->getInputData('mobile'),
            'password' => password_hash($this->getInputData('password', true), PASSWORD_DEFAULT),
            'email' => $this->getInputData('email', true),
            'address' => $this->getInputData('address'),
            'is_active' => $this->getInputData('is_active'),
            'nationalImage' => $this->getInputData('nationalImage'),
            'related_id' => $this->getInputData('related_id', true),
            'stop_id' => $this->getInputData('stop_id'),
            'city_id' => $this->getInputData('city_id', true),
            'type' => 'school'
        );

        $token = $this->getInputData('token');
        // $token = $this->getTokenFromHeader();
        // قم بتنفيذ التحقق من التوكن والتحقق من المستخدم هنا

        $isUpdated = $this->dashboard_model->updateParent($school_id, $school_data);

        if ($isUpdated) {
            $message = array(
                'status' => true,
                'message' => 'تم تحديث المدرسة بنجاح.',
                'data' => $school_data
            );
            $this->response($message, REST_Controller::HTTP_OK);
        } else {
            $error = array(
                'status' => false,
                'message' => 'فشل في تحديث المدرسة.'
            );
            $this->response($error, REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function updateBus_put()
    {
        $bus_id = $this->getInputData('bus_id', true);
        $bus_data = array(
            'name' => $this->getInputData('name'),
            'username' => $this->getInputData('username'),
            'mobile' => $this->getInputData('mobile'),
            'password' => password_hash($this->getInputData('password', true), PASSWORD_DEFAULT),
            'email' => $this->getInputData('email', true),
            'address' => $this->getInputData('address'),
            'is_active' => $this->getInputData('is_active'),
            'nationalImage' => $this->getInputData('nationalImage'),
            'related_id' => $this->getInputData('related_id', true),
            'stop_id' => $this->getInputData('stop_id'),
            'city_id' => $this->getInputData('city_id', true),
            'type' => 'bus'
        );

        $token = $this->getInputData('token');
        // $token = $this->getTokenFromHeader();
        // قم بتنفيذ التحقق من التوكن والتحقق من المستخدم هنا

        $isUpdated = $this->dashboard_model->updateParent($bus_id, $bus_data);

        if ($isUpdated) {
            $message = array(
                'status' => true,
                'message' => 'تم تحديث الباص بنجاح.',
                'data' => $bus_data
            );
            $this->response($message, REST_Controller::HTTP_OK);
        } else {
            $error = array(
                'status' => false,
                'message' => 'فشل في تحديث الباص.'
            );
            $this->response($error, REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function deleteBus_delete($bus_id = 0)
    {
        // $parent_id = $this->getInputData('parent_id', true);

        $token = $this->getInputData('token');
        // $token = $this->getTokenFromHeader();
        // قم بتنفيذ التحقق من التوكن والتحقق من المستخدم هنا

        if ($bus_id == 0) {
            $error = array(
                'status' => false,
                'message' => 'يرجى ارسال id الباص المراد حذفه'
            );
            $this->response($error, REST_Controller::HTTP_BAD_REQUEST);
        }
        $isDeleted = $this->dashboard_model->deleteParent($bus_id);

        if ($isDeleted) {
            $message = array(
                'status' => true,
                'message' => 'تم حذف الباص بنجاح.'
            );
            $this->response($message, REST_Controller::HTTP_OK);
        } else {
            $error = array(
                'status' => false,
                'message' => 'فشل في حذف الباص.'
            );
            $this->response($error, REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    // function getAllSchool_get()
    // {

    //     $token = $this->getInputData('token');
    //     // $token = $this->getTokenFromHeader();
    //     $checkUser = $this->dashboard_model->checkToken($token);

    //     if (!$checkUser) {
    //         // التوكن غير متطابق أو غير موجود، يمكنك اتخاذ الإجراء المناسب هنا
    //         $error = array(
    //             'status' => false,
    //             'message' => 'التوكن غير صالح.',
    //             'data' => $checkUser,
    //             'token' => $token
    //         );
    //         $this->response($error, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
    //     }

    //     $user_id = $checkUser->id;
    //     $is_admin = ($checkUser->group_name == 'admin') ? true : false;
    //     $is_agent = ($checkUser->group_name == 'agent') ? true : false;


    //     if ($is_admin) {
    //         // المستخدم هو المشرف، يرى جميع المدارس
    //         $schools = $this->db->where('type', 'school')->get('customer')->result();
    //     } elseif ($is_agent) {
    //         // المستخدم هو الوكيل، يرى المدارس المتاحة في المناطق التي يتولى مسؤوليتها
    //         $agent_city_ids = $this->dashboard_model->getAgentCityIds($user_id);
    //         $schools = $this->db->where('type', 'school')->where_in('city_id', $agent_city_ids)->get('customer')->result();
    //     } else {
    //         // المستخدم هو المدرسة، يرى مدرسته فقط
    //         $schools = $this->db->where('type', 'school')->where('user_id', $user_id)->get('customer')->result();
    //     }

    //     $this->response($schools, 200);
    // }
    function getAllSchool_get()
    {
        $token = $this->getInputData('token');
        // $token = $this->getTokenFromHeader();
        $checkUser = $this->dashboard_model->checkToken($token);

        if (!$checkUser) {
            // التوكن غير متطابق أو غير موجود، يمكنك اتخاذ الإجراء المناسب هنا
            $error = array(
                'status' => false,
                'message' => 'التوكن غير صالح.',
                'data' => $checkUser,
                'token' => $token
            );
            $this->response($error, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }

        $user_id = $checkUser->id;
       
        $is_admin = ($checkUser->group_name == 'admin') ? true : false;
        $is_agent = ($checkUser->group_name == 'agent') ? true : false;

        // Step 1: Execute the SQL query to get child counts for each school
        $sql = "SELECT c.name AS school_name, COUNT(ch.id) AS child_count
            FROM customer c
            LEFT JOIN customer p ON c.id = p.related_id AND p.type = 'parent'
            LEFT JOIN children ch ON p.id = ch.parent_id
            WHERE c.type = 'school'
            GROUP BY c.id, c.name";

        $query = $this->db->query($sql);
        $childCounts = $query->result();

        // Step 2: Merge child counts with school data
        if ($is_admin) {
            // المستخدم هو المشرف، يرى جميع المدارس
            $schools = $this->db->where('type', 'school')->get('customer')->result();
        } elseif ($is_agent) {
            // المستخدم هو الوكيل، يرى المدارس المتاحة في المناطق التي يتولى مسؤوليتها
            $agent_city_ids = $this->dashboard_model->getAgentCityIds($user_id);
            $schools = $this->db->where('type', 'school')->where_in('city_id', $agent_city_ids)->get('customer')->result();
        } else {
            // المستخدم هو المدرسة، يرى مدرسته فقط
            $schools = $this->db->where('type', 'school')->where('user_id', $user_id)->get('customer')->result();
        }

        // Merge child counts with school data
        foreach ($schools as &$school) {
            $school->child_count = 0; // Initialize child count to 0
            // Find the corresponding child count in the $childCounts array
            foreach ($childCounts as $childCount) {
                if ($childCount->school_name === $school->name) {
                    $school->child_count = $childCount->child_count; // Set the child count
                    break; // No need to continue searching
                }
            }
        }

        $this->response($schools, 200);
    }

    public function getBusesByschoolId_get($school_id)
    {
        if ($school_id === null) {
            $school_id = $this->input->get('school_id');
        }

        if ($school_id === null) {
            // لم يتم تحديد school_id
            $error = array(
                'status' => false,
                'message' => 'يرجى تحديد school_id.'
            );
            $this->response($error, REST_Controller::HTTP_BAD_REQUEST);
        }
        $buses = $this->db->where('type', 'bus')->where('related_id', $school_id)->get('customer')->result();

        $this->response($buses, 200);
    }


    public function addBusLine_post()
    {
        $title = $this->getInputData('title');
        $description = $this->getInputData('description');
        $bus_id = $this->getInputData('bus_id');

        if (empty($title) || empty($description) || empty($bus_id)) {
            $error = array(
                'status' => false,
                'message' => 'يرجى تعبئة جميع الحقول المطلوبة.'
            );
            $this->response($error, REST_Controller::HTTP_BAD_REQUEST);
        }

        // بقية الكود لإضافة الخط والتحقق من نجاح العملية
        // ...
        $line_data = array(
            'title' => $title,
            'description' => $description,
            'bus_id' => $bus_id
        );

        $isAdded = $this->db->insert('bus_line', $line_data);

        if ($isAdded) {
            $message = array(
                'status' => true,
                'message' => 'تمت إضافة الخط بنجاح.'
            );
            $this->response($message, REST_Controller::HTTP_OK);
        } else {
            $error = array(
                'status' => false,
                'message' => 'فشل في إضافة الخط.'
            );
            $this->response($error, REST_Controller::HTTP_BAD_REQUEST);
        }
    }
    public function updateBusLine_put($line_id)
    {
        $title = $this->getInputData('title');
        $description = $this->getInputData('description');
        $bus_id = $this->getInputData('bus_id');

        if (empty($title) || empty($description) || empty($bus_id)) {
            $error = array(
                'status' => false,
                'message' => 'يرجى تعبئة جميع الحقول المطلوبة.'
            );
            $this->response($error, REST_Controller::HTTP_BAD_REQUEST);
        }

        $line_data = array(
            'title' => $title,
            'description' => $description,
            'bus_id' => $bus_id
        );

        $this->db->where('id', $line_id);
        $isUpdated = $this->db->update('bus_line', $line_data);

        if ($isUpdated) {
            $message = array(
                'status' => true,
                'message' => 'تم تحديث الخط بنجاح.'
            );
            $this->response($message, REST_Controller::HTTP_OK);
        } else {
            $error = array(
                'status' => false,
                'message' => 'فشل في تحديث الخط.'
            );
            $this->response($error, REST_Controller::HTTP_BAD_REQUEST);
        }
    }
    public function deleteBusLine_delete($line_id)
    {
        $this->db->where('id', $line_id);
        $isDeleted = $this->db->delete('bus_line');

        if ($isDeleted) {
            $message = array(
                'status' => true,
                'message' => 'تم حذف الخط بنجاح.'
            );
            $this->response($message, REST_Controller::HTTP_OK);
        } else {
            $error = array(
                'status' => false,
                'message' => 'فشل في حذف الخط.'
            );
            $this->response($error, REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function busStop_post()
    {
        $busline_id = $this->getInputData('busline_id');
        $stop_name = $this->getInputData('stop_name');
        $address = $this->getInputData('address');
        $lat = $this->getInputData('lat');
        $long = $this->getInputData('long');
        $time_arrivals = $this->getInputData('time_arrivals');

        // if (empty($busline_id) || empty($stop_name) || empty($address) || empty($lat) || empty($long) || empty($time_arrivals)) {
        //     $error = array(
        //         'status' => false,
        //         'message' => 'يرجى تعبئة جميع الحقول المطلوبة.'
        //     );
        //     $this->response($error, REST_Controller::HTTP_BAD_REQUEST);
        // }
        if (empty($busline_id) || empty($stop_name) || empty($address) || empty($lat) || empty($long) || empty($time_arrivals)) {
            $emptyFields = array();
            if (empty($busline_id)) {
                $emptyFields[] = 'busline_id';
            }
            if (empty($stop_name)) {
                $emptyFields[] = 'stop_name';
            }
            if (empty($address)) {
                $emptyFields[] = 'address';
            }
            if (empty($lat)) {
                $emptyFields[] = 'lat';
            }
            if (empty($long)) {
                $emptyFields[] = 'long';
            }
            if (empty($time_arrivals)) {
                $emptyFields[] = 'time_arrivals';
            }

            $error = array(
                'status' => false,
                'message' => 'يرجى تعبئة جميع الحقول المطلوبة.',
                'empty_fields' => $emptyFields
            );
            $this->response($error, REST_Controller::HTTP_BAD_REQUEST);
        }

        // إنشاء بيانات المحطة
        $bus_stop_data = array(
            'busline_id' => $busline_id,
            'stop_name' => $stop_name,
            'address' => $address,
            'lat' => $lat,
            'long' => $long,
            'time_arrivals' => $time_arrivals
        );

        // إدخال البيانات في قاعدة البيانات
        $isAdded = $this->db->insert('bus_stop', $bus_stop_data);

        if ($isAdded) {
            $response = array(
                'status' => true,
                'message' => 'تمت إضافة محطة الباص بنجاح.',
                'data' => $isAdded
            );
            $this->response($response, REST_Controller::HTTP_OK);
        } else {
            $error = array(
                'status' => false,
                'message' => 'حدث خطأ أثناء إضافة محطة الباص. يرجى المحاولة مرة أخرى.'
            );
            $this->response($error, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    public function busStop_delete($id)
    {
        // التحقق من وجود محطة الباص بالاي دي المحدد
        $isExist = $this->db->where('id', $id)->get('bus_stop')->num_rows();

        if ($isExist) {
            // حذف محطة الباص
            $this->db->where('id', $id)->delete('bus_stop');

            $response = array(
                'status' => true,
                'message' => 'تم حذف محطة الباص بنجاح.',
                'data' => $isExist

            );
            $this->response($response, REST_Controller::HTTP_OK);
        } else {
            $error = array(
                'status' => false,
                'message' => 'محطة الباص غير موجودة.'
            );
            $this->response($error, REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function busStop_put()
    {
        $id = $this->getInputData('id');
        // $error = array(
        //     'status' => false,
        //     'message' => 'يرجى تعبئة جميع الحقول المطلوبة.',
        //     'data'=>$id
        // );
        // $this->response($error, REST_Controller::HTTP_BAD_REQUEST);
        // التحقق من وجود محطة الباص بالاي دي المحدد
        $isExist = $this->db->where('id', $id)->get('bus_stop')->num_rows();

        if ($isExist) {
            $busline_id = $this->getInputData('busline_id');
            $stop_name = $this->getInputData('stop_name');
            $address = $this->getInputData('address');
            $lat = $this->getInputData('lat');
            $long = $this->getInputData('long');
            $time_arrivals = $this->getInputData('time_arrivals');

            if (empty($stop_name) || empty($address) || empty($lat) || empty($long) || empty($time_arrivals)) {
                $error = array(
                    'status' => false,
                    'message' => 'يرجى تعبئة جميع الحقول المطلوبة.'
                );
                $this->response($error, REST_Controller::HTTP_BAD_REQUEST);
            }

            // بيانات التعديل
            $updated_data = array(
                'busline_id' => $busline_id,
                'stop_name' => $stop_name,
                'address' => $address,
                'lat' => $lat,
                'long' => $long,
                'time_arrivals' => $time_arrivals
            );

            // تحديث بيانات محطة الباص
            $this->db->where('id', $id)->update('bus_stop', $updated_data);

            $response = array(
                'status' => true,
                'message' => 'تم تعديل محطة الباص بنجاح.'
            );
            $this->response($response, REST_Controller::HTTP_OK);
        } else {
            $error = array(
                'status' => false,
                'message' => 'محطة الباص غير موجودة.'
            );
            $this->response($error, REST_Controller::HTTP_NOT_FOUND);
        }
    }

    function getBusLinesByBusId_get($bus_id)
    {
        $bus_lines = $this->db->where('bus_id', $bus_id)->get('bus_line')->result();

        if ($bus_lines) {
            $this->response($bus_lines, REST_Controller::HTTP_OK);
        } else {
            $error = array(
                'status' => false,
                'message' => 'لا توجد خطوط باص متاحة لهذا الحافلة.'
            );
            $this->response($error, REST_Controller::HTTP_NOT_FOUND);
        }
    }
    function getBusStopsByBusLineId_get($busline_id)
    {
        $bus_stops = $this->db->where('busline_id', $busline_id)->get('bus_stop')->result();

        if ($bus_stops) {
            $this->response($bus_stops, REST_Controller::HTTP_OK);
        } else {
            $error = array(
                'status' => false,
                'message' => 'لا توجد مواقف متاحة لهذا الخط.'
            );
            $this->response($error, REST_Controller::HTTP_NOT_FOUND);
        }
    }
    function getAllBusStops_get()
    {
        $bus_stops = $this->db->get('bus_stop')->result();

        if ($bus_stops) {
            $this->response($bus_stops, REST_Controller::HTTP_OK);
        } else {
            $error = array(
                'status' => false,
                'message' => 'لا توجد مواقف متاحة لهذا الخط.'
            );
            $this->response($error, REST_Controller::HTTP_NOT_FOUND);
        }
    }
    public function addParentToSchool_post()
    {
        $parent_data = array(
            'name' => $this->getInputData('name'),
            'name_ar' => $this->getInputData('name_ar'),
            'name_en' => $this->getInputData('name_en'),
            'username' => $this->getInputData('username', true),
            'mobile' => $this->getInputData('mobile'),
            'password' => password_hash($this->getInputData('password', true), PASSWORD_DEFAULT),
            'email' => $this->getInputData('email', true),
            'address' => $this->getInputData('address'),
            'nationalImage' => $this->getInputData('nationalImage'),
            'related_id' => $this->getInputData('related_id', true),
            'stop_id' => $this->getInputData('stop_id'),
            'city_id' => $this->getInputData('city_id', true),
            'type' => 'parent'
        );

        $token = $this->getInputData('token');
        // $token = $this->getTokenFromHeader();
        $checkUser = $this->dashboard_model->checkToken($token);
        if (!$checkUser) {
            // التوكن غير متطابق أو غير موجود، يمكنك اتخاذ الإجراء المناسب هنا
            $error = array(
                'status' => false,
                'message' => 'التوكن غير صالح.'
            );
            $this->response($error, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }


        // التحقق من صحة معرف المدينة المرتبطة بالوكيل
        if ($checkUser->group_name == 'agent') {
            $agent_city_ids = $this->dashboard_model->getAgentCityIds($checkUser->id);
            if (!in_array($parent_data['city_id'], $agent_city_ids)) {
                $error = array(
                    'status' => false,
                    'message' => 'غير مسموح لك بإضافة مدرسة خارج المدن المسؤول عنها.'
                );
                $this->response($error, REST_Controller::HTTP_UNAUTHORIZED);
            }
        }

        // if ($checkUser->group_name == 'school') {
        //     $school_id = $checkUser->id;
        //     if ($parent_data['related_id'] != $school_id) {
        //         $error = array(
        //             'status' => false,
        //             'message' => 'غير مسموح لك بإضافة ولي أمر لمدرسة أخرى. تأكد من معرف المدرسة'
        //         );
        //         $this->response($error, REST_Controller::HTTP_FORBIDDEN);
        //     }
        // }

        $isAddedParentId = $this->dashboard_model->addParent($parent_data);
        if ($isAddedParentId) {
            $message = array(
                'status' => true,
                'message' => 'تمت إضافة الأهل بنجاح.',
                'data' => $isAddedParentId
            );
            $this->response($message, REST_Controller::HTTP_OK);
        } else {
            $error = array(
                'status' => false,
                'message' => 'فشل في إضافة المدرسة.'
            );
            $this->response($error, REST_Controller::HTTP_BAD_REQUEST);
        }
    }


    public function updateParent_put()
    {
        $parent_id = $this->getInputData('parent_id', true);


        $parent_data = array(
            'name' => $this->getInputData('name'),
            'username' => $this->getInputData('username'),
            'mobile' => $this->getInputData('mobile'),
            'password' => password_hash($this->getInputData('password', true), PASSWORD_DEFAULT),
            'email' => $this->getInputData('email', true),
            'address' => $this->getInputData('address'),
            'is_active' => $this->getInputData('is_active'),
            'nationalImage' => $this->getInputData('nationalImage'),
            'related_id' => $this->getInputData('related_id', true),
            'stop_id' => $this->getInputData('stop_id'),
            'city_id' => $this->getInputData('city_id', true),
            'type' => 'parent'
        );

        $token = $this->getInputData('token');
        // $token = $this->getTokenFromHeader();
        // قم بتنفيذ التحقق من التوكن والتحقق من المستخدم هنا

        $isUpdated = $this->dashboard_model->updateParent($parent_id, $parent_data);

        if ($isUpdated) {
            $message = array(
                'status' => true,
                'message' => 'تم تحديث الأهل بنجاح.',
                'data' => $parent_data
            );
            $this->response($message, REST_Controller::HTTP_OK);
        } else {
            $error = array(
                'status' => false,
                'message' => 'فشل في تحديث الأهل.'
            );
            $this->response($error, REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function deleteParent_delete($parent_id = 0)
    {
        // $parent_id = $this->getInputData('parent_id', true);

        $token = $this->getInputData('token');
        // $token = $this->getTokenFromHeader();
        // قم بتنفيذ التحقق من التوكن والتحقق من المستخدم هنا

        if ($parent_id == 0) {
            $error = array(
                'status' => false,
                'message' => 'يرجى ارسال id الأهل المراد حذفه'
            );
            $this->response($error, REST_Controller::HTTP_BAD_REQUEST);
        }
        $isDeleted = $this->dashboard_model->deleteParent($parent_id);

        if ($isDeleted) {
            $message = array(
                'status' => true,
                'message' => 'تم حذف الأهل بنجاح.'
            );
            $this->response($message, REST_Controller::HTTP_OK);
        } else {
            $error = array(
                'status' => false,
                'message' => 'فشل في حذف الأهل.'
            );
            $this->response($error, REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function getParentsBySchoolId_get($school_id)
    {
        // $school_id = $this->get('school_id'); // استلام معرف المدرسة من الطلب GET
        // $school_id = $this->getInputData('school_id');
        if (!$school_id) {
            $error = array(
                'status' => false,
                'message' => 'يجب تقديم معرف المدرسة.'
            );
            $this->response($error, REST_Controller::HTTP_BAD_REQUEST);
        }

        $parents = $this->dashboard_model->getParentsBySchoolId($school_id);

        if (!empty($parents)) {
            $message = array(
                'status' => true,
                'message' => 'تم جلب الأهل بنجاح.',
                'data' => $parents
            );
            $this->response($message, REST_Controller::HTTP_OK);
        } else {
            $message = array(
                'status' => true,
                'message' => 'لا توجد أهل مرتبطين بهذه المدرسة.',
                'data' => array()
            );
            $this->response($message, REST_Controller::HTTP_OK);
        }
    }

    public function search_post()
    {
        // استقبال البيانات من الطلب الوارد
        $type = $this->getInputData('type');
        $data = $this->getInputData('data');

        // التحقق من القيمة المرسلة لـ number وتعيين قيمة type وفقًا لذلك
        switch ($type) {
            case 1:
                $type = 'school';
                break;
            case 2:
                $type = 'bus';
                break;
            case 3:
                $type = 'parent';
                break;
            default:
                // إذا لم يتم تحديد قيمة صحيحة لـ number يمكنك تعيين رسالة خطأ أو اتخاذ إجراء مناسب هنا
                $this->response(['status' => false, 'message' => 'Invalid type value'], REST_Controller::HTTP_BAD_REQUEST);
                return;
        }

        // قم بعملية البحث في جدول "customer" استنادًا إلى البيانات المرسلة وقيمة type
        $result = $this->dashboard_model->searchCustomers($type, $data);

        // قم بإرجاع النتائج كاستجابة للطلب
        $this->response(['status' => true, 'data' => $result], REST_Controller::HTTP_OK);
    }

    public function addChild_post()
    {
        $child_data = array(
            'name' => $this->getInputData('name', true),
            // اسم الطفل المُرسل
            'parent_id' => $this->getInputData('id', true) // رقم الوالد (parent_id) المُرسل
        );

        $isAddedChildId = $this->dashboard_model->addChild($child_data);

        if ($isAddedChildId) {
            $message = array(
                'status' => true,
                'message' => 'تمت إضافة الطفل بنجاح.',
                'child_id' => $isAddedChildId
            );
            $this->response($message, REST_Controller::HTTP_OK);
        } else {
            $error = array(
                'status' => false,
                'message' => 'فشل في إضافة الطفل.'
            );
            $this->response($error, REST_Controller::HTTP_BAD_REQUEST);
        }
    }
    public function deleteChild_post()
    {
        $child_id = $this->getInputData('child_id', true); // رقم الطفل (child_id) المراد حذفه
        $city_id = $this->getInputData('city_id', true);
        $isDeleted = $this->dashboard_model->deleteChild($child_id, $city_id);

        if ($isDeleted) {
            $message = array(
                'status' => true,
                'message' => 'تم حذف الطفل بنجاح.'
            );
            $this->response($message, REST_Controller::HTTP_OK);
        } else {
            $error = array(
                'status' => false,
                'message' => 'فشل في حذف الطفل.'
            );
            $this->response($error, REST_Controller::HTTP_BAD_REQUEST);
        }
    }


    public function getChildrenByParentId_get()
    {
        $parent_id = $this->getInputData('parent_id', true); // رقم الوالد (parent_id) المراد جلب أطفاله

        $children = $this->dashboard_model->getChildrenByParentId($parent_id);

        if ($children) {
            $this->response($children, REST_Controller::HTTP_OK);
        } else {
            $error = array(
                'status' => false,
                'message' => 'لا يوجد أطفال مرتبطين بهذا الوالد.'
            );
            $this->response($error, REST_Controller::HTTP_NOT_FOUND);
        }
    }

}