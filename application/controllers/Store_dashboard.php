<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define( 'API_ACCESS_KEY', 'AAAADnM6PCE:APA91bHEKyfUs34p2nY2OyRpoX4ZDpEJLbbyYgx8ZrpOUcZR_bCBnu8Kyg8SHrxi0UXA7XyMllsO_OAOL83o3lU45uqDolfWL08J_-LK35bCQO6UsldZcHqViyos3OrUZGEFC6GOI5it' );//AIzaSyBLV2ZQQFHy0lR7T-LUFOl-CyoWFiHe9q0
class store_dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *cs
	 * Maps to the following URL
	 * 		http://example.com/index.php/dabookinshboard
	 *	- or -iadd_balance
	 * 		http://example.com/index.php/dashboard/index
	 *	- or -pr
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/dashboard/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	function __construct()
    {
        parent::__construct();
		// if(strtotime("2019/04/24") <= time())
		// 	die();
        $this->load->database();
        $this->load->helper('url','message');
        $this->load->library('grocery_CRUD');
		$this->load->library("ion_auth");
		$this->load->model("api_model");
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		$lang=$this->session->userdata('lang');

        if(isset($lang)){
            if($lang=="ar")
                $this->lang->load("message", "arabic");
            else
                $this->lang->load("message", "english");
            
            $this->data["lang"]=$lang;
        }
        else{
            $this->lang->load("message", "english");
            $this->data["lang"]="en";
		}
		
	
        $this->user = $this->ion_auth->user()->row_array();
		// $unactive_count = $this->db->select("count(*) as visit_count")->from("agency")->where("is_active",0)->get()->result();
		 $this->data["unactive_agency"]=0;
		 
		// $unactive_count = $this->db->select("count(*) as visit_count")->from("agency")->get()->result();
		 $this->data["all"]=0;
		 
		// $unactive_count = $this->db->select("count(*) as visit_count")->where("status",0)->from("balance_request")->get()->result();
		 $this->data["pending"]=0;
		
	
	
 
    }
	

	public function index()
	{
		//$this->load->view("dashboard/index.php");
		$this->manageOrders_C();
	}


	function manage_ratings() 
    {
        	$this->data["title"]="ادارة تقييمات المستخدمين";
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		$crud->set_table('ratings');
	   
	    	$crud->callback_column('offer_id',array($this,'_callback_webpage_url'));
	    	$crud->set_relation('offer_id','offers','title');
	    	$crud->set_relation('user_id','customer','{name}- {mobile}');
	    	$crud->add_action('عرض التفاصيل', '', '','ui-icon-image',array($this,'view_offer_action'));
	    	$crud->unset_add();
	    	$crud->unset_edit();
	    	$crud->unset_delete();
	    //$crud->add_action('موافقة', '', '','ui-icon-image',array($this,'approve_action'));
	    $crud->order_by("id","DESC");
		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "dashboard-store/management";

			 //$this->load->view('includes/template',$data);
		$this->load->view( $data['main_content'],$this->data);
    }
    function add_new_product()
    {
        $this->data["gm_active"] = true;
		$this->data["title"]="add Products";
		$this->data['categories'] = $this->db->get("category")->result_array();
	
		$data['main_content'] = "dashboard-store/add_new_product";

			 //$this->load->view('includes/template',$data);
		$this->load->view( $data['main_content'],$this->data);
    }
    function products()
    {
        $this->data["gm_active"] = true;
		$this->data["title"]="add Products";
		$this->data['categories'] = $this->db->get("category")->result_array();
	    $this->data["products"] = $this->db->where("store_id", $this->user["id"])->get("products")->result_array();
		$data['main_content'] = "dashboard-store/products";

			 //$this->load->view('includes/template',$data);
		$this->load->view( $data['main_content'],$this->data);
    }
    function save_product()
    {
        $data = $this->input->post();
        //var_dump($data);
        $product_categories = $data["categories"];
        unset($data["categories"]);
        
        $data["store_id"] = $this->user["id"];
        $this->db->insert("products",$data);
        $res = $this->db->insert_id();
        $product_cats = explode($product_categories);
        foreach($product_cats as $val)
        {
            $thi->db->insert("products_category",array("product_id"=>$res,"sub_category_id"=>$val));
        }
        echo json_encode(array("data"=>$res));
    }
    
   
     function saveImages()
    {
       // var_dump($_POST);
          $this->load->helper("fancy_file_uploader_helper");
          if (isset($_REQUEST["action"]) && $_REQUEST["action"] === "fileuploader")
          {
           
            $allowedexts = array(
              "jpg" => true,
              "png" => true,
            );
        
            $files = FancyFileUploaderHelper::NormalizeFiles("files");
            //print_r($files[0]);
            if (!isset($files[0]))  $result = array("success" => false, "error" => "File data was submitted but is missing.", "errorcode" => "bad_input");
            else if (!$files[0]["success"])  $result = $files[0];
            else if (!isset($allowedexts[strtolower($files[0]["ext"])]))
            {
              $result = array(
                "success" => false,
                "error" => "Invalid file extension.  Must be '.jpg' or '.png'.",
                "errorcode" => "invalid_file_ext"
              );
            }
            else
            {
              // For chunked file uploads, get the current filename and starting position from the incoming headers.
              $name = FancyFileUploaderHelper::GetChunkFilename();
              //var_dump($name);
              if ($name !== false)
              {
                $startpos = FancyFileUploaderHelper::GetFileStartPosition();
        
                // [Do stuff with the file chunk.]
              }
              else
              {
                // [Do stuff with the file here.]
                $t=round(microtime(true) * 1000);
                 copy($files[0]["file"], "uploads/" . $t.".".$files[0]["ext"]);
                 $data_insert["image"]=$t.".".$files[0]["ext"];
                 $data_insert["product_id"]= $_POST["product_id"];
                 $product = $this->db->where("id",$data_insert["product_id"])->get("products")->row_array();
                 $this->load->library('image_moo');
    	
            	$file_uploaded = 'uploads/'.$data_insert["image"];
                 if($product["image"])
                 {
                      $this->db->insert("images",$data_insert);
                      
            		$target='uploads/thumb__'.$data_insert["image"]; 
            		$this->image_moo->load($file_uploaded)->resize(500,500)->save($target,true);
                 }
                 else
                 {
                     $this->db->where("id",$data_insert["product_id"])->update("products",array("image"=>$data_insert["image"]));
                     	$target='uploads/thumb/'.$data_insert["image"]; 
            		$this->image_moo->load($file_uploaded)->resize(500,500)->save($target,true);
                 }
                
              }
        
              $result = array(
                "success" => true
              );
            }
          }
        //print_r($result);
        echo json_encode($result);
    }
	function manage_products()
	{
	    $this->data["gm_active"] = true;
		$this->data["title"]="manage Products";
		
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		$crud->set_table('products');
		$crud->set_relation('category_id','category','name');
		$crud->where("store_id",$this->user["id"]);
		//$crud->add_action('المستودعات', '', '','ui-icon-image',array($this,'manage_product_stores'));
		$crud->set_relation('store_id','users','{first_name} {last_name}');
		$crud->set_relation_n_n('Categories', 'products_category', 'sub_categories', 'product_id', 'sub_category_id', '{name} - {name_en}');
		$crud->set_field_upload('image','uploads');
		$crud->callback_after_upload(array($this,'compress_callback_after_upload'));
		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "dashboard-store/management";

			 //$this->load->view('includes/template',$data);
		$this->load->view( $data['main_content'],$this->data);
	}
	function manage_product_stores($primary_key , $row)
	{
	    return base_url('/dashboard/product_stores').'/'.$row->id;
	}
	function manage_store_products($primary_key , $row)
	{
	    return base_url('/dashboard/store_products').'/'.$row->id;
	}
	function product_stores($item_id)
	{
		$this->data["gm_active"] = true;
		$this->data["title"]="Product Stores";
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		$crud->set_table('users_products');
		
		$crud->where("item_id",$item_id);
		$crud->field_type('item_id', 'hidden', $item_id);
		$crud->set_relation('user_id','users','username');
		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "dashboard-store/management";

			 //$this->load->view('includes/template',$data);
		$this->load->view( $data['main_content'],$this->data);
	}
	function manageCustomers()
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		$this->data['title']="customers management";
		$crud->set_table('customer');
		$crud->add_action('View Coupons', '', '','ui-icon-image',array($this,'customer_coupon_action'));
		$crud->unset_columns("password","created_date","android_token");
		$crud->unset_fields("password","created_date","android_token");
		$crud->order_by('id','desc');
		$crud->set_field_upload('image','uploads');
		$crud->unset_add();
		$output = $crud->render();
		 $this->data['output'] = $output;
			 $data['main_content'] = "dashboard-store/management";	
			 //$this->load->view('includes/template',$data);
			 $this->load->view( $data['main_content'],$this->data);
	}

	function manage_customer_Orders($user_id)
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
			$this->data['title']="ادارة طلبات الزبون ";
		$crud->set_table('orders');
		$crud->order_by('id','desc');
		$crud->where("customer_id",$user_id);
			$crud->add_action('الطلبية', '', '','ui-icon-image',array($this,'product_Orders'));
	
		$crud->set_relation('customer_id','customer','{mobile} - {name} ');
			$crud->set_relation('delivery_location_id','location','{location_name_ar} - {location_price} ');
			//	$crud->set_relation('item_id','item','{article_number} - {code}');

		$output = $crud->render();
		 $this->data['output'] = $output;
			 $data['main_content'] = "dashboard-store/management";	
			 //$this->load->view('includes/template',$data);
			 $this->load->view( $data['main_content'],$this->data);
	}

	function customer_coupon_action($primary_key , $row)
	{
	    return base_url('/dashboard/manage_customer_coupon').'/'.$row->id;
	}
	function manage_customer_coupon($customer_id)
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
			$this->data['title']="ادارة Coupons ";
		$crud->set_table('customer_coupon');
		$crud->order_by('id','desc');
		$crud->where("customer_id",$customer_id);
		$crud->field_type('customer_id', 'hidden', $customer_id);
		//$crud->set_relation('customer_id','customer','{name} - {mobile} ');
		$crud->set_relation('coupon_id','coupon','{code} - {amount}% ');
        $crud->unset_add();
		$output = $crud->render();
		 $this->data['output'] = $output;
			 $data['main_content'] = "dashboard-store/management";	
			 //$this->load->view('includes/template',$data);
			 $this->load->view( $data['main_content'],$this->data);
	}
	function product_Orders($primary_key , $row)
	{
	    return base_url('/dashboard/manage_product_Orders').'/'.$row->id;
	}
	
	function manage_product_Orders($order_id)
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
			$this->data['title']="منتجات الطلبية رقم ".$order_id;
		$crud->set_table('product_order');
		$crud->order_by('id','desc');
		$crud->where("order_id",$order_id);
		
		$crud->set_relation('order_id','orders','{date} - {status} ');
		$crud->set_relation('item_id','products','{name} - {name_eng}');

		$output = $crud->render();
		 $this->data['output'] = $output;
			 $data['main_content'] = "dashboard-store/management";	
			 //$this->load->view('includes/template',$data);
			 $this->load->view( $data['main_content'],$this->data);
	}
	
	function check_status($post_array, $primary_key) {
	    $order = $this->db->where("id",$primary_key)->get("orders")->row_array();
	    if($order["status"] != $post_array["status"])
	    {
	        $status = $post_array["status"];
	        $order_id=$primary_key;
	        $customer_id = $order["customer_id"];
	        if($status=="new")
    	    {
    	        $this->sendNotification("تم الموافقة على طلبكم, رقم الطلب ".$order_id,$customer_id,$order_id);
    	       
    	       
    	    }
    	   else if($status=="approved")
    	   {
    	        $this->sendNotification("تم الموافقة على طلبكم, رقم الطلب ".$order_id,$customer_id,$order_id);
    	        
    	      
    	        
    	   }
    	   else if($status=="charged")
    	   {
    	       $this->sendNotification("تم شحن  طلبكم, رقم الطلب ".$order_id,$customer_id,$order_id);
    	      
    	       
    	   }
    	     else if($status=="finished")
    	   {
    	       
    	        $this->sendNotification("تم تسليم  طلبكم , رقم الطلب ".$order_id,$customer_id,$order_id);
    	       
    	   }
    	   else
    	   {
    	        $this->sendNotification("تم إلغاء طلبكم".$order_id,$customer_id,$order_id);
    	   }
	    }
	    return $post_array;
	}
    function manageOrders_charged()
	{
		$crud = new grocery_CRUD();
		$this->data["orders_active"] =true;
		$crud->set_theme('bootstrap');
			$this->data['title']="active  request management";
		$crud->set_table('orders');
		$crud->order_by('id','desc');
		$crud->where("status","Sent");
		$crud->unset_add();
		$crud->set_relation('customer_id','customer','{mobile} - {name}');
	//	$crud->set_relation('delivery_location_id','location','{location_name_ar} - {location_price} ');
	//	$crud->set_relation('payments_method','payment_method','{method_name} - {account_number} ');
		$crud->set_relation('address_id','customer_addresses','{address}');
		$crud->add_action('الطلبية', '', '','ui-icon-image',array($this,'product_Orders'));
	//	$crud->set_relation('delivery_id','delivery_person','{mobile} - {name} -{type}');
	//	$crud->add_action('done', '', '','ui-icon-image',array($this,'changeOrderStatus'));
		$crud->add_action('canceled', '', '','ui-icon-image',array($this,'cancelOrder'));
		$crud->callback_before_update(array($this,'check_status'));
		$output = $crud->render();
		 $this->data['output'] = $output;
			 $data['main_content'] = "dashboard-store/management";	
			 //$this->load->view('includes/template',$data);
			 $this->load->view( $data['main_content'],$this->data);
	}
	
	
	function manageOrders_A()
	{
	    $this->data["orders_active"] =true;
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
			$this->data['title']="InProgress  request management";
		$crud->set_table('orders');
		$crud->order_by('id','desc');
		$crud->where("status","InProgress");
		$crud->unset_add();
		$crud->set_relation('customer_id','customer','{mobile} - {name}');
		//$crud->set_relation('payments_method','payment_method','{method_name} - {account_number} ');
		//$crud->set_relation('delivery_location_id','location','{location_name_ar} - {location_price} ');
		$crud->set_relation('address_id','customer_addresses','{address}');
		$crud->set_relation('delivery_id','delivery_person','{mobile} - {name} -{type}');
		$crud->add_action('الطلبية', '', '','ui-icon-image',array($this,'product_Orders'));
		$crud->add_action('finish', '', '','ui-icon-image',array($this,'changeOrderStatus'));
		$crud->add_action('canceled', '', '','ui-icon-image',array($this,'cancelOrder'));
		$output = $crud->render();
		 $this->data['output'] = $output;
			 $data['main_content'] = "dashboard-store/management";	
			 //$this->load->view('includes/template',$data);
			 $this->load->view( $data['main_content'],$this->data);
	}
	
	function manageOrders_C()
	{
	    $this->data["orders_active"] =true;
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
			$this->data['title']="active  request management";
		$crud->set_table('orders');
		$crud->order_by('id','desc');
		$crud->where("status","new");
		$crud->unset_add();
	//	$crud->set_relation('payments_method','payment_method','{method_name} - {account_number} ');
		$crud->set_relation('address_id','customer_addresses','{address}');
		$crud->set_relation('customer_id','customer','{mobile} - {name}');
	//	$crud->set_relation('delivery_location_id','location','{location_name_ar} - {location_price} ');
		$crud->add_action('الطلبية', '', '','ui-icon-image',array($this,'product_Orders'));
		$crud->add_action('approved', '', '','ui-icon-image',array($this,'changeOrderStatus'));
		$crud->add_action('canceled', '', '','ui-icon-image',array($this,'cancelOrder'));
		$output = $crud->render();
		 $this->data['output'] = $output;
			 $data['main_content'] = "dashboard-store/management";	
			 //$this->load->view('includes/template',$data);
			 $this->load->view( $data['main_content'],$this->data);
	}
	function changeOrderStatus($primary_key , $row)
	{
	    if($row->status=="new")
	    {
	        //$this->sendNotification("تم الموافقة على طلبكم, رقم الطلب ".$row->id,$row->customer_id);
	        return base_url('/dashboard/changeOrderStatus_method').'/'.$row->id.'/InProgress/'.$row->customer_id;
	    }
	   else if($row->status=="InProgress")
	   {
	      //  $this->sendNotification("تم شحن  طلبكم, رقم الطلب ".$row->id,$row->customer_id);
	        return base_url('/dashboard/changeOrderStatus_method').'/'.$row->id.'/Sent/'.$row->customer_id;
	        
	   }
	   else
	   {
	       // $this->sendNotification("تم تسليم على طلبكم , رقم الطلب ".$row->id,$row->customer_id);
	         return base_url('/dashboard/changeOrderStatus_method').'/'.$row->id.'/finished/'.$row->customer_id;
	   }
	   
	}
	function cancelOrder($primary_key , $row)
	{
	   
	    return base_url('/dashboard/changeOrderStatus_method').'/'.$row->id.'/canceled/'.$row->customer_id;
	}
	function changeOrderStatus_method($order_id,$status,$customer_id)
	{
		$this->db->where("id",$order_id)->update("orders",array("status"=>$status,"finish_date"=>date('Y-m-d H:i:s')));
		if($status=="new")
	    {
	        $this->sendNotification("تم الموافقة على طلبكم, رقم الطلب ".$order_id,$customer_id);
	       
	       
	    }
	   else if($status=="approved")
	   {
	        $this->sendNotification("تم الموافقة على طلبكم, رقم الطلب ".$order_id,$customer_id);
	        
	      
	        
	   }
	   else if($status=="charged")
	   {
	       $this->sendNotification("تم شحن  طلبكم, رقم الطلب ".$order_id,$customer_id);
	      
	       
	   }
	     else if($status=="finished")
	   {
	       
	        $this->sendNotification("تم تسليم  طلبكم , رقم الطلب ".$order_id,$customer_id);
	       
	   }
	   else
	   {
	        $this->sendNotification("تم إلغاء طلبكم".$order_id,$customer_id);
	   }
		redirect($_SERVER['HTTP_REFERER']);
	}
	function manageOrders_Done()
	{
	    $this->data["orders_active"] =true;
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
			$this->data['title']="finished  request management";
		$crud->set_table('orders');
		$crud->order_by('id','desc');
		$crud->where("status","Delivered");
		$crud->unset_add();
		//$crud->set_relation('payments_method','payment_method','{method_name} - {account_number} ');
		$crud->set_relation('address_id','customer_addresses','{address}');
		$crud->set_relation('customer_id','customer','{mobile} - {name}');
		//$crud->set_relation('delivery_location_id','location','{location_name_ar} - {location_price} ');
			$crud->add_action('الطلبية', '', '','ui-icon-image',array($this,'product_Orders'));
	//	$crud->set_relation('item_id','item','{article_number} - {code}');
		$crud->set_relation('delivery_id','delivery_person','{mobile} - {name} -{type}');
    	$crud->add_action('الاشعارات', '', '','ui-icon-image',array($this,'customerorders_notifications'));
		$output = $crud->render();
		 $this->data['output'] = $output;
			 $data['main_content'] = "dashboard-store/management";	
			 //$this->load->view('includes/template',$data);
			 $this->load->view( $data['main_content'],$this->data);
	}
	function manageOrders_Deleted()
	{
	    $this->data["orders_active"] =true;
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
			$this->data['title']="canceled  request management";
		$crud->set_table('orders');
		$crud->order_by('id','desc');
		$crud->where("status","canceled");
		$crud->unset_add();
		$crud->set_relation('address_id','customer_addresses','{address}');
		$crud->set_relation('customer_id','customer','{mobile} - {name}');
			$crud->add_action('الطلبية', '', '','ui-icon-image',array($this,'product_Orders'));
				$crud->set_relation('delivery_id','delivery_person','{mobile} - {name} -{type}');
				//$crud->set_relation('delivery_location_id','location','{location_name_ar} - {location_price} ');
	//	$crud->set_relation('item_id','item','{article_number} - {code}');
    	$crud->add_action('الاشعارات', '', '','ui-icon-image',array($this,'customerorders_notifications'));
		$output = $crud->render();
		 $this->data['output'] = $output;
			 $data['main_content'] = "dashboard-store/management";	
			 //$this->load->view('includes/template',$data);
			 $this->load->view( $data['main_content'],$this->data);
	}
	
	

function manageUnActiveCustomers()
	{
	    $this->data["customer_active"] =true;
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
			$this->data['title']="un-active customers management";
		$crud->set_table('customer');
		$crud->where("is_active",0);
		$crud->unset_columns("password","created_date","android_token");
		$crud->unset_fields("password","created_date","android_token");
		$crud->set_field_upload('image','uploads');
		$crud->add_action('تفعيل', '', '','ui-icon-image',array($this,'active_user'));
		$crud->unset_columns(array('ip','password'));
		$crud->order_by('id','desc');
		$output = $crud->render();
		 $this->data['output'] = $output;
			 $data['main_content'] = "dashboard-store/management";	
			 //$this->load->view('includes/template',$data);
			 $this->load->view( $data['main_content'],$this->data);
	}
	

    function encrypt_password_callback($post_array, $primary_key = null)
	{
		$this->load->library("auth");
        $this->load->model("login_model");
		$post_array['password'] = $this->login_model->hash($post_array['password']);
		$post_array['is_active'] = 1;
	  	return $post_array;
	}
	
	function reset_password_action($primary_key , $row)
	{
	    return base_url('/dashboard/reset_password').'/'.$row->id."/replace";
	}
	
	function reset_password($agency_id)
	{
		$this->load->library("auth");
        $this->load->model("login_model");
		$pass = $this->login_model->hash("12345678");
		$this->db->where("id",$agency_id)->update("agency",array("password"=>$pass));
		redirect($_SERVER['HTTP_REFERER'].'/success/1','refresh');
	}
	function activate($agency_id)
	{
		$this->db->where("id",$agency_id);
		$this->db->update("agency",array("is_active"=>1));
		redirect($_SERVER['HTTP_REFERER']);
	}
	function deactivate($agency_id)
	{
		$this->db->where("id",$agency_id);
		$this->db->update("agency",array("is_active"=>0));
		redirect($_SERVER['HTTP_REFERER']);
	}

	function add_balance()
	{
	    
		$agency_id = $this->input->post("agency_id");
		$new_balance = abs($this->input->post("balance"));
		$note = $this->input->post("note");
		$agency_info = $this->api_model->getAgency($agency_id);
		$this->db->insert("balance_history",array("agency_id"=>$agency_id,"balance"=>$new_balance,"by_user"=>$this->ion_auth->get_user_id(),"note"=>$note));
		if($new_balance >0)
		{
			$this->db->set('balance', '`balance`+'.$new_balance, FALSE);
		}
		else
		{
		   // echo "enter";
			$this->db->set('balance', '`balance`'.$new_balance, FALSE);
		}
		$this->db->where(array('id'=>$agency_id))->update('agency');
		
		$this->db->insert("balance_request",array("agency_id"=>$agency_id,"value"=>$new_balance,'currency'=>$agency_info["currency"],"status"=>'approved',"note"=>'By Administrator'));
	//	echo $this->db->last_query();die();
		redirect($_SERVER['HTTP_REFERER']);
	}


    function page404()
    {
        $this->data['main_content'] = "404";
        $this->load->view('includes/main',$this->data);
    }
    
    function booking_info()
    {
        $booking_id=$this->input->get("id");
        $this->data["booking_info"] = $this->api_model->get_booking_info_admin($booking_id);
        if($this->data["booking_info"]==NULL)
        {
            redirect(base_url("dashboard/page404"));
        }
        $booking_users = $this->api_model->get_booking_users($booking_id);
        foreach($booking_users as $us)
        {
            if($us["type"]=="adult")
            {
                $this->data["booking_info"]["rooms"][$us["room"]]["adults"][] = $us;
            }
            else
            {
                $this->data["booking_info"]["rooms"][$us["room"]]["kids"][] = $us;
            }
        }
        $this->data['main_content'] = "booking_info_dashboard";
        	$this->load->view('includes/main',$this->data);
	}
	
	function manageConfig()
	{
		$this->data["title"]=lang("manage_config");
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		$crud->set_table('config');
		$crud->field_type('key','readonly');
		$crud->unset_add();
		$output = $crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "dashboard-store/management";

			 //$this->load->view('includes/template',$data);
		$this->load->view( $data['main_content'],$this->data);
		
	}

    
	function manage_news()
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		$crud->set_table('news');
	
		$output = $crud->render();
		$this->data['output'] = $output;
		$this->data['title'] = "News Management";
		$data['main_content'] = "dashboard-store/management";
		
			 //$this->load->view('includes/template',$data);
		$this->load->view( $data['main_content'],$this->data);
	}

    
    public function sendNotification_category1($message,$category_id,$topic="some")
	{
	    //var_dump($user_id);
	    $users = $this->db->where("category_id",$category_id)->get("customer")->result_array();
	    
	  //  $android_token = $user["android_token"];
	   // $ios_token = $user["ios_token"];
	   $not_ids = array();
	   foreach($users as $user)
	   {
	       if($user["android_token"] != "")
	        $not_ids[]=$user["android_token"];
	   }
        $msg = array
        (
            'body'  => strip_tags($message),
            'title'     => "Jaweesh",
        );
        if($topic!="all")
        $fields = array
        (
            'registration_ids'  =>$not_ids,
            'data'          => $msg,
            'priority'=>'high'
        );
        else
		$fields = array
		(
			'to' 	=> '/topics/' . $topic,
			'data'			=> $msg
		);

		$headers = array
		(
			'Authorization: key=' . API_ACCESS_KEY,
			'Content-Type: application/json'
		);
        //echo API_ACCESS_KEY;
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch);
		//echo($result);
		
        if($result === false)
        {
            echo 'Curl error: ' . curl_error($ch);
        }
        else
        {
            //echo 'Operation completed without any errors';
        }
		if($errno = curl_errno($ch)) {
                $error_message = curl_strerror($errno);
                echo "cURL error ({$errno}):\n {$error_message}";
            }
		curl_close( $ch );
//		die();
		/*
		    TODO exho result
		*/
	//	echo $result;

	}
	public function sendNotification($message,$user_id,$order_id=null,$allow_insert=true)
	{
	    //var_dump($user_id);
	    $user = $this->db->where("id",$user_id)->get("customer")->result_array()[0];
	    if($allow_insert)
	        $this->db->insert("notifications",array("customer_id"=>$user_id,"notification_msg"=>$message));
	    $android_token = $user["android_token"];
	   // var_dump(API_ACCESS_KEY);
	   // $ios_token = $user["ios_token"];
        $msg = array
        (
            'body'  => strip_tags($message),
            'title'     => "One-Deals",
        );
        $notification = array(
            'body'  => strip_tags($message),
            'title'     => "One-Deals",
            "click_action"=>''
            );
        if($order_id)
        {
            $msg["order_id"]=$order_id;
        }
        $fields = array
        (
            //'to'  =>$android_token,
            "registration_ids"=> [$android_token],
            'data'          => $msg,
            'notification'=>$notification,
            'priority'=>'high'
        );
        


		$headers = array
		(
			'Authorization: key=' . API_ACCESS_KEY,
			'Content-Type: application/json'
		);
        //echo API_ACCESS_KEY;
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch);
	//	echo($result);
		
        if($result === false)
        {
            echo 'Curl error: ' . curl_error($ch);
        }
        else
        {
            //echo 'Operation completed without any errors';
        }
		if($errno = curl_errno($ch)) {
                $error_message = curl_strerror($errno);
                echo "cURL error ({$errno}):\n {$error_message}";
            }
		curl_close( $ch );
//		die();
		/*
		    TODO exho result
		*/
	//	echo $result;

	}
	
	public function sendNotification_all($message,$topic="all")
	{
	    //var_dump($user_id);
	    
	   
        $msg = array
        (
            'body'  => strip_tags($message),
            'title'     => "One-Deals",
        );
        
         $notification = array(
            'body'  => strip_tags($message),
            'title'     => "One-Deals",
            "click_action"=>''
            );
        $fields = array
		(
			'to' 	=> '/topics/' . $topic,
			'data'			=> $msg,
			'notification'=>$notification,
		);
        

		$headers = array
		(
			'Authorization: key=' . API_ACCESS_KEY,
			'Content-Type: application/json'
		);
        //echo API_ACCESS_KEY;
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch);
	//	echo($result);
		
        if($result === false)
        {
            echo 'Curl error: ' . curl_error($ch);
        }
        else
        {
            //echo 'Operation completed without any errors';
        }
		if($errno = curl_errno($ch)) {
                $error_message = curl_strerror($errno);
                echo "cURL error ({$errno}):\n {$error_message}";
            }
		curl_close( $ch );
//		die();
		/*
		    TODO exho result
		*/
	//	echo $result;

	}

	
	function manageActiveCustomers()
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
			$this->data['title']="active customers management";
		$crud->set_table('customer');
		$crud->unset_columns("password","created_date","android_token");
		$crud->unset_fields("password","created_date","android_token");
		$crud->set_field_upload('image','uploads');
		$crud->add_action('الطلبات', '', '','ui-icon-image',array($this,'customer_Orders'));
			$crud->add_action('الاشعارات', '', '','ui-icon-image',array($this,'customer_notifications'));
			//	$crud->set_relation('category_id','category','category_name');
		$crud->where("is_active",1);
	
		$crud->unset_columns(array('ip','password'));
		$crud->order_by('id','desc');
		$output = $crud->render();
		 $this->data['output'] = $output;
			 $data['main_content'] = "dashboard-store/management";	
			 //$this->load->view('includes/template',$data);
			 $this->load->view( $data['main_content'],$this->data);
	}


    function customer_Orders($primary_key , $row)
	{
	    return base_url('/dashboard/manage_customer_Orders').'/'.$row->id;
	}
	function customer_notifications($primary_key , $row)
	{
	    return base_url('/dashboard/manage_customer_notifications').'/'.$row->id;
	}


	function customerorders_notifications($primary_key , $row)
	{
	    return base_url('/dashboard/manage_customer_notifications').'/'.$row->customer_id;
	}
	
	function manage_customer_notifications($user_id)
	{
	    $crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
			$this->data['title']="Customer Notification";
		$crud->set_table('notifications');
		$crud->order_by('id','desc');
		$crud->where("customer_id",$user_id);
		$crud->field_type('customer_id', 'hidden', $user_id);
		$crud->fields('customer_id',"notification_msg");
		$crud->callback_after_insert(array($this, 'sendNotification_user'));
		//$crud->callback_after_update(array($this, 'sendNotification_user'));
		
		$output = $crud->render();
		 $this->data['output'] = $output;
			 $data['main_content'] = "dashboard-store/management";	
			 //$this->load->view('includes/template',$data);
			 $this->load->view( $data['main_content'],$this->data);
	}
	function sendNotification_user($post_array,$primary_key)
    {
        //print_r($post_array);
       $this->sendNotification($post_array["notification_msg"],$post_array["customer_id"],null,false);
        return true;
    }
	
	
	
	function manage_general_notifications()
	{
	    $crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
			$this->data['title']="General Notifications";
		$crud->set_table('general_notifications');
		$crud->order_by('id','desc');
	
		$crud->fields("message");
		$crud->callback_after_insert(array($this, 'sendNotification_alluser'));
		//$crud->callback_after_update(array($this, 'sendNotification_user'));
		
		$output = $crud->render();
		 $this->data['output'] = $output;
			 $data['main_content'] = "dashboard-store/management";	
			 //$this->load->view('includes/template',$data);
			 $this->load->view( $data['main_content'],$this->data);
	}
	function sendNotification_alluser($post_array,$primary_key)
    {
        //print_r($post_array);
       $this->sendNotification_all($post_array["message"]);
        return true;
    }
	
	

	
	
	
	function sendAcivateNotification($post_array,$primary_key)
    {
        //print_r($post_array);
	   $this->sendNotification("لقد تم تفعيل حسابكم..يرجى استكمال المعلومات الخاصة بكم",$primary_key);
	   
        return true;
    }
    
    

	
	

	

	function album_products($primary_key , $row)
	{
	    return base_url('/dashboard/manage_album_products').'/'.$row->id;
	}

	function manage_album_products($album_id)
	{
		
			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
				$this->data['title']="product gallery management";
			$crud->set_table('kt_ms_art');
			$crud->columns('ArticleCodeId','ArticleName1Id','Color','Size','Remarks');
			$crud->fields('ArticleCodeId','ArticleName1Id','Color','Size','Remarks');
			$crud->where("Remarks",$album_id);
			$crud->set_relation('Remarks','album','name');
			$crud->unset_add();
			//$crud->set_relation('city_id','cities','name');
			$crud->unset_columns(array('ip','password'));
			$crud->order_by('id','desc');
			$output = $crud->render();
			 $this->data['output'] = $output;
				 $data['main_content'] = "dashboard-store/management";	
				 //$this->load->view('includes/template',$data);
				 $this->load->view( $data['main_content'],$this->data);
		
	}
	function setAlbum($post_array,$primary_key)
    {
        $this->db->where("ArticleCodeId Between '".$post_array["from_id"]."' and '".$post_array["to_id"]."'")->update("kt_ms_art",array("Remarks"=>$primary_key));
         
        return true;
    }
 
	function album($primary_key , $row)
	{
		return base_url('/dashboard/manage_album').'/'.$row->id;
	}
	function manage_album($album_id)
	{
		 $this->load->library('image_CRUD');
		$image_crud = new image_CRUD();
	 		$this->data['title']="إدارة الألبوم";
		$image_crud->set_table('images');
	 
		$this->data['is_images']=true;
		$image_crud->set_url_field('image');
	 
		$image_crud->set_relation_field('album_id')
		->set_image_path('uploads');
		
		$output = $image_crud->render();
		// var_dump($output);die();
		 $this->data['output'] = $output;
			 $data['main_content'] = 'management';	
			 //$this->load->view('includes/template',$data);
			 $this->load->view( $data['main_content'],$this->data);
	}
	function manage_gallery()
	{
		 $this->load->library('image_CRUD');
		$image_crud = new image_CRUD();
	 		$this->data['title']="إدارة معرض الصور";
		$image_crud->set_table('gallery');
	 
		$this->data['is_images']=true;
		$image_crud->set_url_field('image');
	 
		$image_crud->set_image_path('uploads');
		
		$output = $image_crud->render();
		// var_dump($output);die();
		 $this->data['output'] = $output;
			 $data['main_content'] = 'management';	
			 //$this->load->view('includes/template',$data);
			 $this->load->view( $data['main_content'],$this->data);
	}
	function add_user_product()
	{
		$data = (array) $this->input->post();
		$data["user_id"] = $this->ion_auth->get_user_id();
		//var_dump($data);
		$this->api_model->add_user_product($data);
		redirect($_SERVER['HTTP_REFERER']);
	}
	function edit_user_product()
	{
		$data = (array) $this->input->post();
		$data["user_id"] = $this->ion_auth->get_user_id();
		//var_dump($data);
		$this->api_model->update_user_product($data);
		redirect($_SERVER['HTTP_REFERER']);
	}
	public function my_products()
	{
		$category_id = $this->input->get("id");
		
		$user_id = $this->ion_auth->get_user_id();
		$this->data["products"]= $this->api_model->get_user_products($user_id);
		$this->data["title"]="My products";
		$this->data["categories"] = $this->api_model->get_categories();
		$data['main_content'] = 'products';
		$this->load->view( $data['main_content'], $this->data);
		
	}
	public function all_products()
	{
		$user_id = $this->ion_auth->get_user_id();
		$this->data["products"]= $this->api_model->get_all_products($user_id);
		$this->data["title"]="all products";
		$data['main_content'] = 'products';
		//$this->data["categories"] = $this->api_model->get_categories();
		$this->load->view( $data['main_content'], $this->data);
		
	}
	public function manage_users(){
		//var_dump($this->auth->user());die();
		if(!$this->ion_auth->is_admin())
		{
			redirect(base_url()."dashboard/manage_connection");
		}
		$this->data['title']="إدارة المستخدمين";
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		$crud->set_table('users');
		$crud->set_subject('User');
		$crud->required_fields('username');
		
		$crud->columns('username','first_name','last_name','active');
		$crud->fields('username','first_name','password','last_name','active');
		$crud->change_field_type('password', 'password');
			$output = $crud->render();
				 $this->data['output'] = $output;
			 $data['main_content'] = 'management';	
			 //$this->load->view('includes/template',$data);
			 $this->load->view( $data['main_content'],$this->data);
		}

	// function encrypt_password_callback($post_array, $primary_key = null)
	// {
	// 	$this->load->library("auth");
    //     $this->load->model("login_model");
	// 	$post_array['password'] = $this->login_model->hash($post_array['password']);
	//   	return $post_array;
	// }

	function decrypt_password_callback($value)
	{
	  $this->load->library('encrypt');
	  $key = 'bcb04b4a8b6a0cffe54763945cef08bc88abe000fdebae5e1d417e2ffb2a12a3';
	  $decrypted_password = $this->encrypt->decode($value, $key);
	  //var_dump($decrypted_password);die();
	  return "<input type='password' name='password' value='$decrypted_password' />";
	}
function compress_callback_after_upload($uploader_response,$field_info, $files_to_upload)
	{
		$this->load->library('image_moo');
		//var_dump($field_info->upload_path);die();
		//Is only one file uploaded so it ok to use it with $uploader_response[0].
		$file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name; 
		$target=$field_info->upload_path.'/thumb/'.$uploader_response[0]->name; 
		$this->image_moo->load($file_uploaded)->resize(250,250)->save($target,true);
		
		//$this->image_moo->load($file_uploaded)->resize(250,250)->save($target,true);
	 
		return true;
	}
	
	function manage_card_files()
	{
	    	$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
		$this->data['title']="إدارة ملفات البطاقات";
		$crud->set_table('card_files');
	
		$crud->set_field_upload('file','uploads');
			$crud->callback_after_upload(array($this,'import_callback_after_upload'));
		//	$crud->add_action('الألبوم', '', '','ui-icon-image',array($this,'album'));
		$output = $crud->render();
		 $this->data['output'] = $output;
			 $data['main_content'] = 'management';	
			 //$this->load->view('includes/template',$data);
			 $this->load->view( $data['main_content'],$this->data);
	}
	
	function import_callback_after_upload($uploader_response,$field_info, $files_to_upload)
	{
    	$file = fopen($field_info->upload_path.'/'.$uploader_response[0]->name,"r");
    	$i = 0 ;
		while(! feof($file))
          {
          //print_r(fgetcsv($file));
          $arr =fgetcsv($file);
            $data[$i]["number"] = $arr[0];
            $data[$i]["card_value"] = $arr[1];
            $i++;
          }
        $this->db->insert_batch("card",$data);
        fclose($file);
		
		//$this->image_moo->load($file_uploaded)->resize(250,250)->save($target,true);
	 
		return true;
	}
	
	
	 public static function convert_to_utf8_recursively($dat)
    {
       if (is_string($dat)) {
          return iconv('WINDOWS-1256', 'UTF-8',$dat);
       } elseif (is_array($dat)) {
          $ret = [];
          foreach ($dat as $i => $d) $ret[ $i ] = self::convert_to_utf8_recursively($d);
 
          return $ret;
       } elseif (is_object($dat)) {
          foreach ($dat as $i => $d) $dat->$i = self::convert_to_utf8_recursively($d);
 
          return $dat;
       } else {
          return $dat;
       }
    }

	function import_csv()
	{
    	$file = fopen(base_url().'data/data.csv',"r");
    
    	$this->db->truncate('item');
    	$i = 0 ;
		while(! feof($file))
          {
          //print_r(fgetcsv($file));
            $arr =fgetcsv($file);
            $data[$i]["code"] = $arr[0];
            $data[$i]["category"] = $arr[1];
            	//var_dump(iconv('UTF-8', 'ISO-8859-1//TRANSLIT',  $arr[1]));
            //echo mb_convert_encoding( $data[$i]["category"], 'UTF-16LE', 'UTF-8')."<br>";
            $data[$i]["group"] = $arr[2];
            $data[$i]["article_number"] = $arr[0];
            $data[$i]["id"] = $arr[3];
            $data[$i]["image"] = $arr[3].".jpg";
            if(isset($arr[4]))
            {
                 $data[$i]["packeging"] = $arr[4];
                  $data[$i]["price"] = $arr[5];
            }
            $i++;
          }
          //print_r($data);
          unset($data[0]);
          $res = self::convert_to_utf8_recursively($data);
         // print_r($res);
        $this->db->insert_batch("item",$res);
        fclose($file);
		
		//$this->image_moo->load($file_uploaded)->resize(250,250)->save($target,true);
	 
		return true;
	}
   function import_json()
	{
        /*$string = file_get_contents(base_url().'data/data.json');
        //var_dump($string);
        $string = utf8_decode($string);
        $string = preg_replace('/[[:cntrl:]]/', '', $string);*/
        
        $string = file_get_contents(base_url().'data/jsondata.json');
		
		$string = self::convert_to_utf8_recursively($string);
	//	$result = json_decode($string, true, JSON_UNESCAPED_UNICODE);
        $res_data = json_decode($string,true, JSON_UNESCAPED_UNICODE);
        //var_dump($res_data);
        var_dump(json_last_error_msg());
    	$this->db->truncate('item');
    	$i = 0 ;
    	
    	foreach($res_data["Output"] as $arr)
    	{
    	    $data[$i]["code"] = $arr["CODE"];
            $data[$i]["category"] = $arr["CAT"];
            	//var_dump(iconv('UTF-8', 'ISO-8859-1//TRANSLIT',  $arr[1]));
            //echo mb_convert_encoding( $data[$i]["category"], 'UTF-16LE', 'UTF-8')."<br>";
            $data[$i]["group"] = $arr["GROUP-ID"];
            $data[$i]["article_number"] = $arr["ART-NUM"];
            $data[$i]["id"] = $arr["CODE"];
            $data[$i]["image"] = $arr["ART-NUM"].".jpg";
            $data[$i]["store_quantity"] = $arr["QUANTITY"];
                 $data[$i]["packeging"] = $arr["PCK"];
                  $data[$i]["price"] = $arr["PRICE"];
            
            $i++;
    	}
        //  $res = self::convert_to_utf8_recursively($data);
         // print_r($res);
        $this->db->insert_batch("item",$data);
     
		return true;
	}
}
