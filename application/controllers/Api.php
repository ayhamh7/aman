<?php
ini_set('max_execution_time', 6000);
defined('BASEPATH') OR exit('No direct script access allowed');
define('API_ACCESS_KEY', 'AAAAaC8DVYk:APA91bHtSzJffQPKEhCuOZCk79IgAZV1WRQ9T2dPMy_QcfYOtjAb68G39kOq0nJA06kdX7D0flaqPt-RIOF3zvE18iPl3R6NB104apudBc6Q98uUz1-Mt4c586ANi44bZwYNymc6rWaq'); //AIzaSyBLV2ZQQFHy0lR7T-LUFOl-CyoWFiHe9q044444444444444444444444444444444444444444444444
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require(APPPATH.'controllers/BaseController.php');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Api extends BaseController {

   function __construct()
    {
        // Construct the parent class
        parent::__construct();
        //$this->set_timezone();
        //header('content-Type: text/html; charset=utf-8');
      //  header('Content-Type: application/json; charset=utf-8');
      //  $this->load->model('api_model');  // to fix
      $this->load->helper("message");
      $this->load->library("pagination");
        $this->load->model('api_model');
        $this->load->model('auth_model');
    }

    function saveStop_post()
    {
      $data = (array) $this->post();
      $user_id=$this->current_user["id"];
      $busLise = $this->db->where("bus_id",$user_id)->order_by("id","desc")->get("bus_line")->row_array();
      $parent_info = $this->db->where("id",$data['parent_id'])->order_by("id","desc")->get("customer")->row_array();
      $stop['busline_id'] = $busLise['id'];
      $stop['stop_name'] = 'stop '.$parent_info['name'];
      $stop['lat'] = $data['lat'];
      $stop['long'] = $data['long'];
      $this->db->insert("bus_stop",$stop);
      $stop_id = $this->db->insert_id();
      $this->db->where("id",$data['parent_id'])->update("customer",array("stop_id"=>$stop_id));
      $this->response(array("data"=>true),200);
    }

    public function stops_get()
    {
      $user_id=$this->current_user?$this->current_user["id"]:5;
      $users = $this->db->join("bus_line","bus_line.id = busline_id")->join("customer","customer.stop_id = bus_stop.id","inner")->select("bus_stop.*,customer.id as customer_id")->where("bus_line.bus_id",$user_id)->get("bus_stop")->result_array();
      $this->response($users,200);
    }

    public function sendUserNotification_get()
	{
		//var_dump($user_id);
    $stop_id = $this->get("id");
		$user = $this->db->where("customer.stop_id", $stop_id)->get("customer")->row_array();
		
		//$android_token = $user["firebase_token"];
    $message = 'لقد اقترب الباص من الوصول';
    $user_id=$this->current_user["id"];
    $res = $this->db->where(array("driver_id"=>$user_id))->order_by("id", "desc")->get("movement")->row_array();
    if($res!= null && count($res))
    {
      $res2 = $this->db->where(array("movment_id"=>$res['id'],"user_id"=>$user['id']))->get("notifications")->row_array();
      if($res2!= null && count($res2))
      {
        return;
      }
      $this->db->insert("notifications",array("movment_id"=>$res['id'],"user_id"=>$user['id']));
      
    }
		$this->sendUserNotification($user['id'],$message);

	}


  public function sendUserNotification($user_id,$message)
	{
		//var_dump($user_id);
    //$user_id = $this->get("id");
		$user = $this->db->where("id", $user_id)->get("customer")->row_array();
		
		$android_token = $user["firebase_token"];
    //$message = 'لقد اقترب الباص من الوصول';
		// var_dump(API_ACCESS_KEY);
		// $ios_token = $user["ios_token"];
		$msg = array(
			'body'  => strip_tags($message),
			'title'     => "حافلتي",
		);
		$notification = array(
			'body'  => strip_tags($message),
			'title'     => "حافلتي",
			"click_action" => ''
		);
		
		$fields = array(
			//'to'  =>$android_token,
			"registration_ids" => [$android_token],
			'data'          => $msg,
			'notification' => $notification,
			'priority' => 'high'
		);



		$headers = array(
			'Authorization: key=' . API_ACCESS_KEY,
			'Content-Type: application/json'
		);
		//echo API_ACCESS_KEY;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		$result = curl_exec($ch);
		//echo($result);

		if ($result === false) {
			echo 'Curl error: ' . curl_error($ch);
		} else {
			//echo 'Operation completed without any errors';
		}
		if ($errno = curl_errno($ch)) {
			$error_message = curl_strerror($errno);
			echo "cURL error ({$errno}):\n {$error_message}";
		}
		curl_close($ch);
		//die();
		/*
		    TODO exho result
		*/
		//echo $result;

	}
    public function sendNotification_get()
    {
        $message = ($this->get("is_go_school") && $this->get("is_go_school")=='true')?"بدأت رحلة الانطلاق":"بدأت رحلة العودة من المدرسة";
        $user_id=$this->current_user["id"];
        //var_dump($user_id);
        $this->db->where('bus_id', $user_id);
        $this->db->join('bus_stop', 'busline_id = bus_line.id');
        
        $this->db->join('customer', 'customer.stop_id = bus_stop.id');
        $this->db->select('customer.*');
        $users =  $this->db->get('bus_line')->result_array();
        
       // echo $this->db->last_query();
         $msg = array
          (
              'body'  => strip_tags($message),
              'title'     => "",
          );
          $notification = array(
              'body'  => strip_tags($message),
              'title'     => "حافلتي",
              "click_action"=>''
              );
       foreach ($users as $key => $user) {
        # code... 
        $android_token = $user["firebase_token"];
          $fields = array
          (
              //'to'  =>$android_token,
              "registration_ids"=> [$android_token],
              'data'          => $msg,
              'notification'=>$notification,
              'priority'=>'high',
              'android' => array( 'priority'=>'high')
          );
           $headers = array
          (
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
          );
          $ch = curl_init();
          curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
          curl_setopt( $ch,CURLOPT_POST, true );
          curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
          curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
          curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
          curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
          $result = curl_exec($ch);
          //echo($result);
          
              if($result === false)
              {
                  echo 'Curl error: ' . curl_error($ch);
              }
              else
              {
                  //echo 'Operation completed without any errors';
              }
          if($errno = curl_errno($ch)) {
                      $error_message = curl_strerror($errno);
                      echo "cURL error ({$errno}):\n {$error_message}";
                  }
          curl_close( $ch );
       }
       
      echo $result;
  
    }
    function parents_get()
    {
         $school_id = isset($this->current_user["related_id"])?$this->current_user["related_id"]:39;
          $this->db->where("type","parent")->where("related_id",$school_id);
         $data = $this->db->get("customer")->result_array();
         $this->response($data , 200);
       
    }
    function schools_get()
    {
       
        if($this->current_user && $this->current_user['type'] == 'school')
        {
            $this->db->where("id",$this->current_user["id"]);
        }
        else if($this->current_user && $this->current_user['type'] == 'parent')
        {
             $this->db->where("id",$this->current_user["related_id"]);
        }
        $this->db->where("type","school");
         $data = $this->db->get("customer")->result_array();
         $this->response($data , 200);
    }
    function startDriverMoving_post()
    {
         $user_id=$this->current_user["id"];
         $this->db->insert("movement",array("date"=>date("Y-m-d"),"start"=>date("H:i:s"),"driver_id"=>$user_id));
         $this->response(true , 200);
        
    }
    function children_get()
    {
        $parent_id = isset($this->current_user["id"])?$this->current_user["id"]:7;
        $data = $this->api_model->get_children($parent_id);
        $this->response($data,200);
    }
    function  bus_children_get()
    {
      $user_id=$this->current_user["id"]??104;
      $data = $this->api_model->get_bus_children($user_id);
      //echo $this->db->last_query();
      
      $this->response($data , 200);
    }

    function child_attend_get()
    {
      $data = $this->get();
      $data['date'] = date('Y-m-d H:i:s');
      $data['is_attend'] = $data['is_attend']=='true'?true:false;
      $this->db->insert('child_attend',$data);
      $child = $this->db->where('id', $data['child_id'])->get('children')->row_array();
      $messsage = $data['is_attend']?' صعد '.$child['child_name'].' إلى الحافلة':'لم يصعد '.$child['child_name'].' إلى الحافلة';
     // echo $message;
      $this->sendUserNotification($child['parent_id'],$messsage);
      $this->response(true , 200);
    }
    function notes_get()
    {
        $child_id = $this->get("id");
        $data = $this->db->where("child_id",$child_id)->get("child_notes")->result_array();
         $this->response($data,200);
    }
  
    
    function buses_get()
    {
        $school_id = $this->get("id");
        
        $this->db->where("related_id",$school_id);
        
        if($this->current_user['type'] == 'parent')
        {
             //$this->db->where("id",$this->current_user["related_id"]);
             $this->db->join("bus_line","bus_line.bus_id = customer.id");
             $this->db->join("bus_stop","bus_line.id = busline_id");
             
             $this->db->where("bus_stop.id",$this->current_user['stop_id']);
        }
        $this->db->where("type","bus");
         $data = $this->db->get("customer")->result_array();
         $this->response($data , 200);
    }
     function stops2_get()
    {
        $stop_id = $this->current_user["stop_id"];
        $stop_info = $this->db->where("id",$stop_id)->get("bus_stop")->row_array();
        $data =  $this->db->where("busline_id",$stop_info['busline_id'])->get("bus_stop")->result_array();
       // echo $this->db->last_query();die();
         $this->response($data , 200);
    }
     function lines_get()
    {
        $school_id = $this->current_user["related_id"];
        //$stop_info = $this->db->where("id",$stop_id)->get("bus_stop")->row_array();
        $data =  $this->db->join("customer","customer.id = bus_id")->where("customer.related_id",$school_id)->select("bus_line.*")->get("bus_line")->result_array();
       //echo $this->db->last_query();die();
         $this->response($data , 200);
    }
    function savechangeStopRequest_post()
    {
        $data = (array)$this->post();
       // print_r( $this->current_user);die();
        $data['requester_id'] =  $this->current_user["id"];
        $this->db->insert("change_stop_request",$data);
          $this->response(array("data"=>true) , 200);
    }
    
     function savechangeLineRequest_post()
    {
        $data = (array)$this->post();
       // print_r( $this->current_user);die();
        $data['requester_id'] =  $this->current_user["id"];
        $this->db->insert("change_line_request",$data);
          $this->response(array("data"=>true) , 200);
    }
    function all_stops_get()
    {
         $data =  $this->db->get("bus_stop")->result_array();
         $this->response($data , 200);
    }
    
     function all_lines_get()
    {
         $data =  $this->db->get("bus_line")->result_array();
         $this->response($data , 200);
    }
    function endDriverMoving_post()
    {
         $user_id=$this->current_user["id"];
         $movement = $this->db->where("driver_id",$user_id)->order_by("id","desc")->get("movement")->row_array();
         $this->db->where("id",$movement['id'])->update("movement",array("end"=>date("H:i:s")));
         $message = "تم وصول الباص";
         $this->db->where('bus_id', $user_id);
         $this->db->join('bus_stop', 'busline_id = bus_line.id');
         
         $this->db->join('customer', 'customer.stop_id = bus_stop.id');
         $this->db->select('customer.*');
         $users =  $this->db->get('bus_line')->result_array();
         
        // echo $this->db->last_query();
          $msg = array
           (
               'body'  => strip_tags($message),
               'title'     => "",
           );
           $notification = array(
               'body'  => strip_tags($message),
               'title'     => "حافلتي",
               "click_action"=>''
               );
        foreach ($users as $key => $user) {
         # code... 
         $android_token = $user["firebase_token"];
           $fields = array
           (
               //'to'  =>$android_token,
               "registration_ids"=> [$android_token],
               'data'          => $msg,
               'notification'=>$notification,
               'priority'=>'high',
               'android' => array( 'priority'=>'high'),
           );
            $headers = array
           (
             'Authorization: key=' . API_ACCESS_KEY,
             'Content-Type: application/json'
           );
           $ch = curl_init();
           curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
           curl_setopt( $ch,CURLOPT_POST, true );
           curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
           curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
           curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
           curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
           $result = curl_exec($ch);
           //echo($result);
           
               if($result === false)
               {
                   echo 'Curl error: ' . curl_error($ch);
               }
               else
               {
                   //echo 'Operation completed without any errors';
               }
           if($errno = curl_errno($ch)) {
                       $error_message = curl_strerror($errno);
                       echo "cURL error ({$errno}):\n {$error_message}";
                   }
           curl_close( $ch );
        }
        
       echo $result;
   
         $this->response(true , 200);
        
    }
    
    
    function index_get()
    {
        echo "welocme";
    }
    function prochoures_get()
    {
        $data = $this->api_model->getprochoures(12);
        foreach($data as &$value)
        {
            $value['images'] = $this->db->where("prochoure_id",$value['id'])->get("prochoure_images")->result_array();;
        }
        $this->response($data , 200);
    }

    function cancele_order_post()
    {
        $data = (array)$this->post();
        $order_id = $data["order_id"];
        //var_dump($order_id);
        $user_id=$this->current_user["id"];
        //var_dump($this->current_user);
        $this->db->where("id",$order_id)->update("orders",array("status"=>"canceled","canceled_by_customer"=>$user_id,"finish_date"=>date('Y-m-d H:i:s')));
        //echo $this->db->last_query();
         $result = array("data"=>true,"ErrorCode"=>200);
       $this->response($result,200); 
    }
   
    function pag_config($fun,$table,$attr,$type,$where,$val="")
    {

         $config = array();
        $config["base_url"] = base_url() . "ApiController/".$fun."?".$attr."=".$type;
        $config["total_rows"] = $this->api_model->record_count($table,$where,$val);
		    $config['page_query_string']=true;
       //var_dump($config["total_rows"] );die();
        $config["per_page"] = 4;
        $config["uri_segment"] = 3;
        $config['cur_tag_open'] = '&nbsp;<a style="background-color: darkgrey;">';

        // Close tag for CURRENT link.
        $config['cur_tag_close'] = '</a>';
        
        // By clicking on performing NEXT pagination.
       // $config['next_link'] = 'Next';
        
        // By clicking on performing PREVIOUS pagination.
        //$config['prev_link'] = 'Previous';

        $this->pagination->initialize($config);
        
         //$this->data["links"] = $this->pagination->create_links();
         return $config["total_rows"];
    }
    
     function requestReset_get()
    {
        $mobile = $this->get("mobile");
        $data["mobile"]=$mobile;
         $requestResult = $this->api_model->requestReset($mobile);
         if($requestResult==0)
         {
          $result = array("data"=>false,"ErrorCode"=>404);
         }
        else
          $result = array("data"=>true,"ErrorCode"=>200);
        $this->response($result,200); 
    }

    function notifications_get()
    {
      $user_id=$this->current_user["id"];
      $data = $this->api_model->get_notifications($user_id??1);// $this->db->where("customer_id",$user_id)->get("notifications")->result_array();
      foreach ($data as $key => &$value) {
       $value["notification_msg"]= strip_tags($value["notification_msg"]);
      }
      $result = array("data"=>$data,"ErrorCode"=>200);
      $this->response($data,200); 
    }
    function addresses_get()
   {
 // var_dump($this->current_user["id"]);die();
    $this->data["addresses"] = $this->api_model->customer_addresses(1);//$this->current_user["id"]
  
    $this->response( $this->data["addresses"]);
   }
    function reset_post()
    {
      $newPass= $this->post("password");
      $code =$this->post("code");
      $mobile =$this->post("mobile");
      $res = $this->api_model->Reset($mobile,strtolower($code),$newPass);
      if($res==0)
      {
       $result = array("data"=>false,"ErrorCode"=>404);
      }
     else
       $result = array("data"=>true,"ErrorCode"=>200);
     $this->response($result,200); 
    }

    public function userInfo_post()
    {
        $object =  (array) $this->post();
        
        $object["id"]=$user_id=($this->current_user!=null)?$this->current_user["id"]:null;
        if($user_id == null)
        {
            $result = array("data"=>null,"ErrorCode"=>403);
             $this->response($result,200); 
        }
        $user = $this->current_user;
        $authResult = $this->auth_model->updateUser($object);
        $result = array("data"=>$authResult,"ErrorCode"=>200);
        $this->response($result,200); 
    }

    public function checkActive_get()
    {
        if($this->current_user==null)
        {
              $result = array("data"=>false,"ErrorCode"=>399);//token is invalid
            $this->response($result,200); 
        }
        else
        {
             $is_active  = $this->auth_model->checkActive($this->current_user["id"]);
            //var_dump("amer ".$this->current_user["id"]);
            $result = array("data"=>$is_active,"ErrorCode"=>200);
            $this->response($result,200); 
        }
       
    }
   
  

    function slider_get()
    {
      $data  =$this->db->get("slider")->result_array();
      $result = array("data"=>$data,"ErrorCode"=>200);
      $this->response($data , 200);
    }
    function checkCard_get()
    {
        $card_number= $this->get("card_number");
        if($this->current_user==null)
        {
            $result = array("data"=>false/*$this->utf8ize($data)*/,"ErrorCode"=>400);//user not logged in
            
        }
        else
        {
            $user_id =$this->current_user["id"];
            $card = $this->api_model->checkCard($card_number);
            if($card==false)
            {
                $result = array("data"=>false/*$this->utf8ize($data)*/,"ErrorCode"=>404);//card not found
            }
            else if($card["costumer_id"]!=null && $card["costumer_id"]!=NULL && $card["costumer_id"]!="" && $card["costumer_id"]!=0)
            {
                 $result = array("data"=>false/*$this->utf8ize($data)*/,"ErrorCode"=>403);//card is used
            }
            else
            {
                $date = date('Y-m-d H:i:s');
                 $this->api_model->assign_card($user_id,$card["id"],$date);
                
                 $authResult = $this->api_model->addUserCard($user_id,$card["card_value"]);
                 $result = array("data"=>$card["card_value"]+$this->current_user["money"],"ErrorCode"=>200);
            }
        }
        
        $this->response($result , 200);
    }
    function albumProducts_get()
    {
         $album_id = $this->get("album_id");
      $data = $this->api_model->getAlbumProducts($album_id);
      $result = array("data"=>$data,"ErrorCode"=>200);
      $this->response($result , 200);
    }
    function album_images_get()
    {
      $album_id = $this->get("id");
      $data = $this->api_model->getAlbumImages($album_id);
      $result = array("data"=>$data,"ErrorCode"=>200);
      $this->response($result , 200);

    }
    function utf8ize( $mixed ) {
      if (is_array($mixed)) {
          foreach ($mixed as $key => $value) {
              $mixed[$key] = self::utf8ize($value);
          }
      } elseif (is_string($mixed)) {
       
          return iconv('WINDOWS-1256', 'UTF-8', $mixed);//self::ConvertToUTF8($mixed);
      }
      return $mixed;
  }
  
 
  function home_cats_get()
  {
      $user_id = ($this->current_user!=null)?$this->current_user["id"]:null;
      $data["home_categories"] = $this->api_model->getProductsGrouped($user_id,"",10);
       //echo "<pre>";print_r($this->data["home_categories"]);die();
       $this->response($data["home_categories"] , 200); 
  }
  
    function getProduct_get()
    {
        $id = $this->get("id");
        $user_id = ($this->current_user!=null)?$this->current_user["id"]:null;
        $data = $this->api_model->getProduct($id,$user_id);
        $data['images'] = $this->api_model->get_product_images($id);
        //$data["similar"] = $this->api_model->getReplacment($id);
       // $data["similar"] = $this->api_model->getProducts($data["product"]["category_id"],$user_id,"products.id !=".$id);
       // $result = array("data"=>$data/*$this->utf8ize($data)*/,"ErrorCode"=>200);
        $this->response($data , 200); 
    }
    function getSimilar_get()
    {
         $id = $this->get("id");
        $user_id = ($this->current_user!=null)?$this->current_user["id"]:null;
        $data1 = $this->api_model->getProduct($id,$user_id);
      
        $data = $this->api_model->getProducts($data1["category_id"],$user_id,"products.id !=".$id,"",5);
       // $result = array("data"=>$data/*$this->utf8ize($data)*/,"ErrorCode"=>200);
        $this->response($data , 200); 
    }
  function home_products_get()
  {
      $user_id = ($this->current_user!=null)?$this->current_user["id"]:null;
      $data  =$this->api_model->getProducts(null,$user_id,"","",20,0,1);
      foreach($data as &$value)
      {
       
        $value["description"] = strip_tags_content($value["description"]);
      
        $value["images"] = $this->api_model->get_product_images($value["id"]);
      }
     
    //  $result = array(0=>$data["new_products"],1=>$data["most_selles"]/*$this->utf8ize($data)*/);
      $this->response($data , 200); 
  }
    function new_products_get()
  {
      $user_id = ($this->current_user!=null)?$this->current_user["id"]:null;
      $data["new_products"]  =$this->api_model->getProducts(null,$user_id,"","",20);
      foreach($data["new_products"] as &$value)
      {
       // $value["stores"]= $this->api_model->get_product_users($value["id"]);
        $value["description"] = strip_tags_content($value["description"]);
       
        $value["images"] = $this->api_model->get_product_images($value["id"]);
        if(count($value["images"])==0)
        {
            $value["images"][] = $value['image'];
            $value["images"][] = $value['image'];
            $value["images"][] = $value['image'];
        }
        
      }
     
      //$result = array("new_products"=>$data["new_products"],"most_selles"=>$data["most_selles"]/*$this->utf8ize($data)*/);
      $this->response($data["new_products"] , 200); 
  }
    function mostsale_products_get()
  {
      $user_id = ($this->current_user!=null)?$this->current_user["id"]:null;
     
      $data["most_selles"] = $this->api_model->getProductsMostSelles($user_id,"",12);
      foreach($data["most_selles"] as &$value)
      {
       // $value["stores"]= $this->api_model->get_product_users($value["id"]);
        $value["description"] = strip_tags_content($value["description"]);
         $value["categories"] = $this->api_model->getProductSubCategories($value["id"]);
         $value["images"] = $this->api_model->get_product_images($value["id"]);
      
      }
      $result = $data["most_selles"];/*$this->utf8ize($data)*/
      $this->response($result , 200); 
  }
   
   function getChildCount($parent_id)
   {
        $childs  =$this->api_model->getCategories($parent_id);
        $max_height = 0;
        if(!count($childs))
        {
            return 0;
        }
        foreach($childs as &$val)
        {
             $x =  1+$this->getChildCount($val['id']);
             if($x > $max_height)
                $max_height = $x;
        }
        return $max_height;
   }
   function test_count_get()
   {
       $id = $this->get("id");
       echo $this->getChildCount($id);
   }
    function getCats($parent_id)
    {
      $childs  =$this->api_model->getCategories($parent_id);
       if($parent_id == NULL)
      {
           foreach($childs as &$val)
            {
                 $val["attrs"] = $this->api_model->get_attrs($val['id']);
                 
            }
      }
      $res = array();
        if(!count($childs))
        {
            return array();
        }
        else
        {
            foreach($childs as &$val)
            {
                 $val["childs"] = $this->getCats($val['id']);
                 
            }
           
        }
        return $childs;
    }
    
    
   function Categories_get()
    {
        $data = $this->getCats(NULL);
       // echo "<pre>";print_r($toto);die();
     // $data  =$this->api_model->getCategories();
     
      /*foreach($data as &$value)
      {
          $value['sub_categories']= $this->api_model->getSubCategories($value['id']);
          foreach($value['sub_categories'] as &$value2)
          {
            $value['types']= $this->api_model->getTypes($value2['id']);
          }
      }*/
     
      $this->response($data , 200); 

    }
    function SubCategories_get()
    {
      $cat_id = $this->get("cat_id");
      $data  =$this->api_model->getSubCategories($cat_id);
      //$result = array("data"=>$data,"ErrorCode"=>200);
     
      $this->response($data , 200); 

    }
    
    function product_get()
    {
        $id = $this->get("id");
         $user_id = ($this->current_user!=null)?$this->current_user["id"]:null;
        $data = $this->api_model->getProduct($id,$user_id);
        $data["description"] = strip_tags($data["description"]);
       
          $data['images'] = $this->api_model->product_images($id);
         $this->response($data , 200); 
    }
    function products_get()
    {
      $category_id = $this->get("id");
      $child_count =$category_id?$this->getChildCount($category_id):0;
     $user_id = ($this->current_user!=null)?$this->current_user["id"]:null;
     $like = $this->get("search")?$this->get("search"):"";
      $data  =$this->api_model->getProducts($category_id,$user_id,"",$like,100,0,0,$child_count);
      foreach($data as &$value)
      {
       
        $value["description"] = strip_tags($value["description"]);
        
      }
      $result = array("data"=>$data/*$this->utf8ize($data)*/,"ErrorCode"=>200);
      $this->response($data , 200); 
    }
    
    function Myproducts_get()
    {
      
      $user_id = ($this->current_user!=null)?$this->current_user["id"]:null;
      $data  =$this->api_model->getProducts(null,null,"store_id = ".$user_id);
      foreach($data as &$value)
      {
        $value["description"] = strip_tags($value["description"]);
        
      }
      $result = array("data"=>$data/*$this->utf8ize($data)*/,"ErrorCode"=>200);
      $this->response($data , 200); 
    }
    function myFavourite_get()
    {
      
      $user_id = ($this->current_user!=null)?$this->current_user["id"]:null;
      $data  =$this->api_model->getProducts(null,$user_id,"IF(favourite.product_id is NULL, 0,1) = 1");
      foreach($data as &$value)
      {
        $value["description"] = strip_tags($value["description"]);
      
      }
      $result = array("data"=>$data/*$this->utf8ize($data)*/,"ErrorCode"=>200);
      $this->response($data , 200); 
    }
    
   function storeproducts_get()
    {
      $id = $this->get("id");
       $user_id = ($this->current_user!=null)?$this->current_user["id"]:null;
      $category_id = $this->get('category_id')?$this->get('category_id'):null;
    $this->data["products"] = $this->api_model->getProducts($category_id,$user_id,"store_id=$id");
      $this->data["shop_categories"] = $this->api_model->getShopCategories($id);
      //print_r( $this->data["products"]);
      //print_r( $this->data["shop_categories"]);die();
      $this->data['main_content'] = "shop_products";
      //$result = array("data"=>$data/*$this->utf8ize($data)*/,"ErrorCode"=>200);
      $this->response($this->data["products"] , 200); 
    }
    function myOrders_get()
    {
        
        
        $user_id = ($this->current_user!=null && $this->current_user['is_active']==1)?$this->current_user["id"]:null;
        if($user_id == null)
        {
             $result = array("data"=>null/*$this->utf8ize($data)*/,"ErrorCode"=>0);
            $this->response($result , 200); 
        }
     /*$where = "customer_id = ".$user_id;
      $count = $this->api_model->record_count("product_order",$where,"");
      $page = ($this->input->get("page")&&($user_id!=null)) ? $this->input->get("page") : 0;*/
      $page=0;
      $data  =$this->api_model->getMyOrders($user_id);
      $result = array("data"=>$data/*$this->utf8ize($data)*/,"ErrorCode"=>200);
      $this->response($result , 200); 
        
    }
    
    function orderItems_get()
    {
        $order_id = $this->get("order_id");
        $data = $this->api_model->getorderItems($order_id);
        $result = array("data"=>$data/*$this->utf8ize($data)*/,"ErrorCode"=>200);
      $this->response($result , 200); 
    }
    function search_get()
    {
      $search = $this->get();
      $user_id = ($this->current_user!=null && $this->current_user['is_active']==1)?$this->current_user["id"]:null;
     // var_dump($search);
      //$count = $this->api_model->record_count("KT_MS_ART",$search["search_for"],"");
      $page = ($this->input->get("page")) ? $this->input->get("page") : 0;
      $data  =$this->api_model->searchProducts($search["search_for"],$search["filter"],$user_id);
      foreach($data as &$value)
      {
        $value["stores"]= $this->api_model->get_product_users($value["id"]);
        $value["details"] = strip_tags($value["details"]);
        $value["details"] = str_replace("\r", '', $value["details"]);
         $value["details"] = str_replace("\n", ' ', $value["details"]);
         $value["details"] = str_replace("\t", ' ', $value["details"]);
          $value["details"] = str_replace("&nbsp;", ' ', $value["details"]);
         
        $value["details_eng"] = strip_tags($value["details_eng"]);
         $value["details_eng"] = str_replace("\r", '', $value["details_eng"]);
         $value["details_eng"] = str_replace("\n", ' ', $value["details_eng"]);
         $value["details_eng"] = str_replace("\t", ' ', $value["details_eng"]);
        $value["details_eng"] = str_replace("&nbsp;", ' ', $value["details_eng"]);
      }
      //echo $this->db->last_query();
      $result = array("data"=>$data,"ErrorCode"=>200);
      $this->response($result , 200); 
    }

    

    
    function countries_get()
    {
      $data  =$this->api_model->getCountries();
      $result = array("data"=>$data/*$this->utf8ize($data)*/,"ErrorCode"=>200);
      $this->response($result , 200); 
    }
    function cities_get()
    {
      $country_id=$this->get("country_id");
      $data  =$this->api_model->getCities();
      $result = array("data"=>$data/*$this->utf8ize($data)*/,"ErrorCode"=>200);
      $this->response($data , 200); 

    }

    function colorsList_get()
    {
      $data  =$this->api_model->getColors2();
      $result = array("data"=>$data,"ErrorCode"=>200);
      $this->response($result , 200); 
    }




    function policy_get()
    {
      $url = $this->api_model->getConfigVal("001");
      $url["content_en"] = strip_tags($url["content_en"]);
      $url["content_ar"] = strip_tags($url["content_ar"]);
      $result = array("data"=>$url/*$this->utf8ize($data)*/,"ErrorCode"=>200);
      $this->response($result , 200);
    }
    
    function conditions_get()
    {
      $url = $this->api_model->getConfigVal("002");
       $url["content_en"] = strip_tags($url["content_en"]);
      $url["content_ar"] = strip_tags($url["content_ar"]);
      $result = array("data"=>$url/*$this->utf8ize($data)*/,"ErrorCode"=>200);
      $this->response($url , 200);
    }
    
    function about_get()
    {
      $url = $this->api_model->getConfigVal("003");
       $url["content_en"] = strip_tags($url["content_en"]);
      $url["content_ar"] = strip_tags($url["content_ar"]);
      $result = array("data"=>$url/*$this->utf8ize($data)*/,"ErrorCode"=>200);
      $this->response($url , 200);
    }
    function gallery_get()
    {
         $data  =$this->api_model->getGallery();
      $result = array("data"=>$data,"ErrorCode"=>200);
      $this->response($result , 200); 
    }
    function productBranches_get()
    {
        $ArticleCodeId = $this->get("ArticleCodeId");
      $data  =$this->api_model->getProductBranches($ArticleCodeId);
      $result = array("data"=>$data/*$this->utf8ize($data)*/,"ErrorCode"=>200);
      $this->response($result , 200);
    }
   function delivery_config_get()
    {
      $data  =$this->db->get("delivery_config")->row_array();
      $data["payments_method"]  =$this->db->get("payment_method")->result_array();
      $data["delivery_locations"]  =$this->db->get("location")->result_array();
      $result = array("data"=>$data/*$this->utf8ize($data)*/,"ErrorCode"=>200);
      $this->response($result , 200);
    }
    
    function states_get()
    {
        $city_id = $this->get("id");
        $data = $this->db->where("city_id",$city_id)->get("state")->result_array();
        $result = array("data"=>$data/*$this->utf8ize($data)*/,"ErrorCode"=>200);
      $this->response($data , 200);
    }
    
    function locations_get()
    {
        $id = $this->get("id");
        $data = $this->db->where("city_id",$id)->get("location")->result_array();
        $result = array("data"=>$data/*$this->utf8ize($data)*/,"ErrorCode"=>200);
      $this->response($result , 200);
    }
    function generate_string($input, $strength = 10)
    {
      $input_length = strlen($input);
      $random_string = '';
      for ($i = 0; $i < $strength; $i++) {
        $random_character = $input[mt_rand(0, $input_length - 1)];
        $random_string .= $random_character;
      }
    
      return $random_string;
    }
 private function upload_files($title, $files)
  {
    $permitted_chars = '123456789';
    $string_length = 6;
    $target_dir = "uploads/" ;
    $countfiles = count($files['name']);
	  $res = [];
    // Looping all files
    for ($i = 0; $i < $countfiles; $i++) {
      if ($files['name'][$i] != '') {
        $filename = $files['name'][$i];
        $rand_string = $this->generate_string($permitted_chars, $string_length);

        $target_file = $target_dir . basename($files['name'][$i]);
        $FileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
        $file_name = $title . "_" . $rand_string . '.' . $FileType;
        $target_file = $target_dir . $title . "_" . $rand_string . '.' . $FileType;

        // Upload file
        move_uploaded_file($files['tmp_name'][$i], $target_file);
        $res[] = $file_name;
      }
    }
    return $res;
  }
  
  
   private function upload_file($title, $files)
  {
    $permitted_chars = '123456789';
    $string_length = 6;
    $target_dir = "uploads/" ;
    
    $res = '';
      if ($files['name'] != '') {
        $filename = $files['name'];
        $rand_string = $this->generate_string($permitted_chars, $string_length);

        $target_file = $target_dir . basename($files['name']);
        $FileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
        $file_name = $title . "_" . $rand_string . '.' . $FileType;
        $target_file = $target_dir . $title . "_" . $rand_string . '.' . $FileType;

        // Upload file
        move_uploaded_file($files['tmp_name'], $target_file);
        $res = $file_name;
      }
    
    return $res;
  }
  
  
    function product_post()
    {
        $data = (array)$this->post();
        $user_id = $this->current_user['id'];
        
        $files = $this->upload_files(time(), $_FILES["images"]);
        if(count($files))
        {
            $data['image'] = $files[0];
        }
        if(isset($_FILES["video"]))
        {
              $video = $this->upload_file(time(), $_FILES["video"]);
            if($video != '')
            {
                $data['video'] = $video;
            }
        }
       
        $data['store_id'] = $user_id;
        $product_id = $this->api_model->save_product($data);
        $i=0;
        //print_r($data);
		foreach ($files as $file) {
		  $file_data['product_id'] = $product_id;
		  $file_data['image'] = $file;
		  //var_dump($file_data);
		  $this->db->insert('images', $file_data);
		  $i++;
		}
    
      
      $this->response(array("result"=>$product_id,"erroDescription"=>'added success'));
       
    }
  function order_post()
    {
      $data = (array)$this->post();
     
      
      $products = $data["products"];
      //var_dump($products);die();
       $user_id = ($this->current_user!=null && $this->current_user['is_active']==1)?$this->current_user["id"]:null;
       if($user_id==null)
       {
            $result = array("data"=>array("messsage"=>"user not logged in"),"ErrorCode"=>403);
             $this->response($result,200); 
       }
       $user = $this->current_user;
       foreach ($products as $key => $item) {
        $stores[$item["store_id"]]["store_id"] = $item["store_id"];
         $stores[$item["store_id"]]["total_quantity"] += $item["quantity"];
         $stores[$item["store_id"]]["total_price"] += $item["quantity"];
         $stores[$item["store_id"]]["customer_id"] =  $user_id;
         $stores[$item["store_id"]]["items"][$item["item_id"]]["item_id"] = $item["item_id"];
         $stores[$item["store_id"]]["items"][$item["item_id"]]["quantity"] = $item["quantity"];
         $stores[$item["store_id"]]["items"][$item["item_id"]]["price"] = $item["price"];
       }
       $COUNT = 0 ;
       foreach ($stores as $key => $value) {
          
          $products = $value["items"];
          unset($value["items"]);
          $order_id = $this->api_model->add_order($value);
          $i=0;
          $COUNT++;
          $product_data = array();
          foreach ($products as $key => &$item) {
            $product_data[$i] = $item;
            $product = $this->api_model->getProduct($item["item_id"]);
            $product_data[$i]["order_id"] = $order_id;
            $product_data[$i]["price"] = $product["price"];
            $i++;
          }
          $this->api_model->add_order_products($product_data);
       }
      $result = array("order_number"=>$COUNT,"ErrorCode"=>200);
      $this->response($result,200); 
    }
    
    function favourite_post()
    {
        $data = $this->post();
        $data["costumer_id"]=$this->current_user["id"];
        if( $data["costumer_id"]==null)
        {
              $result = array("data"=>false/*$this->utf8ize($data)*/,"ErrorCode"=>400);//user not logged in
            $this->response($result , 200);
        }
        else
        {
           // var_dump($data);
            $this->api_model->add_favourite($data);
            $result = array("data"=>true/*$this->utf8ize($data)*/,"ErrorCode"=>200);
            $this->response($result , 200);
        }
        
    }
     function un_favourite_post()
    {
        $data = $this->post();
        $data["costumer_id"]=$this->current_user["id"];
        if( $data["costumer_id"]==null)
        {
              $result = array("data"=>false/*$this->utf8ize($data)*/,"ErrorCode"=>400);//user not logged in
            $this->response($result , 200);
        }
        else
        {
           // var_dump($data);
            $this->api_model->delete_favourite($data["costumer_id"],$data["product_id"]);
            $result = array("data"=>true/*$this->utf8ize($data)*/,"ErrorCode"=>200);
            $this->response($result , 200);
        }
        
    }
    function user_favourites_get()
    {
        $user_id  = $this->current_user["id"];
        
      
         $data  =$this->api_model->user_favourites($user_id);
         //echo $this->db->last_query();
      $result = array("data"=>$data,"ErrorCode"=>200);
      $this->response($result , 200);
    }
    function unFavourite_post()
    {
        $user_id = $this->current_user["id"];
        $product_id = $this->post("product_id");
       // var_dump($this->post());
       // var_dump($product_id." ".$user_id);
        $this->api_model->delete_favourite($user_id,$product_id);
            $result = array("data"=>true/*$this->utf8ize($data)*/,"ErrorCode"=>200);
            $this->response($result , 200);
        
    }
    
     function like_post()//liked_product_id
    {
        $data = $this->post();
        $data["costumer_id"]=$this->current_user["id"];
        if( $data["costumer_id"]==null)
        {
              $result = array("data"=>false/*$this->utf8ize($data)*/,"ErrorCode"=>400);//user not logged in
            $this->response($result , 200);
        }
        else
        {
            $this->api_model->add_like($data);
            $points = $this->api_model->getPoints(array("001"));
            $this->api_model->addUserPoints($data["costumer_id"],$points);
            $this->api_model->addPoint_history($data["costumer_id"],array("001"));
            $result = array("data"=>true/*$this->utf8ize($data)*/,"ErrorCode"=>200);
            $this->response($result , 200);
        }
        
    }
    function user_likes_get()
    {
        $user_id  = $this->current_user["id"];
         $where = "costumer_id = $user_id";
      $count = $this->api_model->record_count("likes",$where,"");
      $page = ($this->input->get("page")&&($user_id!=null)) ? $this->input->get("page") : 0;
         $data  =$this->api_model->user_likes($user_id,10,$page);
      $result = array("data"=>$data,"count"=>$count,"ErrorCode"=>200);
      $this->response($result , 200);
    }
    function delete_product_post()
    {
        $data = $this->post();
        $id = $data['id'];
        $this->db->where("id",$id)->update("products",array("is_deleted"=>1));
         $this->response(true , 200);
    }
    function unLike_post()
    {
        $user_id = $this->current_user["id"];
        $product_id = $this->post("liked_product_id");
        $this->api_model->delete_like($user_id,$product_id);

        $points = $this->api_model->getPoints(array("001"));
        $this->api_model->RemoveUserPoints($user_id,$points);
            $result = array("data"=>true/*$this->utf8ize($data)*/,"ErrorCode"=>200);
            $this->response($result , 200);
        
    }
    
    
    function Requset_post()
    {
        $all_object = (array)$this->post();
        $request_data["user_id"] =  $this->current_user["id"];
        $request_id = $this->api_model->saveRequest( $request_data);
        foreach ($all_object as  &$value) {
          # code...
          $value["request_id"] = $request_id;
        }
        $this->api_model->saveRequestProduct($all_object);
        $result = array("message"=>$request_id,"error"=>false);
        $this->response($result , 200);
    }

  
   
  



    function addTransaction_post()
    {
        // header('Content-type: bitmap; charset=utf-8');
      $target_dir = "uploads/";

      $this->load->library('image_lib');

    	$all_object = (array)$this->post();
      $object = json_decode($all_object["data"]);
      $object=(array)$object;
      $error = "false" ;
      $transaction_data = (array)$object["data"];
    //  var_dump($transaction_data);die();
      unset($object["data"]);
      //var_dump($object);
      $object["from_id"] = $this->current_user["id"];
      $transId= $this->api_model->saveTransaction($object);

      //var_dump($transaction_data);
      $result  =$transaction_data ;
      if(isset($transaction_data[0])&&!is_object($transaction_data[0]))
        $result  = json_decode($transaction_data[0]);
    //  var_dump($result);die();
      foreach ($result as  $item) {
        $item = (array)$item;
        
        if(isset($item["barcode"]))
        {
          var_dump($item["barcode"]);
          $item_info =$this->api_model->getitem($item["barcode"],true);
          //var_dump($item_info);die();
          if($item_info["id"] == NULL)
          {
            $toSend =  array('status' => false , "ErrorCode"=>404,"message"=>"item not found");
          }
          $item["item_id"]=$item_info["id"];
          $barcode_item = $item["barcode"];
          unset($item["barcode"]);
        }
        //var_dump($item);die();
        $files = (array)$item["ITEM_FIELS"];
        unset($item["ITEM_FIELS"]);
        $item["transaction_id"]=$transId;
        if(isset($object["parent_id"]))
        {
          $item["item_status"]="recieved";
        }
        //var_dump($item);//die();
        $itemId= $this->api_model->saveitems($item);
       /* if(isset($object["parent_id"]))
        {h
          $check = $this->api_model->checkANDupdate($object["parent_id"],$item["SN"],$barcode_item,$itemId);
          if($check == true)
          {
            //$this->api->checkANDUpdateRequest($object["parent_id"]);
          }
        }*/
       $files = array_map(function($item) use ($itemId){
                  $item = (array)$item;
                  $item['RequestItem_id'] = $itemId;
                  return $item;
                  }, $files);
        foreach ($files as $value) {
          $filename=$value["file_name"];
          unset($value["file_name"]);
          $value["image"]=basename($_FILES[$filename]["name"]);
          $this->api_model->savefile($value);
           $target_file = $target_dir . basename($_FILES[$filename]["name"]);
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
       
            //$check = getimagesize($_FILES[$filename]["tmp_name"]);
            //var_dump($_FILES[$filename]["tmp_name"]);
            if(!move_uploaded_file($_FILES[$filename]["tmp_name"], $target_file))
              $error .= $_FILES[$filename]['error'] ;
            // chmod($target_file, 7777);
           $config['image_library'] = 'gd2';
          $config['source_image'] =$target_file;
       
          $config['new_image'] = $target_dir."thumbnil/".basename($_FILES[$filename]["name"]);
          $config['width']     = 240;
          $config['height']   = 320;
        //  $this->image_lib->clear();
          $this->image_lib->initialize($config);
          $this->image_lib->resize();

        }
      }
    	 $this->response(array("result"=>$transId,"error"=>$error) , 200);
    }
   

}