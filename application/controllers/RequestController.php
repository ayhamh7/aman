<?php
ini_set('memory_limit', '-1');
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/REST_Controller.php');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class RequestController extends REST_Controller {

   function __construct()
    {
        // Construct the parent class
        parent::__construct();
        //$this->set_timezone();
        //header('content-Type: text/html; charset=utf-8');
      //  header('Content-Type: application/json; charset=utf-8');
      //  $this->load->model('api_model');  // to fix
    
      $this->load->helper("request_helper");
      
        $this->load->helper("request");
    }

   
    function index_get()
    {
      
    }
   
    public static function convert_to_utf8_recursively($dat)
    {
       if (is_string($dat)) {
          return iconv('WINDOWS-1256', 'UTF-8',$dat);
       } elseif (is_array($dat)) {
          $ret = [];
          foreach ($dat as $i => $d) $ret[ $i ] = self::convert_to_utf8_recursively($d);
 
          return $ret;
       } elseif (is_object($dat)) {
          foreach ($dat as $i => $d) $dat->$i = self::convert_to_utf8_recursively($d);
 
          return $dat;
       } else {
          return $dat;
       }
    }



   
    function hotels_get()
    {
      
      $result = hotels();
      print_r($result);
    }
    
    function bookings_get()
    {
     
        $result = bookings();
      print_r($result);
    }

    function search_get()
    {
      $result = search();
      print_r($result);
    }
   
   

}