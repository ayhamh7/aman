<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/REST_Controller.php');
 
class Users extends REST_Controller {

	public  function __construct(){
        parent::__construct();
        $this->load->model('login_model');
        $this->load->model('api_model');
        $this->load->library("auth");
        $this->load->library('cart');
        $this->load->helper('message');
        $this->data["active_menue"] = "";
        $this->current_user=$this->data["user"]=null;
        if($this->auth->loggedin())
        {
          $this->current_user=$this->auth->user();
        }
        else
        {
            
            redirect(base_url(),'refresh');
            
        }
        if(isset($_SESSION["lang"]))
        {
          $lang = $_SESSION["lang"];
        }
        $this->data["suffix"]='';
      if(isset($lang)){
          if($lang=="ar")
              $this->lang->load("message", "arabic");
          else
             {
              $this->lang->load("message", "english");
              $this->data["suffix"] = "_en";
             } 
          
          $this->data["lang"]=$lang;
      }
      else{
          $this->lang->load("message", "english");
          $this->data["lang"]="en";
          $this->data["suffix"] = "_en";
      }

     
       $this->data["user"] = $this->current_user;
       $this->data["error"] = '';
       $this->data["categories"] = $this->api_model->getCategories();
       foreach ($this->data["categories"] as $key => &$value) {
           $value["sub_categories"] =  $this->api_model->getSubCategories($value["id"]);
       }

    } 

   function profile_get()
   {
    $this->data["user"] = $this->login_model->get($this->current_user["id"]);
    $this->data["orders"] = $this->api_model->getMyOrders($this->current_user["id"]);
    $this->data["addresses"] = $this->api_model->customer_addresses($this->current_user["id"]);
    //var_dump( $this->data["user"]);die();
    $this->data['main_content'] = "profile";

    $this->load->view('includes-web/main',$this->data);
   }

   function edit_profile_get()
   {
    $this->data["edit"]=1;
    $this->data["user"] = $this->login_model->get($this->current_user["id"]);
    //var_dump( $this->data["user"]);die();
    $this->data['main_content'] = "profile";

    $this->load->view('includes-web/main',$this->data);
   }
   function edit_profile_post()
   {
      
       $data = (array)$this->post();
       $data["id"] =  $this->data["user"]["id"];
       //var_dump($data);die();
        $this->form_validation->set_rules("name", "Name", "required|max_length[50]|min_length[3]");
       $this->form_validation->set_rules("mobile", "Mobile", "required");
       if($this->form_validation->run() == FALSE)
       {
           $this->edit_profile_get();
           return;
       }
        $this->api_model->edit_profile($data);
        redirect(base_url("profile"));
   }
   function rate_post()
   {
        $id =$data["product_id"] = $this->post("product_id");
        if($this->current_user)
        {
        $user_id = $data["user_id"] = $this->current_user["id"];
        $data["rate"] = $this->post("rate");
        $this->api_model->add_rate($data);
       // echo $this->db->last_query();
        
        $this->response(array("data"=>true,"message"=>lang("added_success"),"errorCode"=>200));
        }
        else{
        $this->response(array("data"=>false,"message"=>lang("not_logged_in"),"errorCode"=>404));
        }
   }
   
   

   function add_wishlist_get()
   {
     $id =$data["product_id"] = $this->get("id");
     if($this->current_user)
     {
       $user_id = $data["costumer_id"] = $this->current_user["id"];
       $this->api_model->add_favourite($data);
       $this->response(array("data"=>true,"message"=>lang("added_success"),"errorCode"=>200));
     }
     else{
       $this->response(array("data"=>false,"message"=>lang("not_logged_in"),"errorCode"=>404));
     }
   

   }

   function checkout_get()
   {
        if($this->current_user)
        {
            $user_id = $data["customer_id"] = $this->current_user["id"];
            $this->data["customer_info"] = $this->current_user;
            $this->data["addresses"] = $this->api_model->customer_addresses($this->current_user["id"]);
            $this->data["coupons"] = $this->api_model->customer_coupons($this->current_user["id"],$this->cart->total());
            //echo $this->db->last_query();die();
            
            $i=0;
            $total = 0;
            $this->data["products"] = array();
            foreach ($this->cart->contents() as $key => $value)
            {
                //var_dump($value);die();
                $this->data["products"][$i]  = $this->api_model->get_product($value["id"]);
                if(!$this->data["products"][$i])
                {
                        unset($this->data["products"][$i]);
                   
                }
                else
                {
                    $this->data["products"][$i]["subtotal"] = $this->cart->format_number($value['subtotal']);;
                    $this->data["products"][$i]["qty"] = $value['qty'];;
                     //$total += $this->data["products"][$i]["subtotal"];
                     $i++;
                }
                
            }
           
            $this->data["total"] = $this->cart->format_number($this->cart->total());
            $this->data['main_content'] = "checkout";
        }
        else
        {
            
            redirect(base_url('Login'),'refresh');
            
        }
       $this->load->view('includes-web/main',$this->data);
   }


   function checkout2_get()
   {
        if($this->current_user)
        {
            $user_id = $data["customer_id"] = $this->current_user["id"];
            $data["customer_info"] = $this->current_user;
            $i=0;
            $total = 0;
            foreach ($_SESSION["cart_products"] as $key => $value)
            {
                $this->data["products"][$i]  = $this->api_model->$this->api_model->get_product($value["id"]);
                if(!$this->data[$i]["product"])
                {
                        unset($this->data[$i]["product"]);
                    $this->data["products"][$i]["subtotal"] = $this->data["products"][$i]["price"]*$value["quantity"];
                     $total += $this->data["products"][$i]["subtotal"];
                }

            }
            $this->data["total"] = $total;
            $this->data['main_content'] = "checkout";
        }
        else
        {
            
            redirect(base_url('Login'),'refresh');
            
        }
       $this->load->view('includes-web/main',$this->data);
   }
   function wishlist_get()
   {
     if($this->current_user)
     {
       $user_id = $data["customer_id"] = $this->current_user["id"];
       $this->data["products"]  = $this->api_model->get_wishlist($user_id);
       $this->data['main_content'] = "wishlist";
     }
     else
     {
       $this->data['main_content'] = "404";
     }
        $this->load->view('includes-web/main',$this->data);
   }

   function wishlist_post()
   {
     $product_id = $this->post("id");
     $customer_id = $this->current_user["id"];
     $this->api_model->delete_wishlist($product_id,$customer_id);
     $this->response(true);
   }
   
    public function user_post()
    {
        $object =  (array) $this->post();
        $authResult = $this->login_model->update($this->current_user["id"],$object);
        $result = array("message"=>true,"ErrorCode"=>200);
        redirect(base_url("profile"));
    }

    function order_info_get()
    {
        $id = $this->get("id");
        $user_id = $this->current_user["id"];
        $this->data["products"] = $this->api_model->OrderInfo($user_id,$id);
        //print_r( $this->data["products"] );die();
        $this->data['main_content'] = "order_info";
        $this->load->view('includes-web/main',$this->data);

    }
    function order_post()
    {
        $data = (array)$this->post();
        //print_r($data);die();
      $products = $this->cart->contents();
     
        if($data["address_text"]!="")
        {
            $addr_key = "address_temp";
            $addr = $data["address_text"];
        }
        else
        {
            $addr_key = "address_id";
            $addr = $data["address"];
        }
    
       $user = $this->current_user;
       $user_id = $user["id"];
       foreach ($products as $key => $item) {
        $product = $this->api_model->get_product($item["id"]);
        $stores[$product["store_id"]]["store_id"] = $product["store_id"];
        if(!isset( $stores[$product["store_id"]]["total_quantity"]))
        {
            $stores[$product["store_id"]]["total_quantity"] = 0;
            $stores[$product["store_id"]]["total_price"] =0;
        }
         $stores[$product["store_id"]]["total_quantity"] += $item["qty"];
         $stores[$product["store_id"]]["total_price"] += $item["subtotal"];
         $stores[$product["store_id"]]["customer_id"] =  $user_id;
         $stores[$product["store_id"]]["items"][$item["id"]]["item_id"] = $item["id"];
         $stores[$product["store_id"]]["items"][$item["id"]]["quantity"] = $item["qty"];
         $stores[$product["store_id"]]["items"][$item["id"]]["price"] = $item["price"];
       }
       $COUNT = 0 ;
       foreach ($stores as $key => &$value) {
          
          $products = $value["items"];
          unset($value["items"]);
          $value[$addr_key] =$addr;
          $order_id = $this->api_model->add_order($value);
          $i=0;
          $COUNT++;
          $product_data = array();
          foreach ($products as $key => &$item) {
            $product_data[$i] = $item;
            $product_data[$i]["order_id"] = $order_id;
            $i++;
          }
          $this->api_model->add_order_products($product_data);
       }

       foreach($this->cart->contents() as $item){
            $data = array(
                'rowid' => $item['rowid'],
                'qty'   => 0
            );
        $this->cart->update($data);    
        }
      
      redirect(base_url('thank_you'));
      
    }
}