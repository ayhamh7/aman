<?php
require(APPPATH.'libraries/REST_Controller.php');
class Login extends REST_Controller {
	
	function __construct() {
		parent::__construct();
		//var_dump("e");die();
		$this->load->model('login_model');
		$this->load->model('api_model');
		$this->load->library("auth");
			$lang=$this->session->userdata('lang');
			$this->load->library('form_validation');
        $this->load->helper('message_helper');
        if(isset($lang)){
            if($lang=="ar")
                $this->lang->load("message", "arabic");
            else
                $this->lang->load("message", "english");
            
            $this->data["lang"]=$lang;
        }
        else{
            $this->lang->load("message", "english");
            $this->data["lang"]="en";
		}
		// header('Content-Type: text/html; charset=utf-8');
		
	}
	
	function index_get()
	{
		if($this->auth->loggedin())
		{
		    redirect(base_url('AgencyController'));
		}
		$this->data["title"] = "Login";
	    if($this->get("error"))
	    {
	        $this->data["error"]= $this->get("error");
	    }
	$this->data["sliders"] = $this->api_model->get_sliders();
	  $this->data["title"] = "Home Page";
      $this->data['main_content'] = "index";

        $this->load->view('includes-web/main',$this->data);
	}
	
    function register_get()
    {
        $this->data["title"] = "Register";
          $this->data['main_content'] = "register";

        $this->load->view('includes-web/main',$this->data);
    }
	function user_get()
	{
	
		$this->load->view('login');
			
	}
	
	 function arabic_e2w($str)
    {
        $arabic_eastern = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');
        $arabic_western = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
        return str_replace($arabic_eastern, $arabic_western, $str);
    }
	function log_in_post()
	{
	    $this->data["title"] = "Login";
		if ($this->post('mobile')) {
            $remember = $this->post('remember') ? TRUE : FALSE;
            $user = $this->login_model->get('mobile', $this->post('mobile'));
            
            if ($user) {
			   // $this->auth->login($user['id'], $remember,$user);
                if ($this->login_model->check_password($this->input->post('password'), $user['password'])) {//$this->post('password')== $user['password']
                    // mark user as logged in
                    unset( $user['password']);
                    if($user["is_active"])
                    {
                        	$this->auth->login($user['id'], $remember,$user);
					        redirect(strip_query($_SERVER['HTTP_REFERER'],"error_msg"));
                    }
				    else
				    {
				        $error = lang('login.activate_error');
				        //redirect($_SERVER['HTTP_REFERER'].(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_QUERY) ? '&' : '?') . "error_msg=$error");
                       // $this->load->view('login', array('error' => $error));
				    }
			       
                } else {
                     $error = lang('login.wrong_password');
                     //redirect($_SERVER['HTTP_REFERER'].(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_QUERY) ? '&' : '?') . "error_msg=$error");
                    //$this->load->view('login', array('error' => $error));
                }
            } 
            else
            {
                 $error = lang('login.user_doesnt_exist_error');
                 //redirect(base_url("Login?error=".$error));
               // $this->load->view('login', array('error' => $error));
            }
        }
        else
		    $error = lang('login.user_doesnt_exist_error');
        redirect($_SERVER['HTTP_REFERER'].(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_QUERY) ? '&' : '?') . "error_msg=$error");
		
	}
	
	function login_post()
	{
		if ($this->post('email')) {
            $user = $this->login_model->get('email', $this->post('email'));
            
            if ($user) {
                if ($this->login_model->check_password($this->input->post('password'), $user['password'])) {//$this->post('password')== $user['password']
                   
                    unset( $user['password']);
                    $this->db->where("id",$user['id'])->update("customer",array("is_new"=>0));
                    if($user["is_active"])
                    {
                         if($this->post("firebase_token"))
                        {
                            $this->db->where("id",$user['id'])->update("customer",array("firebase_token"=>$this->post("firebase_token")));
                        }
                        $token = bin2hex(openssl_random_pseudo_bytes(16));
                        $user['token'] = $token;
	                    $toAdd =  array('user_id'=>$user["id"]  , 'token'=>$token);
                        //$this->db->where("id",$user["id"])->delete("token");
    		        	$this->db->insert('token',$toAdd);
    		        	
    		        	$this->response($user,200);
					       
                    }
				    else
				    {
				        $this->response(array("message"=>"not active"),403);
				        
				    }
			       
                } else {
                     $this->response(array("message"=>"incorrect password"),404);
                    
                }
            } 
            else
            {
                 $this->response(array("message"=>"incorrect password"),404);
                
            }
        }
        else
		    $this->response(array("message"=>"incorrect mobile"),404);
      
		
	}
	function register_web_post()
	{
	    $this->data["title"] = "Register";
	    $this->form_validation->set_rules("email", "Email", "required|max_length[50]|min_length[3]|valid_email");
        $this->form_validation->set_rules("mobile", "Mobile", "required");
        $this->form_validation->set_rules("name", "Name", "required");
        $this->form_validation->set_rules("password", "Password", "required");
        ///$this->form_validation->set_rules('confirm', 'Confirm Password', 'required|matches[password]');
        $this->form_validation->set_rules("address", "Address", "required");
        
        if($this->form_validation->run() == FALSE)
        {
            $error = strip_tags(validation_errors());//lang('login.fill_required_field');
            redirect($_SERVER['HTTP_REFERER'].(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_QUERY) ? '&' : '?') . "error_msg=$error");
        }
	    $p_data = $this->input->post();
	    
	    $user = $this->login_model->get('email', $this->input->post('email'));
            $this->data["title"] = "Register";
            if ($user) {
                 $error = lang("login.user_exist_error");
                 redirect($_SERVER['HTTP_REFERER'].(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_QUERY) ? '&' : '?') . "error_msg=$error");
            }else
            {
                  $user = (array)$this->post();
                  unset($user["confirm"]);
                    /*$config['upload_path']          = 'uploads/';
                    $config['allowed_types']        = 'gif|jpg|png|jpeg';
                    $config['max_size']             = 2048000;
                    $config['max_width']            = 2048;
                    $config['max_height']           = 2048;
                    $new_name = time()."_".$_FILES["fileToUpload"]['name'];
                    $config['file_name'] = $new_name;
                $this->load->library('upload', $config);
                if($this->upload->do_upload("fileToUpload"))
                    {
                       $user["logo"] = $this->upload->data('file_name');
                     }*/
              
              
                $a=$this->login_model->insert_return_user($user);
                $this->auth->login($a["id"], FALSE,$a);
                
                redirect(strip_query($_SERVER['HTTP_REFERER'],"error_msg"));
                
               // $a=$this->login_model->insert($user);
                 
            }
	}
	function register_post()
	{
	    $user = $this->post();
	   
		$user['mobile'] = $this->arabic_e2w($user['mobile']);
	    $user = $this->login_model->get('mobile', $this->input->post('mobile'));
            if ($user) {
                $result = array("data"=>false,"errorCode"=>300,"errorMsg"=>"البريد الالكتروني موجود مسبقاً");
            }else
            {
                $user = $this->post();
                unset($user["confirm"]);
                 $random = uniqid();
                 $user["activation_code"]=$random;
                 $user['email'] = trim($user['email']);
		        $user['mobile'] = trim($user['mobile']);
                $user_res=$this->login_model->insert_return_user($user);
                $token = bin2hex(openssl_random_pseudo_bytes(16));
                $user['token'] = $token;
                $toAdd =  array('id' => $user_res["id"],'user_id'=>$user_res["id"]  , 'token'=>$token);
	        	$this->db->insert('token',$toAdd);
    	       // $to_email = $user["email"];
            //   $this->load->library('Notification');
            //   $message = "شكرا لتسجبلكم في تطبيق دلال لتفعيل حسابكم يرجى الضغط على الرابط التالي "." ".base_url("activate?activation_key=".$random);
            //   $this->notification->send_mail($to_email,"رمز تفعيل حساب تطبيق دلال",$message);
    	      $result = array("data"=>$user,"errorCode"=>200,"errorMsg"=>"");
                 
            }
            $this->response($result,200);	
	}
	function forget_password_post()
	{
		$data = (array)$this->post();
		$data['email'] = trim($data['email']);
		 $check_email = $this->login_model->get('email', $this->input->post('email'));
		 if(!$check_email)
		 {
			// $this->session->set_flashdata('message', 'البريد الالكتروني غير موجود مسبقا');
			  $result = array("data"=>false,"errorCode"=>404,"errorMsg"=>"البريد الالكتروني غير موجود مسبقاً");
      		
		 }else
		 {
			 $random =substr(str_shuffle(time()."123456789"), 0, 6);
			 $udata["reset_code"]=$random;
			 $this->db->where('email',$data['email'])->update('customer',$udata);
			 $to_email = $data["email"];
			  $this->load->library('Notification');
			  $message = "لاستعادة كلمة السر يمكنكم ادخال الرمز التالي في واجهة استعادة كلمة السر : "." ".$random;
			  $this->notification->send_mail($to_email,"رمز استعادة كلمة السر في تطبيق دلال",$message);
		     $result = array("data"=>true,"errorCode"=>200,"errorMsg"=>"البريد الالكتروني غير موجود مسبقاً");
		 }
		 $this->response($result,200);	
	}
	function reset_post()
   	{
		
		
			$data = (array)$this->post();
			$user = $this->login_model->get('reset_code', $data['reset_key']);
			if($user)
		    {	
		        $this->login_model->update($user['id'],$data["new_password"]);
    		    $this->db->where("reset_code",$data['reset_key'])->update("login",array("password"=>$data["new_password"]));
    			$res = $this->db->affected_rows();
    			 $result = array("data"=>true,"errorCode"=>200,"errorMsg"=>"");
		        
		    }
		 	else
		 	{
		 	    $msg = 'رمز التفعيل خاطئ يرجى التحقق من الايميل';
		 	     $result = array("data"=>false,"errorCode"=>404,"errorMsg"=>$msg);
		 	}
     
   
   	}
	function logout_get()
	{
		$this->auth->logout();
		
		redirect(base_url());
	}
	

}