<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('API_ACCESS_KEY', 'BCnQ6wy8_A0eQYLIqw-3_GlAw_MGZ_9JIsi4noLjr3JAW05AwVB52m6ISSNdmAsZv57jbho5jDPMTNsRDqwOEUQ'); //AIzaSyBLV2ZQQFHy0lR7T-LUFOl-CyoWFiHe9q0
class SchoolDashboard extends CI_Controller
{

	/**
	 * Index Page for this controller.manage_types
	 *csINVISIBLEstore
	 * Maps to the following URL
	 * 		http://example.com/index.php/dabookinshboard
	 *	- or -iadd_balance
	 * 		http://example.com/index.php/SchoolDashbbusstopoard/index
	 *	- or -pr
	 * Since this controller is set as the default controller ing
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/SchoolDashboard/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
	{
		parent::__construct();
		// if(strtotime("2019/04/24") <= time())
		// 	die();
		$this->load->database();
		$this->load->helper('url', 'message');
		$this->load->library('grocery_CRUD');
		$this->load->library("ion_auth");
		$this->load->model("api_model");
		if (!$this->ion_auth->logged_in()) {
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		$lang = $this->session->userdata('lang');

		if (isset($lang)) {
			if ($lang == "ar")
				$this->lang->load("message", "arabic");
			else
				$this->lang->load("message", "english");

			$this->data["lang"] = $lang;
		} else {
			$this->lang->load("message", "arabic");
			$this->data["lang"] = "ar";
		}



		// $unactive_count = $this->db->select("count(*) as visit_count")->from("agency")->where("is_active",0)->get()->result();
		$this->data["unactive_agency"] = 0;

		// $unactive_count = $this->db->select("count(*) as visit_count")->from("agency")->get()->result();
		$this->data["all"] = 0;

		// $unactive_count = $this->db->select("count(*) as visit_count")->where("status",0)->from("balance_request")->get()->result();
		$this->data["pending"] = 0;

		// $unactive_users = $this->db->select("count(*) as visit_count")->from("customer")->where("is_active",0)->get()->result();
		// $this->data["unactive"]=$unactive_users[0]->visit_count;;

		// $active_users = $this->db->select("count(*) as visit_count")->from("customer")->where("is_active",1)->get()->result();
		// $this->data["active"]=$active_users[0]->visit_count;;


		// $items = $this->db->select("count(*) as visit_count")->from("item")->where("has_real_image",0)->get()->result();
		// $this->data["not_image"]=$items[0]->visit_count;;
		//var_dump($this->db->last_query());
		
		$this->crud = new grocery_CRUD();
		$this->crud->set_theme('bootstrap');
		$this->crud->set_language("arabic");
		//$this->crud->unset_delete();
		$this->db->where("email",$this->ion_auth->user()->row_array()["email"]);
        $this->school_id = $this->db->get("customer")->row_array()['id'];
		if(!$this->school_id)
		{
			
			redirect(base_url('auth/logout'),'refresh');
			
		}
        $this->data['controller']= 'SchoolDashboard';

	}


	public function index()
	{
		//$this->load->view("dashboard/index.php");
		redirect(base_url('SchoolDashboard/manageParents'));
	}



	function test_notification()
	{
		$message = "تجربة";
		$this->sendNotification($message, 1, 1);
	}

	function test_allnotification()
	{
		$message = "تجربة الجميع";
		$this->sendNotification_all($message);
	}
	function _users_output($output = null)
	{
		$this->load->view('example.php', $output);
	}


	
	function encrypt_password_callback2($post_array, $primary_key = null)
	{
		$this->load->library("auth");
		$this->load->model("ion_auth_model");
		$post_array['password'] = $this->ion_auth_model->hash_password($post_array['password']);
		//$post_array['is_active'] = 1;
		return $post_array;
	}

	function manage_changestop_request()
	{
		//change_stop_request
		
		$this->data["title"] = 'طلب تغيير موقف';
		$this->data["ads_active"] = true;
		$this->crud->where("school_id",$this->school_id);
		$this->crud->set_table('change_stop_request');
		$this->crud->set_relation('requester_id', 'customer', '{name}-{mobile}');
		$this->crud->order_by("request_date","DESC");
		$output = $this->crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";

		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
		
	}
	
	
	function manage_changebus_request()
	{
		//change_stop_request
		
		$this->data["title"] = 'طلب تغيير خط باص';
		$this->data["ads_active"] = true;
		$this->crud->where("school_id",$this->school_id);
		
		$this->crud->set_language('arabic');
		$this->crud->set_table('change_line_request');
		$this->crud->required_fields("title", "image");
		$this->crud->set_relation('requester_id', 'customer', '{name}-{mobile}');
		$this->crud->order_by("request_date","DESC");
		$output = $this->crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";

		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
		
	}



	
	function manageSchools()
	{
		$this->data['customer_active'] = true;
		
		
		
		$this->data['title'] = "إدارة  المدارس";
		$this->crud->set_table('customer');
		$this->crud->unset_add();
		$this->crud->unset_edit();
		$this->crud->where("id",$this->school_id);
		$this->crud->where("customer.type","school");
		$this->crud->add_action('عرض الباصات', '', '','ui-icon-image',array($this,'school_buses_action'));
		$this->crud->add_action('عرض الأهل', '', '','ui-icon-image',array($this,'school_parents_action'));
		$this->crud->add_action('عرض خطوط النقل', '', '','ui-icon-image',array($this,'manageSchoolLines_action'));
		$this->crud->order_by('id', 'desc');
		$this->crud->set_field_upload('image', 'uploads');
		
		$this->crud->display_as("name", "الاسم");
		$this->crud->display_as("mobile", "الموبايل");
		$this->crud->display_as("address", "العنوان");
		$this->crud->display_as("nationalImage","صورة الهوية");
		$this->crud->set_field_upload('nationalImage', 'uploads');
		$this->crud->display_as("register_date", "تاريخ التسجيل");
		$this->crud->display_as("email", "الايميل");
		$this->crud->callback_before_insert(array($this,'encrypt_password_callback'));
		$this->crud->unset_columns("stp_id","reset_code","is_new","related_id","android_token", "activation_code", "password", "created_date", "firebase_token");
		$this->crud->unset_edit_fields("stop_id","reset_code","is_new","related_id","android_token", "activation_code", "password", "created_date", "firebase_token");
		$this->crud->unset_add_fields("stop_id","reset_code","is_new","related_id","android_token", "activation_code", "created_date", "firebase_token");
		//$this->crud->unset_add();
		$output = $this->crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";
		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}
	function manageSchoolLines_action($primary_key, $row)
	{
	    
	    return base_url('/SchoolDashboard/manageSchoolLines') . '/' . $row->id;
	}
	
	function school_buses_action($primary_key, $row)
	{
		return base_url('/SchoolDashboard/manageSchoolBuses') . '/' . $row->id;
	}
	function school_parents_action($primary_key, $row)
	{
		return base_url('/SchoolDashboard/manageSchoolParents') . '/' . $row->id;
	}
	
	
	function teacher_info_action($primary_key,$row)
    {
        return base_url('/SchoolDashboard/teacher_info') . '/' . $row->id;
    }
	
   
	
	function manageSchoolBuses($school_id= NULL)
	{
		$this->data['customer_active'] = true;
		
			$school_id = $this->school_id;
		$this->data['title'] = "إدارة  باصات مدرسة";
		$this->crud->set_table('customer');
		$this->crud->where("customer.type","bus");
		$this->crud->where("related_id",$school_id);
		$this->crud->field_type('related_id', 'hidden', $school_id);
		//$this->crud->set_relation("related_id","customer","{name}-{mobile}");
		//$this->crud->add_action('View Coupons', '', '','ui-icon-image',array($this,'customer_coupon_action'));
		$this->crud->order_by('id', 'desc');
		$this->crud->set_field_upload('image', 'uploads');
		$this->crud->add_action('إدارة الخطوط', '', '','ui-icon-image',array($this,'bus_line_action'));
		$this->crud->display_as("name", "الاسم");
		$this->crud->display_as("mobile", "الموبايل");
		$this->crud->display_as("address", "العنوان");
		$this->crud->display_as("type","نوع الحساب");
		$this->crud->display_as("related_id","المدرسة التابع لها");
		$this->crud->display_as("nationalImage","صورة الهوية");
		$this->crud->set_field_upload('nationalImage', 'uploads');
		$this->crud->display_as("register_date", "تاريخ التسجيل");
		$this->crud->display_as("email", "الايميل");
		$this->crud->callback_before_insert(array($this,'encrypt_password_callback'));
		$this->crud->unset_columns("stop_id","related_id","reset_code","is_new","android_token", "activation_code", "password", "created_date", "firebase_token");
		$this->crud->unset_edit_fields("stop_id","reset_code","is_new","android_token", "activation_code", "password", "created_date", "firebase_token");
		$this->crud->unset_add_fields("stop_id","reset_code","is_new","android_token", "activation_code", "created_date", "firebase_token");
	
		$this->crud->unset_edit();
		$this->crud->unset_delete();
		$output = $this->crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";
		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}
	function manageSchoolParents($school_id = NULL)
	{
		$this->data['customer_active'] = true;
		
		$school_id = $this->school_id;
		$this->data['title'] = "إدارة  حسابات الاهل لمدرسة";
		$this->crud->set_table('customer');
		$this->crud->where("customer.type","parent");
		$this->crud->where("related_id",$school_id);
		$this->crud->field_type('related_id', 'hidden', $school_id);
		$this->crud->set_relation("stop_id","bus_stop","{stop_name}-{address}");
		//$this->crud->add_action('View Coupons', '', '','ui-icon-image',array($this,'customer_coupon_action'));
		$this->crud->order_by('id', 'desc');
		$this->crud->set_field_upload('image', 'uploads');
		$this->crud->add_action('الأطفال', '', '','ui-icon-image',array($this,'children_action'));
		$this->crud->display_as("name", "الاسم");
		$this->crud->display_as("mobile", "الموبايل");
		$this->crud->display_as("address", "العنوان");
		$this->crud->display_as("type","نوع الحساب");
		$this->crud->display_as("related_id","المدرسة التابع لها");
		$this->crud->display_as("nationalImage","صورة الهوية");
		$this->crud->set_field_upload('nationalImage', 'uploads');
		$this->crud->display_as("register_date", "تاريخ التسجيل");
		$this->crud->display_as("email", "الايميل");
		$this->crud->callback_before_insert(array($this,'encrypt_password_callback'));
		$this->crud->unset_columns("reset_code","is_new","android_token", "activation_code", "password", "created_date", "firebase_token");
		$this->crud->unset_edit_fields("reset_code","is_new","android_token", "activation_code", "password", "created_date", "firebase_token");
		$this->crud->unset_add_fields("reset_code","is_new","android_token", "activation_code", "created_date", "firebase_token");
		//$this->crud->unset_add();
		
		$output = $this->crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";
		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}
	function manageBuses()
	{
		$this->data['customer_active'] = true;
		
		
		$this->data['title'] = "إدارة  الباصات";
		$this->crud->set_table('customer');
		$this->crud->where("customer.type","bus");
		$this->crud->set_relation("related_id","customer","{name}-{mobile}","type = 'school'");
		$this->crud->add_action('إدارة الخطوط', '', '','ui-icon-image',array($this,'bus_line_action'));
		$this->crud->order_by('id', 'desc');
		$this->crud->set_field_upload('image', 'uploads');
		
		$this->crud->display_as("name", "الاسم");
		$this->crud->display_as("mobile", "الموبايل");
		$this->crud->display_as("address", "العنوان");
		$this->crud->display_as("type","نوع الحساب");
		$this->crud->display_as("related_id","المدرسة التابع لها");
		$this->crud->display_as("nationalImage","صورة الهوية");
		$this->crud->set_field_upload('nationalImage', 'uploads');
		$this->crud->display_as("register_date", "تاريخ التسجيل");
		$this->crud->display_as("email", "الايميل");
		$this->crud->callback_before_insert(array($this,'encrypt_password_callback'));
		$this->crud->unset_columns("reset_code","is_new","android_token", "activation_code", "password", "created_date", "firebase_token");
		$this->crud->unset_edit_fields("stop_id","reset_code","is_new","android_token", "activation_code", "password", "created_date", "firebase_token");
		$this->crud->unset_add_fields("stop_id","reset_code","is_new","android_token", "activation_code", "created_date", "firebase_token");
		$output = $this->crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";
		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}
	function bus_line_action($primary_key, $row)
	{
		return base_url('/SchoolDashboard/manageBusLine') . '/' . $row->id;
	}
	function manageBusLine($bus_id)
	{
		$this->data['customer_active'] = true;
		
		
		
		$this->data['title'] = "إدارة خط باص";
		$this->crud->set_table('bus_line');
		$this->crud->add_action('إدارة المواقف', '', '','ui-icon-image',array($this,'bus_stop_action'));
		$this->crud->where("bus_id",$bus_id);
		$this->crud->field_type('bus_id', 'hidden', $bus_id);
		$this->crud->field_type('school_id', 'hidden', $this->school_id);
		$this->crud->display_as("stop_name", "اسم الموقف");
		$this->crud->display_as("address", "عنوان الموقف");
		$this->crud->display_as("time_arrivals", "أوقات الوصول");
		$output = $this->crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";
		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}
	function manageSchoolLines($school_id = NULL)
	{
		$this->data['customer_active'] = true;
		
		$school_id = $this->school_id;
		
		$this->data['title'] = "إدارة خط باص";
		$this->crud->set_table('bus_line');
		$this->crud->add_action('إدارة المواقف', '', '','ui-icon-image',array($this,'bus_stop_action'));
		$this->crud->where("school_id",$school_id);
		$this->crud->field_type('school_id', 'hidden', $school_id);
		$this->crud->display_as("stop_name", "اسم الموقف");
		$this->crud->display_as("address", "عنوان الموقف");
		$this->crud->display_as("time_arrivals", "أوقات الوصول");
		$this->crud->set_relation("bus_id","customer","{name}-{mobile}","type = 'bus' and related_id = ".$school_id);
		$output = $this->crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";
		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}
	function bus_stop_action($primary_key, $row)
	{
		return base_url('/SchoolDashboard/manageBusStop') . '/' . $row->id;
	}
	function manageBusStop($busline_id)
	{
		$this->data['customer_active'] = true;
		//var_dump($busline_id);die();
		
		
		$this->data['title'] = "إدارة مواقف باص";
		$this->crud->set_table('bus_stop');
		$this->crud->where("busline_id",$busline_id);
		$this->crud->field_type('busline_id', 'hidden', $busline_id);
		$this->crud->display_as("address", "عنوان الموقف");
		$this->crud->display_as("time_arrivals", "أوقات الوصول");
		$output = $this->crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "map-mangment";
		$this->data['stops'] = $this->db->where("busline_id",$busline_id)->get("bus_stop")->result_array();
		$this->data['busline_id'] = $busline_id;
		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}
	
	function saveStop()
	{
	    $data = $this->input->post();
	    $this->db->insert("bus_stop",$data);
	    redirect($_SERVER['HTTP_REFERER']);
	}

	
	function manageParents()
	{
		$this->data['customer_active'] = true;
		
		
		$this->data['title'] = "إدارة  حسابات الأهل";
		$this->crud->set_table('customer');
		$this->crud->where("customer.type","parent");
		$this->crud->set_relation("related_id","customer","{name}-{mobile}","type = 'school'");
		$this->crud->set_relation("stop_id","bus_stop","{stop_name}-{address}");
		$this->crud->add_action('الأطفال', '', '','ui-icon-image',array($this,'children_action'));
		$this->crud->order_by('id', 'desc');
		$this->crud->set_field_upload('image', 'uploads');
		
		$this->crud->display_as("name", "الاسم");
		$this->crud->display_as("mobile", "الموبايل");
		$this->crud->display_as("address", "العنوان");
		$this->crud->display_as("type","نوع الحساب");
		$this->crud->display_as("related_id","المدرسة التابع لها");
		$this->crud->display_as("nationalImage","صورة الهوية");
		$this->crud->set_field_upload('nationalImage', 'uploads');
		$this->crud->display_as("register_date", "تاريخ التسجيل");
		$this->crud->callback_before_insert(array($this,'encrypt_password_callback'));
		$this->crud->display_as("email", "الايميل");
		$this->crud->unset_columns("reset_code","is_new","android_token", "activation_code", "password", "created_date", "firebase_token");
		$this->crud->unset_edit_fields("reset_code","is_new","android_token", "activation_code", "password", "created_date", "firebase_token");
		$this->crud->unset_add_fields("reset_code","is_new","android_token", "activation_code", "created_date", "firebase_token");
		//$this->crud->unset_add();
		$output = $this->crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";
		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}
	function children_action($primary_key, $row)
	{
	    return base_url('/SchoolDashboard/managechildren') . '/' . $row->id;
	}
    
   
	
	function manageStudent()
	{
	    $this->data['customer_active'] = true;
		
		
		$this->data['title'] = "إدارة  الطلاب";
		$this->crud->set_table('children');
		//$this->crud->where("parent_id",$id);
		 //$this->crud->field_type('parent_id', 'hidden', $id);
		 $classes = $this->db->where("school_id",$this->school_id)->get("classes")->result_array();
		foreach($classes as $val )
		{
		    $class_ids[] = $val['id'];
		}
		$this->crud->set_relation("section_id","section","{section_description}","class_id in (".implode(",",$class_ids).")");
		$this->crud->set_relation("parent_id","customer","{name}","related_id=".$this->school_id);
		$this->crud->order_by('id', 'desc');
		
		$this->crud->add_action('التأخير والغياب', '', '','ui-icon-image',array($this,'child_late_action'));
		
		$this->crud->display_as("child_name", "الاسم");
	
		$output = $this->crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";
		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}
	
	function managechildren($id)
	{
	    $this->data['customer_active'] = true;
		
		$this->data['title'] = "إدارة  حسابات الأطفال";
		$this->crud->set_table('children');
		$this->crud->where("parent_id",$id);
		 $this->crud->field_type('parent_id', 'hidden', $id);
		//$this->crud->set_relation("section_id","section","{section_description}","class_id in (".implode(",",$class_ids).")");
		$this->crud->order_by('id', 'desc');
		
		$this->crud->display_as("child_name", "الاسم");
	
		$output = $this->crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";
		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}
	function child_notes_action($primary_key,$row)
	{
	    return base_url('/SchoolDashboard/manageChildNotes') . '/' . $row->id;
	}
	function child_late_action($primary_key,$row)
	{
	     return base_url('/SchoolDashboard/manageChildLates') . '/' . $row->id;
	}
	function child_info_action($primary_key,$row)
	{
	     return base_url('/SchoolDashboard/child_info') . '/' . $row->id;
	}
	
	function manageChildLates($id)
	{
	    $this->data['customer_active'] = true;
		
		
		$this->data['title'] = "إدارة  تأخيرات الطفل ";
		$this->crud->set_table('late_table');
		$this->crud->where("child_id",$id);
	    $this->crud->field_type('child_id', 'hidden', $id);
		$this->crud->order_by('id', 'desc');
		
	
		$output = $this->crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";
		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}
	

	function encrypt_password_callback($post_array, $primary_key = null)
	{
		$this->load->library("auth");
		$this->load->model("login_model");
		$post_array['password'] = $this->login_model->hash($post_array['password']);
		$post_array['is_active'] = 1;
		return $post_array;
	}

	function reset_password_action($primary_key, $row)
	{
		return base_url('/SchoolDashboard/reset_password') . '/' . $row->id . "/replace";
	}

	function reset_password($agency_id)
	{
		$this->load->library("auth");
		$this->load->model("login_model");
		$pass = $this->login_model->hash("12345678");
		$this->db->where("id", $agency_id)->update("agency", array("password" => $pass));
		redirect($_SERVER['HTTP_REFERER'] . '/success/1', 'refresh');
	}
	function activate($agency_id)
	{
		$this->db->where("id", $agency_id);
		$this->db->update("agency", array("is_active" => 1));
		redirect($_SERVER['HTTP_REFERER']);
	}
	function deactivate($agency_id)
	{
		$this->db->where("id", $agency_id);
		$this->db->update("agency", array("is_active" => 0));
		redirect($_SERVER['HTTP_REFERER']);
	}

	function page404()
	{
		$this->data['main_content'] = "404";
		$this->load->view('includes/main', $this->data);
	}



	function manageConfig()
	{
		$this->data["title"] = lang("manage_config");
		
		
		$this->crud->set_table('config');
		$this->crud->field_type('key', 'readonly');
		$this->crud->unset_add();
		$output = $this->crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";

		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}



	public function sendNotification($message, $user_id, $order_id = null, $allow_insert = true)
	{
		//var_dump($user_id);
		$user = $this->db->where("id", $user_id)->get("customer")->result_array()[0];
		if ($allow_insert)
			$this->db->insert("general_notifications", array("customer_id" => $user_id, "notification_msg" => $message));
		$android_token = $user["android_token"];
		// var_dump(API_ACCESS_KEY);
		// $ios_token = $user["ios_token"];
		$msg = array(
			'body'  => strip_tags($message),
			'title'     => "One-Deals",
		);
		$notification = array(
			'body'  => strip_tags($message),
			'title'     => "One-Deals",
			"click_action" => ''
		);
		if ($order_id) {
			$msg["order_id"] = $order_id;
		}
		$fields = array(
			//'to'  =>$android_token,
			"registration_ids" => [$android_token],
			'data'          => $msg,
			'notification' => $notification,
			'priority' => 'high'
		);



		$headers = array(
			'Authorization: key=' . API_ACCESS_KEY,
			'Content-Type: application/json'
		);
		//echo API_ACCESS_KEY;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		$result = curl_exec($ch);
		//echo($result);

		if ($result === false) {
			echo 'Curl error: ' . curl_error($ch);
		} else {
			//echo 'Operation completed without any errors';
		}
		if ($errno = curl_errno($ch)) {
			$error_message = curl_strerror($errno);
			echo "cURL error ({$errno}):\n {$error_message}";
		}
		curl_close($ch);
		//die();
		/*
		    TODO exho result
		*/
		//echo $result;

	}

	public function sendNotification_all($message, $topic = "all")
	{
		//var_dump($user_id);


		$msg = array(
			'body'  => strip_tags($message),
			'title'     => "Dllaal-app",
		);

		$notification = array(
			'body'  => strip_tags($message),
			'title'     => "Dllaal-app",
			"click_action" => ''
		);
		$fields = array(
			'to' 	=> '/topics/' . $topic,
			'data'			=> $msg,
			'notification' => $notification,
		);


		$headers = array(
			'Authorization: key=' . API_ACCESS_KEY,
			'Content-Type: application/json'
		);
		//echo API_ACCESS_KEY;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		$result = curl_exec($ch);
		//	echo($result);

		if ($result === false) {
			echo 'Curl error: ' . curl_error($ch);
		} else {
			//echo 'Operation completed without any errors';
		}
		if ($errno = curl_errno($ch)) {
			$error_message = curl_strerror($errno);
			echo "cURL error ({$errno}):\n {$error_message}";
		}
		curl_close($ch);
		//		die();
		/*
		    TODO exho result
		*/
		//	echo $result;

	}



	function manage_customer_notifications($user_id)
	{
		
		
		$this->data['title'] = "Customer Notification";
		$this->crud->set_table('general_notifications');
		$this->crud->order_by('id', 'desc');
		$this->crud->where("customer_id", $user_id);
		$this->crud->field_type('customer_id', 'hidden', $user_id);
		$this->crud->fields('customer_id', "notification_msg");
		$this->crud->callback_after_insert(array($this, 'sendNotification_user'));
		//$this->crud->callback_after_update(array($this, 'sendNotification_user'));

		$output = $this->crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";
		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}
	function sendNotification_user($post_array, $primary_key)
	{
		//print_r($post_array);
		$this->sendNotification($post_array["notification_msg"], $post_array["customer_id"], null, false);
		return true;
	}



	function manage_general_notifications()
	{
		
		
		$this->data['title'] = "General Notifications";
		$this->crud->set_table('general_notifications');
		$this->crud->order_by('id', 'desc');    
        $this->crud->field_type('school_id', 'hidden', $this->school_id);
	    $this->crud->where("school_id",$this->school_id);
		$this->crud->callback_after_insert(array($this, 'sendNotification_alluser'));
		//$this->crud->callback_after_update(array($this, 'sendNotification_user'));

		$output = $this->crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";
		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}
	function sendNotification_alluser($post_array, $primary_key)
	{
		//print_r($post_array);
		$this->sendNotification_all($post_array["message"]);
		return true;
	}

	function sendAcivateNotification($post_array, $primary_key)
	{
		//print_r($post_array);
		$this->sendNotification("لقد تم تفعيل حسابكم..يرجى استكمال المعلومات الخاصة بكم", $primary_key);

		return true;
	}


	function album_products($primary_key, $row)
	{
		return base_url('/SchoolDashboard/manage_album_products') . '/' . $row->id;
	}

	function manage_album_products($album_id)
	{

		
		$this->crud->set_theme('datatables');
		$this->data['title'] = "product gallery management";
		$this->crud->set_table('kt_ms_art');
		$this->crud->columns('ArticleCodeId', 'ArticleName1Id', 'Color', 'Size', 'Remarks');
		$this->crud->fields('ArticleCodeId', 'ArticleName1Id', 'Color', 'Size', 'Remarks');
		$this->crud->where("Remarks", $album_id);
		$this->crud->set_relation('Remarks', 'album', 'name');
		$this->crud->unset_add();
		//$this->crud->set_relation('city_id','cities','name');
		$this->crud->unset_columns(array('ip', 'password'));
		$this->crud->order_by('id', 'desc');
		$output = $this->crud->render();
		$this->data['output'] = $output;
		$data['main_content'] = "management";
		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}
	function setAlbum($post_array, $primary_key)
	{
		$this->db->where("ArticleCodeId Between '" . $post_array["from_id"] . "' and '" . $post_array["to_id"] . "'")->update("kt_ms_art", array("Remarks" => $primary_key));

		return true;
	}


	function album($primary_key, $row)
	{
		return base_url('/SchoolDashboard/manage_album') . '/' . $row->id;
	}

	function manage_album($product_id)
	{
		$this->load->library('image_CRUD');
		$image_crud = new image_CRUD();
		$this->data['title'] = "إدارة الألبوم";
		$image_crud->set_table('images');

		$this->data['is_images'] = true;
		$image_crud->set_url_field('image');

		$image_crud->set_relation_field('product_id')
			->set_image_path('uploads');

		$output = $image_crud->render();
		// var_dump($output);die();
		$this->data['output'] = $output;
		$data['main_content'] = 'management';
		//$this->load->view('includes/template',$data);
		$this->load->view($data['main_content'], $this->data);
	}






	function decrypt_password_callback($value)
	{
		$this->load->library('encrypt');
		$key = 'bcb04b4a8b6a0cffe54763945cef08bc88abe000fdebae5e1d417e2ffb2a12a3';
		$decrypted_password = $this->encrypt->decode($value, $key);
		//var_dump($decrypted_password);die();
		return "<input type='password' name='password' value='$decrypted_password' />";
	}
	function compress_callback_after_upload($uploader_response, $field_info, $files_to_upload)
	{
		$this->load->library('image_moo');
		//var_dump($field_info->upload_path);die();
		//Is only one file uploaded so it ok to use it with $uploader_response[0].
		$file_uploaded = $field_info->upload_path . '/' . $uploader_response[0]->name;
		$target = $field_info->upload_path . '/thumb/' . $uploader_response[0]->name;
		$this->image_moo->load($file_uploaded)->resize(250, 250)->save($target, true);

		//$this->image_moo->load($file_uploaded)->resize(250,250)->save($target,true);

		return true;
	}



	function import_callback_after_upload($uploader_response, $field_info, $files_to_upload)
	{
		$file = fopen($field_info->upload_path . '/' . $uploader_response[0]->name, "r");
		$i = 0;
		while (!feof($file)) {
			//print_r(fgetcsv($file));
			$arr = fgetcsv($file);
			$data[$i]["number"] = $arr[0];
			$data[$i]["card_value"] = $arr[1];
			$i++;
		}
		$this->db->insert_batch("card", $data);
		fclose($file);
		//$this->image_moo->load($file_uploaded)->resize(250,250)->save($target,true);

		return true;
	}


	public static function convert_to_utf8_recursively($dat)
	{
		if (is_string($dat)) {
			return iconv('WINDOWS-1256', 'UTF-8', $dat);
		} elseif (is_array($dat)) {
			$ret = [];
			foreach ($dat as $i => $d) $ret[$i] = self::convert_to_utf8_recursively($d);

			return $ret;
		} elseif (is_object($dat)) {
			foreach ($dat as $i => $d) $dat->$i = self::convert_to_utf8_recursively($d);

			return $dat;
		} else {
			return $dat;
		}
	}
}
