<?php
ini_set('max_execution_time', 6000);
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require(APPPATH.'libraries/REST_Controller.php');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Products extends REST_Controller {

  function __construct()
  {
      // Construct the parent class
      parent::__construct();
      //$this->set_timezone();
      //header('content-Type: text/html; charset=utf-8');
    //  header('Content-Type: application/json; charset=utf-8');
    //  $this->load->model('api_model');  // to fix
    $this->load->library("pagination");
    $this->load->library('cart');
    
    $this->load->model("login_model");
      $this->load->library("auth");
      $this->load->helper("request_helper");
      $this->load->model('api_model');
      $this->load->model('auth_model');
       $this->load->helper('message_helper');
       $this->load->library('form_validation');
      $this->current_user=$this->data["user"]=null;
      //die();
      if($this->auth->loggedin())
      {
        $this->current_user=$this->auth->user();
      }
     
      $lang=$this->session->userdata('lang');
      $this->data["suffix"]='';
      if(isset($lang)){
          if($lang=="ar")
              $this->lang->load("message", "arabic");
          else
             {
              $this->lang->load("message", "english");
              $this->data["suffix"] = "_en";
             } 
          
          $this->data["lang"]=$lang;
      }
      else{
          $this->lang->load("message", "english");
          $this->data["lang"]="en";
          $this->data["suffix"] = "_en";
      }
  
    //	var_dump($this->current_user);die();
    $this->data["active_menue"] = "";
      $this->data["user"] = $this->current_user;
      $this->data["error"] = '';
      $this->data["categories"] = $this->api_model->getCategories();
      foreach ($this->data["categories"] as $key => &$value) {
          $value["sub_categories"] =  $this->api_model->getSubCategories($value["id"]);
      }
      //$this->data["sliders"]= $this->api_model->get_sliders();
      //$this->data["sections"]= $this->api_model->get_categories();
  }

  function index_get()
  {
    $this->data["active_menue"] = "home";
     $this->data["sliders"] = $this->api_model->get_sliders();
     $this->data["partners"] = $this->api_model->get_partners();
      $this->data["title"] = "Home Page";
    $this->data["show_menu"] = true;
      $this->data["ads"] = $this->api_model->get_ads(12);
      $this->data["sideads_header"] = $this->api_model->get_sideads('position="header"',2);
      $this->data["sideads_main"] = $this->api_model->get_sideads('position="main"',6);
      $user_id = $this->current_user?$this->current_user["id"]:null;
       $this->data["new_products"] = $this->api_model->getProducts(null,$user_id,"",12);
       //var_dump(($this->data["new_products"]));die();
       $this->data["most_selles"] = $this->api_model->getProductsMostSelles($user_id,"",12);
        $this->data["prochoures"] = $this->api_model->getprochoures(12);
       $this->data["day_offers"] = $this->api_model->getProductsGrouped($user_id,"is_day_offer=1",10);
       $this->data["week_offers"] = $this->api_model->getProductsGrouped($user_id,"is_week_offer=1",10);
       $this->data["home_categories"] = $this->api_model->getProductsGrouped($user_id,"",10);
       //echo "<pre>";print_r($this->data["home_categories"]);die();
       $this->data["home_shops"] =  $this->api_model->get_stores("show_inhome=1"); 
      //print_r($this->data["home_category"]);die();
      $this->data['main_content'] = "index";
      $this->load->view('includes-web/main',$this->data);
      
  }
  function prochoure_get()
  {
      $id = $this->get("id");
      $this->data['images'] = $this->db->where("prochoure_id",$id)->get("prochoure_images")->result_array();
      $this->data['main_content'] = "prochoure";
      $this->load->view('includes-web/main',$this->data);
  }
  function shops_get()
  {
    $this->load->library('ion_auth');
    $this->data["active_menue"] = "shops";
      $this->data["shops"] = $this->api_model->getShops();// $this->ion_auth->users('store')->result_array();
      //var_dump( $this->data["shops"]);die();
      foreach ($this->data["shops"] as $key => &$value) {
          $value["categories"]= $this->api_model->get_store_categories($value["id"]);
      }
      
      $this->data['main_content'] = "shops";
    $this->load->view('includes-web/main',$this->data);
  }
  function shop_get()
  {
    $this->data["active_menue"] = "shops";
      $id =  $this->get("id");
      $user_id = $this->current_user?$this->current_user["id"]:null;
      $this->load->library('ion_auth');
      $this->data["shop"] = $this->api_model->getShop($id);//$this->ion_auth->user($id)->row_array();
      $page = ($this->input->get("per_page")) ? $this->input->get("per_page") : 0;
      
      $config = array();
      $config["base_url"] = base_url() . "shop?id=".$id;
      $config["total_rows"] = $this->api_model->products_record_count(null,null,"store_id=$id");

      $config['page_query_string']=true;
      $config["per_page"] = 12;
      $config["uri_segment"] = 2;
      $config['cur_tag_open'] = '&nbsp;<li class="page-item active" aria-current="page"><a class="page-link">';
      $config['data_page_attr'] = 'class = "page-link"';
      $config['cur_tag_close'] = '</li></a>';
      //For PREVIOUS PAGE Setup
        $config['prev_link'] = 'prev';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        
        //For NEXT PAGE Setup
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
      $this->pagination->initialize($config);
      $this->data["links"] = $this->pagination->create_links();
      
      $this->data["products"] = $this->api_model->getProducts(null,$user_id,"store_id=$id",12,$page);
      $this->data["shop_categories"] = $this->api_model->getShopCategories($id);
      //print_r( $this->data["products"]);
      //print_r( $this->data["shop_categories"]);die();
      $this->data['main_content'] = "shop_products";
    $this->load->view('includes-web/main',$this->data);
  }


    
    public function shipping_get()
    {
      $this->data['main_content'] = "shipping";
  
      $this->load->view('includes-web/main',$this->data);
    }
    function offers_get()
    {

      $page = ($this->input->get("per_page")) ? $this->input->get("per_page") : 0;
      $config = array();
      $config["base_url"] = base_url() . "Products/offers";

      $config["total_rows"] = $this->api_model->offers_record_count();
      $config['page_query_string']=true;
      $config["per_page"] = 12;
      $config["uri_segment"] = 3;
      $config['cur_tag_open'] = '&nbsp;<a style="background-color: darkgrey;">';
      $config['cur_tag_close'] = '</a>';
      $this->pagination->initialize($config);
      $this->data["links"] = $this->pagination->create_links();


      $this->data["products"] = $this->api_model->get_offers(12,$page);
      foreach ($this->data["products"] as $key => &$value2) {
         $value2["rate"] = $this->api_model->get_product_rate($value2["id"]);
      }
      $this->data["title"] = "Offers";
      $this->data["viewed_products"] = $this->api_model->get_most_viewed_products();
      foreach ($this->data["viewed_products"] as $key => &$value2) {
        $value2["rate"] = $this->api_model->get_product_rate($value2["id"]);
     }
      $this->data['main_content'] = "listing";

      $this->load->view('includes-web/main',$this->data);
    }


    function newProducts_get()
    {
      $user_id = $this->current_user?$this->current_user["id"]:null;
      $this->data["title"] = lang("New Products");
      $page = ($this->input->get("per_page")) ? $this->input->get("per_page") : 0;
      $config = array();
      $config["base_url"] = base_url() . "newProducts";
      $config["total_rows"] = $this->api_model->products_record_count(null,null);

      $config['page_query_string']=true;
      $config["per_page"] = 12;
      $config["uri_segment"] = 3;
      $config['cur_tag_open'] = '&nbsp;<a style="background-color: darkgrey;">';
      $config['cur_tag_close'] = '</a>';
      $this->pagination->initialize($config);
      $this->data["products"] =$this->api_model->getProducts(null,$user_id,"",12,$page);
      $this->data["links"] = $this->pagination->create_links();
       $this->data['main_content'] = "products";
       $this->load->view('includes-web/main',$this->data);
    }
    
    function mostSell_get()
    {
      $user_id = $this->current_user?$this->current_user["id"]:null;
      $this->data["title"] = lang("Most Selles");
      $page = ($this->input->get("per_page")) ? $this->input->get("per_page") : 0;
      $config = array();
      $config["base_url"] = base_url() . "mostSell";
      $config["total_rows"] = $this->api_model->products_record_count(null,null);

      $config['page_query_string']=true;
      $config["per_page"] = 12;
      $config["uri_segment"] = 3;
      $config['cur_tag_open'] = '&nbsp;<a style="background-color: darkgrey;">';
      $config['cur_tag_close'] = '</a>';
      $this->pagination->initialize($config);
      $this->data["products"] =$this->api_model->getProductsMostSelles($user_id,"",12,$page);
      $this->data["links"] = $this->pagination->create_links();
       $this->data['main_content'] = "products";
       $this->load->view('includes-web/main',$this->data);
    }
    function products_list_get()
    {
      $category_id = $this->get("id")?$this->get("id"):null;
      $main_category_id = $this->get("main")?$this->get("main"):null;
      $user_id = $this->current_user?$this->current_user["id"]:null;
      if($category_id)
        $this->data["category"] = $this->api_model->getSubCategorie($category_id);
      else
        $this->data["category"] = $this->api_model->getCategory($main_category_id);
      if($this->data["category"])
      {
        $page = ($this->input->get("per_page")) ? $this->input->get("per_page") : 0;
        $config = array();
        if($category_id)
          $config["base_url"] = base_url() . "Products/products_list?id=".$category_id;
        else
          $config["base_url"] = base_url() . "Products/products_list?main=".$main_category_id;

        $config["total_rows"] = $this->api_model->products_record_count($category_id,$main_category_id);

        $config['page_query_string']=true;
        $config["per_page"] = 12;
        $config["uri_segment"] = 3;
        $config['cur_tag_open'] = '&nbsp;<li class="page-item active" aria-current="page"><a class="page-link">';
      $config['data_page_attr'] = 'class = "page-link"';
      $config['cur_tag_close'] = '</li></a>';
      //For PREVIOUS PAGE Setup
        $config['prev_link'] = 'prev';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        
        //For NEXT PAGE Setup
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $this->data["links"] = $this->pagination->create_links();

        if($main_category_id)
          $this->data["products"] = $this->api_model->getProducts($main_category_id,$user_id,"",12,$page);//$this->api_model->get_category_products($category_id,"viewes",12,$page);
        else
          $this->data["products"] = $this->api_model->getProducts2($category_id,$user_id,"",12,$page);
        $this->data['main_content'] = "products";
      }
      else
        $this->data['main_content'] = "404";

      $this->load->view('includes-web/main',$this->data);
    }
    function product_get()
    {
      $id = $this->get("id");
      $user_id = $this->current_user?$this->current_user["id"]:null;
      //$this->api_model->inc($id); 
      $this->data["product"] = $this->api_model->get_product($id);
      $this->data["product"]["rate"] =  $this->api_model->get_product_rate($id);
      if($this->data["product"])
      {
        $this->data["images"]  = $this->api_model->get_product_images($id);
        $this->data["product_cats"]  = $this->api_model->getProductSubCategories($id);
        $this->data["related"] = $this->api_model->getProducts($this->data["product"]["category_id"],$user_id,"products.id !=".$id,5);
        $this->data['main_content'] = "product";
      }
      else
        $this->data['main_content'] = "404";
   // print_r( $this->data["product"]);die();
      $this->load->view('includes-web/main',$this->data);
    }
    function offer_post()
    {
      $data = (array)$this->post();
      $offer_id = $this->api_model->save_offer($data);
      $this->response(array("data"=>$offer_id),200);
    }
    function saveImage_post()
    {
          $this->load->helper("fancy_file_uploader_helper");
          if (isset($_REQUEST["action"]) && $_REQUEST["action"] === "fileuploader")
          {
           
            $allowedexts = array(
              "jpg" => true,
              "png" => true,
            );
        
            $files = FancyFileUploaderHelper::NormalizeFiles("files");
            //print_r($files[0]);
            if (!isset($files[0]))  $result = array("success" => false, "error" => "File data was submitted but is missing.", "errorcode" => "bad_input");
            else if (!$files[0]["success"])  $result = $files[0];
            else if (!isset($allowedexts[strtolower($files[0]["ext"])]))
            {
              $result = array(
                "success" => false,
                "error" => "Invalid file extension.  Must be '.jpg' or '.png'.",
                "errorcode" => "invalid_file_ext"
              );
            }
            else
            {
              // For chunked file uploads, get the current filename and starting position from the incoming headers.
              $name = FancyFileUploaderHelper::GetChunkFilename();
              //var_dump($name);
              if ($name !== false)
              {
                $startpos = FancyFileUploaderHelper::GetFileStartPosition();
        
                // [Do stuff with the file chunk.]
              }
              else
              {
                // [Do stuff with the file here.]
                $t=round(microtime(true) * 1000);
                 copy($files[0]["file"], "uploads/" . $t.".".$files[0]["ext"]);
                 $data_insert["image"]=$t.".".$files[0]["ext"];
                 $data_insert["offer_id"]= $_POST["offer_id"];
                 $this->db->insert("offer_images",$data_insert);
              }
        
              $result = array(
                "success" => true
              );
            }
          }
        //print_r($result);
        $this->response($result,200);
    }
    

    function cartInfo_get()
    {
      $data["cart_count"] = count($this->cart->contents());
      $data["cart_sub_total"] = $this->cart->total();
      
      $this->response($data);
    }
    function search_get()
    {
      $keyword = $this->get("q");
      $this->data["title"] ="Search Result for: ".$keyword;
      $this->data["products"] = $this->api_model->search_products($keyword);
      foreach ($this->data["products"] as $key => &$value2) {
          $value2["rate"] = $this->api_model->get_product_rate($value2["id"]);
      }
      $this->data["viewed_products"] = $this->api_model->get_most_viewed_products();
      foreach ($this->data["viewed_products"] as $key => &$value2) {
        $value2["rate"] = $this->api_model->get_product_rate($value2["id"]);
     }
      $this->data['main_content'] = "listing";

      $this->load->view('includes-web/main',$this->data);

    }
   
    function contact_get()
    {
        	$this->data['main_content'] = "contact";

		 $this->load->view('includes-web/main',$this->data);
    }

    function cart_get()
    {
      $this->data['main_content'] = "cart";

		 $this->load->view('includes-web/main',$this->data);
    }
 function editCart_post()
    {
      $data = $this->post();
      for($i=0; $i<count($data["rowid"]);$i++)
      {
          $edata[$i] = array("rowid"=>$data["rowid"][$i],"qty"=>$data["qty"][$i]);
      }
     
      if(count($edata))
        $this->cart->update($edata);
     
     redirect(base_url('cart'),'refresh');
     
    }
    function addToCart2_post()
    {
        $product = array();
        $id = $this->post("id");
        $product_info = $this->api_model->get_product($id);
        $enter = false;
        if($product_info)
        {
          $errorCode = 200;
          $product_info["quantity"] = $this->post("quantity")?$this->post("quantity"):1;
         
          foreach ($_SESSION["cart_products"] as $key => &$value) {
            if($value["id"] == $product_info["id"])
            {
              $value["quantity"] += $product_info["quantity"];
              $value["price"] = $product_info["price"]*$value["quantity"];
              $enter = true;
            }
          }
          if(!$enter)
          {
            $product_info["price"] *= $product_info["quantity"];
            $_SESSION["cart_products"][] = $product_info;
          }
           
        }
        else
          $errorCode = 404;
        $this->response(array("data"=>$product_info,"existInCart"=>$enter,"errorCode"=>$errorCode),200);
    }
    function addToCart_post()
    {
        $product = array();
        $id = $this->post("id");
        $product_info = $this->api_model->get_product($id);
        $enter = false;
        if($product_info)
        {
          $errorCode = 200;
          $product_info["quantity"] = $this->post("quantity")?$this->post("quantity"):1;

          $cart = $this->cart->contents(); //get all items in the cart
            $exists = false;             //lets say that the new item we're adding is not in the cart
            $rowid = '';

            foreach($cart as $item){
                if($item['id'] == $id)     //if the item we're adding is in cart add up those two quantities
                {
                    $exists = true;
                    $rowid = $item['rowid'];
                    $qty = $item['qty'] + $product_info["quantity"];
                }       
            }

            if($exists)
            {
              $enter = true;
                    $data = array(
                      'rowid' => $rowid,
                      'qty'   => $qty
                  );
        
            // Update the cart with the new information
                $this->cart->update($data);
               // $this->cart_model->update_item($rowid, $qty);                   
               // echo 'true'; // For ajax calls if javascript is enabled, return true, so the cart gets updated          
            }
            else
            {
              $data = array(
                'id'      => $product_info["id"],
                'qty'     =>  $product_info["quantity"],
                'price'   => $product_info["sale_price"],
                'name'    => $product_info["name"],
                'image'   => $product_info["image"]
                );
                
                $this->cart->insert($data);
            }   
           
        }
        else
          $errorCode = 404;
        $this->response(array("data"=>$product_info,"existInCart"=>$enter,"errorCode"=>$errorCode),200);
    }

    function removeFromCart_post()
    {
      $id = $this->post("id");
     $this->cart->remove($id);
      $this->response(array("data"=>true,"errorCode"=>200),200);
    }

    function removeFromCart2_get()
    {
      $id = $this->get("id");
      foreach ($_SESSION["cart_products"] as $key => &$value) {
        if($value["id"] == $id)
        {
          unset($_SESSION["cart_products"][$key]);
        }
      }
      $this->response(array("data"=>true),200);
    }

    function clearCart_get()
    {
      $_SESSION["cart_products"] = array();
      
      redirect(base_url(),'refresh');
      
    }
    function pag_config3($fun,$type)
    {

         $config = array();
        $config["base_url"] = base_url() . "Products/".$fun."?"."id=".$type;
        $config["total_rows"] = $this->api_model->record_count($type);
		    $config['page_query_string']=true;
       //var_dump($config["total_rows"] );die();
        $config["per_page"] = 12;
        $config["uri_segment"] = 3;
        $config['cur_tag_open'] = '&nbsp;<a style="background-color: darkgrey;">';

        // Close tag for CURRENT link.
        $config['cur_tag_close'] = '</a>';
        
        // By clicking on performing NEXT pagination.
       // $config['next_link'] = 'Next';
        
        // By clicking on performing PREVIOUS pagination.
        //$config['prev_link'] = 'Previous';

        $this->pagination->initialize($config);
        
         $this->data["links"] = $this->pagination->create_links();
        // var_dump($this->data["links"]);
    }

    function pag_config($fun,$table,$attr,$type,$where,$val="")
    {

         $config = array();
        $config["base_url"] = base_url() . "Products/".$fun."?".$attr."=".$type;

        $config["total_rows"] = $this->api_model->record_count($table,$where,$val);
		    $config['page_query_string']=true;
        $config["per_page"] = 9;
        $config["uri_segment"] = 3;
        $config['cur_tag_open'] = '&nbsp;<a style="background-color: darkgrey;">';
        $config['cur_tag_close'] = '</a>';
        $this->pagination->initialize($config);
        $this->data["links"] = $this->pagination->create_links();
    }




    
  

}