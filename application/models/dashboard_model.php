<?php

class dashboard_model extends CI_Model
{

    public function checkAdminPermission()
    {
        return $this->ion_auth->is_admin();
    }

    public function checkToken($token)
    {
        if ($token == null)
            return false;
        $this->db->select('users.*, groups.name as group_name');
        $this->db->join('users_groups', 'users_groups.user_id = users.id');
        $this->db->join('groups', 'groups.id = users_groups.group_id');
        $this->db->where('users.token', $token);
        $query = $this->db->get('users');

        if ($query->num_rows() > 0) {
            $user = $query->row();
            return $user;
        }

        return false;
    }

    public function register($username, $password, $email, $additional_data, $group)
    {
        // التحقق من وجود اسم المستخدم في الجدول
        $existing_user = $this->db->get_where('users', array('username' => $username))->row();

        // التحقق من وجود البريد الإلكتروني في الجدول
        $existing_email = $this->db->get_where('users', array('email' => $email))->row();

        if ($existing_user) {
            // في حالة وجود اسم المستخدم بالفعل
            return array(
                'status' => 'Failed',
                'message' => 'Username already exists',
                'ErrorCode' => 400
            );
        } elseif ($existing_email) {
            // في حالة وجود البريد الإلكتروني بالفعل
            return array(
                'status' => 'Failed',
                'message' => 'Email already exists',
                'ErrorCode' => 400
            );
        } else {
            // إعداد بيانات المستخدم
            $user_data = array(
                'username' => $username,
                'password' => password_hash($password, PASSWORD_DEFAULT),
                // استخدام دالة password_hash() بدلاً من md5()
                'email' => $email,
                // 'is_active' => 1
                // يمكنك إضافة حقول إضافية إذا لزم الأمر

            );
            $user_data = array_merge($user_data, $additional_data);
            // إدخال بيانات المستخدم في جدول "users" أو أي جدول آخر حسب احتياجاتك
            $this->db->insert('users', $user_data);
            $user_id = $this->db->insert_id();

            if ($user_id) {
                // نجاح إدخال بيانات المستخدم، لذا سنقوم بإضافته إلى المجموعة المحددة
                $group_data = array(
                    'user_id' => $user_id,
                    'group_id' => $group
                );

                // إدخال بيانات المستخدم في جدول "users_groups" أو أي جدول آخر حسب احتياجاتك
                $this->db->insert('users_groups', $group_data);

                // في حالة نجاح إدخال البيانات في جدول "users_groups"
                return array(
                    'status' => 'Success',
                    'message' => 'Account Created Successfully',
                    'ErrorCode' => 200,
                    'user_id' => $user_id
                );
            } else {
                // في حالة فشل إدخال بيانات المستخدم
                return array(
                    'status' => 'Failed',
                    'message' => 'Failed to create user',
                    'ErrorCode' => 400
                );
            }
        }

    }
    public function updateUser($user_id, $update_data)
    {
        // التحقق من وجود المستخدم
        $existing_user = $this->db->get_where('users', array('id' => $user_id))->row();

        if ($existing_user) {
            // تحديث بيانات المستخدم
            $this->db->where('id', $user_id);
            $this->db->update('users', $update_data);

            return array(
                'status' => 'Success',
                'message' => 'User updated successfully'
            );
        } else {
            // المستخدم غير موجود
            return array(
                'status' => 'Failed',
                'message' => 'User does not exist',
                'ErrorCode' => 400
            );
        }
    }

    public function deleteUser($user_id)
    {
        // التحقق من وجود المستخدم
        $existing_user = $this->db->get_where('users', array('id' => $user_id))->row();

        if ($existing_user) {
            // حذف المستخدم
            $this->db->where('id', $user_id);
            $this->db->delete('users');

            return array(
                'status' => 'Success',
                'message' => 'User deleted successfully'
            );
        } else {
            // المستخدم غير موجود
            return array(
                'status' => 'Failed',
                'message' => 'User does not exist',
                'ErrorCode' => 400
            );
        }
    }
    function storeToken($userId, $token)
    {
        // تحديث السجل في جدول المستخدمين لتخزين التوكن
        $data = array(
            'token' => $token
        );
        $this->db->where('id', $userId);
        $this->db->update('users', $data);
    }
    public function getAgentCities($user_id)
    {
        // $this->db->select('cities.city_name_arabic, cities.city_name_english');
        $this->db->select('*');
        $this->db->from('user_cities');
        $this->db->join('cities', 'user_cities.city_id = cities.id');
        $this->db->where('user_cities.user_id', $user_id);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }
    }

    public function addCity($city_name_arabic, $city_name_english)
    {
        $data = array(
            'city_name_arabic' => $city_name_arabic,
            'city_name_english' => $city_name_english
        );

        $this->db->insert('cities', $data);

        if ($this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function updateCity($city_id, $city_name_arabic, $city_name_english)
    {
        $data = array(
            'city_name_arabic' => $city_name_arabic,
            'city_name_english' => $city_name_english
        );

        $this->db->where('city_id', $city_id);
        $this->db->update('cities', $data);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getAllCities()
    {
        $query = $this->db->get('cities');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }
    }

    public function addCitiesToUser($user_id, $city_id)
    {
        $existingCity = $this->db->get_where('cities', array('id' => $city_id))->row();

        if ($existingCity) {
            $userCityData = array(
                'user_id' => $user_id,
                'city_id' => $city_id
            );

            $this->db->insert('user_cities', $userCityData);
            return true;
        } else {
            return false;
        }
    }

    function getUserCities_get($user_id)
    {
        $this->db->select('city_id');
        $this->db->from('user_cities');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }
    }


    public function updateCitiesForUser($user_id, $cities)
    {
        // حذف المدن الحالية للمستخدم
        $this->db->where('user_id', $user_id);
        $this->db->delete('user_cities');

        $successCount = 0;
        $errorCount = 0;

        // إضافة المدن الجديدة للمستخدم
        foreach ($cities as $city_id) {
            $data = array(
                'user_id' => $user_id,
                'city_id' => $city_id
            );
            $isAdded = $this->db->insert('user_cities', $data);
            if ($isAdded) {
                $successCount++;
            } else {
                $errorCount++;
            }
        }

        // إرجاع العدد الإجمالي للمدن المضافة بنجاح
        return $successCount;
    }



    public function addSchool($school_data)
    {
        // بناء بيانات المدرسة
        $school = array(
            'name' => $school_data['name'],
            'name_ar' => $school_data['name_ar'],
            'name_en' => $school_data['name_en'],
            'username' => $school_data['username'],
            'mobile' => $school_data['mobile'],
            'password' => $school_data['password'],
            'email' => $school_data['email'],
            'address' => $school_data['address'],
            'nationalImage' => $school_data['nationalImage'],
            'type' => 'school',
            'related_id' => '',
            'stop_id' => '',
            'created_date' => date('Y-m-d H:i:s'),
            'is_active' => 1,
            'register_date' => date('Y-m-d'),
            'firebase_token' => '',
            'is_new' => 1,
            'city_id' => $school_data['city_id']
        );

        // إدخال البيانات في قاعدة البيانات
        $isAdded = $this->db->insert('customer', $school);
        if ($isAdded) {
            $insertedId = $this->db->insert_id();
            // قم بتحديث الحقل "number_parent" في جدول "cities"
            $this->db->where('id', $school_data['city_id']);
            $this->db->set('number_schools', 'number_schools + 1', false);
            $this->db->update('cities');

            return $insertedId;

            // return $this->db->insert_id();
        } else {
            return false;
        }
    }

    // public function getAgentCityId($user_id)
    // {
    //     $this->db->select('city_id');
    //     $this->db->from('agents');
    //     $this->db->where('user_id', $user_id);
    //     $query = $this->db->get();

    //     if ($query->num_rows() > 0) {
    //         $row = $query->row();
    //         return $row->city_id;
    //     } else {
    //         return null;
    //     }
    // }
    public function updateCustomer($school_id, $user_id)
    {
        $this->db->where('id', $school_id);
        $data = array(
            'user_id' => $user_id
        );
        return $this->db->update('customer', $data);
    }
    public function getAgentCityIds($user_id)
    {
        $this->db->select('city_id');
        $this->db->from('user_cities');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();

        $city_ids = array(); // مصفوفة لتخزين جميع city_id

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $city_ids[] = $row->city_id;
            }
        }

        return $city_ids;
    }


    // public function addBusToCustomer($data)
    // {
    //     $this->db->insert('customer', $data);

    //     return $this->db->affected_rows() > 0; // إرجاع قيمة بولية تشير إلى نجاح عملية الإدخال
    // }
    public function addBusToCustomer($data)
    {
        $this->db->insert('customer', $data);

        if ($this->db->affected_rows() > 0) {
            return $this->db->insert_id(); // إرجاع رقم الصف المضاف
        } else {
            return false;
        }
    }

    public function getSchoolCityId($school_id)
    {
        // استعلام قاعدة البيانات للحصول على city_id المرتبط بالعميل
        $this->db->select('city_id');
        $this->db->from('customer');
        $this->db->where('id', $school_id);
        $query = $this->db->get();

        // التحقق من وجود العميل
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->city_id;
        } else {
            return false; // في حالة عدم وجود العميل
        }
    }

    public function getEmailById($id)
    {
        $this->db->select('email');
        $this->db->from('customer');
        $this->db->where('id', $id);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->email;
        } else {
            return null;
        }
    }

    public function addParent($Parent_data)
    {
        // بناء بيانات المدرسة
        $Parent = array(
            'name' => $Parent_data['name'],
            'username' => $Parent_data['username'],
            'mobile' => $Parent_data['mobile'],
            'password' => $Parent_data['password'],
            'email' => $Parent_data['email'],
            'address' => $Parent_data['address'],
            'nationalImage' => $Parent_data['nationalImage'],
            'type' => 'Parent',
            'related_id' => $Parent_data['related_id'],
            'stop_id' => $Parent_data['stop_id'],
            'created_date' => date('Y-m-d H:i:s'),
            // 'is_active' => $Parent_data['is_active'],
            'register_date' => date('Y-m-d'),
            'firebase_token' => '',
            'is_new' => 1,
            'city_id' => $Parent_data['city_id']
        );

        // إدخال البيانات في قاعدة البيانات
        $isAdded = $this->db->insert('customer', $Parent);
        if ($isAdded) {

            $insertedId = $this->db->insert_id();
            // قم بتحديث الحقل "number_parent" في جدول "cities"
            $this->db->where('id', $Parent_data['city_id']);
            $this->db->set('number_parent', 'number_parent + 1', false);
            $this->db->update('cities');

            return $insertedId;
        } else {
            return false;
        }
    }
    public function updateParent($parent_id, $data)
    {
        $this->db->where('id', $parent_id);
        $this->db->update('customer', $data);
        return $this->db->affected_rows() > 0;
    }

    public function deleteParent($parent_id)
    {
        // $this->db->where('id', $parent_id);
        // $this->db->delete('customer');
        // return $this->db->affected_rows() > 0;
        // Get the city_id of the parent before deleting
        $this->db->select('city_id');
        $this->db->where('id', $parent_id);
        $query = $this->db->get('customer');
        $parent = $query->row();

        // Delete the parent from customer table
        $this->db->where('id', $parent_id);
        $this->db->delete('customer');

        // Update the cities table to decrease the number of parents
        if ($this->db->affected_rows() > 0) {
            $this->db->set('number_parents', 'number_parents - 1', false);
            $this->db->where('id', $parent->city_id);
            $this->db->update('cities');
        }
        return $this->db->affected_rows() > 0;
    }
    public function getParentsBySchoolId($school_id)
    {
        // $this->db->where('related_id', $school_id);
        // $this->db->where('type', 'parent');
        // $query = $this->db->get('customer');

        // if ($query->num_rows() > 0) {
        //     return $query->result();
        // } else {
        //     return array();
        // }
        // لجلب المواقف .. 
        $this->db->select('customer.*, bus_stop.stop_name');
        $this->db->from('customer');
        // $this->db->join('bus_stop', 'customer.stop_id = bus_stop.id');
        $this->db->join('bus_stop', 'customer.stop_id = bus_stop.id', 'left');
        $this->db->where('customer.related_id', $school_id);
        $this->db->where('customer.type', 'parent');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }
    }
    public function searchCustomers($type, $data)
    {
        // قم بتنفيذ استعلام قاعدة البيانات حسب البيانات المرسلة وقيمة type
        $query = $this->db->select('*')->from('customer')
            ->where('type', $type)
            ->like('name', $data)
            ->or_like('name_ar', $data)
            ->or_like('name_en', $data)
            ->or_like('username', $data)
            ->or_like('mobile', $data)
            ->or_like('email', $data)
            ->get();

        // إرجاع النتائج كمصفوفة
        return $query->result_array();
    }



    // public function addChild($child_data)
    // {
    //     // بناء بيانات الطفل
    //     $child = array(
    //         'child_name' => $child_data['name'],
    //         'parent_id' => $child_data['parent_id']
    //     );

    //     // إدخال البيانات في قاعدة البيانات
    //     $isAdded = $this->db->insert('children', $child);
    //     if ($isAdded) {
    //         return $this->db->insert_id();
    //     } else {
    //         return false;
    //     }
    // }
    public function addChild($child_data)
    {
        // بناء بيانات الطفل
        $child = array(
            'child_name' => $child_data['name'],
            'parent_id' => $child_data['parent_id']
        );

        // إدخال البيانات في قاعدة البيانات
        $isAdded = $this->db->insert('children', $child);
        if ($isAdded) {
            $insertedId = $this->db->insert_id();
            // جلب city_id من جدول الكوستمر
            $this->db->select('city_id');
            $this->db->where('id', $child_data['parent_id']);
            $query = $this->db->get('customer');
            $parent = $query->row();

            if ($parent) {
                // قم بتحديث الحقل "number_students" في جدول "cities"
                $this->db->where('id', $parent->city_id);
                $this->db->set('number_students', 'number_students + 1', false);
                $this->db->update('cities');
            }

            return $insertedId;
        } else {
            return false;
        }
    }

    // public function deleteChild($child_id,$city_id)
    // {
    //     $this->db->where('id', $child_id);
    //     $isDeleted = $this->db->delete('children');

    //     return $isDeleted;
    // }

    public function deleteChild($child_id, $city_id)
    {
        $this->db->where('id', $child_id);
        $isDeleted = $this->db->delete('children');

        if ($isDeleted) {
            // تحديث عدد الأطفال في جدول السيتيس
            $this->db->where('id', $city_id);
            $this->db->set('number_students', 'number_students - 1', false);
            $this->db->update('cities');
        }

        return $isDeleted;
    }
    // في النموذج "dashboard_model"
    public function getChildrenByParentId($parent_id)
    {
        $this->db->where('parent_id', $parent_id);
        $query = $this->db->get('children');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

}