<?php

class home_model extends CI_Model {

	
	function getChickenPriceStat()
	{
		$q= "SELECT
    SUM(IF(MONTH = 1, meanPrice, 0)) AS 'January',
    SUM(IF(MONTH = 2, meanPrice, 0)) AS 'Feburary',
    SUM(IF(MONTH = 3, meanPrice, 0)) AS 'March',
    SUM(IF(MONTH = 4, meanPrice, 0)) AS 'April',
    SUM(IF(MONTH = 5, meanPrice, 0)) AS 'May',
    SUM(IF(MONTH = 6, meanPrice, 0)) AS 'June',
    SUM(IF(MONTH = 7, meanPrice, 0)) AS 'July',
    SUM(IF(MONTH = 8, meanPrice, 0)) AS 'August',
    SUM(IF(MONTH = 9, meanPrice, 0)) AS 'September',
    SUM(IF(MONTH = 10, meanPrice, 0)) AS 'October',
    SUM(IF(MONTH = 11, meanPrice, 0)) AS 'November',
    SUM(IF(MONTH = 12, meanPrice, 0)) AS 'December' 
    FROM(
        SELECT id, MONTH(publishdate) AS MONTH, AVG(price) AS meanPrice
        FROM chickenprice 
         WHERE publishdate BETWEEN  date(Date_add(Now(),interval - 12 month)) AND date(NOW())
        GROUP BY MONTH
    )AS SubTable1 ";
		$query =$this->db->query($q);
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
	}

	function getFeedPriceStat()
	{
		$q= "SELECT
    SUM(IF(MONTH = 1, meanPrice, 0)) AS 'January',
    SUM(IF(MONTH = 2, meanPrice, 0)) AS 'Feburary',
    SUM(IF(MONTH = 3, meanPrice, 0)) AS 'March',
    SUM(IF(MONTH = 4, meanPrice, 0)) AS 'April',
    SUM(IF(MONTH = 5, meanPrice, 0)) AS 'May',
    SUM(IF(MONTH = 6, meanPrice, 0)) AS 'June',
    SUM(IF(MONTH = 7, meanPrice, 0)) AS 'July',
    SUM(IF(MONTH = 8, meanPrice, 0)) AS 'August',
    SUM(IF(MONTH = 9, meanPrice, 0)) AS 'September',
    SUM(IF(MONTH = 10, meanPrice, 0)) AS 'October',
    SUM(IF(MONTH = 11, meanPrice, 0)) AS 'November',
    SUM(IF(MONTH = 12, meanPrice, 0)) AS 'December' 
    FROM(
        SELECT id, MONTH(publishdate) AS MONTH, AVG(price) AS meanPrice
        FROM feedprice 
         WHERE publishdate BETWEEN  date(Date_add(Now(),interval - 12 month)) AND date(NOW())
        GROUP BY MONTH
    )AS SubTable1 ";
		$query =$this->db->query($q);
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
	}

	function all_jobs($offset=0){
		
		$this->db->select('*');
		$this->db->from('job');
		$this->db->limit(4, $offset*4);
		$query = $this->db->get();
		
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		else 
			return false;	
	}
	
	function all_certificates($id){
		$this->db->select('*');
		$this->db->from('Certificate');
		$this->db->where('Certificate.candidateId', $id);
		$query = $this->db->get();
		
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		else 
			return false;	
	}
	
	function addCompany($data)
	{
		$query=$this->db->select("*")->from("company")->where("login",$data["login"])->get();
		if($query->num_rows() > 0) {
				return false;
		}
		$this->db->insert("company",$data);
		return $this->db->insert_id();
	}

	function addCertificate($data)
	{
		$this->db->insert("Certificate",$data);
		return $this->db->insert_id();
	}

	function addCandidate($data){
		$query=$this->db->select("*")->from("candidate")->where("login",$data->login)->get();
		if($query->num_rows() > 0) {
				return false;
		}
		$this->db->insert("Candidate",$data);
		return $this->db->insert_id();
	}

	function addJob($data){
		$this->db->insert("job",$data);
		return $this->db->insert_id();
	}

	function jobsToCandidates($id,$offset,$type=false){
		$this->db->select('*');
		$this->db->from('job');
		$this->db->join('Certificate','Certificate.diplomaTitle = job.requiredEducationLevel');
		$this->db->join('Candidate','Candidate.experienceYears >= job.requiredExperienceYears','inner');
		$this->db->where('Certificate.candidateId', $id)->group_by("job.id");
		if($type)
			$this->db->limit(4, $offset*4);
		$query = $this->db->get();
		
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		else 
			return false;
	}

	function candidatesToJob($id){
		$this->db->select('*');
		$this->db->from('job');
		$this->db->join('Certificate','Certificate.diplomaTitle = job.requiredEducationLevel');
		$this->db->join('Candidate','Candidate.experienceYears >= job.requiredExperienceYears and  candidate.id = certificate.candidateId','inner');
		$this->db->where('job.id', $id)->group_by("Candidate.id");
		$query = $this->db->get();
		
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		else 
			return false;
	}

	function filterJob($type=0,$companyId=0){
		$this->db->select('*');
		$this->db->from('job');
		if($type == 0)
			$this->db->order_by('requiredExperienceYears');
		else if($type == 1)
			$this->db->order_by('requiredEducationLevel');
		else
			$this->db->order_by('salary');
		if($companyId!=0)
			$this->db->where("companyId",$companyId);
		$query = $this->db->get();
		
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		else 
			return false;		
	}
}