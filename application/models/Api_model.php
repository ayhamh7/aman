<?php

class api_model extends CI_Model {
        
        function getCategories($parent_id= NULL)
        {
            $this->db->where("parent_id",$parent_id);
            $this->db->order_by("order","ASC");
            return $this->db->get("category")->result_array();
        }
        function get_children($parent_id)
        {
              $this->db->where("parent_id",$parent_id);
               $this->db->where("is_active",1);
          //  $this->db->order_by("order","ASC");
            return $this->db->get("children")->result_array();
        }
		function get_bus_children($bus_id)
		{
			$this->db->where('bus_id', $bus_id);
			$this->db->join('bus_stop', 'bus_stop.busline_id = bus_line.id');
			$this->db->join('customer', 'customer.stop_id = bus_stop.id');
			$this->db->join('children', 'children.parent_id = customer.id');
			$this->db->select('children.*');
			$this->db->distinct(); 
			return $this->db->get("bus_line")->result_array();
		}
        function get_attrs($cat_id)
        {
            $this->db->where("category_id",$cat_id);
            return $this->db->get("attrs")->result_array();
        }
        function get_partners()
        {
            return $this->db->get("partners")->result_array();
        }
        
		function get_random_category($limit)
		{
			
				$this->db->limit($limit);
					
				$this->db->order_by('rand()');
				$this->db->where("parent_id",null);
				return $this->db->get("category")->result_array();
		}
	   function getprochoures($limit)
	   {
	       $this->db->limit($limit);
	       $this->db->order_by('id','DESC');
	       return $this->db->get("prochoure")->result_array();
	   }
		function getSubCategories($cat_id)
		{
			$this->db->where('category_id', $cat_id);
			return $this->db->get('sub_categories')->result_array();
			
			
		}
		function getProductSubCategories($product_id)
		{
			$this->db->where('product_id', $product_id);
			$this->db->join('products_category', 'sub_category_id = sub_categories.id');
			return $this->db->get('sub_categories')->result_array();
			
			
		}
		function getSubCategorie($cat_id)
		{
			$this->db->where('category_id', $cat_id);
		
			$this->db->select('sub_categories.*');
			
			return $this->db->get('sub_categories')->result_array();
			
			
		}
		function getTypes($cat_id)
		{
			$this->db->where('sub_category_id', $cat_id);
		
			$this->db->select('types.*');
			
			return $this->db->get('types')->result_array();
			
			
		}
		function getShopsInCategory($cat_id)
		{
			$this->db->where('user_categories.category_id', $cat_id);
			$this->db->join('users', 'user_id = users.id');
			$this->db->select('users.id as id, CONCAT(first_name," ",last_name) as name,image');
			return $this->db->get('user_categories')->result_array();
			
			
		}
		function get_ads($limit)
		{
			$this->db->limit($limit);
			$this->db->order_by('order', 'desc');
			return $this->db->get('ads')->result_array();
			
		}
		function get_sideads($where="",$limit=3)
		{
		    	$this->db->limit($limit);
		    	if($where != "")
		    	$this->db->where($where);
			$this->db->order_by('order', 'desc');
			return $this->db->get('side_ads')->result_array();
		}
		function get_random_subcategory($limit)
		{
			
				$this->db->limit($limit);
					
				$this->db->order_by('rand()');
				$this->db->where("parent_id !=",null);
				return $this->db->get("category")->result_array();
		}
        function get_notifications($user_id)
        {
            if($user_id)
            {
                 $sql = " select *
                    from (
                        select notification_msg, date from notifications where customer_id = $user_id 
                        union all
                        select message as notification_msg,date from general_notifications
                    ) a
                    order by date desc";
            }
           else
           {
               $sql = "select message as notification_msg,date from general_notifications order by date desc";
           }
            return $this->db->query($sql)->result_array();
        }
	    function edit_profile($data)
	    {
	        $this->db->where("id",$data["id"]);
	        $this->db->update("agency",$data);
	        //echo $this->db->last_query();die();
	    }
        function get_info($id)
        {
            return $this->db->where("id",$id)->get("agency")->row_array();
        }
        function getConfigVal($code)
        {
            return $this->db->where("code",$code)->get("app_constant")->row_array();
        }
        
       
    
       
		function getAlbums()
		{
			$query= $this->db->get('album');
			return $query->result_array();
			
			
		}
		
	
		function get_sliders()
		{
			return $this->db->get('slider')->result_array();
			
		}
		function get_categories()
		{
			
			return $this->db->get('category')->result_array();
			
		}

		function get_section_offers($cat_id,$start=-1)
		{
			$this->db->where('category_id', $cat_id);
			$this->db->where('is_active', 1);
			if($start>-1)
			{
				//$this->db->limit($limit);
				$this->db->limit(9, $start);
				
			}
			return $this->db->get('offers')->result_array();
			
			
		}
		function get_offers($where=null)
		{
			if($where!=null)
			{
				$this->db->where($where);
			}
			$this->db->where('is_active', 1);
			
			return $this->db->get('offers')->result_array();
			
			
		}
		function save_offer($data)
		{
			//var_dump($data);
			$this->db->insert('offers', $data);
			return $this->db->insert_id();
			
		}
		function get_offer($offer_id)
		{
			$this->db->where('offers.id', $offer_id);
			$this->db->join('category', 'category.id = category_id', 'left');
			return $this->db->get('offers')->row_array();
			
		}

		function get_customer_info($customer_id)
		{
			$this->db->where('id', $customer_id);
			return $this->db->get('customer')->row_array();
			
		}
		function get_offer_images($offer_id)
		{
			$this->db->where('offer_id', $offer_id);
			
			return $this->db->get('offer_images')->result_array();
		}
        function edit_agency($data)
	    {
    		$this->db->where("id",$data["id"]);
    		$this->db->update("agency",$data);
    	}
		function get_ad()
		{
			$this->db->order_by('priority', 'ASC');
			return $this->db->get('ad', 2, 0)->result_array();
			
		}
		function requestReset($mobile)
		{
		    $code = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
		    $this->db->where("mobile",$mobile);
			$data["reset_code"]= $code;
			$data["reset_password"]=1;
			$this->db->update("costumer",$data);
			return $this->db->affected_rows();
			
		}

		function Reset($mobile,$code,$newPass)
		{
			$this->db->where("mobile",$mobile);
			$this->db->where('reset_code', $code);
			$data["password"]=$newPass;
			$data["reset_code"]= "";
			$data["reset_password"]=0;
			$this->db->update("costumer",$data);
			return $this->db->affected_rows();
		}
		
	
        function add_order($data)
		{
			$this->db->insert("orders",$data);
			return $this->db->insert_id();
			
		}
        function add_order_products($data)
        {
            $this->db->insert_batch("product_order",$data);
			return true;
        }
		function getGallery()
		{
		    	$query= $this->db->get('gallery');
			return $query->result_array();
		}
		function getAlbumImages($album_id)
		{
		   	$query= $this->db->where("album_id",$album_id)->get('images');
			return $query->result_array();
		}
		function getAlbumProducts($album_id)
		{
		
		    return $this->db->select("*,ArticleCodeId as image,ArticleName1Id as name")->where("Remarks",$album_id)->get('kt_ms_art')->result_array();
		}
		function add_favourite($data)
		{
			$this->db->db_debug = false;
			$this->db->insert('favourite', $data);
			$this->db->db_debug = true;
			
		    //$this->db->insert("favourite",$data);
		    return true;
		}
		
		function user_favourites($user_id)
		{
		   
		     
		   $query=  $this->db->where("favourite.costumer_id",$user_id)->join("products","product_id=products.id")->join("category","category.id = category_id")->select("products.*,category.name as category_name,IF(product_id is NULL, 0,1) as is_customer_favorite")->get("favourite");
		    return $query->result_array();
		   
		}
		function delete_favourite($user_id,$product_id)
		{
		    return $this->db->where(array("costumer_id"=>$user_id,"product_id"=>$product_id))->delete("favourite");
		}
		
		function add_like($data)
		{
		    $this->db->insert("likes",$data);
		    return true;
		}
	
		
		
		function record_count($val)
		{
			if(!is_array($val))
			{
				$this->db->where("category_id",$val);
			}
			else if(is_array($val))
			{
			    foreach ($val as $key => $value) {
    				$this->db->like($key, $value);
    			}
			}
			return $this->db->from("offers")->count_all_results();
		}
		function products_record_count($sub_id=null,$main_id=null,$where="")
		{
			if($sub_id)
			{
				$this->db->join('products_category', 'products_category.product_id = products.id');
				$this->db->where('sub_category_id', $sub_id);
				
			}
			if($main_id)
			{
				$this->db->where('category_id', $main_id);
				
			}
			if($where != "")
			{
			    $this->db->where($where);
			}
			return $this->db->from('products')->count_all_results();
			
		}
        public function searchProducts2($search,$limit,$offset)
		{
			foreach ($search as $key => $value) {
				$this->db->like($key, $value);
			}
			
			
			return $this->db->get('products', $limit, $offset)->result_array();
			
		}
		
		 public function searchProducts($search,$filter="product",$user_id)
		{
			if($filter == "product")
			{
			$this->db->like("name", $search);
			$this->db->or_like("name_eng", $search);
			$this->db->or_like("details", $search);
			$this->db->or_like("details_eng", $search);
			}
			else if($filter == "agent")
			{
			     $this->db->join("agent","agent.id= agent_id","left");
			     $this->db->like("agent_name_ar", $search);
			     $this->db->or_like("agent_name_en", $search);
			}
		    else if($filter == "company")
		    {
		        $this->db->join("company","company.id= company_id","left");
		         $this->db->like("company_name_ar", $search);
		         $this->db->or_like("company_name_en", $search);
		    }
		     $str = "";
		    if($user_id != null)
		    {
		        $this->db->join("favourite","product_id=products.id  and `favourite`.`costumer_id` = $user_id","left");
		       
		        $str =",IF(product_id is NULL, 0,1) as is_customer_favorite"; 
		        
		    }
			//return $this->db->get('products')->result_array();
			 $this->db->join("company","company.id= company_id","left");
		   $this->db->join("scientific_names","scientific_name_id=scientific_names.id","left");
		   $this->db->join("agent","agent.id= agent_id","left");
		   $this->db->join("country","country.id = origin_countryid","left");
		   $this->db->join("category","category.id = category_id","left");
			$res =  $this->db->select("products.*,category.name as category_name,agent_name_ar,agent_name_en,country.country as origin_country,scientific_name,scientific_name_eng,company_name_ar,company_name_en".$str)->order_by("entrydate","DESC")->get('products')->result_array();
			return $res;
			
		}
	


		
		
		function getCategory($cat_id)
		{
			return $this->db->where("id",$cat_id)->get("category")->result_array()[0];
		}
		function getProducts3($range,$limit,$offset,$user_id=null)
		{
		    if($user_id != null)
		    {
		        $this->db->join("favourite","product_id=ArticleCodeId","left")->where("(favourite.costumer_id =".$user_id." OR favourite.costumer_id IS NULL)");
		        $this->db->select("IFNULL(product_id, 0) as product_id,kt_ms_art.*");   
		    }
		    
			return $this->db->where("ArticleCodeId BETWEEN '".$range[0]."' and '".$range[1]."'")->where("ArticleName1Id !='' and ArticleName1Id !='-'")->get('kt_ms_art', $limit, $offset)->result_array();
		}
		
		function get_store_categories($store_id)
		{
			$this->db->where('user_id', $store_id);
			$this->db->join('category', 'category.id = category_id', 'inner');
			return $this->db->get('user_categories')->result_array();
		}

		function add_wishlist($data)
		{
			$this->db->db_debug = false;
			$this->db->insert('wishlist', $data);
			$this->db->db_debug = true;
		}
		function get_product_rate($product_id)
        {
            $this->db->select('IFNULL(AVG(rate), 1) AS average');
            $this->db->where('product_id', $product_id);
            $this->db->from('ratings');
            return $this->db->get()->row_array()["average"];
		}
        function add_rate($data)
		{
			$this->db->db_debug = false;
			$this->db->insert('ratings', $data);
			$this->db->db_debug = true;
		}

		function getProductsGrouped($user_id=null,$where ="",$limit=-1)
		{
		    $categories = $this->db->get("category")->result_array();
		    $products = array();
		    foreach($categories as $category)
		    {
				$this->db->where('category_id', $category["id"]);
				
		        if($where!="")
				{
					$this->db->where($where);

				}
				if($limit > 0)
				{
					$this->db->limit($limit);
				}
				$str = "";
				if($user_id != null)
				{
					$this->db->join("favourite","favourite.product_id=products.id  and `favourite`.`costumer_id` = $user_id","left");
				   
					$str =",IF(favourite.product_id is NULL, 0,1) as is_customer_favorite"; 
					
				}
			  $this->db->join('users', 'users.id = store_id');
			  
			   $this->db->join("category","category.id = category_id","left");
			   $this->db->order_by('entrydate', 'desc');
			   $this->db->join('ratings', 'ratings.product_id = products.id', 'left');
			   $this->db->group_by('products.id');
			   
				$p =  $this->db->select("IFNULL(AVG(rate), 0) AS rating,products.*,products.id as id,category.name as category_name,category.name_en as category_name_en,CONCAT(users.first_name,' ',users.last_name) as store_name".$str)->order_by("entrydate","DESC")->get('products')->result_array();
				//echo $this->db->last_query()."<br><br>";//die();
				$category["products"] = $p;
    		   
				if($p)
				{
					$products_cat[] = $category;
				}
    			
		    }
		    return $products_cat;
		}
		function getProductsMostSelles($user_id=null,$where ="",$limit=-1,$page=0)
		{
			if($limit > 0)
			{
				$this->db->limit($limit,$page);
			}
			if($where)
			{
				$this->db->where($where);
				
			}
			 $str = "";
			 if($user_id != null)
			 {
				 $this->db->join("favourite","favourite.product_id=products.id  and `favourite`.`costumer_id` = $user_id","left");
				
				 $str =",IF(favourite.product_id is NULL, 0,1) as is_customer_favorite"; 
				 
			 }
		   $this->db->join('users', 'users.id = store_id');
		   $this->db->join('product_order ', 'product_order.item_id = products.id');
		   $this->db->group_by('item_id');
		   
			$this->db->join("category","category.id = category_id","left");
			$this->db->order_by('SUM(`quantity`)', 'desc');
			$this->db->join('ratings', 'ratings.product_id = products.id', 'left');
			$this->db->group_by('products.id');
			 $res =  $this->db->select("IFNULL(AVG(rate), 0) AS rating,products.*,SUM(quantity) AS TotalQuantity,category.name as category_name,category.name_en as category_name_en,CONCAT(users.first_name,' ',users.last_name) as store_name".$str)->get('products')->result_array();
			 //echo $this->db->last_query();die();
			 
			 return $res;
		}

		function get_wishlist($customer_id)
		{
			$this->db->where('costumer_id', $customer_id);
			$this->db->join('products', 'products.id = product_id', 'inner');
			$this->db->join('users', 'users.id = store_id');
			$this->db->select("products.*,CONCAT(users.first_name,' ',users.last_name) as store_name");
			
			return $this->db->get('favourite')->result_array();
			
			
			
		}
		
		function save_product($data)
		{
		    $this->db->insert("products",$data);
		    return $this->db->insert_id();
		}
		
		function getProducts($category_id=null,$user_id=null,$where ="",$like="",$limit=-1,$page=0,$use_rand = 0,$child_count=0)
		{
		   
		   if($use_rand)
		   {
		        $this->db->order_by("RAND()");
		   }
		   if($where!="")
		   {
		       $this->db->where($where);
		   }
		   if($like!="")
		   {
		       $this->db->like("description",$like);
		   }
		   if($limit > 0)
		   {
		       $this->db->limit($limit,$page);
		   }
		    $str = "";
		    if($user_id != null)
		    {
		        $this->db->join("favourite","favourite.product_id=products.id  and `favourite`.`costumer_id` = $user_id","left");
		       
		        $str =",IF(favourite.product_id is NULL, 0,1) as is_customer_favorite"; 
		        
		    }
		    $this->db->where("is_deleted",0);
		  $this->db->join("category as category0","category0.id = category_id");
		    $this->db->join("types","types.id = type_id","left");
		    $this->db->join("cities","cities.id = city_id","left");
		    $this->db->join("state","state.id = state_id","left");
		    $this->db->join("customer","customer.id = store_id");
		    if($category_id!=null)
		   {
		       for($i=1;$i<=$child_count;$i++)
		       {
		           $j = $i-1;
		           $this->db->join("category as category$i","category$i.id = category$j.parent_id","left");
		          
		       }
		       $this->db->where("category$child_count.id =",$category_id);
		        
		       
		   }
			$res =  $this->db->select("products.*,city_name,state_name,products.id as id,category$child_count.name as category_name,type_name,customer.name,customer.mobile".$str)->order_by("entrydate","DESC")->get('products')->result_array();
			//echo $this->db->last_query();die();
			
			return $res;
		}

	

		function getReplacment($product_id)
		{
		    $this->db->join("replacment","replacment_id = products.id and medicine_id = $product_id");
		      $this->db->join("company","company.id= company_id","left");
		   $this->db->join("scientific_names","scientific_name_id=scientific_names.id","left");
		   $this->db->join("agent","agent.id= agent_id","left");
		   $this->db->join("country","country.id = origin_countryid","left");
		   $this->db->join("category","category.id = category_id","left");
			$res =  $this->db->select("products.*,category.name as category_name,agent_name_ar,agent_name_en,country.country as origin_country,scientific_name,scientific_name_eng,company_name_ar,company_name_en")->order_by("entrydate","DESC")->get('products')->result_array();
			return $res;
		}
		function getProduct($id,$user_id=null)
		{
		    $str = "";
		    if($user_id != null)
		    {
		        $this->db->join("favourite","product_id=products.id  and `favourite`.`costumer_id` = $user_id","left");
		       
		        $str =",IF(product_id is NULL, 0,1) as is_customer_favorite"; 
		        
		    }
		  $this->db->where("products.id",$id);
		 
		   $this->db->join("category","category.id = category_id");
		    $this->db->join("types","types.id = type_id","left");
		    $this->db->join("cities","cities.id = city_id");
		    $this->db->join("state","state.id = state_id","left");
		    $this->db->join("customer","customer.id = store_id");
			$res =  $this->db->select("products.*,city_name,state_name,products.id as id,category.name as category_name,category.name as category_name,type_name,customer.name,customer.mobile".$str)->order_by("entrydate","DESC")->get('products')->row_array();
			//echo $this->db->last_query();die();
			return $res;
		}
		
		function product_images($id)
		{
		    $this->db->where("product_id",$id);
		    return $this->db->get("images")->result_array();
		}
		function getMyOrders($user_id)
		{
		 
			$this->db->where("orders.customer_id",$user_id)->order_by("orders.id","DESC");
			$this->db->join('users', 'store_id = users.id', 'left');
			$this->db->join('customer_addresses', 'customer_addresses.id = address_id', 'left');
			
			return $this->db->select("*,orders.id as id,CONCAT(first_name,' ',last_name) as store_name")->get('orders')->result_array();
		}
		function OrderInfo($user_id,$order_id)
		{
			$this->db->join('product_order', 'product_order.item_id = products.id');
			$this->db->where('order_id', $order_id);
			$this->db->where('customer_id', $user_id);
			$this->db->join('users', 'store_id = users.id', 'left');
			$this->db->join('orders', 'orders.id = product_order.order_id');
			

			return $this->db->get('products')->result_array();
			
		}
		function customer_addresses($customer_id)
		{
			$this->db->where('customer_id', $customer_id);
			return $this->db->get('customer_addresses')->result_array();
			
		}
		function customer_coupons($customer_id,$cart_total)
		{
			$this->db->where('customer_id', $customer_id);
			$this->db->where('date_expires >= CURDATE()');
			$this->db->where('minimum_amount <', $cart_total);
			$this->db->where('(maximum_amount >='.$cart_total." OR maximum_amount=-1)");
			$this->db->join('customer_coupon', 'customer_coupon.coupon_id = coupon.id');
			return $this->db->get('coupon')->result_array();
			
			
		}
		function getNewOrders()
		{
		    $this->db->join('users', 'store_id = users.id', 'left');
		    $this->db->join("customer","customer_id = customer.id")->select("*,orders.id as id,CONCAT(first_name,' ',last_name) as store_name")->where("status","new")->order_by("orders.id","DESC");
			return $this->db->get('orders')->result_array();
		}
		function getNewOrders_delivery()
		{
		    $this->db->join('users', 'store_id = users.id', 'left');
		    $this->db->join("customer","customer_id = customer.id")->select("*,orders.id as id,CONCAT(first_name,' ',last_name) as store_name")->where("(status = 'new' or status='InProgress') and delivery_id IS NULL")->order_by("orders.id","DESC");
			return $this->db->get('orders')->result_array();
		}
		function getDeliveryOrders($user_id)
		{
		 $this->db->join('users', 'store_id = users.id', 'left');
		    $this->db->where("delivery_id",$user_id)->join("customer","customer_id = customer.id")->select("*,orders.id as id,CONCAT(first_name,' ',last_name) as store_name")->where("status","sent")->order_by("orders.id","DESC");
			return $this->db->get('orders')->result_array();
		}
		function getWaitingOrders_delivery($user_id)
		{
		    $this->db->join('users', 'store_id = users.id', 'left');
		    $this->db->where("delivery_id",$user_id)->join("customer","customer_id = customer.id")->select("*,orders.id as id,CONCAT(first_name,' ',last_name) as store_name")->where("(status = 'sent')")->order_by("orders.id","DESC");
			return $this->db->get('orders')->result_array();
		}
		function getInProgressOrders_delivery($user_id)
		{
		    $this->db->join('users', 'store_id = users.id', 'left');
		     $this->db->where("delivery_id",$user_id)->join("customer","customer_id = customer.id")->select("*,orders.id as id,CONCAT(first_name,' ',last_name) as store_name")->where("(status = 'new' or status='InProgress')")->order_by("orders.id","DESC");
			return $this->db->get('orders')->result_array();
		}
		function getDeliveredOrders($user_id)
		{
		 $this->db->join('users', 'store_id = users.id', 'left');
		    $this->db->where("delivery_id",$user_id)->join("customer","customer_id = customer.id")->select("*,orders.id as id,CONCAT(first_name,' ',last_name) as store_name")->where("(status = 'Delivered' or status = 'canceled')")->order_by("orders.id","DESC");
			return $this->db->get('orders')->result_array();
		}
		function order_details_delivery($order_id)
		{
		    $this->db->join('users', 'store_id = users.id', 'left');
		    $this->db->where("orders.id",$order_id)->join("customer","customer_id = customer.id")->select("*,orders.id as id,CONCAT(first_name,' ',last_name) as store_name");
			return $this->db->get('orders')->row_array();
		}
		
		function getorderItems($order_id,$user_id=null)
		{
		     if($user_id != null)
		    {
		        $this->db->join("favourite","product_id=products.id  and `favourite`.`costumer_id` = $user_id","left");
		       
		        $str =",IF(product_id is NULL, 0,1) as is_customer_favorite"; 
		        
		    }
		    $this->db->join("category","category.id = category_id","left");
		    $this->db->select("products.*,product_order.quantity as order_quantity,product_order.price as item_price,discount_percent");
		    $this->db->join("product_order","product_order.item_id = products.id")->where("order_id",$order_id)->order_by("product_order.id","DESC");
			return $this->db->get('products')->result_array();
		}
		
		function getCountries()
		{
			return $this->db->get('countries')->result_array();
			
		}
		function getCities()
		{
			return $this->db->get('cities')->result_array();
		}
		
		function getItemPrice($ArticleCodeId,$PriceKind)
		{

			$this->secondDB = $this->load->database('sql',True);
			$q =  $this->secondDB->query("SELECT SalesPrice FROM KT_SPR where PriceKind=$PriceKind and ArticleCodeId='".$ArticleCodeId."'")->result_array();
			$result = $q[0]["SalesPrice"];
			return $result;
		

		
		}
		function add_user_product($data)
		{
			$this->db->insert('users_products', $data);
		}
		function update_user_product($data)
		{
			$this->db->where('id', $data["id"]);
			$this->db->where('user_id', $data["user_id"]);
			$this->db->update('users_products', $data);
			
			
		}
		function get_user_products($user_id)
		{
			$this->db->join('users_products', 'item_id = products.id', 'inner');
			$this->db->join('category', 'category_id = category.id');
			
			$this->db->where('user_id', $user_id);
			$this->db->select('*,users_products.id as user_product_id,products.id as id,category.name as category_name,products.name as name');
			
			return $this->db->get('products')->result_array();
			
		}
		function get_product_users($product_id)
		{
			$this->db->join('users_products', 'item_id = products.id', 'inner');
			//$this->db->join('category', 'category_id = category.id');
			
			$this->db->where('product_id', $product_id);
			$this->db->select('*,users_products.id as user_product_id,users.id as store_id');
			
			return $this->db->get('users')->result_array();
			
		}
		function get_stores($where="")
		{
			if($where !="")
			{
				$this->db->where( $where);
				
			}
			$this->db->select("id,CONCAT(users.first_name,' ',users.last_name) as store_name,image");
			
			return $this->db->get('users')->result_array();
			
		}
		function get_product($id)
		{
			$this->db->where('products.id', $id);
			$this->db->join('category', 'category_id = category.id');
			$this->db->join('users', 'users.id = store_id');
			$this->db->join('ratings', 'ratings.product_id = products.id', 'left');
			$this->db->group_by('products.id');
			$this->db->select("IFNULL(AVG(rate), 0) AS rating,products.*,products.id as id,category.name as category_name,category.name_en as category_name_en,CONCAT(users.first_name,' ',users.last_name) as store_name");
			return $this->db->get('products')->row_array();
			
		}
		function get_product_images($product_id)
		{
			$this->db->where('product_id', $product_id);
			return $this->db->get("images")->result_array();
			
		}
		function getShops($where=null)
		{
			if($where)
				$this->db->where($where);
			
			$this->db->where('is_store', 1);
			$this->db->select("users.*,CONCAT(users.first_name,' ',users.last_name) as name");
			
			return $this->db->get('users')->result_array();
			
			
		}
		function getShop($id)
		{
			$this->db->where('id', $id);
			
			$this->db->where('is_store', 1);
			$this->db->select("users.*,CONCAT(users.first_name,' ',users.last_name) as name");
			
			return $this->db->get('users')->row_array();
		}
		function getShopCategories($id)
		{
			$this->db->where('user_categories.user_id', $id);
			$this->db->join('category', 'category_id = category.id');
			$this->db->select("*,category.id as id");
			return $this->db->get('user_categories')->result_array();
			
			
		}
		function get_related_products($category_id,$product_id)
		{
			$this->db->where('category_id', $category_id);
			$this->db->where('id !=', $product_id);
			return $this->db->get('products', 5)->result_array();
			
			
		}
		function get_all_products($user_id)
		{
			$this->db->join('users_products', 'item_id = products.id', 'left');
			$this->db->join('category', 'category_id = category.id');
			
			$this->db->where("user_id = $user_id");$this->db->or_where("user_id IS NULL");
			$this->db->select('*,users_products.id as user_product_id,products.id as id,category.name as category_name,products.name as name');
			
			return $this->db->get('products')->result_array();
		}
	    function getitem($ArticleCodeId,$user_id=null)
		{
		    $this->db->where("article_number",$ArticleCodeId);
			$res = $this->db->get("item");
			//echo $this->db->last_query();//die();
			if($res->num_rows() > 0) {
				$result=$res->result_array()[0];
        		return $result;
			}
			//echo $this->db->last_query();
			return false;
		}
		
        

		function getUsers($type)
		{
			$this->db->where("type",$type);
			$res = $this->db->get("user");
			if($res->num_rows() > 0) {
				$result=$res->result_array();
				 $obj =  array('data' => $result , "status" => "Success" , "ErrorCode" => 200);
        		return $obj;
			}
			else 
			{
				 $obj =  array('data' => array() , "status" => "Success" , "ErrorCode" => 404);
        		return $obj;
			}
		}
	


		// ayham 
		function getAllUsers(){
			$res = $this->db->get("users");
			if($res->num_rows() > 0) {
				$result=$res->result_array();
				 $obj =  array('data' => $result , "status" => "Success" , "ErrorCode" => 200);
        		return $obj;
			}
			else 
			{
				 $obj =  array('data' => array() , "status" => "Success" , "ErrorCode" => 404);
        		return $obj;
			}
		}

	
}