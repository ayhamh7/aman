<?php
class Auth_model extends CI_Model {
    
    function login($object){
    	if (isset($object->email) && isset($object->password)){
    		$this->db->where("email",$object->email);
			$this->db->where("password",md5($object->password));
			//$this->db->where("is_active",$object->password);
    		$result = $this->db->select("customer.*")->get("customer")->result_array();
    		if (count($result) > 0){
    			$token = bin2hex(openssl_random_pseudo_bytes(16));
    			
    			//add new token 
    			$user = $result[0];
    			unset($user["password"]);
    			$toAdd =  array('id' => $user["id"],'user_id'=>$user["id"]  , 'token'=>$token);
                $this->db->where("id",$user["id"])->delete("token");
    			$this->db->insert('token',$toAdd);
    			if(isset($object->android_token))
    			    $this->db->where("id",$user["id"])->update("customer",array("android_token"=>$object->android_token));
    			//return data
				$user["token"] = $token;
				$ErrorCode = 200 ;
				if($user["is_active"]==0)
				{
					$ErrorCode = 201; //user is not activated
					return array('data' => false , "status" => "Faild",'message'=>"User is not Active", "ErrorCode" => $ErrorCode);
				}
			
		        $obj =  array('data' => $user , "status" => "Success",'message'=>"", "ErrorCode" => $ErrorCode);
		        return $obj;
    		}else {
    			return array('message' => 'Username or password is wrong' , "status" => "Faild", "ErrorCode" => 100);
    		}
    	}else {
    		return array('message' => 'Validation Error' , "status" => "Faild", "ErrorCode" => 402);
    	}
    }
	
	function checkActive($user_id)
	{
		$this->db->where("id",$user_id);
		$result = $this->db->get("customer")->result_array();
		if (count($result) > 0){
			$user = $result[0];
			return array("is_active"=>$user["is_active"],"name"=>$user["name"]);
		}
		return false;
	}
	function updateUser($data)
	{
	   // var_dump($data["id"]);
		$this->db->where('id', $data["id"]);
		$this->db->update('customer', $data);
		
		return $this->db->where("id",$data["id"])->get("customer")->result_array()[0];
	}
	function register($object)
	{
	    $o_password = $object->password;
	    $object->password = md5($object->password);
		$insert_query = $this->db->insert_string('customer', $object);
		$insert_query = str_replace('INSERT INTO','INSERT IGNORE INTO',$insert_query);
		$this->db->query($insert_query);
		if($this->db->affected_rows()==0)
		{
			return array('message' => 'User exist before' , "status" => "Faild", "ErrorCode" => 400);
		}
		//$object->password = $o_password;
		return  array('data' => true , 'message'=>"Account Created Successfull", "ErrorCode" => 200);
	}

    function checkToken ($token){
    	$this->db->where("token",$token);
    	$result = $this->db->join("customer","customer.id=token.user_id","left")->get("token")->result_array();
    	return $result;
    }
    
}