<?php

class home_model extends CI_Model {

	
	function companyLogin($login , $password){
		
		$this->db->select('*');
		$this->db->from('company');
		$this->db->where('login', $login);
		$this->db->where('password', $password);
		
		$query = $this->db->get();
		
		if($query->num_rows() == 1) 
			return $query->row('id');
		else 
			return false;
	}
	function candidateLogin($login , $password){
		
		$this->db->select('*');
		$this->db->from('candidate');
		$this->db->where('login', $login);
		$this->db->where('password', $password);
		
		$query = $this->db->get();
		
		if($query->num_rows() == 1) 
			return $query->row('id');
		else 
			return false;
	}

	function addCompany($data)
	{
		$this->db->insert("company",$data);
		return $this->db->insert_id();
	}

	function addCertificate($data)
	{
		$this->db->insert("Certificate",$data);
		return $this->db->insert_id();
	}

	function addCandidate($data){
		$this->db->insert("Candidate",$data);
		return $this->db->insert_id();
	}

	function addJob($data){
		$this->db->insert("job",$data);
		return $this->db->insert_id();
	}

	function jobsToCandidates($id){
		$this->db->select('*');
		$this->db->from('job');
		$this->db->join('Certificate','Certificate.diplomaTitle = job.requiredEducationLevel');
		$this->db->join('Candidate','Candidate.experienceYears >= job.requiredExperienceYears','inner');
		$this->db->where('Certificate.candidateId', $id)->group_by("job.id");

		$query = $this->db->get();
		
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		else 
			return false;
	}

	function candidatesToJob($id){
		$this->db->select('*');
		$this->db->from('job');
		$this->db->join('Certificate','Certificate.diplomaTitle = job.requiredEducationLevel');
		$this->db->join('Candidate','Candidate.experienceYears >= job.requiredExperienceYears','inner');
		$this->db->where('job.id', $id)->group_by("job.id");
		

		$query = $this->db->get();
		
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		else 
			return false;
	}

	function filterJob($type=0,$companyId=null){
		$this->db->select('*');
		$this->db->from('job');
		if($type == 0)
			$this->db->order_by('requiredExperienceYears');
		else if($type == 1)
			$this->db->order_by('requiredEducationLevel');
		else
			$this->db->order_by('salary');
		if($companyId!=null)
			$this->db->where("companyId",$companyId);
		$query = $this->db->get();
		
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		else 
			return false;		
	}
}