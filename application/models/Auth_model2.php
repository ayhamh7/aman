<?php
class Auth_model2 extends CI_Model {
    
    function login($object){
    	if (isset($object["mobile"]) && isset($object["password"])){
    		$this->db->where("mobile",$object["mobile"]);
			$this->db->where("password",$object["password"]);
			//$this->db->where("is_active",$object->password);
    		$result = $this->db->select("delivery_person.*")->get("delivery_person")->result_array();
    		if (count($result) > 0){
    			$token = bin2hex(openssl_random_pseudo_bytes(16));
    			
    			//add new token 
    			$user = $result[0];
    			$toAdd =  array('id' => $user["id"],'user_id'=>$user["id"]  , 'token'=>$token);
                $this->db->where("id",$user["id"])->delete("delivery_token");
    			$this->db->insert('delivery_token',$toAdd);
    			//return data
				$user["token"] = $token;
				$ErrorCode = 200 ;
				if($user["is_active"]==0)
				{
					$ErrorCode = 201; //user is not activated
				}
			
		        $obj =  array('data' => $user , "status" => "Success",'message'=>"", "ErrorCode" => $ErrorCode);
		        return $obj;
    		}else {
    			return array('message' => 'Username or password is wrong' , "status" => "Faild", "ErrorCode" => 404);
    		}
    	}else {
    		return array('message' => 'Validation Error' , "status" => "Faild", "ErrorCode" => 402);
    	}
    }
	function check_password($user_id,$pass)
	{
	    	$this->db->where("id",$user_id);
	    	$this->db->where("password",$pass);
	    	$result = $this->db->get("delivery_person")->result_array();
	    	if(count($result)>0)
	    	    return true;
	    	else
	    	    return false;
	}
	function checkActive($user_id)
	{
		$this->db->where("id",$user_id);
		$result = $this->db->get("customer")->result_array();
		if (count($result) > 0){
			$user = $result[0];
			return array("is_active"=>$user["is_active"],"name"=>$user["name"]);
		}
		return false;
	}
	function updateUser($data)
	{
	   // var_dump($data["id"]);
		$this->db->where('id', $data["id"]);
		$this->db->update('delivery_person', $data);
		
		return $this->db->where("id",$data["id"])->get("delivery_person")->result_array()[0];
	}
	function register($object)
	{
		$insert_query = $this->db->insert_string('customer', $object);
		$insert_query = str_replace('INSERT INTO','INSERT IGNORE INTO',$insert_query);
		$this->db->query($insert_query);
		if($this->db->affected_rows()==0)
		{
			return array('message' => 'User exist before' , "status" => "Faild", "ErrorCode" => 400);
		}
		return $this->login($object);
	}

    function checkToken ($token){
    	$result = $this->db->where("token",$token)->get("delivery_token")->result_array();
    	return $result;
    }
    
}