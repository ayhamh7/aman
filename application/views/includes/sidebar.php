<section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info" style="height: 50px;">
               
                <div class="info-container" style="top: 0px;">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?= $this->ion_auth->user()->row()->username; ?></div>
                    
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="<?= base_url()?>auth/logout"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu" style="overflow-y: scroll;">
                <ul class="list">
               
                      <li class="<?= isset($customer_active)?'active':''?>">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">assignment</i>
                            <span><?= lang("manage_customers")?></span>
                        </a>
                        <ul class="ml-menu">
                      
                           
                             
                        
                        <li>
                                <a href="<?= base_url() ?>dashboard/manageCustomers"><?= lang("manage_all_customers")?></a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>dashboard/manageActiveCustomers"><?= lang("manage_active_customer")?></a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>dashboard/manageUnActiveCustomers"><?= lang("manage_unactive_customer")?></a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>dashboard/manage_general_notifications">General Notifications</a>
                            </li>
                              
                        </ul>
                    </li>
                    
                     <li class="<?= isset($orders_active)?'active':''?>">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">assignment</i>
                            <span><?= lang("manage_orders")?></span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="<?= base_url() ?>dashboard/manageOrders_C">Active Orders</a>
                            </li>
                             <li>
                                <a href="<?= base_url() ?>dashboard/manageOrders_A">InProgress Orders</a>
                            </li>
                             <li>
                                <a href="<?= base_url() ?>dashboard/manageOrders_charged">Sent Orders</a>
                            </li>
                             <li>
                                <a href="<?= base_url() ?>dashboard/manageOrders_Done">Finished Orders</a>
                            </li>
                           <li>
                                <a href="<?= base_url() ?>dashboard/manageOrders_Deleted">Canceled Orders</a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>dashboard/Cash_Report"> Reports</a>
                            </li>
                           
                        </ul>
                    </li>
                    
                    
                    <?php if($this->ion_auth->is_admin()):?>
                      <li class="<?= isset($gm_active)?'active':''?>">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">person</i>
                            <span><?= lang("general_management")?></span>
                        </a>
                        <ul class="ml-menu">
                      
                        <li>
                                <a href="<?= base_url() ?>dashboard/manage_products">Manage Products</a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>dashboard/manage_coupon">Manage Coupons</a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>dashboard/manage_partners">Manage Partners</a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>dashboard/manage_prochoures">Manage prochoures</a>
                            </li>
                            
                        <li>
                                <a href="<?= base_url() ?>auth"><?= lang("manage_admins")?></a>
                            </li>
                          <li>
                                <a href="<?= base_url() ?>dashboard/manage_category"><?= lang("manage_categories")?></a>
                            </li>
                        
 
                            
                            <li>
                                <a href="<?= base_url() ?>dashboard/manage_delivery_person">manage delivery person</a>
                            </li>
                          
                            <li>
                                <a href="<?= base_url() ?>dashboard/manage_slider"><?= lang("manage_slider")?></a>
                            </li>
                          
                            
                            
                              
                        </ul>
                    </li>
                    <?php endif?>
                
                   <li class="<?= isset($site_active)?'active':''?>">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">assignment</i>
                            <span>Site Managment</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="<?= base_url() ?>dashboard/manage_site_ploicy">Policy</a>
                            </li>
                             <li>
                                <a href="<?= base_url() ?>dashboard/manage_contact">Contact</a>
                            </li>
                             <li>
                                <a href="<?= base_url() ?>dashboard/manage_site_about">About</a>
                            </li>
                            
                             <li>
                                <a href="<?= base_url() ?>dashboard/manage_site_faq">FAQ's</a>
                            </li>
                         
                           
                        </ul>
                    </li>
                   
                 
                   
                    
                </ul>
            </div>
            <!-- #Menu -->
           
        </aside>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
     
        <!-- #END# Right Sidebar -->
    </section>