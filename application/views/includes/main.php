<?php $this->load->view('includes/header.php');  ?>
<?php $this->load->view('includes/sidebar.php');  ?>
<section class="content">
    <div class="container-fluid">
    <?php $this->load->view($main_content);  ?>
    </div>
</section>

<?php $this->load->view('includes/footer.php');  ?>