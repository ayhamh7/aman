
<style  >
/*.owl-stage-outer{*/
/*    margin-top: 1%;}*/
</style>

<main class="main" style="<$lang=='ar'?'text-align:right;direction:rtl;':''?>>">
	<div class="mb-lg-2"></div><!-- End .mb-lg-2 -->
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-xxl-8 ">
				<div class="intro-slider-container slider-container-ratio mb-2">
					<div class="intro-slider owl-carousel owl-simple owl-nav-inside" data-toggle="owl" data-owl-options='{
							"nav": false, 
							"dots": true,
							"autoplay":3000
						}'>
						<?php foreach($sliders as $slider):?>
						<div class="intro-slide">
							<figure class="slide-image" >
								<picture>
									<source media="(max-width: 480px)" srcset="<?= base_url('uploads/'.$slider["image"])?>">
									<img src="<?= base_url('uploads/'.$slider["image"])?>" alt="Image Desc">
								</picture>
							</figure><!-- End .slide-image -->

							<div class="intro-content">
								
								<h1 class="intro-title text-black">
									<?= $slider["title"]?>
								</h1><!-- End .intro-title -->
                                 <div class="intro-text text-white">
                                        <?= $slider["description"]?>
                                    </div><!-- End .intro-text -->
                                    <?php if($slider['url'] && $slider['url']!=''):?>
                                    <a href="<?= $slider['url']?>" class="btn btn-primary">
                                        <span>Discover Now</span>
                                        <i class="icon-long-arrow-right"></i>
                                    </a>
                                     <?php elseif($slider['product_id']):?>
                                    <a href="<?= base_url('product?id='.$slider['product_id'])?>" class="btn btn-primary">
                                        <span>Discover Now</span>
                                        <i class="icon-long-arrow-right"></i>
                                    </a>
                                    <?php endif?>
								

							</div><!-- End .intro-content -->
						</div><!-- End .intro-slide -->
						<?php endforeach;?>
					</div><!-- End .intro-slider owl-carousel owl-simple -->
					
					<span class="slider-loader"></span><!-- End .slider-loader -->
				</div><!-- End .intro-slider-container -->
			</div><!-- End .col-xl-9 col-xxl-10 -->
			
			<div class="col-lg-4">
                            <div class="intro-banners">
                                <?php foreach($sideads_header as $value):?>
                                <div class="banner mb-lg-1 mb-xl-2">
                                    <a href="#">
                                        <img src="<?= base_url('uploads/'.$value['image'])?>" style="max-height: 160px;" alt="Banner">
                                    </a>

                                    <div class="banner-content">
                                        <!--<h4 class="banner-subtitle d-lg-none d-xl-block"><a href="#">Top Product</a></h4>-->
                                        <!-- End .banner-subtitle -->
                                        <!--<h3 class="banner-title"><a href="#"> <br>Stereo Bluetooth</a></h3><!-- End .banner-title -->
                                        <a href="<?= $value['url']?>" class="banner-link">Shop Now<i class="icon-long-arrow-right"></i></a>
                                    </div><!-- End .banner-content -->
                                </div><!-- End .banner -->
                                <?php endforeach?>
                              
                            </div><!-- End .intro-banners -->
                        </div>
		</div><!-- End .row -->
	</div><!-- End .container-fluid -->
	<div class="container-fluid" style="margin-top: 2%;">
		<div class="row">
			<div class="col-xl-9 col-xxl-12">
				

	            <div class="heading heading-flex mb-3" style="flex-direction: column;text-align: center;">
					<div class="category-head mb-20" style="transform: translateY(-50%);width: 90%;content: '';height: 2px;background: #cecdcd;z-index: 0;">

							<div class="title_gp2" style="float: right;background: #ffffff;padding-left: 20px;position: relative;z-index: 2;font-size: x-large;margin-top: -2%;font-weight: bold;">
								<span class="subtitle"><?= lang("Most Selles")?></span>
							</div>

							<div class="moreCol" style="float: left;background: #ffffff;padding-right: 20px;position: relative;z-index: 2;font-size: x-large;margin-top: -2%;">
								<i class="icon-arrow-left"></i><a href="<?= base_url('mostSell')?>" style="">  <?= lang("more") ?>        </a>
							</div>

						</div>
					

				</div>
				

				<div class="row cat-banner-row electronics">
					

					<div class="col-xl-12 col-xxl-8">
						<div class="owl-carousel owl-full carousel-equal-height carousel-with-shadow" data-toggle="owl" 
							data-owl-options='{
								"nav": true, 
								"dots": false,
								"margin": 20,
								"loop": false,
								"responsive": {
									"0": {
										"items":2
									},
									"480": {
										"items":2
									},
									"768": {
										"items":3
									},
									"992": {
										"items":4
									},
									"1200": {
										"items":5
									},
									"1600": {
										"items":4
									}
								}
							}'>
							<?php foreach ($most_selles as $key => $product) :?>
								<?php $this->load->view('sub_viewes/product', array("product"=>$product)); ?>
							<?php endforeach?>
						</div><!-- End .owl-carousel -->
					</div><!-- End .col-xl-9 -->
				</div><!-- End .row cat-banner-row -->

				
				<div class="mb-3"></div><!-- End .mb-3 -->
<div class="row">
					<div class="col-lg-12 col-xxl-4-5col" style="">
						<div class="row owl-carousel owl-simple brands-carousel" data-toggle="owl" 
                                data-owl-options='{
                                    "nav": true, 
                                    "dots": false,
                                    "margin": 20,
                                    "loop": false,
                                    "responsive": {
                                        "0": {
                                            "items":1
                                        },
                                        "420": {
                                            "items":1
                                        },
                                        "600": {
                                            "items":2
                                        },
                                        "900": {
                                            "items":2
                                        },
                                        "1600": {
                                            "items":2,
                                            "nav": true
                                        }
                                    }
                                }'>
							<?php for($i=0;$i<4;$i++):?>
								<?php if(isset($ads[$i])):?>
									<div class="col-md-12">
										<div class="banner banner-overlay">
											<a href="#">
												<img src="<?= base_url('uploads/'.$ads[$i]["image"])?>" alt="Banner img desc">
											</a>

											<div class="banner-content">
												<h4 class="banner-subtitle text-white d-none d-sm-block"><a href="#"><?= $ads[$i]["title"]?></a></h4><!-- End .banner-subtitle -->
												<h3 class="banner-title text-white"><a href="#"><?= $ads[$i]["title"]?></a></h3><!-- End .banner-title -->
												<?php if($ads[$i]['url'] && $ads[$i]['url']!=''):?>
												<a href="<?= $ads[$i]["url"]?>" class="banner-link">Shop Now <i class="icon-long-arrow-right"></i></a>
												<?php elseif($ds[$i]['product_id']):?>
												<a href="<?= base_url('product?id='.$ds[$i]['product_id'])?>" class="banner-link">Shop Now <i class="icon-long-arrow-right"></i></a>
												<?php endif?>
											</div><!-- End .banner-content -->
										</div><!-- End .banner -->
									</div><!-- End .col-md-6 -->
								<?php endif?>
							<?php endfor?>
						</div><!-- End .row -->
					</div><!-- End .col-lg-3 col-xxl-4-5col -->
				</div><!-- End .row -->
				
				
				<div class="mb-3"></div><!-- End .mb-3 -->

				<!-- <div class="owl-carousel owl-simple brands-carousel" data-toggle="owl" 
					data-owl-options='{
						"nav": false, 
						"dots": false,
						"margin": 20,
						"loop": false,
						"responsive": {
							"0": {
								"items":2
							},
							"420": {
								"items":3
							},
							"600": {
								"items":4
							},
							"900": {
								"items":5
							},
							"1600": {
								"items":6,
								"nav": true
							}
						}
					}'>
					<?php foreach ($home_shops as $key => $value) :?>
					<a href="<?= base_url('shop?id='.$value["id"])?>" class="brand">
						<img src="<?= base_url('uploads/'.$value["image"])?>" style="max-width: 120px !important;" alt="Brand Name">
					</a>
					<?php endforeach?>
				
				</div> -->
				
				   <div class="owl-carousel owl-simple brands-carousel" data-toggle="owl" 
                            data-owl-options='{
                                "nav": true, 
                                "dots": true,
                                "margin": 20,
                                "loop": false,
                                "responsive": {
                                    "0": {
                                        "items":2
                                    },
                                    "420": {
                                        "items":3
                                    },
                                    "600": {
                                        "items":4
                                    },
                                    "900": {
                                        "items":5
                                    },
                                    "1600": {
                                        "items":6,
                                        "nav": true
                                    }
                                }
                            }'>
				       <?php foreach($prochoures as $prochoure):?>
                            <a href="<?= base_url('prochoure?id='.$prochoure['id'])?>" class="brand">
                                <img src="<?= base_url('uploads')?>/<?= $prochoure['main_image']?>" alt="Brand Name">
                            </a>
                        <?php endforeach?>
                            
                        </div><!-- End .owl-carousel -->
				<!-- End .Home Shops -->

				<div class="mb-5"></div><!-- End .mb-5 -->
				
			


				<div class="heading heading-flex mb-3" style="flex-direction: column;text-align: center;">
					<div class="category-head mb-20" style="transform: translateY(-50%);width: 90%;content: '';height: 2px;background: #cecdcd;z-index: 0;">

							<div class="title_gp2" style="float: right;background: #ffffff;padding-left: 20px;position: relative;z-index: 2;font-size: x-large;margin-top: -2%;font-weight: bold;">
								<span class="subtitle"><?= lang("New Products")?></span>
							</div>

							<div class="moreCol" style="float: left;background: #ffffff;padding-right: 20px;position: relative;z-index: 2;font-size: x-large;margin-top: -2%;">
								<i class="icon-arrow-left"></i><a href="<?= base_url('newProducts')?>" style=""> <?= lang("more") ?>       </a>
							</div>

						</div>
					

				</div>
				

				<div class="row cat-banner-row electronics">
					

					<div class="col-xl-12 col-xxl-12">
						<div class="owl-carousel owl-full carousel-equal-height carousel-with-shadow" data-toggle="owl" 
							data-owl-options='{
								"nav": true, 
								"dots": false,
								"margin": 20,
								"loop": false,
								"responsive": {
									"0": {
										"items":2
									},
									"480": {
										"items":2
									},
									"768": {
										"items":3
									},
									"992": {
										"items":4
									},
									"1200": {
										"items":5
									},
									"1600": {
										"items":4
									}
								}
							}'>
							<?php foreach ($new_products as $key => $product) :?>
								<?php $this->load->view('sub_viewes/product', array("product"=>$product)); ?>
							<?php endforeach?>
						</div><!-- End .owl-carousel -->
					</div><!-- End .col-xl-9 -->
				</div><!-- End .row cat-banner-row -->

				<div class="mb-3"></div><!-- End .mb-3 -->
				

				<div class="row owl-carousel owl-simple brands-carousel" data-toggle="owl" 
                                data-owl-options='{
                                    "nav": true, 
                                    "dots": false,
                                    "margin": 20,
                                    "loop": false,
                                    "responsive": {
                                        "0": {
                                            "items":1
                                        },
                                        "420": {
                                            "items":1
                                        },
                                        "600": {
                                            "items":2
                                        },
                                        "900": {
                                            "items":2
                                        },
                                        "1600": {
                                            "items":2,
                                            "nav": true
                                        }
                                    }
                                }'>
					<?php for($i=4;$i<count($ads);$i++):?>
						<div class="col-md-12">
							<div class="banner banner-overlay">
								<a href="#">
									<img src="<?= base_url('uploads/'.$ads[$i]["image"])?>" alt="Banner img desc">
								</a>

								<div class="banner-content">
									<h4 class="banner-subtitle text-white d-none d-sm-block"><a href="#"><?= $ads[$i]["title"]?></a></h4><!-- End .banner-subtitle -->
									<h3 class="banner-title text-white"><a href="#"><?= $ads[$i]["title"]?></a></h3><!-- End .banner-title -->
									<?php if($ads[$i]['url'] && $ads[$i]['url']!=''):?>
									<a href="<?= $ads[$i]["url"]?>" class="banner-link">Shop Now <i class="icon-long-arrow-right"></i></a>
									<?php elseif($ds[$i]['product_id']):?>
									<a href="<?= base_url('product?id='.$ds[$i]['product_id'])?>" class="banner-link">Shop Now <i class="icon-long-arrow-right"></i></a>
									<?php endif?>
								</div><!-- End .banner-content -->
							</div><!-- End .banner -->
						</div><!-- End .col-md-6 -->
					<?php endfor?>
				</div><!-- End .row -->

				<div class="mb-3"></div><!-- End .mb-3 -->

				<div class="bg-lighter trending-products">
						<div class="heading-left" style="text-align: center;">
							<h2 class="title"><?= lang("Daily Offers")?></h2><!-- End .title -->
						</div><!-- End .heading-left -->
						<br>
					<!--<div class="heading heading-flex mb-3">-->
					

					<!--	<div class="heading-left">-->
					<!--		<ul class="nav nav-pills justify-content-center" role="tablist">-->
					<!--			<?php foreach ($day_offers as $key => $value) :?>-->
					<!--			<li class="nav-item">-->
					<!--				<a class="nav-link" id="trending-elec-link" data-toggle="tab" href="#<?="cat_".$value["id"]?>-tab" role="tab" aria-controls="trending-elec-tab" aria-selected="false"><?= $value["name".$suffix]?></a>-->
					<!--			</li>-->
					<!--			<?php endforeach?>-->
							
					<!--		</ul>-->
					<!--	</div><!-- End .heading-right -->
					<!--</div>-->
					
					<!-- End .heading -->

					

					<div class="">
					 	<div class="row">
					<?php $i=0;shuffle($day_offers);foreach ($day_offers as $key => $value) :
					shuffle($value["products"]);
					?>
						
						        
								<?php foreach ($value["products"] as $key => $product): ?>
								<div class="col-3 col-md-3">
									<?php $this->load->view('sub_viewes/product', array("product"=>$product)); ?>
								</div>
								<?php endforeach?>
							
							<!-- End .owl-carousel -->
					
					<?php $i++;endforeach?>
				        </div>
					
					</div><!-- End .tab-content -->
				</div><!-- End .bg-lighter -->

				

				<div class="mb-3"></div><!-- End .mb-3 -->

				<div class="bg-lighter trending-products">
						<div class="heading-left" style="text-align: center;">
							<h2 class="title"><?= lang("Weekly Offers")?></h2><!-- End .title -->
						</div><!-- End .heading-left -->
					<div class="heading heading-flex mb-3">
					

						<!--<div class="heading-left">-->
						<!--	<ul class="nav nav-pills justify-content-center" role="tablist">-->
						<!--		<?php foreach ($week_offers as $key => $value) :?>-->
						<!--		<li class="nav-item">-->
						<!--			<a class="nav-link" id="trending-elec-link" data-toggle="tab" href="#<?="cat_".$value["id"]?>-tab2" role="tab" aria-controls="trending-elec-tab" aria-selected="false"><?= $value["name".$suffix]?></a>-->
						<!--		</li>-->
						<!--		<?php endforeach?>-->
							
						<!--	</ul>-->
						<!--</div>-->
					</div><!-- End .heading -->

					

					<div class="row">
					<?php $i=0;shuffle($week_offers);foreach ($week_offers as $key => $value) :
					shuffle($value["products"]);
					?>
						
							
								<?php foreach ($value["products"] as $key => $product): ?>
								<div class="col-3 col-md-3">
									<?php $this->load->view('sub_viewes/product', array("product"=>$product)); ?>
								</div>
								<?php endforeach?>
							
							
					
					<?php $i++;endforeach?>
					</div><!-- .End .tab-pane -->
					
					</div><!-- End .tab-content -->
				<div class="icon-boxes-container">
					<div class="container-fluid">
						<div class="row">
							<div class="col-sm-6 col-lg-3">
								<div class="icon-box icon-box-side">
									<span class="icon-box-icon text-dark">
										<i class="icon-rocket"></i>
									</span>
									<div class="icon-box-content">
										<h3 class="icon-box-title">Free Shipping</h3><!-- End .icon-box-title -->
										<p>Orders $50 or more</p>
									</div><!-- End .icon-box-content -->
								</div><!-- End .icon-box -->
							</div><!-- End .col-sm-6 col-lg-3 -->

							<div class="col-sm-6 col-lg-3">
								<div class="icon-box icon-box-side">
									<span class="icon-box-icon text-dark">
										<i class="icon-rotate-left"></i>
									</span>

									<div class="icon-box-content">
										<h3 class="icon-box-title">Free Returns</h3><!-- End .icon-box-title -->
										<p>Within 30 days</p>
									</div><!-- End .icon-box-content -->
								</div><!-- End .icon-box -->
							</div><!-- End .col-sm-6 col-lg-3 -->

							<div class="col-sm-6 col-lg-3">
								<div class="icon-box icon-box-side">
									<span class="icon-box-icon text-dark">
										<i class="icon-info-circle"></i>
									</span>

									<div class="icon-box-content">
										<h3 class="icon-box-title">Get 20% Off 1 Item</h3><!-- End .icon-box-title -->
										<p>When you sign up</p>
									</div><!-- End .icon-box-content -->
								</div><!-- End .icon-box -->
							</div><!-- End .col-sm-6 col-lg-3 -->

							<div class="col-sm-6 col-lg-3">
								<div class="icon-box icon-box-side">
									<span class="icon-box-icon text-dark">
										<i class="icon-life-ring"></i>
									</span>

									<div class="icon-box-content">
										<h3 class="icon-box-title">We Support</h3><!-- End .icon-box-title -->
										<p>24/7 amazing services</p>
									</div><!-- End .icon-box-content -->
								</div><!-- End .icon-box -->
							</div><!-- End .col-sm-6 col-lg-3 -->
						</div><!-- End .row -->
					</div><!-- End .container-fluid -->
				</div><!-- End .icon-boxes-container -->

				<div class="mb-5"></div><!-- End .mb-5 -->
				<div class="owl-carousel owl-simple brands-carousel" data-toggle="owl" 
					data-owl-options='{
						"nav": false, 
						"dots": false,
						"margin": 20,
						"loop": false,
						"responsive": {
							"0": {
								"items":2
							},
							"420": {
								"items":3
							},
							"600": {
								"items":4
							},
							"900": {
								"items":5
							},
							"1600": {
								"items":6,
								"nav": true
							}
						}
					}'>
					<?php foreach ($partners as $key => $value) :?>
					<a href="<?= $value['url']?>" class="brand">
						<img src="<?= base_url('uploads/'.$value["image"])?>" style="max-width: 120px !important;" alt="<?= $value['name']?>">
					</a>
					<?php endforeach?>
				
				</div> 
			</div><!-- End .col-lg-9 col-xxl-10 -->
                	<div class="col-lg-3">
                            <div class="intro-banners">
                                <?php foreach($sideads_main as $value):?>
                                <div class="banner mb-lg-1 mb-xl-2">
                                    <a href="#">
                                        <img src="<?= base_url('uploads/'.$value['image'])?>" alt="Banner">
                                    </a>

                                    <div class="banner-content">
                                        <!--<h4 class="banner-subtitle d-lg-none d-xl-block"><a href="#">Top Product</a></h4>-->
                                        <!-- End .banner-subtitle -->
                                        <!--<h3 class="banner-title"><a href="#"> <br>Stereo Bluetooth</a></h3><!-- End .banner-title -->
                                        <a href="<?= $value['url']?>" class="banner-link">Shop Now<i class="icon-long-arrow-right"></i></a>
                                    </div><!-- End .banner-content -->
                                </div><!-- End .banner -->
                                <?php endforeach?>
                              
                            </div><!-- End .intro-banners -->
                        </div>
			
		</div><!-- End .row -->
	</div><!-- End .container-fluid -->
</main><!-- End .main -->