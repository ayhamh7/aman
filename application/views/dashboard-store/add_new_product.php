
<?php $this->load->view('includes-store/header.php');  ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets2/css/pages.css">
<link rel="stylesheet" href="<?= base_url('web/')?>fancyfileuploader/fancy-file-uploader/fancy_fileupload.css">
<link rel="stylesheet" href="<?= base_url('assets2/')?>css/select2.min.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url('assets2/')?>css/bootstrap-multiselect.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url('assets2/')?>css/multi-select.css" />
<?php $this->load->view('includes-store/sidebar.php');  ?>

                    <div class="pcoded-content">

                        <div class="page-header card">
                            <div class="row align-items-end">
                                <div class="col-lg-8">
                                    <div class="page-header-title">
                                        <i class="feather icon-clipboard bg-c-blue"></i>
                                        <div class="d-inline">
                                            <h5>Add New Product to Your Store</h5>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="page-header-breadcrumb">
                                      
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="pcoded-inner-content">

                            <div class="main-body">
                                <div class="page-wrapper">

                                    <div class="page-body">
                                        <div class="row">
                                            <div class="col-sm-12">

                                                <div class="card">
                                                    <div class="card-header">
                                                        <h5>Product Info</h5>
                                                      
                                                    </div>
                                                    <div class="card-block" style="direction: rtl;text-align: right;">
                                                        
                                                        <form id="product_form" method="post" action="<?= base_url('store_dashboard/saveImages')?>">
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">المنتج</label>
                                                                <div class="col-sm-10">
                                                                    <input name="name" type="text" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label
                                                                    class="col-sm-2 col-form-label">السعر الحقيقي</label>
                                                                <div class="col-sm-10">
                                                                    <input name="regular_price" type="text" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">سعر المبيع</label>
                                                                <div class="col-sm-10">
                                                                    <input name="sale_price" type="text" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">الوصف</label>
                                                                <div class="col-sm-10">
                                                                    
                                                                    <textarea name="description" id="input" class="form-control" rows="3" required="required"></textarea>
                                                                    
                                                                </div>
                                                            </div>
                                                             <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">التصنيف الرئيسي</label>
                                                                <div class="col-sm-10">
                                                                    <select name="category_id" id="category_id" class="js-example-basic-single col-sm-12">
                                                                        <?php foreach($categories as $category):?>
                                                                        <option value="<?= $category['id']?>"><?= $category['name']?></option>
                                                                        <?php endforeach ?>
                                                                   
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            
                                                            
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">التصنيفات الفرعية</label>
                                                                <div class="col-sm-10">
                                                                    <input type="hidden" name="categories" id="categories_str">
                                                                    <select class="js-example-basic-multiple col-sm-12"  id="Categories" multiple="multiple">
                                                                       
                                                                    </select>
                                                                </div>
                                                            </div>
                                                           
                                                             <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">صور المنتج</label>
                                                                <div class="col-sm-10">
                                                                    
                                                                   <input id="demo" type="file" name="files" accept=".jpg, .png, image/jpeg, image/png" multiple style="opacity: 0;">
                                                                </div>
                                                            </div>
                                                            <center></center>
                                                           <button type="button" class="btn btn-lg btn-success" id="save_btn" onclick="savedata();return false;" style="width: 20%;margin-right: 65%;margin-top: 10%;">أضف منتجك الآن</button>
                                                           </center>
                                                        </form>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div id="styleSelector">
                            </div>
                        </div>
                    </div>

<?php $this->load->view('includes-store/footer.php');  ?>

<script src="//code.jquery.com/jquery.min.js"></script>
<script src="<?= base_url('web/')?>fancyfileuploader/fancy-file-uploader/jquery.ui.widget.js"></script>
<script src="<?= base_url('web/')?>fancyfileuploader/fancy-file-uploader/jquery.fileupload.js"></script>
<script src="<?= base_url('web/')?>fancyfileuploader/fancy-file-uploader/jquery.iframe-transport.js"></script>
<script src="<?= base_url('web/')?>fancyfileuploader/fancy-file-uploader/jquery.fancy-fileupload.js"></script>
<script>
setTimeout(()=>{
        $.ajax({
          url: "<?= base_url()?>"+"Api/SubCategories?cat_id="+<?= $categories[0]["id"]?>,
          dataType: "json",
          success: function( data ) {
           
                $('#Categories').empty(); 
            $('#Categories').val(null).trigger('change');
            $.map( data, function( item ) {
             var newState = new Option(item.name +'-'+item.name_en, item.id, true, false);
                // Append it to the 'select'
                $("#Categories").append(newState).trigger('change');
           // $('#Categories').append('<option value='+item.id+'>' + item.name +'-'+item.name_en + '</option>');
            })
             $('#Categories').val(null).trigger('change');
           // $('#Categories').trigger('liszt:updated');
           // $('#Categories').trigger("chosen:updated");
          }
           
        
       });
       
        $('#category_id').change(function(){
        //alert($("#category_id").val())
        $.ajax({
          url: "<?= base_url()?>"+"Api/SubCategories?cat_id="+$("#category_id").val()+"/",
          dataType: "json",
          success: function( data ) {
           
                $('#Categories').empty(); 
            $('#Categories').val(null).trigger('change');
            $.map( data, function( item ) {
             var newState = new Option(item.name +'-'+item.name_en, item.id, true, false);
                // Append it to the 'select'
                $("#Categories").append(newState).trigger('change');
           // $('#Categories').append('<option value='+item.id+'>' + item.name +'-'+item.name_en + '</option>');
            })
             $('#Categories').val(null).trigger('change');
           // $('#Categories').trigger('liszt:updated');
           // $('#Categories').trigger("chosen:updated");
          }
           
        
       });
    })
},2000)

</script>
<!--<script src="http://malsup.github.com/jquery.form.js"></script> -->
<script>


    var files_number = 0;
    var uploaded_file = 0;
    var url ='test';
    $('#demo').FancyFileUpload({
      params: {
        action: 'fileuploader',
        product_id :$("#product_id").val()
      },
      added : function(e, data) {
			// It is okay to simulate clicking the start upload button.
			files_number++;
			this.find('.ff_fileupload_actions button.ff_fileupload_remove_file').click(function(){files_number--;})
			this.find('.ff_fileupload_actions button.ff_fileupload_start_upload').css("display","none");
    },
    'uploadcompleted' : function(){uploaded_file++;console.log("after upload"+$("#product_id").val())},
    'edit' : false,
      maxfilesize: 5000000,
      maxNumberOfFiles: 4,
      'edit': false,
      'url' : '<?= base_url('store_dashboard/saveImages')?>'
    });
 function savedata()
    {
        if(files_number==0)
        {
            alert("يجب ترفيع صورة واحدة على الأقل");
            return false;
        }
      $("#categories_str").val($("#Categories").val().join(','));
     $.ajax({
                url : '<?=  base_url('store_dashboard/save_product')?>',
                type : 'post',
                data : $('#product_form').serialize(),
                success : function (res){
                    res = JSON.parse(res);
                   
              
                $("#product_id").val(res.data)
                console.log(res)
                console.log("res = "+res.data)
                console.log('amer '+$("#product_id").val());
               // $('input[name ="product_id"]').val(res.data)
                $('#demo').next().find('.ff_fileupload_actions button.ff_fileupload_start_upload').click();
                $("#save_btn").attr("disabled", true);
                 var handle = setInterval(function(){
                      if(uploaded_file==files_number)
                      {
                         
                            clearInterval(handle);
                            location.reload();
                      }
                      //  
                      
                  },1000);
                },
                error :  function (msg){
                   // alert(return_url);
                   //window.location = return_url;
                    console.log(msg);
                }
            });
    }
</script>