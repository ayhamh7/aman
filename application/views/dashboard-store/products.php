<?php $this->load->view('includes-store/header.php');  ?>
<link rel="stylesheet" type="text/css" href="<?= base_url('assets2/')?>css/datatables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="<?= base_url('assets2/')?>css/buttons.datatables.min.css">
<link rel="stylesheet" type="text/css" href="<?= base_url('assets2/')?>css/responsive.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets2/css/pages.css">
<?php $this->load->view('includes-store/sidebar.php');  ?>


                    <div class="pcoded-content">

                        <div class="page-header card">
                            <div class="row align-items-end">
                                <div class="col-lg-8">
                                    <div class="page-header-title">
                                        <i class="feather icon-clipboard bg-c-blue"></i>
                                        <div class="d-inline">
                                            <h5>جميع المنتجات</h5>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="page-header-breadcrumb">
                                      
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="pcoded-inner-content">

                            <div class="main-body">
                                <div class="page-wrapper">

                                    <div class="page-body">
                                        <div class="row">
                                            <div class="col-sm-12">

                                                <div class="card">
                                                    <div class="card-header">
                                                        <h5>معلومات المنتجات</h5>
                                                      
                                                    </div>
                                                    <div class="card-block" style="direction: rtl;text-align: right;">
                                                      <table id="multi-colum-dt" class="table table-striped table-bordered nowrap">
                                                            <thead>
                                                                <tr>
                                                                    <th>المنتج</th>
                                                                    <th>السعر الحقيقي</th>
                                                                    <th>سعر المبيع</th>
                                                                    <!--<th>الوصف</th>-->
                                                                    <th>الصورة</th>
                                                                    <th></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php foreach($products as $product):?>
                                                            <tr>
                                                                <td><?= $product["name"]?></td>
                                                                <td><?= $product["regular_price"]?></td>
                                                                <td><?= $product["sale_price"]?></td>
                                                                <!--<td><?= $product["description"]?></td>-->
                                                                <td><img style="max-width: 120px;" src="<?= base_url('uploads/thumb/'.$product["image"])?>"></td>
                                                                <td>
                                                                    <a href="<?= base_url('store_dashboard/manage_album/'.$product["id"])?>" class="btn btn-success waves-effect waves-light" data-toggle="popover" title="" data-placement="bottom" data-original-title="Success color states">الألبوم</a>
                                                                </td>
                                                            </tr>
                                                           <?php endforeach?>
                                                            </tbody>
                                                            <tfoot>
                                                            <tr>
                                                                <th>المنتج</th>
                                                                    <th>السعر الحقيقي</th>
                                                                    <th>سعر المبيع</th>
                                                                    <!--<th>الوصف</th>-->
                                                                    <th>الصورة</th>
                                                                    <th></th>
                                                            </tr>
                                                            </tfoot>
                                                            </table>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div id="styleSelector">
                            </div>
                        </div>
                    </div>

<?php $this->load->view('includes-store/footer.php');  ?>
