

<?php $this->load->view('includes/header.php');  ?>
<?php $this->load->view('includes/sidebar.php');  ?>
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
             
            <div class="row clearfix">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box hover-zoom-effect">
                            <div class="icon bg-pink">
                                <i class="material-icons">attach_money</i>
                            </div>
                            <div class="content">
                                <div class="text"> Orders</div>
                                <div class="number"><?= $orders_count?></div>
                            </div>
                        </div>
    
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box hover-zoom-effect">
                            <div class="icon bg-blue">
                                <i class="material-icons">people_outline</i>
                            </div>
                            <div class="content">
                                <div class="text">unactive users</div>
                                <div class="number"><?= $unactive?></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box hover-zoom-effect">
                            <div class="icon bg-light-blue">
                                <i class="material-icons">people</i>
                            </div>
                            <div class="content">
                                <div class="text">active users</div>
                                <div class="number"><?= $active?></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box hover-zoom-effect">
                            <div class="icon bg-cyan">
                                <i class="material-icons">image</i>
                            </div>
                            <div class="content">
                                <div class="text">Messages Request</div>
                                <div class="number"><?= $not_image?></div>
                            </div>
                        </div>
                    </div>
                </div>
              <div class="card">
                        <div class="header">
                            <h2><?= $title?></h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>
                                        <li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a></li>
                                        <li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                        
                            <?php  if(isset($output->js_files) && isset($is_images)):
                                foreach($output->js_files as $file): ?>
                                 <script src="<?php echo $file; ?>" type="text/javascript"></script>
                                <?php endforeach; ?>
                                <?php endif;?>

                            <div class="table-responsive">
                               <?php echo $output->output; ?>
                            </div>
                        </div>
                    </div>
                 
            </div>
        </div>
    </section>
  <?php $this->load->view('includes/footer.php');  ?>
   <script>
    $('#field-category_id').change(function(){
        //alert($("#field-category_id").val())
        $.ajax({
          url: "<?= base_url()?>"+"Api/SubCategories?cat_id="+$("#field-category_id").val()+"/",
          dataType: "json",
          success: function( data ) {
           
                $('#field-Categories').empty(); 
            
            $.map( data, function( item ) {
            console.log(item.name)
            $('#field-Categories').append('<option value='+item.id+'>' + item.name +'-'+item.name_en + '</option>');
            })
            $('#field-Categories').trigger('liszt:updated');
            $('#field-Categories').trigger("chosen:updated");
          }
           
        
       });
    })
</script>