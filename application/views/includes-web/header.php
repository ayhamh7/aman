<!DOCTYPE html>
<html lang="en">


<!-- 1-Deals/index-14.html  22 Nov 2019 09:59:31 GMT -->
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>1-Deals</title>
    <meta name="keywords" content="HTML5 Template">
    <meta name="description" content="1-Deals - Bootstrap eCommerce Template">
    <meta name="author" content="p-themes">
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url('web/')?>assets/images/2.jpeg">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url('web/')?>assets/images/2.jpeg">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('web/')?>assets/images/2.jpeg">
    <link rel="manifest" href="<?= base_url('web/')?>assets/images/icons/site.html">
    <link rel="mask-icon" href="<?= base_url('web/')?>assets/images/icons/safari-pinned-tab.svg" color="#666666">
    <link rel="shortcut icon" href="<?= base_url('web/')?>assets/images/icons/favicon.ico">
    <meta name="apple-mobile-web-app-title" content="1-Deals">
    <meta name="application-name" content="1-Deals">
    <meta name="msapplication-TileColor" content="#cc9966">
    <meta name="msapplication-config" content="<?= base_url('web/')?>assets/images/icons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="<?= base_url('web/')?>assets/vendor/line-awesome/line-awesome/line-awesome/css/line-awesome.min.css">
    <!-- Plugins CSS File -->
    <link href="<?php echo base_url()?>assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?= base_url('web/')?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url('web/')?>assets/css/plugins/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="<?= base_url('web/')?>assets/css/plugins/magnific-popup/magnific-popup.css">
    <link rel="stylesheet" href="<?= base_url('web/')?>assets/css/plugins/jquery.countdown.css">
    <!-- Main CSS File -->
    <link rel="stylesheet" href="<?= base_url('web/')?>assets/css/style<?=$suffix?>.css">
    <?php if(isset($show_menu)):?>
    <link rel="stylesheet" href="<?= base_url('web/')?>assets/css/skins/skin-demo-14.css">
    <link rel="stylesheet" href="<?= base_url('web/')?>assets/css/demos/demo-14.css">
    <?php endif?>
    <link rel="stylesheet" href="<?= base_url('web/')?>assets/css/plugins/magnific-popup/magnific-popup.css">
    <link rel="stylesheet" href="<?= base_url('web/')?>assets/css/plugins/nouislider/nouislider.css">
    <style>
        .bootstrap-notify-container {
  max-width: 320px;
  text-align: center; }
    </style>
</head>

<body>
    <div class="page-wrapper">
        <header class="header header-14">
            <div class="header-top">
                <div class="container" style="width: 100%;max-width: 100%;">
                    <div class="header-left">
                        <a href="tel:#"><i class="icon-phone"></i>Call: +079 171 17 00</a>
                    </div><!-- End .header-left -->

                    <div class="header-right">

                        <ul class="top-menu">
                            <li>
                                <a href="#">Links</a>
                                <ul class="menus">
                                    <!-- <li>
                                        <div class="header-dropdown">
                                            <a href="#">USD</a>
                                            <div class="header-menu">
                                                <ul>
                                                    <li><a href="#">Eur</a></li>
                                                    <li><a href="#">Usd</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li> -->
                                    <li>   
                                        <div class="header-dropdown">
                                            <a href="#"><?= $lang=='en'?'Engligh':'العربية'?></a>
                                            <div class="header-menu">
                                                <ul style="margin-top: -13%;">
                                                    <li><a href="<?= base_url('lang')?>">العربية</a></li>
                                                    <li><a href="<?= base_url('lang')?>">English</a></li>
                                                </ul>
                                            </div><!-- End .header-menu -->
                                        </div><!-- End .header-dropdown -->
                                    </li>
                                    <?php if(!$user):?>
                                    <li class="login">
                                        <a href="#signin-modal" data-toggle="modal"><?= $lang=='ar'?'تسجيل الدخول / حساب جديد':'Sign in / Sign up'?></a>
                                    </li>
                                    <?php endif?>
                                </ul>
                            </li>
                        </ul><!-- End .top-menu -->
                    </div><!-- End .header-right -->
                </div><!-- End .container -->
            </div><!-- End .header-top -->
			<?php if($lang=='en'):?>
            <div class="header-middle">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-auto col-lg-3 col-xl-3 col-xxl-2">
                            <button class="mobile-menu-toggler">
                                <span class="sr-only">Toggle mobile menu</span>
                                <i class="icon-bars"></i>
                            </button>
                            <a href="<?= base_url()?>" class="logo">
                                <img src="<?= base_url('web/')?>assets/images/2.jpeg" alt="1-Deals Logo" width="105" height="25">
                            </a>
                        </div><!-- End .col-xl-3 col-xxl-2 -->
                    
                        <div class="col col-lg-9 col-xl-9 col-xxl-10 header-middle-right">
                            <div class="row">
                                <div class="col-lg-8 col-xxl-4-5col d-none d-lg-block">
                                    <div class="header-search header-search-extended header-search-visible header-search-no-radius">
                                        <a href="#" class="search-toggle" role="button"><i class="icon-search"></i></a>
                                        <form action="#" method="get">
                                            <div class="header-search-wrapper search-wrapper-wide">

                                                <div class="select-custom">
                                                    <select id="cat" name="cat">
														<option value=""><?= lang("all_department")?></option>
														<?php foreach ($categories as $key => $value) :?>
														<option value="<?= $value["id"]?>"><?= $value["name".$suffix]?></option>
														<?php foreach ($value["sub_categories"] as $value2):?>
															<option value="2">- <?= $value2["name".$suffix]?></option>
														<?php endforeach?>
														<?php endforeach?>
                                                    </select>
                                                </div><!-- End .select-custom -->
                                                <label for="q" class="sr-only"><?= lang("Search")?></label>
                                                <input type="search" class="form-control" name="q" id="q" placeholder="Search product ..." required>

                                                <button class="btn btn-primary" type="submit"><i class="icon-search"></i></button>
                                            </div><!-- End .header-search-wrapper -->
                                        </form>
                                    </div><!-- End .header-search -->
                                </div><!-- End .col-xxl-4-5col -->

                                <div class="col-lg-4 col-xxl-5col d-flex justify-content-end align-items-center" style="<?= $lang=='ar'?'style="direction: rtl;text-align: right;"':''?>">
                                    <div class="header-dropdown-link">
                                        <?php if($user):?>
                                        <a href="<?= base_url('profile')?>" class="wishlist-link">
                                            <i class="icon-user"></i>
                                            <span class="wishlist-txt"><?= lang("Profile")?></span>
                                        </a>

                                        <a href="<?= base_url('wishlist')?>" class="wishlist-link wishlist-link2">
                                            <i class="icon-heart-o"></i>
                                            <span class="wishlist-txt"><?= lang("Wishlist")?></span>
                                        </a>
                                        <?php endif?>
                                        <div class="dropdown cart-dropdown">
                                            <a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static">
                                                <i class="icon-shopping-cart"></i>
                                                <span class="cart-count" id="cart_count"><?= count($this->cart->contents())?></span>
                                                <span class="cart-txt"><?= lang("Cart") ?></span>
                                            </a>

                              
                                      

                                            <div class="dropdown-menu dropdown-menu-right">
                                                <div class="dropdown-cart-products">
                                                    <?php foreach ($this->cart->contents() as $key => $value):?>
                                                    <div class="product">
                                                        <div class="product-cart-details">
                                                            <h4 class="product-title">
                                                                <a href="product.html"><?= $value["name"]?></a>
                                                            </h4>

                                                            <span class="cart-product-info">
                                                                <span class="cart-product-qty" id="header_quantity_<?= $value["id"]?>"><?= $value["qty"]?></span>
                                                                x <?= $value["price"]?>
                                                            </span>
                                                        </div><!-- End .product-cart-details -->

                                                        <figure class="product-image-container">
                                                            <a href="product.html" class="product-image">
                                                                <img src="<?= base_url('uploads/'.$value["image"])?>" alt="product">
                                                            </a>
                                                        </figure>
                                                        <a href="#" class="btn-remove" title="Remove Product"><i class="icon-close"></i></a>
                                                    </div><!-- End .product -->
                                                    <?php endforeach?>
                                                  
                                                </div><!-- End .cart-product -->

                                                <div class="dropdown-cart-total">
                                                    <span>Total</span>

                                                    <span class="cart-total-price"  id="total_pr"><?= $this->cart->total();?></span>
                                                </div><!-- End .dropdown-cart-total -->

                                                <div class="dropdown-cart-action">
                                                    <a href="<?= base_url('cart')?>" class="btn btn-primary">View Cart</a>
                                                    <a href="<?= base_url('checkout')?>" class="btn btn-outline-primary-2"><span>Checkout</span><i class="icon-long-arrow-right"></i></a>
                                                </div><!-- End .dropdown-cart-total -->
                                            </div><!-- End .dropdown-menu -->
                                        </div><!-- End .cart-dropdown -->

                                      
                                    </div>
                                </div><!-- End .col-xxl-5col -->
                            </div><!-- End .row -->
                        </div><!-- End .col-xl-9 col-xxl-10 -->
                    </div><!-- End .row -->
                </div><!-- End .container-fluid -->
			</div><!-- End .header-middle -->
			<?php else:?>
				<div class="header-middle">
					<div class="container-fluid">
						<div class="row">

							<div class="col col-lg-9 col-xl-9 col-sm-6 col-6 col-xxl-10 header-middle-left">
								<div class="row">

									<div class="col-lg-4 col-xxl-5col d-flex justify-content-end align-items-center"
										style="direction: rtl;text-align: right;">
										<div class="header-dropdown-link">
                                        <?php if($user):?>
											<a href="<?= base_url('profile')?>" class="wishlist-link">
												<i class="icon-user"></i>
												<span class="wishlist-txt">حسابي</span>
											</a>

											<a href="<?= base_url('wishlist')?>" class="wishlist-link wishlist-link2">
												<i class="icon-heart-o"></i>
												<span class="wishlist-txt">المفضلة</span>
											</a>
                                            <?php endif?>
											<div class="dropdown cart-dropdown">
												<a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown"
													aria-haspopup="true" aria-expanded="false" data-display="static">
													<i class="icon-shopping-cart"></i>
													<span class="cart-count"id="cart_count" ><?= count($this->cart->contents())?></span>
													<span class="cart-txt">السلة</span>
												</a>




												<div class="dropdown-menu dropdown-menu-left">
													<div class="dropdown-cart-products">
                                                    <?php foreach ($this->cart->contents() as $key => $value):?>
														<div class="product" style="direction: ltr;">
															<div class="product-cart-details">
                                                                <h4 class="product-title">
                                                                    <a href="product.html"><?= $value["name"]?></a>
                                                                </h4>


                                                                <span class="cart-product-info">
                                                                    <span class="cart-product-qty" id="header_quantity_<?= $value["id"]?>"><?= $value["qty"]?></span>
                                                                    x <?= $value["price"]?>
                                                                </span>
															</div><!-- End .product-cart-details -->

                                                            <figure class="product-image-container">
                                                                <a href="product.html" class="product-image">
                                                                    <img src="<?= base_url('uploads/'.$value["image"])?>" alt="product">
                                                                </a>
                                                            </figure>
															<a href="#" class="btn-remove" title="Remove Product"><i
																	class="icon-close"></i></a>
														</div><!-- End .product -->

													<?php endforeach?>
													</div><!-- End .cart-product -->

													<div class="dropdown-cart-total">
														<span>المجوع :</span>

														<span class="cart-total-price" id="total_pr"><?= $this->cart->total();?></span>
													</div><!-- End .dropdown-cart-total -->

                                                    <div class="dropdown-cart-action">
                                                        <a href="<?= base_url('cart')?>" class="btn btn-primary cart-mos">View Cart</a>
                                                        <a href="<?= base_url('checkout')?>" class="btn btn-outline-primary-2"><span>Checkout</span><i class="icon-long-arrow-right"></i></a>
                                                    </div><!-- End .dropdown-cart-total -->
												</div><!-- End .dropdown-menu -->
											</div><!-- End .cart-dropdown -->


										</div>
									</div><!-- End .col-xxl-5col -->

									<div class="col-lg-8 col-xxl-4-5col d-none d-lg-block">
										<div
											class="header-search header-search-extended header-search-visible header-search-no-radius">
											<a href="#" class="search-toggle" role="button"><i class="icon-search"></i></a>
											<form action="#" method="get">
												<div class="header-search-wrapper search-wrapper-wide">

													<div class="select-custom">
														<select id="cat" name="cat" style="font-size: large;">
															<option value=""><?= lang("all_department")?></option>
    														<?php foreach ($categories as $key => $value) :?>
    														<option value="<?= $value["id"]?>"><?= $value["name".$suffix]?></option>
    														<?php foreach ($value["sub_categories"] as $value2):?>
    															<option value="2">- <?= $value2["name".$suffix]?></option>
    														<?php endforeach?>
    														<?php endforeach?>
														</select>
													</div><!-- End .select-custom -->
													<label for="q" class="sr-only">Search</label>
													<input type="search" class="form-control" name="q" id="q"
														placeholder="بحث عن منتج ..." required>

													<button class="btn btn-primary" type="submit"
														style="margin-left: <?= $user?'-6%':'-28%'?>;"><i class="icon-search"></i></button>
												</div><!-- End .header-search-wrapper -->
											</form>
										</div><!-- End .header-search -->
									</div><!-- End .col-xxl-4-5col -->


								</div><!-- End .row -->
							</div><!-- End .col-xl-9 col-xxl-10 -->

						
							<div class="col-auto col-lg-3 col-xl-3 col-sm-6 col-5 col-xxl-2">
	
								
								<a href="http://1-deals.com/project/" class="logo" style="margin-left: 63%;margin-top: 5%;">
									<img src="http://1-deals.com/project/web/assets/images/2.jpeg" alt="Molla Logo" width="100" height="25">
								</a>
							</div>
							<div class="col-auto col-lg-3 col-xl-3 col-sm-6 col-1 col-xxl-2">	
<button class="mobile-menu-toggler">
									<span class="sr-only">Toggle mobile menu</span>
									<i class="icon-bars"></i>
								</button>

</div>


						</div><!-- End .row -->
					</div><!-- End .container-fluid -->
				</div>
			<?php endif?>
            <div class="header-bottom sticky-header">
                <div class="container-fluid" style="<?= $lang=='ar'?'direction: rtl;':''?>;display: contents;">
                    <div class="row">
           <!--             <div class="col-auto col-lg-3 col-xl-3 col-xxl-2 header-left">-->
           <!--                 <div class="dropdown category-dropdown show is-on" data-visible="true">-->
           <!--                     <a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static" title="Browse Categories" style="<?= $lang=='ar'?'font-size: large;':''?>">-->
           <!--                         <?= lang("browse_cat")?>-->
           <!--                     </a>-->

           <!--                     <div class="dropdown-menu show">-->
           <!--                         <nav class="side-nav">-->
           <!--                             <ul class="menu-vertical sf-arrows">-->
											<!--<?php foreach ($categories as $key => $value) :?>-->
           <!--                                 <li class="<?= count($value["sub_categories"])?'megamenu-container':''?>" style="<?= $lang=='ar'?'text-align: right;':''?>">-->
           <!--                                     <a class="sf-with-ul" href="<?= base_url('products?main='.$value["id"])?>" style="<?= $lang=='ar'?'font-size: large;':''?>"><i class="icon-laptop"></i><?= $value["name".$suffix]?></a>-->
           <!--                                     <?php if(count($value["sub_categories"])):?>-->
           <!--                                     <div class="megamenu">-->
											<!--	<div class="row no-gutters">-->
           <!--                                             <div class="col-md-12">-->
           <!--                                                 <div class="menu-col">-->
           <!--                                                     <div class="row">-->
           <!--                                                         <div class="col-md-12">-->
                                                                    <!-- End .menu-title -->
           <!--                                                             <ul style="">-->
											<!--							<?php foreach ($value["sub_categories"] as $value2):?>-->
           <!--                                                                 <li><a href="<?= base_url('products?id='.$value2["id"])?>" style="font-size: large;font-weight: bold;border-bottom: 0.1rem solid #eeeeee;"><?= $value2["name".$suffix]?></a></li>-->
											<!--							<?php endforeach ?>-->
																			
           <!--                                                             </ul>-->

                                                                      
           <!--                                                         </div>-->

                                                              
           <!--                                                     </div>-->
           <!--                                                 </div>-->
           <!--                                             </div>-->

                                                      
           <!--                                         </div>-->
           <!--                                     </div>-->
           <!--                                     <?php endif?>-->
											<!--</li>-->
											<!--<?php endforeach ?>-->
           <!--                             </ul>-->
           <!--                         </nav>-->
           <!--                     </div>-->
           <!--                 </div>-->
           <!--             </div>-->

                        <div class="col col-lg-6 col-xl-6 col-xxl-8 header-center">
                            <nav class="main-nav">
                                <ul class="menu sf-arrows">
                                    <li class="megamenu-container <?= $active_menue=='home'?'active':''?>" style="margin-left: 2rem;">
                                        <a href="<?= base_url()?>" class="" style="<?=$lang=='ar'?'font-size: large;font-weight: bold;':''?>"><?= lang("Home")?></a>

                                    </li>
                                    <li  class="<?= $active_menue=='shops'?'active':''?>">
                                        <a href="<?= base_url('shops')?>" class="" style="<?=$lang=='ar'?'font-size: large;font-weight: bold;':''?>"><?= lang("Shops")?></a>
                                   
                                    </li>
                                    <li  class="<?= $active_menue=='about'?'active':''?>">
                                        <a href="<?= base_url('about_us')?>" class="" style="<?=$lang=='ar'?'font-size: large;font-weight: bold;':''?>"><?= lang("about_us")?></a>
                                  
                                    </li>
                                    <li class="<?= $active_menue=='faq'?'active':''?>">
                                        <a href="<?= base_url('faq')?>" class="" style="<?=$lang=='ar'?'font-size: large;font-weight: bold;':''?>"><?= lang("FAQ")?></a>

                                       
                                    </li>


                                    <li class="<?= $active_menue=='policy'?'active':''?>">
                                        <a href="<?= base_url('policy')?>" class="" style="<?=$lang=='ar'?'font-size: large;font-weight: bold;':''?>"><?= lang("Policy")?></a>

                                        
                                    </li>

                                    <li class="<?= $active_menue=='contact'?'active':''?>">
                                        <a href="<?= base_url('contact_us')?>" class="" style="<?=$lang=='ar'?'font-size: large;font-weight: bold;':''?>"><?= lang("ContactUs")?></a>

                                        
                                    </li>
                                 
                                </ul><!-- End .menu -->
                            </nav><!-- End .main-nav -->
                        </div><!-- End .col-xl-9 col-xxl-10 -->

                        <!--<div class="col col-lg-3 col-xl-3 col-xxl-2 header-right">-->
                        <!--    <i class="la la-lightbulb-o"></i><p>Clearance Up to 30% Off</span></p>-->
                        <!--</div>-->
                    </div><!-- End .row -->
                </div><!-- End .container-fluid -->
            </div><!-- End .header-bottom -->
        </header><!-- End .header -->
