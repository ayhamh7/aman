<div class="msg_history">

          <?php foreach ($messages as $key => $message):?>
          <?php if($message["from_id"] == $customer["id"]):?>
            <div class="incoming_msg">
              <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
              <div class="received_msg">
                <div class="received_withd_msg">
                  <p><?= $message["message"]?></p>
                  <span class="time_date"> <?= date('h:i A', strtotime($message["date"]))?>    |    <?=date('Y-m-d', strtotime($message["date"]))?></span> </div>
              </div>
            </div>
          <?php else:?>
            <div class="outgoing_msg">
              <div class="sent_msg">
                <p><?= $message["message"]?></p>
                <span class="time_date"> <?= date('h:i A', strtotime($message["date"]))?>    |    <?=date('Y-m-d', strtotime($message["date"]))?></span> </div>
            </div>
          <?php endif?>
           
           <?php endforeach?>
          </div>
          <div class="type_msg">
            <div class="input_msg_write">
              <form action="<?= base_url('OfferController/messageRequest')?>" method="POST" >
                  <input type="hidden" name="request_id" value="<?= $request_info["id"]?>">
                  <input type="hidden" name="from_id" value="<?= $customer["id"]?>">
                    <input type="hidden" name="to_id" value=" <?=$request_info["customer_id"]?>">
                    
                <input type="text" name="message" class="write_msg" placeholder="Type a message">
                <button class="msg_send_btn" type="submit"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
            </div>
          </div>