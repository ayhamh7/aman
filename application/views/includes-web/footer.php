<footer class="footer">
           
            <div class="footer-middle border-0" style="<?= $lang=='ar'?'direction: rtl;text-align: right;':''?>">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12 col-lg-4">
                            <div class="widget widget-about">
                                <img src="<?= base_url('web/')?>assets/images/1.png" class="footer-logo" alt="Footer Logo" width="250"
                                    height="25">
                                <p>Praesent dapibus, neque id cursus ucibus, tortor neque egestas augue, eu vulputate
                                    magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan
                                    porttitor, facilisis luctus, metus. </p>

                   
                            </div><!-- End .widget about-widget -->
                        </div>


                        <!-- End .col-sm-12 col-lg-4 -->

                        <div class="col-sm-4 col-lg-2">
                            <div class="widget">
                                <h4 class="widget-title"><?= lang("Useful Links")?></h4><!-- End .widget-title -->

                                <ul class="widget-list">
                                    <li><a href="<?= base_url('about_us')?>"><?= lang('about_us')?></a></li>
                                    <li><a href="<?= base_url('faq')?>"><?= lang('FAQ')?></a></li>
                                    <li><a href="<?=  base_url('contact')?>"><?= lang('ContactUs')?></a></li>
                                    
                                </ul><!-- End .widget-list -->
                            </div><!-- End .widget -->
                        </div><!-- End .col-sm-4 col-lg-2 -->

                        <div class="col-sm-4 col-lg-2">
                            <div class="widget">
                                <h4 class="widget-title">Customer Service</h4><!-- End .widget-title -->

                                <ul class="widget-list">
                                    <li><a href="#">Payment Methods</a></li>
                                    <li><a href="#">Money-back guarantee!</a></li>
                                    <li><a href="#">Returns</a></li>
                                    <li><a href="#">Shipping</a></li>
                                    <li><a href="#">Terms and conditions</a></li>
                                    <li><a href="#">Privacy Policy</a></li>
                                </ul><!-- End .widget-list -->
                            </div><!-- End .widget -->
                        </div><!-- End .col-sm-4 col-lg-2 -->

                        <div class="col-sm-4 col-lg-2">
                            <div class="widget">
                                <h4 class="widget-title">My Account</h4><!-- End .widget-title -->

                                <ul class="widget-list">
                                    <li><a href="#">Sign In</a></li>
                                    <li><a href="<?= base_url('cart')?>">View Cart</a></li>
                                    <li><a href="<?= base_url('wishlist')?>">My Wishlist</a></li>
                                    <li><a href="#">Help</a></li>
                                </ul><!-- End .widget-list -->
                            </div><!-- End .widget -->
                        </div><!-- End .col-sm-4 col-lg-2 -->

                        <div class="col-sm-4 col-lg-2">
                            
                        </div><!-- End .col-sm-4 col-lg-2 -->
                    </div><!-- End .row -->
                </div><!-- End .container-fluid -->
            </div><!-- End .footer-middle -->

            <div class="footer-bottom">
                <div class="container-fluid">
                    <p class="footer-copyright">Copyright © 2020 1-Deals Store. All Rights Reserved.</p>
                    <!-- End .footer-copyright -->
                    <div class="social-icons social-icons-color">
                        <span class="social-label">Social Media</span>
                        <a href="https://fb.me/fastactjo" class="social-icon social-facebook" title="Facebook" target="_blank"><i
                                class="icon-facebook-f"></i></a>
                       
                        <a href="https://instagram.com/fastact.jo" class="social-icon social-instagram" title="Instagram" target="_blank"><i
                                class="icon-instagram"></i></a>
                      
                    </div><!-- End .soial-icons -->
                </div><!-- End .container-fluid -->
            </div><!-- End .footer-bottom -->
        </footer><!-- End .footer -->
</div><!-- End .page-wrapper -->
<button id="scroll-top" title="Back to Top"><i class="icon-arrow-up"></i></button>

<!-- Mobile Menu -->
<div class="mobile-menu-overlay"></div><!-- End .mobil-menu-overlay -->

<div class="mobile-menu-container">
	<div class="mobile-menu-wrapper">
		<span class="mobile-menu-close"><i class="icon-close"></i></span>
		
		<form action="#" method="get" class="mobile-search">
			<label for="mobile-search" class="sr-only">Search</label>
			<input type="search" class="form-control" name="mobile-search" id="mobile-search" placeholder="Search in..." required>
			<button class="btn btn-primary" type="submit"><i class="icon-search"></i></button>
		</form>

		<ul class="nav nav-pills-mobile" role="tablist">
			<li class="nav-item">
				<a class="nav-link active" id="mobile-menu-link" data-toggle="tab" href="#mobile-menu-tab" role="tab" aria-controls="mobile-menu-tab" aria-selected="true">Menu</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" id="mobile-cats-link" data-toggle="tab" href="#mobile-cats-tab" role="tab" aria-controls="mobile-cats-tab" aria-selected="false">Categories</a>
			</li>
		</ul>

		<div class="tab-content">
			<div class="tab-pane fade show active" id="mobile-menu-tab" role="tabpanel" aria-labelledby="mobile-menu-link">
				<nav class="mobile-nav">
					<ul class="mobile-menu">
						<li class="active">
						 <a href="<?= base_url()?>" style="<?= $lang=='ar'?'text-align: right;':''?>" class="" ><?= lang("Home")?></a>

						</li>
						<li>
							<a style="<?= $lang=='ar'?'text-align: right;':''?>" href="<?= base_url('shops')?>"><?= lang("Shops")?></a>
						
						</li>
						
						<li>
							<a href="#" style="<?= $lang=='ar'?'text-align: right;':''?>"><?= $lang=='ar'?'روابط أخرى':'Pages'?></a>
							<ul>
								<li>
								<a style="<?= $lang=='ar'?'text-align: right;':''?>" href="<?= base_url('about_us')?>" class="" ><?= lang("about_us")?></a>

								
								</li>
								<li>
								<a style="<?= $lang=='ar'?'text-align: right;':''?>" href="<?= base_url('faq')?>" class="" ><?= lang("FAQ")?></a>

								
								</li>
								<li>
									<a style="<?= $lang=='ar'?'text-align: right;':''?>" href="<?= base_url('contact_us')?>"><?= lang("ContactUs")?></a>

								
								</li>
								<li><a style="<?= $lang=='ar'?'text-align: right;':''?>" href="#signin-modal" data-toggle="modal"><?= $lang=='ar'?'تسجيل الدخول / حساب جديد':'Sign in / Sign up'?></a></li>
							
							</ul>
						</li>
					
					</ul>
				</nav><!-- End .mobile-nav -->
			</div><!-- .End .tab-pane -->
			<div class="tab-pane fade" id="mobile-cats-tab" role="tabpanel" aria-labelledby="mobile-cats-link">
				<nav class="mobile-cats-nav">
					<ul class="mobile-cats-menu">
					    <?php foreach ($categories as $key => $value) :?>
						<li><a class="mobile-cats-lead" style="<?= $lang=='ar'?'text-align: right;':''?>" href="<?= base_url('products?main='.$value["id"])?>"><?= $value["name".$suffix]?></a></li>
						<?php endforeach?>
						
					</ul><!-- End .mobile-cats-menu -->
				</nav><!-- End .mobile-cats-nav -->
			</div><!-- .End .tab-pane -->
		</div><!-- End .tab-content -->

		<div class="social-icons">
			<a href="#" class="social-icon" target="_blank" title="Facebook"><i class="icon-facebook-f"></i></a>
			<a href="#" class="social-icon" target="_blank" title="Twitter"><i class="icon-twitter"></i></a>
			<a href="#" class="social-icon" target="_blank" title="Instagram"><i class="icon-instagram"></i></a>
			<a href="#" class="social-icon" target="_blank" title="Youtube"><i class="icon-youtube"></i></a>
		</div><!-- End .social-icons -->
	</div><!-- End .mobile-menu-wrapper -->
</div><!-- End .mobile-menu-container -->

<!-- Sign in / Register Modal -->
<div class="modal fade" id="signin-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="icon-close"></i></span>
				</button>

				<div class="form-box">
					<div class="form-tab">
						<ul class="nav nav-pills nav-fill" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" id="signin-tab" data-toggle="tab" href="#signin" role="tab" aria-controls="signin" aria-selected="true"><?= lang("LOG IN")?></a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="register-tab" data-toggle="tab" href="#register" role="tab" aria-controls="register" aria-selected="false"><?= lang("SIGN UP")?></a>
							</li>
						</ul>
						<div class="tab-content" id="tab-content-5">
							<div class="tab-pane fade show active" id="signin" role="tabpanel" aria-labelledby="signin-tab">
								<form action="<?= base_url('Login/log_in')?>" method="POST">
								

									<div class="form-group" style="<?= $lang=='ar'?'direction: rtl;text-align: right;':''?>">
										<label for="singin-email"><?= lang("customer.mobile")?> :</label>
										<input type="text"  class="form-control" id="singin-email" name="mobile" required>
									</div><!-- End .form-group -->

							


									<div class="form-group" style="<?= $lang=='ar'?'direction: rtl;text-align: right;':''?>">
										<label for="singin-password"><?= lang("customer.password")?> :</label>
										<input type="password" class="form-control" id="singin-password" name="password" required>
									</div><!-- End .form-group -->

									<div class="form-footer">
										<button type="submit" class="btn btn-outline-primary-2">
											<span><?= lang("LOG IN")?></span>
											<i class="icon-long-arrow-right"></i>
										</button>

										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" id="signin-remember">
											<label class="custom-control-label" for="signin-remember">Remember Me</label>
										</div><!-- End .custom-checkbox -->

										<a href="#" class="forgot-link">Forgot Your Password?</a>
									</div><!-- End .form-footer -->
								</form>
								<!--<div class="form-choice">
									<p class="text-center">or sign in with</p>
									<div class="row">
										<div class="col-sm-6">
											<a href="#" class="btn btn-login btn-g">
												<i class="icon-google"></i>
												Login With Google
											</a>
										</div>
										<div class="col-sm-6">
											<a href="#" class="btn btn-login btn-f">
												<i class="icon-facebook-f"></i>
												Login With Facebook
											</a>
										</div>
									</div>
								</div>--><!-- End .form-choice -->
							</div><!-- .End .tab-pane -->
							<div class="tab-pane fade" id="register" role="tabpanel" aria-labelledby="register-tab">
								<form action="<?= base_url('Login/register')?>" method="POST">
									<div class="form-group" style="<?= $lang=='ar'?'direction: rtl;text-align: right;':''?>">
										<label for="singin-email"><?= lang("Name")?> :</label>
										<input type="text" class="form-control" id="singin-email" name="name" required>
									</div><!-- End .form-group -->

									<div class="form-group" style="<?= $lang=='ar'?'direction: rtl;text-align: right;':''?>">
										<label for="singin-email"><?= lang("customer.address")?> :</label>
										<input type="text" class="form-control" id="singin-email" name="address" >
									</div><!-- End .form-group -->

									<div class="form-group" style="<?= $lang=='ar'?'direction: rtl;text-align: right;':''?>">
										<label for="singin-email"><?= lang("Email")?> :</label>
										<input type="text" class="form-control" id="singin-email" name="email" >
									</div><!-- End .form-group -->

									<div class="form-group" style="<?= $lang=='ar'?'direction: rtl;text-align: right;':''?>">
										<label for="singin-email"><?= lang("customer.mobile")?> :</label>
										<input type="text" class="form-control" id="singin-email" name="mobile" required>
									</div><!-- End .form-group -->
									<div class="form-group" style="<?= $lang=='ar'?'direction: rtl;text-align: right;':''?>">
										<label for="register-password"><?= lang("customer.password")?> :</label>
										<input type="password" class="form-control" id="password" name="password" required>
									</div><!-- End .form-group -->

									<div class="form-footer">
										<button type="submit" class="btn btn-outline-primary-2">
											<span><?= lang("SIGN UP")?></span>
											<i class="icon-long-arrow-right"></i>
										</button>

										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" id="register-policy" required>
											<label class="custom-control-label" for="register-policy">I agree to the <a href="<?= base_url('policy')?>">privacy policy</a> *</label>
										</div><!-- End .custom-checkbox -->
									</div><!-- End .form-footer -->
								</form>
								
							</div><!-- .End .tab-pane -->
						</div><!-- End .tab-content -->
					</div><!-- End .form-tab -->
				</div><!-- End .form-box -->
			</div><!-- End .modal-body -->
		</div><!-- End .modal-content -->
	</div><!-- End .modal-dialog -->
</div><!-- End .modal -->

    

    <!-- Plugins JS File -->
    <script src="<?= base_url('web/')?>assets/js/jquery.min.js"></script>
    <script src="<?= base_url('web/')?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url('web/')?>assets/js/jquery.hoverIntent.min.js"></script>
    <script src="<?= base_url('web/')?>assets/js/jquery.waypoints.min.js"></script>
    <script src="<?= base_url('web/')?>assets/js/superfish.min.js"></script>
    <script src="<?= base_url('web/')?>assets/js/owl.carousel.min.js"></script>
    <script src="<?= base_url('web/')?>assets/js/bootstrap-input-spinner.js"></script>
    <script src="<?= base_url('web/')?>assets/js/jquery.magnific-popup.min.js"></script>
    <script src="<?= base_url('web/')?>assets/js/jquery.plugin.min.js"></script>
    <script src="<?= base_url('web/')?>assets/js/jquery.countdown.min.js"></script>
    <!-- Main JS File -->
        
    <script src="<?= base_url('web/')?>assets/js/main.js"></script>
	<script src="<?= base_url('web/')?>assets/js/demos/demo-14.js"></script>
	<script src="<?= base_url()?>assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>
	<script src="<?php echo base_url()?>assets/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?= base_url('web/')?>assets/js/wNumb.js"></script>
	<script src="<?= base_url('web/')?>assets/js/bootstrap-input-spinner.js"></script>
	<script src="<?= base_url('web/')?>assets/js/jquery.magnific-popup.min.js"></script>
    <script src="<?= base_url('web/')?>assets/js/nouislider.min.js"></script>
    <script type="text/javascript" src="<?= base_url()?>web/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
    
	<script>
			 $('#checkout-discount-input').on('focus', function () {
				// Hide label on focus
				$(this).parent('form').find('label').css('opacity', 0);
			}).on('blur', function () {
				// Check if input is empty / toggle label
				var $this = $(this);

				if( $this.val().length !== 0 ) {
					$this.parent('form').find('label').css('opacity', 0);
				} else {
					$this.parent('form').find('label').css('opacity', 1);
				}
			});
            <?php if($this->input->get("error_msg")):?>
                swal("Error","<?= $this->input->get("error_msg")?>", "error");
            <?php endif?>
            
             <?php if($this->session->flashdata("error_msg")):?>
                swal("Error","<?= $this->session->flashdata("error_msg")?>", "error");
            <?php endif?>
            
             <?php if($this->input->get("success_msg")):?>
                swal("Success","<?= $this->input->get("success_msg")?>", "success");
            <?php endif?>
        </script>
	<script>
		<?php if($lang=='ar'):?>
			setTimeout(() => {
            //$(".menu-vertical").removeClass("sf-arrows");
			}, 200);

		<?php endif?>
		  function addWhishList(product_id)
                {
                    

                    $.ajax({
                        url : "<?= base_url()?>Users/add_wishlist",
                        type : 'get',
                        data : {"id":product_id},
                        success : function (res){
                            console.log(res);
                            if(res.errorCode==200)
                            {
                               // alert("Added Successfully To WishList");
                               showNotification("alert-success","Added Successfully To WishList","bottom","center","","")
                            }
                            else
                                showNotification("alert-danger","Please, Login","bottom","center","","")
                        },
                        error :  function (msg){
                            
                                showNotification("alert-danger","Please, Login","bottom","center","","")
                    }
                    });
                }
		function removeFromCart(product_id)
		{
			$.ajax({
                        url : "<?= base_url()?>Products/removeFromCart",
                        type : 'post',
                        data : {"id":product_id},
                        success : function (res){
                            console.log(res);
                            if(res.errorCode==200)
                            {
                                //alert("Added Successfully To Cart");
                                showNotification("alert-success","Removed Successfully To Cart","bottom","center","","")
								setTimeout(() => {
									location.reload(); 
								},2000);
                            }
                            else
                                 showNotification("alert-danger","Product not found","bottom","center","","")
							
                        },
                        error :  function (msg){
                            
                                showNotification("alert-warning","Network Error","bottom","center","","")
                    }
                    });
		}
		 function addToCart(product_id)
                {
                    $.ajax({
                        url : "<?= base_url()?>Products/addToCart",
                        type : 'post',
                        data : {"id":product_id},
                        success : function (res){
                            console.log(res);
                            if(res.errorCode==200)
                            {
                                //alert("Added Successfully To Cart");
                                showNotification("alert-success","Added Successfully To Cart","bottom","center","","")
                                if(res.existInCart==true)
                                {
								
                                    var quant = parseInt($("#header_quantity_"+res.data.id).html())+parseInt(res.data.quantity)
                                    $("#header_quantity_"+res.data.id).html(quant)
									var total_pr =  parseInt($("#total_pr").html())+parseInt(res.data.sale_price)
									$("#total_pr").html(total_pr)
									
                                }
                                else{
                                    var base = '<?= base_url()?>'
                                    var item_div = ' <div class="product">'+
                                       				'<div class="product-cart-details">'+
                                                           '<h4 class="product-title">'+
                                                                '<a href="product.html">'+res.data.name+'</a>'+
                                                            '</h4>'+
                                                            '<span class="cart-product-info">'+
                                                               ' <span class="cart-product-qty" id="header_quantity_'+res.data.id+'">'+res.data.quantity+'x</span>'+res.data.sale_price+
                                                            '</span>'+
                                                        '</div>'+

                                                        '<figure class="product-image-container">'+
                                                            '<a href="product.html" class="product-image">'+
                                                                '<img src="'+base+'/uploads/'+res.data.image+'" alt="product">'+
                                                           '</a>'+
                                                       ' </figure>'+
                                                        '<a href="#" class="btn-remove" title="Remove Product"><i class="icon-close"></i></a>'; 
                                   // $(item_div).insertAfter($('.cart-dropdown-item').last());
									$(".dropdown-cart-products").append(item_div)
                                    var quant = parseInt($("#cart_count").html())+1
                                    $("#cart_count").html(quant)
									var total_pr =  parseInt($("#total_pr").html())+parseInt(res.data.sale_price)
									$("#total_pr").html(total_pr)
                                }
                            }
                            else
                                 showNotification("alert-danger","Product not found","bottom","center","","")
                        },
                        error :  function (msg){
                            
                                showNotification("alert-warning","Network Error","bottom","center","","")
                    }
                    });
				}
	function showNotification(colorName, text, placementFrom, placementAlign, animateEnter, animateExit) {
			if (colorName === null || colorName === '') { colorName = 'bg-black'; }
			if (text === null || text === '') { text = 'Turning standard Bootstrap alerts'; }
			if (animateEnter === null || animateEnter === '') { animateEnter = 'animated fadeInDown'; }
			if (animateExit === null || animateExit === '') { animateExit = 'animated fadeOutUp'; }
			var allowDismiss = true;

			$.notify({
				message: text
			},
				{
					type: colorName,
					allow_dismiss: allowDismiss,
					newest_on_top: true,
					timer: 1000,
					placement: {
						from: placementFrom,
						align: placementAlign
					},
					animate: {
						enter: animateEnter,
						exit: animateExit
					},
					template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +
					'<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
					'<span data-notify="icon"></span> ' +
					'<span data-notify="title">{1}</span> ' +
					'<span data-notify="message">{2}</span>' +
					'<div class="progress" data-notify="progressbar">' +
					'<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
					'</div>' +
					'<a href="{3}" target="{4}" data-notify="url"></a>' +
					'</div>'
				});
		}
	</script>
</body>
<script src="<?= base_url("web/")?>light-gallery/js/lightgallery-all.js"></script>
<script>
setTimeout(() => {
	<?php if(!isset($show_menu)):?>
		$(".dropdown-menu").removeClass("show")
	<?php endif?>
	$('#lightgallery').lightGallery();
}, 200);
</script>
<script src="<?= base_url('web/')?>assets/js/jquery.elevateZoom.min.js"></script>
<!-- molla/index-14.html  22 Nov 2019 09:59:54 GMT -->
</html>