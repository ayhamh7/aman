    <section>
        <!-- Left Sidebar -->
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="<?php echo base_url()?>assets/images/user.png" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?="" ?></div>
                    
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="<?= base_url()?>login/logout"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
                       <!-- Menu -->
            <div class="menu">
                <ul class="list">
                <?php if($this->ion_auth->is_admin()|| $this->ion_auth->in_group("manage_app")):?>
                      <li class="<?= isset($g_active)?'active':''?>">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">assignment</i>
                            <span>ادارة التطبيق</span>
                        </a>
                        <ul class="ml-menu">
                      
                           
                             
                             <li>
                                <a href="<?= base_url() ?>dashboard/items">إدارة المنتجات</a>
                            </li>
                         
                            <li>
                                <a href="<?= base_url() ?>dashboard/categories">إدارة المجموعات</a>
                            </li>
                        </ul>
                    </li>
                    <?php endif?>
                    <?php if($this->ion_auth->is_admin()|| $this->ion_auth->in_group("manage_clients")):?>
                    <li class="<?= isset($customer_active)?'active':''?>">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">assignment</i>
                            <span>ادارة الزبائن</span>
                        </a>
                        <ul class="ml-menu">
                      
                             <li>
                                <a href="<?= base_url() ?>dashboard/manageUnActiveCostumers">إدارة الزبائن الغير مفعلين</a>
                            </li>
                           <li>
                                <a href="<?= base_url() ?>dashboard/manageActiveCostumers">إدارة الزبائن المغعلين</a>
                            </li>
                           <li>
                                <a href="<?= base_url() ?>dashboard/manageCostumers">إدارة الزبائن</a>
                            </li>
                           
                           
                        </ul>
                    </li>
                    <?php endif?>
                    <?php if($this->ion_auth->is_admin()|| $this->ion_auth->in_group("manage_cash")):?>
                     <li class="<?= isset($mnews_active)?'active':''?>">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">assignment</i>
                            <span>ادارة الطلبات </span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="<?= base_url() ?>dashboard/manageOrders_C">الطلبات الفعالة</a>
                            </li>
                             <li>
                                <a href="<?= base_url() ?>dashboard/manageOrders_Done">الطلبات المنتهية</a>
                            </li>
                           <li>
                                <a href="<?= base_url() ?>dashboard/manageOrders_Deleted">الطلبات الملغية</a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>dashboard/Cash_Report"> تقارير</a>
                            </li>
                           
                        </ul>
                    </li>
                    <?php endif?>
                 
                   
                    <?php if($this->ion_auth->is_admin()):?>
                    <!--<li class="<?= isset($general_active)?'active':''?>">-->
                    <!--    <a href="javascript:void(0);" class="menu-toggle">-->
                    <!--        <i class="material-icons">assignment</i>-->
                    <!--        <span>الإدارة العامة</span>-->
                    <!--    </a>-->
                    <!--    <ul class="ml-menu">-->
                       
                            
                    <!--         <li>-->
                    <!--            <a href="<?= base_url() ?>dashboard/config">إدارة الاعدادات</a>-->
                    <!--        </li>-->
                             
                           
                    <!--    </ul>-->
                    <!--</li>-->
                    <?php endif?>
                    <?php if($this->ion_auth->is_admin()):?>
                  <li>
                    <a href="<?= base_url() ?>auth"><!--dashboard/manage_users-->
                          <i class="material-icons">person</i>
                           <span>&#1575;&#1583;&#1575;&#1585;&#1577; &#1575;&#1604;&#1605;&#1587;&#1572;&#1608;&#1604;&#1610;&#1606;</span>
                        </a>
                    </li>
                     <li>
                    <a href="<?= base_url() ?>dashboard/import_csv"><!--dashboard/manage_users-->
                          <i class="material-icons">settings</i>
                           <span>استيراد ملف csv</span>
                        </a>
                    </li>
                <?php endif?>
                  
                   
                 
                   
                    
                </ul>
            </div>
            <!-- #Menu -->
           
        </aside>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
        <!-- #END# Right Sidebar -->
    </section>