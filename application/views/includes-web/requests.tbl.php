<div class="inbox_chat" style="height: 0%;">
<script>
<?php if($requests):?>
  setTimeout(() => {
    get_messages(<?=$requests[0]["id"]?>,'<?=$requests[0]["name"]?>')
  }, 1000); 
<?php endif?>
</script>
<?php foreach($requests as $value):?>
        <div class="chat_list" id="request_<?= $value['id']?>" onclick="get_messages(<?= $value['id']?>,'<?= $value["name"]?>')">
          <div class="chat_people">
            <div class="chat_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
            <div class="chat_ib">
              <h5><?= $value["name"]?> <span class="chat_date" style="float: left;"><?= date("M d h:i")?></span></h5>
              <p><?= $value["title"]?></p>
            </div>
          </div>
        </div>
<?php endforeach?>     
</div>