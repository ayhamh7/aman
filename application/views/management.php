<?php $this->load->view('includes2/header.php');  ?>
<?php $this->load->view('includes2/sidebar.php');  ?>
<style>
    .chosen-container .chosen-drop {

  /*left: 0px !important;*/
    }
</style>
<div class="pcoded-content" style="direction: rtl;">

                      

                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <div class="page-body">

                                        <div class="row" style="direction: rtl;text-align: right;">
                                            <div class="col-sm-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h5><?= $title?$title:'Dashboard'?></h5>
                                                        <div class="card-header-right">
                                                            <ul class="list-unstyled card-option">
                                                                <li class="first-opt"><i
                                                                        class="feather icon-chevron-left open-card-option"></i>
                                                                </li>
                                                                <li><i class="feather icon-maximize full-card"></i></li>
                                                                <li><i class="feather icon-minus minimize-card"></i>
                                                                </li>
                                                                <li><i class="feather icon-refresh-cw reload-card"></i>
                                                                </li>
                                                                <li><i class="feather icon-trash close-card"></i></li>
                                                                <li><i
                                                                        class="feather icon-chevron-left open-card-option"></i>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="card-block">
                                                        <p>
                                                        <?php  if(isset($output->js_files) && isset($is_images)):
                                                                foreach($output->js_files as $file): ?>
                                                                <script src="<?php echo $file; ?>" type="text/javascript"></script>
                                                                <?php endforeach; ?>
                                                                <?php endif;?>

                                                            <div class="table-responsive">
                                                            <?php echo $output->output; ?>
                                                            </div>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="styleSelector">
                    </div>

<?php $this->load->view('includes2/footer.php');  ?>
<script type="d2d1d6e2f87cbebdf4013b26-text/javascript">
    $('#field-category_id').change(function(){
       // alert($("#field-category_id").val())
        $.ajax({
          url: "<?= base_url()?>"+"Api/SubCategories?cat_id="+$("#field-category_id").val()+"/",
          dataType: "json",
          success: function( data ) {
           
                $('#field-Categories').empty(); 
            
            $.map( data, function( item ) {
            console.log(item.name)
            $('#field-Categories').append('<option value='+item.id+'>' + item.name +'-'+item.name_en + '</option>');
            })
            $('#field-Categories').trigger('liszt:updated');
            $('#field-Categories').trigger("chosen:updated");
          }
           
        
       });
    })
</script>