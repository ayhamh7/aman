<nav class="pcoded-navbar">
        <div class="nav-list">
            <div class="pcoded-inner-navbar main-menu">
                <div class="pcoded-navigation-label">One-Deals System</div>
                <ul class="pcoded-item pcoded-left-item">
                   
                    <li class="pcoded-hasmenu <?= isset($orders_active)?' active pcoded-trigger':''?>">
                        <a href="javascript:void(0)" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="feather icon-sidebar"></i></span>
                            <span class="pcoded-mtext">Manage Orders</span>
                            <!-- <span class="pcoded-badge label label-warning">Oreders</span> -->
                        </a>
                        <ul class="pcoded-submenu">
                            <li>
                                <a href="<?= base_url() ?>store_dashboard/manageOrders_C">Active Orders</a>
                            </li>
                             <li>
                                <a href="<?= base_url() ?>store_dashboard/manageOrders_A">Approved Orders</a>
                            </li>
                             <li>
                                <a href="<?= base_url() ?>store_dashboard/manageOrders_charged">Charged Orders</a>
                            </li>
                             <li>
                                <a href="<?= base_url() ?>store_dashboard/manageOrders_Done">Finished Orders</a>
                            </li>
                           <li>
                                <a href="<?= base_url() ?>store_dashboard/manageOrders_Deleted">Canceled Orders</a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>store_dashboard/Cash_Report"> Reports</a>
                            </li>
                        </ul>
                    </li>
                  
                    <li class="pcoded-hasmenu">
                        <a href="javascript:void(0)" class="waves-effect waves-dark">
                            <span class="pcoded-micon">
                                <i class="feather icon-layers"></i>
                            </span>
                            <span class="pcoded-mtext">General Managment</span>
                        </a>
                        <ul class="pcoded-submenu">
                        <li>
                                <a href="<?= base_url() ?>store_dashboard/manage_products">Manage Products</a>
                            </li>
                        
                        </ul>
                    </li>
                </ul>
               
            </div>
        </div>
    </nav>