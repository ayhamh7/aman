<main class="main" style="<?= $lang=='ar'?'direction: rtl;text-align: right;':''?>">
    <div class="page-header text-center" style="background-image: url('assets/images/page-header-bg.jpg')">
        <div class="container">
            <h1 class="page-title">My Account<span>Shop</span></h1>
        </div><!-- End .container -->
    </div><!-- End .page-header -->
    

    <div class="page-content">
        <div class="dashboard">
            <div class="container">
                <div class="row">
                    <aside class="col-md-4 col-lg-3">
                        <ul class="nav nav-dashboard flex-column mb-3 mb-md-0" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="tab-dashboard-link" data-toggle="tab" href="#tab-dashboard"
                                    role="tab" aria-controls="tab-dashboard" aria-selected="true"><?= lang("Dashboard")?></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="tab-orders-link" data-toggle="tab" href="#tab-orders"
                                    role="tab" aria-controls="tab-orders" aria-selected="false"><?= lang("Orders")?></a>
                            </li>
                            <!-- <li class="nav-item">
                                <a class="nav-link" id="tab-downloads-link" data-toggle="tab" href="#tab-downloads"
                                    role="tab" aria-controls="tab-downloads" aria-selected="false">Downloads</a>
                            </li> -->
                            <li class="nav-item">
                                <a class="nav-link" id="tab-address-link" data-toggle="tab" href="#tab-address"
                                    role="tab" aria-controls="tab-address" aria-selected="false"><?= lang("Adresses")?></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="tab-account-link" data-toggle="tab" href="#tab-account"
                                    role="tab" aria-controls="tab-account" aria-selected="false"><?= lang("Account  Details")?></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Sign Out</a>
                            </li>
                        </ul>
                    </aside><!-- End .col-lg-3 -->

                    <div class="col-md-8 col-lg-9">
                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="tab-dashboard" role="tabpanel"
                                aria-labelledby="tab-dashboard-link">

                                <form action="#">
                                    <div class="form-fields" style="border: 1px solid #e9e9e9;float: left;padding: 25px 25px;width: 100%;">
                                        <h2 style="border-bottom: 1px solid #e9e9e9;color: #252531;font-family: &quot;Roboto&quot;,sans-serif;font-size: 20px;font-weight: 600;margin: 0 0 16px;padding: 0 0 12px;text-transform: uppercase;"><?= lang("My Profaile")?></h2>

                                        <p style="margin-top: 3%;">
                                            <label for="login-name" class="" style="color: #444444;display: inline-block;font-size: 17px;font-weight: 700;letter-spacing: 0.5px;margin: 0 0 8px;position: relative;">
                                                <?= lang('customer.name')?> :</label>
                                            <label for="login-name" class="" style="font-size: 15px;font-weight: lighter;">
                                                mostafa
                                                amer</label>

                                        </p>
                                        <p style="margin-top: 3%;">
                                            <label for="login-name" class="" style="color: #444444;display: inline-block;font-size: 17px;font-weight: 700;letter-spacing: 0.5px;margin: 0 0 8px;position: relative;"><?= lang('customer.name')?>
                                                :</label>
                                            <label for="login-name" class="" style="font-size: 15px;font-weight: lighter;">mostav@gmail.com</label>

                                        </p>
                                        <p style="margin-top: 3%;">
                                            <label for="login-name" class="" style="color: #444444;display: inline-block;font-size: 17px;font-weight: 700;letter-spacing: 0.5px;margin: 0 0 8px;position: relative;"><?= lang('customer.mobile')?>
                                                :</label>
                                            <label for="login-name" class="" style="font-size: 15px;font-weight: lighter;">
                                                +966
                                                232 35 446</label>

                                        </p>
                                        <p style="margin-top: 3%;">
                                            <label for="login-name" class="" style="color: #444444;display: inline-block;font-size: 17px;font-weight: 700;letter-spacing: 0.5px;margin: 0 0 8px;position: relative;"><?= lang('customer.address')?>
                                                :</label>
                                            <label for="login-name" class="" style="font-size: 15px;font-weight: lighter;">
                                                damas
                                                ,hasdjc,j34n</label>

                                        </p>
                                    </div>
                                    <div class="form-action" style="background: #f5f5f5 none repeat scroll 0 0;border: 1px solid #e9e9e9;float: left;padding: 15px 10px;width: 100%;">

                                        <a href="#tab-account" class="tab-trigger-link" style="background: #252531 none repeat scroll 0 0;color: #ffffff;cursor: pointer;float: right;font-weight: 600;letter-spacing: 0.5px;margin-right: 14px;padding: 10px 35px;text-transform: uppercase;">Edit
                                            Profial</a>

                                    </div>
                                </form>


                            </div><!-- .End .tab-pane -->

                            <div class="tab-pane fade" id="tab-orders" role="tabpanel" aria-labelledby="tab-orders-link">


                                    <table class="table table-cart table-mobile">
                                            <thead>
                                                <tr>
                                                    <th>Order ID</th>
                                                    <th>Date</th>
                                                    <th>Quantity</th>
                                                    <th>Total Price</th>
                                                    <th style="text-align: center;">Details</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
        
                                            <tbody>
                                                <tr>
                                                    <td class="product-col">
                                                        <div class="product">
                                                            <figure class="product-media">
                                                                <a href="#">
                                                                    <img src="assets/images/products/table/product-1.jpg" alt="Product image">
                                                                </a>
                                                            </figure>
        
                                                            <h3 class="product-title">
                                                                <a href="#">Beige knitted elastic runner shoes</a>
                                                            </h3><!-- End .product-title -->
                                                        </div><!-- End .product -->
                                                    </td>
                                                    <td class="price-col">2/2/2020</td>
                                                    <td class="price-col">3</td>
                                                
                                                    <td class="total-col">$84.00</td>
                                                    <td class="remove-col"><a href="order info.html" class="btn btn-outline-dark-2"><span>More Info</span><i class="icon-long-arrow-left"></i></a></td>
                                                </tr>
                                                <tr>
                                                    <td class="product-col">
                                                        <div class="product">
                                                            <figure class="product-media">
                                                                <a href="#">
                                                                    <img src="assets/images/products/table/product-2.jpg" alt="Product image">
                                                                </a>
                                                            </figure>
        
                                                            <h3 class="product-title">
                                                                <a href="#">Blue utility pinafore denim dress</a>
                                                            </h3><!-- End .product-title -->
                                                        </div><!-- End .product -->
                                                    </td>
                                                    <td class="price-col">9/8/2019</td>
                                                    <td class="price-col">2</td>
                                        
                                                    <td class="total-col">$76.00</td>
                                                    <td class="remove-col"><a href="order info.html" class="btn btn-outline-dark-2"><span>More Info</span><i class="icon-long-arrow-right"></i></a></td>
                                                </tr>
                                            </tbody>
                                        </table>






                                        <a href="#" class="btn btn-outline-dark-2"><span>UPDATE CART</span><i class="icon-refresh"></i></a>
                            </div><!-- .End .tab-pane -->

                            <div class="tab-pane fade" id="tab-downloads" role="tabpanel" aria-labelledby="tab-downloads-link">
                                <p>No downloads available yet.</p>
                                <a href="category.html" class="btn btn-outline-primary-2"><span>GO SHOP</span><i
                                        class="icon-long-arrow-right"></i></a>
                            </div>
                            
                            <!-- .End .tab-pane -->

                            <div class="tab-pane fade" id="tab-address" role="tabpanel" aria-labelledby="tab-address-link">
                                <p>The following addresses will be used on the checkout page by default.</p>
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@fat" style="float: left;margin-top: -6%;">ADD New address <i class="icon-plus"></i></a>

                                <div class="row" style="margin-top: 5%;">
                                    <div class="col-lg-6">
                                        <div class="card card-dashboard">
                                            <div class="card-body">
                                                <h3 class="card-title">Billing Address</h3><!-- End .card-title -->

                                                <p>User Name<br>
                                                    User Company<br>
                                                    John str<br>
                                                    New York, NY 10001<br>
                                                    1-234-987-6543<br>
                                                    yourmail@mail.com<br>
                                                    <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@fat">Edit <i class="icon-edit"></i></a></p>
                                            </div><!-- End .card-body -->
                                        </div><!-- End .card-dashboard -->
                                    </div><!-- End .col-lg-6 -->

                                    <div class="col-lg-6">
                                        <div class="card card-dashboard">
                                            <div class="card-body">
                                                <h3 class="card-title">Shipping Address</h3><!-- End .card-title -->

                                                <p>You have not set up this type of address yet.<br>
                                                    <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@fat">Edit <i class="icon-edit"></i></a></p>
                                            </div><!-- End .card-body -->
                                        </div><!-- End .card-dashboard -->
                                        <div class="card card-dashboard">
                                            <div class="card-body">
                                                <h3 class="card-title">Shipping Address</h3><!-- End .card-title -->

                                                <p>You have not set up this type of address yet.<br>
                                                    <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@fat">Edit <i class="icon-edit"></i></a></p>
                                            </div><!-- End .card-body -->
                                        </div><!-- End .card-dashboard -->
                                    </div><!-- End .col-lg-6 -->
                                </div><!-- End .row -->
                            </div><!-- .End .tab-pane -->

                            <div class="tab-pane fade" id="tab-account" role="tabpanel" aria-labelledby="tab-account-link">
                                <form action="#">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>First Name *</label>
                                            <input type="text" class="form-control" required>
                                        </div><!-- End .col-sm-6 -->

                                        <div class="col-sm-6">
                                            <label>Last Name *</label>
                                            <input type="text" class="form-control" required>
                                        </div><!-- End .col-sm-6 -->
                                    </div><!-- End .row -->

                                    <label>Display Name *</label>
                                    <input type="text" class="form-control" required>
                                    <small class="form-text">This will be how your name will be displayed in
                                        the account section and in reviews</small>

                                    <label>Email address *</label>
                                    <input type="email" class="form-control" required>

                                    <label>Current password (leave blank to leave unchanged)</label>
                                    <input type="password" class="form-control">

                                    <label>New password (leave blank to leave unchanged)</label>
                                    <input type="password" class="form-control">

                                    <label>Confirm new password</label>
                                    <input type="password" class="form-control mb-2">

                                    <button type="submit" class="btn btn-outline-primary-2">
                                        <span>SAVE CHANGES</span>
                                        <i class="icon-long-arrow-right"></i>
                                    </button>
                                </form>
                            </div><!-- .End .tab-pane -->
                        </div>
                    </div><!-- End .col-lg-9 -->
                </div><!-- End .row -->
            </div><!-- End .container -->
        </div><!-- End .dashboard -->
    </div><!-- End .page-content -->
</main><!-- End .main -->
