
<div class="product text-center" data-type="<?= $product["category_id"]?>" data-stars="3" data-facility="Pool" data-price="<?=$product["sale_price"]?>">
    <figure class="product-media">
        <?php if( $product["regular_price"] != $product["sale_price"]):?>
        <span class="product-label label-new"><?= $product["regular_price"] != $product["sale_price"]?round(100 - ($product["sale_price"]/$product["regular_price"]*100)).'%':''?></span>
        <?php endif?>
        <a href="<?= base_url("product?id=".$product["id"])?>">
            <img src="<?= base_url('uploads/'.$product["image"])?>"  alt="Product image" class="product-image" style="max-height: 240px;">
        </a>

        <div class="product-action-vertical">
            <a href="#" class="btn-product-icon btn-wishlist"  onclick="addWhishList(<?= $product['id']?>);return false;" title="Add to wishlist" style="<?= isset($product["is_customer_favorite"])&&$product["is_customer_favorite"]?'background-color: #fe4c02;color: #fff;':''?>"><span>add to wishlist</span></a>
            
        </div><!-- End .product-action-vertical -->

        <div class="product-action">
            <a href="#" onclick="addToCart(<?= $product['id']?>);return false;" class="btn-product btn-cart" title="Add to cart"><span>add to cart</span></a>
        </div><!-- End .product-action -->
    </figure><!-- End .product-media -->

    <div class="product-body">
        <div class="product-cat">
            <a href="#"><?= $product["category_name".$suffix]?></a>
        </div><!-- End .product-cat -->
        <h3 class="product-title"><a href="<?= base_url('product?id='.$product["id"])?>"><?= $product["name"]?></a></h3><!-- End .product-title -->
        <div class="product-price">
            <?= $product["sale_price"]?> <?= $lang=='ar'?'د.أ':'JD'?>
            <?php if($product["sale_price"] != $product["regular_price"]):?>
             <span class="old-price">&nbsp;&nbsp;<?= $product["regular_price"]?> <?= $lang=='ar'?'د.أ':'JD'?></span>
             <?php endif?>
        </div><!-- End .product-price -->
        <div class="ratings-container">
            <div class="ratings">
                <div class="ratings-val" style="width: <?= $product["rating"]*100/5?>%;"></div><!-- End .ratings-val -->
            </div><!-- End .ratings -->
            <span class="ratings-text"></span>
        </div><!-- End .rating-container -->
    </div><!-- End .product-body -->
</div><!-- End .product -->