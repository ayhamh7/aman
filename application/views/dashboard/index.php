<!DOCTYPE html>
<html lang="en">
<head>
<title>Matrix Admin</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="<?= base_url()?>dash/css/bootstrap.min.css" />
<link rel="stylesheet" href="<?= base_url()?>dash/css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="<?= base_url()?>dash/css/fullcalendar.css" />
<link rel="stylesheet" href="<?= base_url()?>dash/css/matrix-style.css" />
<link rel="stylesheet" href="<?= base_url()?>dash/css/matrix-media.css" />
<link href="font-awesome/<?= base_url()?>dash/css/font-awesome.css" rel="stylesheet" />
<link rel="stylesheet" href="<?= base_url()?>dash/css/jquery.gritter.css" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
<script src="<?= base_url()?>dash/js/excanvas.min.js"></script> 
<script src="<?= base_url()?>dash/js/jquery.min.js"></script> 
  <?php 
  if(isset($output->css_files)&&is_array($output->css_files)):
  foreach($output->css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
   
  <?php endforeach;endif; ?>
    <?php 
  if(isset($output->js_files)&&is_array($output->js_files)):
  foreach($output->js_files as $file): ?>
   
    <script src="<?php echo $file; ?>"></script>
  <?php endforeach;endif; ?>
</head>
<body>

<!--Header-part-->
<div id="header">
  <h1><a href="dashboard.html">Matrix Admin</a></h1>
</div>
<!--close-Header-part--> 


<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav">
    <li  class="dropdown" id="profile-messages" ><a title="" href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle"><i class="icon icon-user"></i>  <span class="text">Welcome User</span><b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li><a href="#"><i class="icon-user"></i> My Profile</a></li>
        <li class="divider"></li>
        <li><a href="#"><i class="icon-check"></i> My Tasks</a></li>
        <li class="divider"></li>
        <li><a href="login.html"><i class="icon-key"></i> Log Out</a></li>
      </ul>
    </li>
    <li class="dropdown" id="menu-messages"><a href="#" data-toggle="dropdown" data-target="#menu-messages" class="dropdown-toggle"><i class="icon icon-envelope"></i> <span class="text">Messages</span> <span class="label label-important">5</span> <b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li><a class="sAdd" title="" href="#"><i class="icon-plus"></i> new message</a></li>
        <li class="divider"></li>
        <li><a class="sInbox" title="" href="#"><i class="icon-envelope"></i> inbox</a></li>
        <li class="divider"></li>
        <li><a class="sOutbox" title="" href="#"><i class="icon-arrow-up"></i> outbox</a></li>
        <li class="divider"></li>
        <li><a class="sTrash" title="" href="#"><i class="icon-trash"></i> trash</a></li>
      </ul>
    </li>
    <li class=""><a title="" href="#"><i class="icon icon-cog"></i> <span class="text">Settings</span></a></li>
    <li class=""><a title="" href="login.html"><i class="icon icon-share-alt"></i> <span class="text">Logout</span></a></li>
  </ul>
</div>
<!--close-top-Header-menu-->
<!--start-top-serch-->
<div id="search">
  <input type="text" placeholder="Search here..."/>
  <button type="submit" class="tip-bottom" title="Search"><i class="icon-search icon-white"></i></button>
</div>
<!--close-top-serch-->
<!--sidebar-menu-->
<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
  <ul>
    <li class="active">
      <a href="<?= site_url('dashboard/manageCotumers'); ?>"><i class="icon icon-home"></i> <span>Costumer managment</span></a>
    </li>
    <!-- <li class="">
      <a href="<?= site_url('dashboard/items'); ?>"><i class="icon icon-home"></i> <span>Items managment</span></a>
    </li> -->
    
  </ul>
</div>
<!--sidebar-menu-->

<!--main-container-part-->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
<!--End-breadcrumbs-->

<!--Action boxes-->
  <div class="container-fluid" style="background-color: white;">
    <div class="quick-actions_homepage">
      <ul class="quick-actions">
        <li class="bg_lb"> <a href="index.html"> <i class="icon-dashboard"></i> <span class="label label-important">20</span> Return materials Request </a> </li>
      
       
        <li class="bg_lg"> <a href="tables.html"> <i class="icon-th"></i><span class="label label-important">10</span> Recived Materials</a> </li>
       
        
        <li class="bg_lb"> <a href="interface.html"> <i class="icon-pencil"></i><span class="label label-important">1000</span> Materials</a> </li>
     

      </ul>
    </div>
<!--End-Action boxes-->    

<!--Chart-box-->    
  <!--  <div class="row-fluid">
      <div class="widget-box">
        <div class="widget-title bg_lg"><span class="icon"><i class="icon-signal"></i></span>
          <h5>Site Analytics</h5>
        </div>
        <div class="widget-content" >
          <div class="row-fluid">
            <div class="span9">
              <div class="chart"></div>
            </div>
           
          </div>
        </div>
      </div>
    </div>
<!--End-Chart-box--> 
    <hr/>
    <div class="row-fluid">
        <?php echo $output->output; ?>
     </div>
  </div>
</div>

<!--end-main-container-part-->

<!--Footer-part-->

<div class="row-fluid">
  <div id="footer" class="span12"> 2013 &copy; Matrix Admin. Brought to you by <a href="http://themedesigner.in">Themedesigner.in</a> </div>
</div>

<!--end-Footer-part-->

<!-- <script src="<?= base_url()?>dash/js/excanvas.min.js"></script> 
<script src="<?= base_url()?>dash/js/jquery.min.js"></script>  -->
<script src="<?= base_url()?>dash/js/jquery.ui.custom.js"></script> 
<script src="<?= base_url()?>dash/js/bootstrap.min.js"></script> 
<script src="<?= base_url()?>dash/js/jquery.flot.min.js"></script> 
<script src="<?= base_url()?>dash/js/jquery.flot.resize.min.js"></script> 
<script src="<?= base_url()?>dash/js/jquery.peity.min.js"></script> 
<script src="<?= base_url()?>dash/js/fullcalendar.min.js"></script> 
<script src="<?= base_url()?>dash/js/matrix.js"></script> 
<script src="<?= base_url()?>dash/js/matrix.dashboard.js"></script> 
<script src="<?= base_url()?>dash/js/jquery.gritter.min.js"></script> 
<script src="<?= base_url()?>dash/js/matrix.interface.js"></script> 
<script src="<?= base_url()?>dash/js/matrix.chat.js"></script> 
<script src="<?= base_url()?>dash/js/jquery.validate.js"></script> 
<script src="<?= base_url()?>dash/js/matrix.form_validation.js"></script> 
<script src="<?= base_url()?>dash/js/jquery.wizard.js"></script> 
<script src="<?= base_url()?>dash/js/jquery.uniform.js"></script> 
<script src="<?= base_url()?>dash/js/select2.min.js"></script> 
<script src="<?= base_url()?>dash/js/matrix.popover.js"></script> 
<script src="<?= base_url()?>dash/js/jquery.dataTables.min.js"></script> 
<script src="<?= base_url()?>dash/js/matrix.tables.js"></script> 

<script type="text/javascript">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != "") {
      
          // if url is "-", it is this page -- reset the menu:
          if (newURL == "-" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}
</script>
</body>
</html>
