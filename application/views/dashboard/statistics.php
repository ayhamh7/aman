

<canvas id="myChart"></canvas>

<canvas id="myChart2"></canvas>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js" />
<script src="<?= base_url(); ?>asset/Chart.js"></script>
<script type="text/javascript">

<?php
$php_array = array('abc','def','ghi');
$js_array = json_encode(array_keys(get_object_vars($chickenprice[0])));
$data_array= json_encode(array_values(get_object_vars($chickenprice[0])));

echo "var labels_array = ". $js_array . ";\n";
echo "var data_array = ". $data_array . ";\n";
?>
//console.log(data_array);
var ctx = document.getElementById('myChart').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'bar',

    // The data for our dataset
    data: {
        labels: labels_array,
        datasets: [{
            label: "chicken price",
            backgroundColor: 'rgb(255, 99, 132)',
            borderColor: 'rgb(255, 99, 132)',
            data: data_array,
        }]
    },

    // Configuration options go here
    options: {}
});


<?php

$js_array2 = json_encode(array_keys(get_object_vars($feedprice[0])));
$data_array2= json_encode(array_values(get_object_vars($feedprice[0])));

echo "var labels_array2 = ". $js_array2 . ";\n";
echo "var data_array2 = ". $data_array2 . ";\n";
?>
var ctx2 = document.getElementById('myChart2').getContext('2d');
var chart2 = new Chart(ctx2, {
    // The type of chart we want to create
    type: 'bar',

    // The data for our dataset
    data: {
        labels: labels_array2,
        datasets: [{
            label: "Feed price",
             backgroundColor: 'rgb(57, 198, 236)',
            borderColor: 'rgb(57, 198, 236)',
            data: data_array2,
        }]
    },

    // Configuration options go here
    options: {}
});
</script>