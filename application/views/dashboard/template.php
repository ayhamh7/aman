<?php  include 'include/header.php'; ?>
<body class="nav-md" <?php if($this->session->userdata('user')['lang'] == 'ar') echo 'style="direction: rtl;"';?> >
    <div class="container body">
      <div class="main_container">
        <?php 
          $this->load->view('include/sideBar');
          $this->load->view('include/topNavBar');
          if(isset($mainView)){
            echo '<div class="right_col" id="mainView" role="main">';
            $this->load->view($mainView); 
            echo '</div>';
          }
          else
            redirect('access/not_found');
        ?>
        </div>
      </div>
</body>
<?php if(!isset($koko)):?>
<?php include 'include/footer.php'; ?>
<?php endif?>