<div class="top_nav" style="height: 56px;">

        <div class="nav_menu">
          <nav class="" role="navigation">
            <div class="nav toggle">
              <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
              <li class="">
                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  <img src="<?= base_url(); ?>assets/images/user.png" alt=""><?= $this->session->userdata('user')['username'] ;?>
                  <span class=" fa fa-angle-down" style="margin-left: 5px; margin-right: 5px;"></span>
                </a>
                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                  <li><a href="<?= base_url('/profile/user/' . $this->session->userdata('user')['user_id']); ?>">profile</a>
                  </li>
                  <li><a href="<?= base_url(). 'login/logout';?>"><i class="fa fa-sign-out pull-right"></i>تسجيل خروج</a>
                  </li>
                </ul>
              </li>

            </ul>
          </nav>
        </div>
      </div>