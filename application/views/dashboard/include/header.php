<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  

  <!-- Bootstrap core CSS -->
  <link href="<?= base_url(); ?>assets/dashboard/css/bootstrap.en.min.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/dashboard/fonts/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/dashboard/css/animate.min.css" rel="stylesheet">
  <!-- Custom styling plus plugins -->
  <link href="<?= base_url(); ?>assets/dashboard/css/custom.en.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/dashboard/css/icheck/flat/green.css" rel="stylesheet" />

  <link href="<?= base_url(); ?>assets/dashboard/css/floatexamples.css" rel="stylesheet" type="text/css" />
  <link href="<?= base_url(); ?>assets/dashboard/css/select/select2.min.css" rel="stylesheet">
  <!-- switchery -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/dashboard/css/switchery/switchery.min.css" />
  <style>
    .daterangepicker{
      z-index:1151 !important;
    }
    .calendar-time {
      padding-left: 15px;
      padding-top: 15px;
    }
    .ranges {
      padding-left: 10px;
    }
  </style>

 
   <script src="<?= base_url(); ?>assets/dashboard/js/jquery.min.js"></script>
  <script src="<?= base_url(); ?>assets/dashboard/js/nprogress.js"></script>
	<?php 
	if(isset($output->css_files)&&is_array($output->css_files)):
	foreach($output->css_files as $file): ?>
		<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
	 
	<?php endforeach;endif; ?>
    <?php 
  if(isset($output->js_files)&&is_array($output->js_files)):
  foreach($output->js_files as $file): ?>
   
    <script src="<?php echo $file; ?>"></script>
  <?php endforeach;endif; ?>

<body class="nav-md" >
    <div class="container body">
      <div class="main_container">
<!-- End of header-->
    <div style='height:20px;'></div>  
    <div>
  
</head>

