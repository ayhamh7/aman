
  <script src="<?= base_url(); ?>assets/dashboard/js/bootstrap.min.js"></script>

  <!-- bootstrap progress js -->
  <script src="<?= base_url(); ?>assets/dashboard/js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="<?= base_url(); ?>assets/dashboard/js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="<?= base_url(); ?>assets/dashboard/js/icheck/icheck.min.js"></script>
  <!-- pace -->
  <!-- select2 -->
  <script src="<?= base_url(); ?>assets/dashboard/js/select/select2.full.js"></script>
		
  <script src="<?= base_url(); ?>assets/dashboard/js/pace/pace.min.js"></script>
  <script src="<?= base_url(); ?>assets/dashboard/js/notify/pnotify.core.js"></script>
  <script src="<?= base_url(); ?>assets/dashboard/js/validator/validator.js"></script>
  <script src="<?= base_url(); ?>assets/dashboard/js/moment/moment.min.js"></script>
  <script src="<?= base_url(); ?>assets/dashboard/js/datepicker/daterangepicker.js"></script>
  <script src="<?= base_url(); ?>assets/dashboard/js/calendar/fullcalendar.min.js"></script>
  <script src="<?= base_url(); ?>assets/dashboard/js/custom.js"></script>

</html>
