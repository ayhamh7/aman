
	<?php 
		  $this->load->view('dashboard/include/header');
          $this->load->view('dashboard/include/sideBar');
          $this->load->view('dashboard/include/topNavBar');
          if(isset($output)){
            echo '<div class="right_col" id="mainView" role="main">'; ?>
				<?php echo $output->output; ?>
         <?php    
		 echo '</div>';
          }
		  else if(isset($mainContent)){
			echo '<div class="right_col" id="mainView" role="main">'; 
			$this->load->view('dashboard/'.$mainContent);
			 echo '</div>';
			}
          else
            redirect('access/not_found');
			
        ?>
    </div>
<!-- Beginning footer -->
	<?php $this->load->view('dashboard/include/footer'); ?>
<!-- End of Footer -->
        </div>
      </div>
