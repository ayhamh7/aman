  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12" style="direction: rtl;">
      <div class="x_panel">
        <div class="x_title">
          <h2 style="float:right">إرسال رسالة</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <form id="demo-form2" class="form-horizontal form-label-left" method="post" action="<?= site_url('dashboard/sendReplay'); ?>" novalidate>
            <div class="item form-group">
              <div class="col-md-3 col-sm-3 col-xs-12"></div>
			  <div class="col-md-6 col-sm-6 col-xs-12">
                <input id="email" class="form-control col-md-7 col-xs-12" data-validate-length-range="6, 20" name="first_name" placeholder="أدخل بريد المستقبل" required="required" type="text" value="<?php echo $email ?>">
              </div>
			  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">بريد المستقبل<span class="required">*</span>
              </label>
            </div>
            <div class="item form-group">
              <div class="col-md-3 col-sm-3 col-xs-12"></div>
			  <div class="col-md-6 col-sm-6 col-xs-12">
                <input id="subject" class="form-control col-md-7 col-xs-12" name="subject" required="required" type="text">
              </div>
			   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first_name">العنوان<span class="required">*</span>
              </label>
            </div>
            <div class="item form-group">
              <div class="col-md-3 col-sm-3 col-xs-12"></div>
			  <div class="col-md-6 col-sm-6 col-xs-12">
                <textarea id="message" name="message" class="form-control col-md-7 col-xs-12"></textarea>
              </div>
			  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">نص الرسالة
              </label>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-md-offset-3">
                <button id="send" type="submit" class="btn btn-success">إرسال الرسالة</button>
              </div>
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>