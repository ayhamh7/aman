<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                    <h2><?php echo lang('create_user_heading');?></h2>
                    <p><?php echo lang('edit_user_subheading');?></p>
            </div>
            <div class="body">
                <?php echo form_open("auth/create_user");?>
                    <div id="infoMessage"><?php echo $message;?></div>
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-5 form-control-label" >
                            <label for="first_name"><?php echo lang('create_user_fname_label');?></label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="first_name" class="form-control"  name="first_name">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-5 form-control-label" >
                            <label for="last_name"> <?php echo lang('create_user_lname_label');?> </label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="last_name" name="last_name" class="form-control" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                      if($identity_column!=='email') { ?>
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-5 form-control-label" >
                                <label for="identity"> <?php echo lang('create_user_identity_label');?> </label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="identity" name="identity" class="form-control" placeholder="">
                                    </div>
                                </div>
                            </div>
                            <p><?php echo form_error('identity');?></p>
                        </div>
                      <?php 
                      }
                      ?>
                   
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-5 form-control-label" >
                            <label for="phone"> <?php echo lang('create_user_phone_label');?> </label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="phone" name="phone" class="form-control" placeholder="" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-5 form-control-label" >
                            <label for="password">  <?php echo lang('create_user_password_label');?> </label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="password" id="password" name="password" class="form-control" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-5 form-control-label" >
                            <label for="password_confirm">  <?php echo lang('create_user_password_confirm_label');?> </label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="password" id="password_confirm" name="password_confirm" class="form-control" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                  
                    <div class="row clearfix">
                        <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect"><?= lang('create_user_submit_btn') ?></button>
                        </div>
                    </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url();?>assets/js/demo.js"></script>
