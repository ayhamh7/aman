<p><?php echo anchor('auth/create_user', lang('index_create_user_link'), array('class' => 'btn btn-primary waves-effect'))?> &nbsp; <?php echo anchor('auth/create_group', lang('index_create_group_link'), array('class' => 'btn btn-info waves-effect'))?></p>
 <script>
var arr = [[]];
</script>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    <?php echo lang('index_heading');?>
                </h2>
                <p><?php echo lang('index_subheading');?></p>
            </div>
            <div id="infoMessage"><?php echo $message;?></div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                            <tr>
                                <th><?php echo lang('index_fname_th');?></th>
                                <th><?php echo lang('index_lname_th');?></th>
                                <th><?php echo lang('index_email_th');?></th>
                                <th><?php echo lang('index_groups_th');?></th>
                                <th><?php echo lang('index_status_th');?></th>
                                <th><?php echo lang('index_action_th');?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                            <?php foreach ($users as $user):?>
                                <tr>
                                    <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
                                    <td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
                                    <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
                                    <td>
                                        <button onclick="view(<?= $user->id?>)">view previlige</button>
                                       
                                        <script type="text/javascript"> var i=0;
 arr[<?= $user->id?>] = [];
                                              <?php foreach ($user->groups as $group):?>

                                            arr[<?= $user->id?>].push( '<?php echo anchor("", htmlspecialchars($group->name,ENT_QUOTES,"UTF-8"), array("class"=>"btn bg-deep-purple waves-effect","style"=>"margin-top: 11px;")) ;?>&nbsp; &nbsp;');
i++;
                                            <?php endforeach?>;

                                        </script>
                                    </td>
                                    <td><?php echo ($user->active) ? form_submit("x", "Active", array('class' => 'btn btn-danger waves-effect', 'onclick' => "deactivate('".$user->id."','".htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8')."','id_".$i."')", 'id'=>'id_'.$i)) : form_submit("x", "UnActive", array('class' => 'btn btn-success waves-effect', 'onclick' => "deactivate('".$user->id."','".htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8')."','id_".$i."')", 'id'=>'id_'.$i) );?></td>
                                    <td><?php echo anchor("auth/edit_user/".$user->id, 'Edit', array('class' => 'btn btn-info waves-effect')) ;?></td>
                                </tr>
                                <?php $i++; ?>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="largeModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    <div class="row clearfix" id="modal_body">

                    </div>
                        
                </div>
                <div class="modal-footer">
                    <button type="button" onclick="addMember()" id="Add" class="btn btn-link waves-effect"><?= lang('global.add')?></button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal"><?= lang('global.close')?></button>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
    function view(id)
    {
        $("#modal_body").html(arr[id]);
        $('#largeModal').modal('show'); 
    }
    function deactivate(userId, name, buttonId){
        var active = 0;
        var msg = "<?= lang('deactivate_subheading') ?> ' " + name+ " ' ";
        if ( document.getElementById(buttonId).classList.contains('btn-success') ){
            msg = "<?= lang('activate_subheading') ?> ' " + name+ " ' ";
            active = 1;
        }
        var r = confirm(msg);
        if (r == true) {
              var xhttp = new XMLHttpRequest();
              xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    if ( document.getElementById(buttonId).classList.contains('btn-success') ){
                        document.getElementById(buttonId).classList.add('btn-danger');
                        document.getElementById(buttonId).classList.remove('btn-success');
                        document.getElementById(buttonId).value="مفعل"
                    }
                    else{
                        document.getElementById(buttonId).classList.add('btn-success');
                        document.getElementById(buttonId).classList.remove('btn-danger');
                        document.getElementById(buttonId).value="غير مفعل"
                    }
                }
              };
              xhttp.open("get","<?= base_url("auth/deactivate?active=") ?>"+active+"&id="+userId, true);
              xhttp.send();
        }
    }
</script>