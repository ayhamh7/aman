<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                    <h2><?php echo lang('create_group_heading');?></h2>
                    <p><?php echo lang('create_group_subheading');?></p>
            </div>
            <div class="body">
                <?php echo form_open("auth/create_group");?>
                    <div id="infoMessage"><?php echo $message;?></div>
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-5 form-control-label <?php if(!isset($lang) || $lang=="ar") echo "pull-right"?>" <?php if(isset($lang) && $lang=="en") echo "style=' text-align: left; '"; else echo "style='text-align: right;'"; ?>>
                            <label for="group_name"><?php echo lang('create_group_name_label');?></label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="group_name" class="form-control"  name="group_name">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-5 form-control-label <?php if(!isset($lang) || $lang=="ar") echo "pull-right"?>" <?php if(isset($lang) && $lang=="en") echo "style=' text-align: left; '"; else echo "style='text-align: right;'"; ?>>
                            <label for="description"> <?php echo lang('create_group_desc_label');?> </label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="description" name="group_description" class="form-control" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect"><?= lang('create_group_submit_btn') ?></button>
                        </div>
                    </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url();?>assets/js/demo.js"></script>
