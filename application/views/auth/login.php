<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>login</title>
    <!-- Favicon-->
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url(); ?>assets/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo base_url(); ?>assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <style>
        .login-page {
            background-image: url('<?php echo base_url(); ?>assets/images/background.svg');
            
        }
    </style>
</head>

<body class="login-page">
    <div class="login-box">
        <div class="logo">
            <a href="javascript:void(0);">
                <h1><?php echo lang('login_heading'); ?></h1>
            </a>
            <small>
                <p><?php echo lang('login_subheading'); ?></p>
            </small>
        </div>
        <div class="card">
            <div class="body">
                <?php echo form_open("auth/login");
                //,array("autocomplete"=>"false")
                ?>
                <div class="msg">
                    <div id="infoMessage"><?php echo $message; ?></div>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="material-icons">person</i>
                    </span>
                    <div class="form-line">
                        <input type="text" class="form-control" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');" onfocusout="this.setAttribute('readonly','readonly');" id="identity" name="identity" placeholder="<?php echo lang('login_identity_label'); ?>" required autofocus>
                    </div>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="material-icons">lock</i>
                    </span>
                    <div class="form-line">
                        <input type="password" style="display:none;">
                        <input type="text" id="passfld" autocomplete="off" class="auto-complete-off form-control" name="password" readonly onfocus="this.removeAttribute('readonly');" onfocusout="this.setAttribute('readonly','readonly');" placeholder="<?php echo lang('create_user_password_label'); ?>" required>
                    </div>
                </div>
                <div class="row">
                    <!--                         <div class="col-xs-8 p-t-5">
                            <input type="checkbox" name="remember" id="remember" class="filled-in chk-col-pink">
                             <?php echo lang('login_remember_label', 'remember'); ?>
                        </div> -->
                    <div class="col-xs-4">
                        <button class="btn btn-block bg-pink waves-effect" type="submit"><?php echo lang('login_submit_btn'); ?></button>
                    </div>
                </div>
                <!--<div class="row m-t-15 m-b--20">-->
                <!--    <div class="col-xs-6 align-right">-->
                <!--        <p><a href="forgot_password"><?php echo lang('login_forgot_password'); ?></a></p>-->
                <!--    </div>-->
                <!--</div>-->
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
    <!-- Jquery Core Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript">
        $(function() {
            var passElem = $("input#passfld");
            passElem.focus(function() {
                passElem.prop("type", "password");
            });
            $("#identity").foucus(function() {
                passElem.val("");
                passElem.prop("type", "text");
            });
        });
    </script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url(); ?>assets/js/admin.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/pages/examples/sign-in.js"></script>
</body>

</html>