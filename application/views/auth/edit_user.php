<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                    <h2><?php echo lang('edit_user_heading');?></h2>
                    <p><?php echo lang('edit_user_subheading');?></p>
            </div>
            <div class="body">
                <?php echo form_open(uri_string());?>
                    <div id="infoMessage"><?php echo $message;?></div>
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-5 form-control-label ">
                            <label for="first_name" class=""><?php echo lang('edit_user_fname_label');?></label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="first_name" class="form-control"  name="first_name" value="<?php echo $first_name['value'];?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-5 form-control-label ">
                            <label for="last_name" class=""> <?php echo lang('edit_user_lname_label');?> </label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="last_name" name="last_name" class="form-control" placeholder="" value="<?php echo $last_name['value'];?>">
                                </div>
                            </div>
                        </div>
                    </div>
                  
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-5 form-control-label ">
                            <label for="phone" class=""> <?php echo lang('edit_user_phone_label');?> </label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="phone" name="phone" class="form-control" placeholder="" value="<?php echo $phone['value'];?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-5 form-control-label ">
                            <label for="password" class="">  <?php echo lang('edit_user_password_label');?> </label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="password" id="password" name="password" class="form-control" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-5 form-control-label ">
                            <label for="password_confirm" class="">  <?php echo lang('edit_user_password_confirm_label');?> </label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="password" id="password_confirm" name="password_confirm" class="form-control" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                     <div class="row clearfix">
                    
                        <?php if ($this->ion_auth->is_admin()): ?>
                        <h3><?php echo lang('edit_user_groups_heading');?></h3>
                          <?php 
                              $i=0;
                              foreach ($groups as $group):?>
                              <?php
                                  $gID=$group['id'];
                                  $checked = null;
                                  $item = null;
                                  foreach($currentGroups as $grp) {
                                      if ($gID == $grp->id) {
                                          $checked= ' checked="checked"';
                                      break;
                                      }
                                  }
                                  $i++;
                              ?>

                                <!-- <div class="demo-switch-title"><?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?></div> -->
                                <div class="switch">
                                    <label><input type="checkbox" name="groups[]" value="<?php echo $group['id'];?>" <?php if($checked) echo 'checked="true"';?>><span class="lever switch-col-light-blue"></span></label>
                                    <label><?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?></label>
                                </div>
                          <?php endforeach?>

                      <?php endif; ?>

                      <?php echo form_hidden('id', $user->id);?>
                      <?php echo form_hidden($csrf); ?>
                    <div class="row clearfix">
                        <center>
                            <button type="submit" class="btn btn-block btn-lg btn-info waves-effect" style="width: 50%;"><?= lang('edit_user_submit_btn') ?></button>
                        </center>
                    </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url();?>assets/js/demo.js"></script>
