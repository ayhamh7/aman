<nav class="pcoded-navbar" >
    <div class="nav-list">
        <div class="pcoded-inner-navbar main-menu">
            <div class="pcoded-navigation-label">Hafilate</div>
            <ul class="pcoded-item pcoded-left-item">
                <li class="pcoded-hasmenu  <?= isset($customer_active) ? ' active pcoded-trigger' : '' ?>">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                        <span class="pcoded-mtext">إدارة المستخدمين</span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li>
                            <a href="<?= base_url() ?><?= isset($controller) ? $controller : 'dashboard' ?>/manageCustomers">جميع المستخدمين</a>
                        </li>
                        <li>
                            <a href="<?= base_url() ?><?= isset($controller) ? $controller : 'dashboard' ?>/manageSchools">جميع المدارس</a>
                        </li>
                        <li>
                            <a href="<?= base_url() ?><?= isset($controller) ? $controller : 'dashboard' ?>/manageBuses">جميع الباصات</a>
                        </li>
                        <li>
                            <a href="<?= base_url() ?><?= isset($controller) ? $controller : 'dashboard' ?>/manageParents">جميع حسابات الاهل</a>
                        </li>
                        <li>
                            <a href="<?= base_url() ?><?= isset($controller) ? $controller : 'dashboard' ?>/manageActiveCustomers">المستخدمين المفعلين</a>
                        </li>
                        <!-- <li>
                                <a href="<?= base_url() ?><?= isset($controller) ? $controller : 'dashboard' ?>/manageUnActiveCustomers">المستخدمين غير المفعلين</a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?><?= isset($controller) ? $controller : 'dashboard' ?>/manage_general_notifications">الإشعارات العامة</a>
                            </li> -->
                    </ul>
                </li>

                <li class="pcoded-hasmenu  <?= isset($ads_active) ? ' active pcoded-trigger' : '' ?>">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                        <span class="pcoded-mtext">إدارة الطلبات</span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li>
                            <a href="<?= base_url() ?><?= isset($controller) ? $controller : 'dashboard' ?>/manage_changestop_request">طلب تغيير موقف</a>
                        </li>
                        <li>
                            <a href="<?= base_url() ?><?= isset($controller) ? $controller : 'dashboard' ?>/manage_changebus_request">طلب تغيير خط</a>
                        </li>


                    </ul>
                </li>
                <?php if ($this->ion_auth->is_admin()) : ?>
                    <li class="pcoded-hasmenu  <?= isset($ads_active) ? ' active pcoded-trigger' : '' ?>">
                        <a href="javascript:void(0)" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                            <span class="pcoded-mtext">إدارة عامة</span>
                        </a>
                        <ul class="pcoded-submenu">
                            <li>
                                <a href="<?= base_url() ?><?= isset($controller) ? $controller : 'dashboard' ?>/manage_users">إدارة المدراء </a>
                            </li>


                        </ul>
                    </li>
                <?php endif ?>

            </ul>

        </div>
    </div>
</nav>