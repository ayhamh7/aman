<?php 
class Notification
{
    protected $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
      //  $this->CI->load->model('notification_model');
    }

    public function push($data)
    {
        //$data["from_id"] = $from_id;
        $data["date"]=date('Y-m-d H:i:s');
        $this->CI->notification_model->insert($data);
    }
   

    public function getNotification($user_id)
    {
        return $this->CI->notification_model->get_notification($user_id);
    }

  
    public function send_mail($to_email,$title,$message,$cc=NULL) { 
		//
        $from_email = "info@dllaal.com"; 
        $to_email = $to_email;//'a.hosary@bbsy.loc'; 
       
        $this->CI->load->library('email'); 
      
        $this->CI->email->from($from_email, 'Dllaal App'); 
        $this->CI->email->to($to_email);
		 $this->CI->email->set_mailtype("html");
       // $this->CI->email->bcc($cc_amer);
        $this->CI->email->subject($title); 
        $this->CI->email->message($message); 
        //Send mail 
		
        $result= $this->CI->email->send();
		
     
     } 
    
    
}
