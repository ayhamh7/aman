<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
| example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
| http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
| $route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
| $route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
| $route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples: my-controller/index -> my_controller/index
|   my-controller/my-method -> my_controller/my_method
*/
$route['default_controller'] = 'Welcome';//'AgencyController';
$route['error_page'] = 'Welcome/error_page';
$route['contact_us'] = 'Welcome/contact_us';
$route['thank_you'] = 'Welcome/thank_you';
$route['faq'] = 'Welcome/faq';
$route['policy'] = 'Welcome/policy';
$route['about_us'] = 'Welcome/about_us';
$route['lang'] = 'Welcome/lang';
$route['home'] = 'Products';//'AgencyController';
$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;
$route["logout"]= "Login/logout";
$route["register"]= "Login/register";
$route["products"]= "Products/products_list";
$route["prochoure"]= "Products/prochoure";

$route["product"]= "Products/product";
$route["profile"]= "Users/profile";
$route["order_info"]= "Users/order_info";
$route["wishlist"]= "Users/wishlist";
$route["checkout"]= "Users/checkout";
$route["cart"]= "Products/cart";
$route["search"]= "Products/search";
$route["shops"]= "Products/shops";
$route["shop"]= "Products/shop";
$route["newProducts"]= "Products/newProducts";
$route["mostSell"]= "Products/mostSell";

$route["logout"]= "Login/logout";
/*
| -------------------------------------------------------------------------
| Sample REST API Routes
| -------------------------------------------------------------------------
*/
$route['api/example/users/(:num)'] = 'api/example/users/id/$1'; // Example 4
$route['api/example/users/(:num)(\.)([a-zA-Z0-9_-]+)(.*)'] = 'api/example/users/id/$1/format/$3$4'; // Example 8
