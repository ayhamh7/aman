<?php 
	$lang = array(
				// global\
				'manage_categories'=>'Manage Categories',
				'manage_customer'=>'Manage customer',
				'manage_all_customers'=>'Manage All customers',
				'manage_customers'=>'Manage customers',
				'manage_active_customer'=>'Manage Active customer',
				'manage_unactive_customer'=>'Manage UnActive customer',
				'fill_required' => 'please fill all required fields',
				'customer.name'=>'Name',
				'prochoure'=>'Prochoure',
				'customer.logo'=>'Logo',
				'customer.email'=>'Email',
				'customer.country'=>'Country',
				'customer.city'=>'City',
				'customer.address'=>'Address',
				'customer.mobile'=>'Mobile',
				'customer.phone'=>'Phone',
				'customer.password'=>'Password',
				'customer.managerName'=>'Manager Name',
				'manage_admins'=>'Manage Admins',
				'manage_config'=>'Config Managment',
				'general_management'=>"General Management",
				'manage_slider'=>'Slider Management',
				'profile.Contact'=>"Contact",
				'profile.Balance'=>'Balance',
				'profile.Markup'=>'Markup',
				'profile.aboutcustomer'=>'ABOUT customer',
				'sub_customers'=>'Sub customers',
				'profile.Balances'=>'Balances',
				'profile.Bookings'=>'Bookings',
				'login.user_exist_error'=>'User already Exist',
				'login.fill_required_field'=>'Fill Required Fields',
				'login.user_doesnt_exist_error'=>"User doesn't Exist",
				'login.login_after_activate'=>'your account created successfully, you can login after activate your acount',
				'login.activate_error'=>"your account isn't active",
				'login.wrong_password'=>'incorrect password',
				"pending_balance_request" => "Pending Balance Requests",
				"balance_requests" => "Balance Requests",
				"manage_orders" =>"Orders Management",
				"bookings_accounts"=>"Booking Operations",
				"error.insufficient_balance" => "insufficient balance",
				"error.insufficient_customer_balance" => "insufficient customer balance",
				'error.value_beween0and100'=>"Value Should be between 0 and 100",
				'error.value_greather_than_0'=>'Value Should be greather than 0',
				"all_department"=>"all department",
				"Search"=>"Search",
				"Profile"=>"Profile",
				"Wishlist"=>"Wishlist",
				"browse_cat"=>"Browse Categories",
				"Home"=>"Home",
				"Shops"=>"Shops",
				"about_us"=>"About US",
				"FAQ"=>"FAQ",
				"Policy"=>"Policy",
				"ContactUs"=>"Contact Us",
				"Who We Are"=>"Who We Are",
				"keep_touch"=>"keep in touch with us",
				"Contact Information"=>"Contact Information",
				"Got Any Questions?" =>"Got Any Questions?",
				"enter_info"=>"Use the form below to get in touch with the sales team",
				"Name"=>"Name",
				"Address"=>"Address",
				"Email"=>"Email",
				"Phone"=>"Phone",
				"Subject"=>"Subject",
				"Message"=>"Message",
				"submit"=>"SUBMIT",
				"New Products"=>"New Products",
				"Cart"=>"Cart",
				"Most Selles"=>"Most Selles",
				"LOG IN"=>"LOG IN",
				"SIGN UP"=>"SIGN UP",
				"You May Also Like"=>"You May Also Like",
				"Product"=>"Product",
				"Price"=>"Price",
				"Quantity"=>"Quantity",
				"Total"=>"Total",
				"Checkout"=>"Checkout",
				"biling details"=>"biling details",
				"Dashboard"=>"Dashboard",
				"Orders"=>"Orders",
				"Adresses"=>"Adresses",
				"Account  Details"=>"Account  Details",
				"My Profaile"=>"My Profaile",
				"Order ID"=>"Order ID",
				"OrderDate"=>"Order Date",
				"address_desc"=>"The following addresses will be used on the checkout page by default.",
				"current_pass"=>"Current password ",
				"leave_blank"=>"(leave blank to leave unchanged)",
				"new_pass"=>"New Password",
				"confirm_pass"=>"Confirm Password",
				"Oerer Info"=>"Oerer Info",
				"Daily Offers"=>"Daily Offers",
				"Weekly Offers"=>"Weekly Offers",
				"more"=>"more",
				"one-deals"=>"one-deals",
				"add to cart"=>"add to cart",
				"Add to Wishlist"=>"Add to Wishlist",
			);
?>
