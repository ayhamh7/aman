<?php 
	$lang = array(
				// global
		'global.add' => 'add',
		'global.edit' => 'edit',
		'global.delete' => 'delete',
		'global.copy' => 'copy',
		'global.view' => 'view',
		'global.editData' => 'Edit Form Data',
		'global.close' => 'close',
		'global.save' => 'save',
		'global.done' => 'done',
		'global.print' => 'print',
		'global.accept'=>'accept',
		'global.reject'=>'reject',
		'global.conseration'=>'consultation',
		'global.messageDelete' => 'Are you sure ?! Deleted item cannot be restored',
		'global.porto'=>'rotogravure printing',
		'global.flexo'=>'flexographic printing',
                'global.send'=>'send',
'global.assign'=>'assign',
'global.restore'=>'restore',
'Cylinder'=>'Cylinders',
		//Template File
		'template.equation'=>'equation value',
		'template.nameInEng'=>'Name in english',
		'template.nameInAra'=>'Name in arabic',
		'template.multipleOrderMsg'=>'add multiple table specification in the same agreement',
		'template.id'=>'#',
		'template.Actions'=>'Actions',
'assadi.members'=>"assadi members",
'template.number'=>'number',
'part_of'=>'part of',
		//Materials
		'materials'=>'Types of films',

		//Products
		'products'=>'Types of packaged products',

		//Strapeless
		'strapless'=>"Types of handles",

		//banton
		'bantonColor'=>'Printing Pantone colors',

		//directions
		'directions'=>"directions",

		//respects
		'respects'=>"respects",

		//Color
		'color'=>'Printing',
		'colorOre'=>'Films colors',

		//RequestForm
		'RequestForm'=>'Order form',
                'agreement.OrderInfo2'=>"Basic Order information's",
		//RequestUsed
		'RequestUsed'=>'Order Use',

		//PrintingFace
		'PrintingFace'=>'Printing faces',

		//PrintingType
		'PrintingType'=>'Printing type',

		//company
		'LayersNumber'=>'layers number',

		'company'=>'Company',

		'company.name'=>'Company name',
		'company.mobile'=>'Mobile num',
		'company.address'=>'Address',
		'company.email'=>'Email',
		'company.website'=>'website',
		'company.phone'=>'Phone num',
		'company.fax'=>'Fax num',
		'company.assadi'=>'Al - Asadi Company for printing and trading of plastic materials',
		'company.view'=>'View company profile',
		'company.edit'=>'edit company profile',
		'company.workers'=>"company's representative",

		//Category
		'category'=>'Classifications',
		'category.view'=>'view Classifications',
		'category.viewFeatures'=>'Classifications Attributes',
		'category.TypeChoice'=>'Possibility to choose the type of packing',
		'category.StraplessChoice'=>'Possibility to choose the color of the handle',
		'category.StraplessColor'=>'Possibility to choose the type of the handle',
		'category.StraplessSoflaih' => 'Possibility to choose Soufleh type',
		'category.soflaih_up' => 'Top',
		'category.soflaih_below' => 'Below',
                'category.soflaih_side' => 'side',
		'category.copy'=>"copy",

		//units
		'units'=>'Units',

		//Currency
		'Currency'=>'Currencies',

		//ExtendSpecification
		'ExtendSpecification'=>'Complementary Specifications',

		//Info
		'info.title'=>'Al-Assadi information',
		'info.save'=>'save editions',
		'info.alert'=>'Now, you can edit and save your information',

		//Agreement
		'agreement.letter_star'=>'String *',
		'agreement.number_star'=>'Number *',

		//Memeber
		'member'=>"company's representative",
		'member.add'=>'add new representative to the company',
		'member.view'=>"view all company's representatives",
		'member.name'=>'Name',

		//Attr
		'Attr'=>'Attributes',
		'Attr.view'=>'view attributes',

		//drafts
		'drafts'=>'Drafts',
		'drafts.add'=>'write contract draft',
		'drafts.draft'=>'Draft',
		'drafts.date'=>'Date',
		'drafts.addeduser'=>'Added By',
		//Agreements
		'agreement.number'=> 'Number',
		'agreement.agreementNumber'=> 'Contract number',
		'agreement.agreementOrderName'=> 'Order name',
		'agreement.quantity'=> 'Quantity',
		'agreement.createdDate'=> 'creation date',
		'agreement.deleiverDate'=> 'delivery date',
		'agreement.note' => 'Note',
		'agreement.copy' => 'Copy',
		'agreement.viewNotes' => 'view notes',
		'agreement.viewDraft' => 'view draft',
		'agreement.addNotes' => 'add new note',
		'agreement.writeNote'=>'write the note',
		'agreement.msg1'=>'Are you sure you want to accept the contract ?!',
		'agreement.msg2'=>'Are you sure you want to reject the contract ?!',
		'agreement.msg3'=>'Are you sure you want to request a consultation about the contract ?!',
		'agreement.msg4'=>'Are you sure you want to continue ?!',

		//agreement_spec
		'agreement.specType'=>'Classifications types',
		'agreement.matbo'=>'Printed',
		'agreement.sadah'=>'Simple',
		'agreement.layers'=>'Layers',
		'agreement.layersMsg'=>'please add the layers number field',
		'agreement.type'=>'Type',
		'agreement.Materialtype'=>'Films type',
		'agreement.oreTypeMsg'=>'please add Films types',
		'agreement.inColor'=>'Inside color',
		'agreement.oreColorMsg'=>'please add Films colors',
		'agreement.outColor'=>'Outside color',
		'agreement.thickness'=>'thickness',
		'agreement.micron'=>'micron',
		'agreement.OrderInfo'=>'Order information',
		'agreement.basic'=>'Basic',
		'agreement.choseCompany'=>'chose company',
		'agreement.choseCompany.msg'=>'There are no companies to choose',
		'agreement.choseCompany.msg1'=>'add new company',
		'agreement.companyBy' => 'representative by ',
		'agreement.choseCompany.msg2'=>"please add company's representative",
		'agreementView.title'=>'Technical contract',
		'agreementView.quantity'=>'quantity',
		'agreementView.material'=>'films',
		'agreement.generalNotes'=>'General Notes',
		'agreementView.print'=>'Printing',
		'agreementView.printType'=>'Printing type',
		'agreementView.color'=>'Color',
		'agreementView.BantonColorNum'=>'Pantone number',
		'agreementView.roll'=>'Roll Specifications',
		'agreementView.kess'=>'Bag Specifications',
		'agreementView.agProduct' => 'Final Product Specifications',
		'agreementView.extras'=>'Complementary Specifications',
		"agreementView.printingLength"=>"Printing Length",
		'agreement.choseMsg1'=>'please add order form field',
		'agreement.choseMsg2'=>'please add order type field',
		'agreement.choseMsg3'=>'please add films type field',
		'agreement.choseMsg4' =>'please add unit field',
		'agreement.printingInfo'=>'Printing information',
		'agreement.MaterialDesc'=>'Films Specifications',
		'agreement.MaterialDescType'=>'Specifications type',
		'agreement.printingColors'=>'Printing colors',
		'agreement.printingInfo'=>'Printing information',
		'agreement.colorNumbaer'=> 'colors number',
		'agreement.taly'=>'Cold seal coated',
		'agreement.changedColors'=>'Changed Colors',
		'agreement.addchangedColors'=>'add new changed color',
		'agreement.details'=>'specification',
		'agreement.specialaiztions'=>'Classifications',
		'agreement.Extendspecialaiztions'=>'Complementary Specifications',
		'agreement.addNewColor'=>'add new color',
		'agreement.WarningMsg'=>"you can't leave this field empty!",
		'agreement.printingLength' =>"printing Length",
                'agreement.multipleColors'=>'Multiple colors',

		//editableTable

		'agreement.table.specType'=>'Classification details',
		'agreement.table.straplessType'=>'handle type',
		'agreement.table.choose'=>'--- chose a choice ---',
		'agreement.table.straplessMsg'=>"handle types havn't been entered yet",
		'agreement.table.straplessColor'=>'handle color',
		'agreement.table.colorMsg'=>"handle colors havn't been entered yet",
		'agreement.table.fillType'=>'type of packing',
		'agreement.table.fillType.top'=>'From the top',
		'agreement.table.fillType.down'=>'From the bottom',
                'agreement.table.fillType.behind'=>'From the behind',
                'agreement.table.soflaihType' => 'Soufleh Type',
		//FinancialAgreement
		'financial.title'=>'Financial contract',
		'financial.number'=>'Contract number',
		'financial.firstTeam'=>'Fisrt team',
		'financial.companyMember'=>"The company's representative",
		'financial.secondTeam'=>'Second Team',
		'financial.stayedIn'=>'Located in',
		'financial.singlePrice'=>'Individual price per one',
		'financial.currencyMsg'=>'please enter units fields',
		'financial.unitMsg'=>'please enter currencies fields',
		'financial.only'=>'Only',
		'financial.designCost'=>'Design fees',
		'financial.printKlesh'=>'Print Clips',
		'financial.printCilender'=>'Print Cylinders',
		'financial.totalCost'=>'Total Cost',
		'financial.value'=>'Cost',
                'fin.value'=>'value',

                'fin.date'=>'date',
                'fin.number'=>'number',
                'fin.notes'=>'notes',

		//Ajax pages
		'changedColor.title'=>'Changed Printing Colors',
		'changedColor.colorNum'=>'Colors number',
		'cards_number.tableCount'=>'table count',
		'cards_number.orderName'=>'Order Name',

		'cards_generated.categoryType'=>'Classification type',
		'cards_generated.Msg1'=>'please add classification type',
		'cards_generated.Msg2'=>'chose classification type please',

		'cards.categoryNum'=>'Classification numbers',

		//sidebar
		'contracts'=>'Contracts Managements',
		'contracts.drafts'=>'View drafts',
		'contracts.addNew'=>'Add new Contract',
		'contracts.viewAll'=>'New Technical Contracts',
		'contracts.financial'=>'New Financial Contracts',
		'contracts.archived'=>'ٌRunning Technical Contracts',
                'Financialcontracts.archived'=>'Running Financial Contracts',
'contracts.rejected'=>'Rejected Contracts',
		'home'=>'Administration',

		'priceOffers'=>'price offers',
		'priceOffers.add'=>'Add Price Offer',
		'priceOffers.view'=>'View Price Offers',
		'priceoffer.title'=>'price offer',
		'users.managment'=>"Users Managment",
		"users.view"=>"view Users",
		"users.add"=>"add User",

		'public.administration'=>'Administration',
		"public.packagedproducts"=>"Packaged Products Types",
		"public.filmstypes"=>"Films types",
		"public.handletypes"=>"Handle types",
		"public.printingPantone"=>"Printing Pantone",
		"public.PrintingColors"=>"Printing Colors",
		"public.filmsColors"=>"Films Colors",
		"public.printingForms"=>"Printing Forms",
		"public.orderuse"=>"Order use",
		"public.PrintingFaces"=>"Printing Faces",
		"public.PrintingTypes"=>"Printing Types",
		"public.LayersNumber"=>"Layers Number",
		"public.Companies"=>"Companies",
		"public.classifications"=>"Classifications",
		"public.currencies"=>"Currencies",
		"public.ComplementarySpecifications"=>"Complementary Specifications",
		"public.units"=>"Units",
		"public.AssadiInformation"=>"Assadi Information",
		"public.Cont.num"=>"Contract Number",
		"public.wage"=>"wages",
		"banton"=>"banton",
		"colors"=>"colors",
		"quantity"=>"quantity",
		"new contracts"=>"new contracts",
		"consultation"=>"consultation",
		"notes from technical to Executive"=>"notes from technical manager to Executive manager",
			//Wages
		'wage'=>"Wages Managment",

			'agreement.respects.msg1'=>'please add respect field',
			'roles' => 'roles ',
			'signOut' => 'logout',
		'and'=>'and',
		'mailBox.messageBody' => 'Message Content',
		'mailBox.sendMsg' => 'Send Mail',
		'mailBox.saveDraft' => 'Save as Draft',
		'mailBox.writeMsg' => 'Write your message here',
		'mailBox.cc' => 'CC',
		'mailBox.to' => 'TO',
		'mailBox.MsgTitle' => 'Message Title',
		'mailBox.newMsg' => 'New Message',
		'mailBox.new' => 'newest',
		'mailBox.last' => 'oldest',
		'mailBox.msg' => 'Message',
		'mailBox.sender' => 'Sender',
		'mailBox.msgDate' => 'Message Date',
		'mailBox.archive' => 'Archive',
		'mailBox.draft' => 'Drafts',
		'mailBox.sentMail' => 'Sent Mails',
		'mailBox.Inbox' => 'Inbox',
		'mailBox.InboxTitle' => 'Local Inbox'
		);
?>
