<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Auth Lang - English
*
* Author: Ben Edmunds
* 		  ben.edmunds@gmail.com
*         @benedmunds
*
* Author: Daniel Davis
*         @ourmaninjapan
*
* Location: http://github.com/benedmunds/ion_auth/
*
* Created:  03.09.2013
*
* Description:  English language file for Ion Auth example views
*
*/

// Errors
$lang['error_csrf'] = 'المعلومات الموجودة لا تحقق شروط الخصوصية والأمان الخاصة بنا';

// Login
$lang['login_heading']         = 'تسجيل الدخول';
$lang['login_subheading']      = 'تسجيل الدخول باستخدام اسم المستخدم';
$lang['login_identity_label']  = 'اسم المستخدم:';
$lang['login_password_label']  = 'كلمة المرور:';
$lang['login_remember_label']  = 'تذكرني';
$lang['login_submit_btn']      = 'التسجيل';
$lang['login_forgot_password'] = 'هل نسيت كلمة المرور؟';
$lang['login_title'] = 'لوحة التحكم';

// Index
$lang['index_heading']           = 'المستخدمون';
$lang['index_subheading']        = 'قائمة بالمستخدمين';
$lang['index_fname_th']          = 'الاسم الأول';
$lang['index_lname_th']          = 'الاسم الأخير';
$lang['index_email_th']          = 'البريد الالكتروني';
$lang['index_groups_th']         = 'مجموعات الصلاحيات';
$lang['index_status_th']         = 'الحالة';
$lang['index_action_th']         = 'العمليات';
$lang['index_active_link']       = 'مفعل';
$lang['index_inactive_link']     = 'غير مفعل';
$lang['index_create_user_link']  = 'إنشاء مستخدم جديد';
$lang['index_create_group_link'] = 'إنشاء مجموعة جديدة';

// Deactivate User
$lang['deactivate_heading']                  = 'إلغاء تفعيل المستخدم';
$lang['deactivate_subheading']               = 'هل أنت متأكد من إلغاء تفعيل حساب المستخدم';
$lang['activate_subheading']               = 'هل أنت متأكد من تفعيل حساب المستخدم';
$lang['deactivate_confirm_y_label']          = 'نعم:';
$lang['deactivate_confirm_n_label']          = 'لا:';
$lang['deactivate_submit_btn']               = 'إرسال';
$lang['deactivate_validation_confirm_label'] = 'تأكيد';
$lang['deactivate_validation_user_id_label'] = 'معرف المستخدم';

// Create User
$lang['create_user_heading']                           = 'إنشاء حساب';
$lang['create_user_subheading']                        = 'رجاء، أدخل معلومات المستخدم';
$lang['create_user_fname_label']                       = 'الاسم الأول:';
$lang['create_user_lname_label']                       = 'الاسم الأخير:';
$lang['create_user_identity_label']                    = 'اسم المستخدم:';
$lang['create_user_company_label']                     = 'اسم الشركة:';
$lang['create_user_email_label']                       = 'البريد الالكتروني:';
$lang['create_user_phone_label']                       = 'رقم الهاتف:';
$lang['create_user_password_label']                    = 'كلمة المرور:';
$lang['create_user_password_confirm_label']            = 'تأكيد كلمة المرور:';
$lang['create_user_submit_btn']                        = 'إنشاء';
$lang['create_user_validation_fname_label']            = 'الاسم الأول';
$lang['create_user_validation_lname_label']            = 'الاسم الأخير';
$lang['create_user_validation_identity_label']         = 'اسم المستخدم';
$lang['create_user_validation_email_label']            = 'البريد الالكتروني';
$lang['create_user_validation_phone_label']            = 'رقم الهاتف';
$lang['create_user_validation_company_label']          = 'اسم الشركة';
$lang['create_user_validation_password_label']         = 'كلمة المرور';
$lang['create_user_validation_password_confirm_label'] = 'تأكيد كلمة المرور';

// Edit User
$lang['edit_user_heading']                           = 'تعديل بيانات المستخدم';
$lang['edit_user_subheading']                        = 'رجاء, أدخل معلومات المستخدم';
$lang['edit_user_fname_label']                       = 'الاسم الأول:';
$lang['edit_user_lname_label']                       = 'الاسم الأخير:';
$lang['edit_user_company_label']                     = 'اسم الشركة:';
$lang['edit_user_email_label']                       = 'البريد الالكتروني:';
$lang['edit_user_phone_label']                       = 'رقم الهاتف:';
$lang['edit_user_password_label']                    = 'كلمة المرور: (إذا كنت تريد تغيير كلمة المرور)';
$lang['edit_user_password_confirm_label']            = 'تأكيد كلمة المرور: (إذا كنت تريد تغيير كلمة المرور)';
$lang['edit_user_groups_heading']                    = 'أعضاء المجموعة';
$lang['edit_user_submit_btn']                        = 'حفظ المعلومات';
$lang['edit_user_validation_fname_label']            = 'الاسم الأول';
$lang['edit_user_validation_lname_label']            = 'الاسم الأخير';
$lang['edit_user_validation_email_label']            = 'البريد الالكتروني';
$lang['edit_user_validation_phone_label']            = 'رقم الهاتف';
$lang['edit_user_validation_company_label']          = 'اسم الشركة';
$lang['edit_user_validation_groups_label']           = 'المجموعات';
$lang['edit_user_validation_password_label']         = 'كلمة المرور';
$lang['edit_user_validation_password_confirm_label'] = 'تأكيد كلمة المرور';

// Create Group
$lang['create_group_title']                  = 'إنشاء مجموعة';
$lang['create_group_heading']                = 'إنشاء مجموعة';
$lang['create_group_subheading']             = 'أدخل معلومات المجموعة';
$lang['create_group_name_label']             = 'اسم المجموعة:';
$lang['create_group_desc_label']             = 'الوصف:';
$lang['create_group_submit_btn']             = 'إنشاء المجموعة';
$lang['create_group_validation_name_label']  = 'اسم المجموعة';
$lang['create_group_validation_desc_label']  = 'الوصف';

// Edit Group
$lang['edit_group_title']                  = 'تعديل المجموعة';
$lang['edit_group_saved']                  = 'تم حفظ التعديلات';
$lang['edit_group_heading']                = 'تعديل بيانات المجموعة';
$lang['edit_group_subheading']             = 'رجاء، أدخل معلومات المجموعة';
$lang['edit_group_name_label']             = 'اسم المجموعة:';
$lang['edit_group_desc_label']             = 'الوصف:';
$lang['edit_group_submit_btn']             = 'حفظ المجموعة';
$lang['edit_group_validation_name_label']  = 'اسم المجموعة';
$lang['edit_group_validation_desc_label']  = 'الوصف';

// Change Password
$lang['change_password_heading']                               = 'تغيير كلمة المرور';
$lang['change_password_old_password_label']                    = 'كلمة المرور القديمة:';
$lang['change_password_new_password_label']                    = 'كلمة المرور الجديدة (عدد المحارف على الأقل %s ):';
$lang['change_password_new_password_confirm_label']            = 'تأكيد كلمة المرور الجديدة:';
$lang['change_password_submit_btn']                            = 'تغيير';
$lang['change_password_validation_old_password_label']         = 'كلمة المرور القديمة';
$lang['change_password_validation_new_password_label']         = 'كلمة المرور الجديدة';
$lang['change_password_validation_new_password_confirm_label'] = 'تأكيد كلمة المرور الجديدة';

// Forgot Password
$lang['forgot_password_heading']                 = 'نسيت كلمة المرور';
$lang['forgot_password_subheading']              = 'رجاء، أدخل %s لنرسل لك بريد الكتروني لتهيئة كلمة المرور';
$lang['forgot_password_email_label']             = '%s:';
$lang['forgot_password_submit_btn']              = 'إرسال';
$lang['forgot_password_validation_email_label']  = 'البريد الالكتروني';
$lang['forgot_password_username_identity_label'] = 'اسم المستخدم';
$lang['forgot_password_email_identity_label']    = 'البريد الالكتروني';
$lang['forgot_password_email_not_found']         = 'البريد الالكتروني غير موجود';
$lang['forgot_password_identity_not_found']         = 'اسم المستخدم غير موجود';

// Reset Password
$lang['reset_password_heading']                               = 'تغيير كلمة المرور';
$lang['reset_password_new_password_label']                    = 'كلمة المرور الجديدة (عدد المحارف على الأقل %s):';
$lang['reset_password_new_password_confirm_label']            = 'تأكيد كلمة المرور الجديدة:';
$lang['reset_password_submit_btn']                            = 'تغيير';
$lang['reset_password_validation_new_password_label']         = 'كلمة المررو الجديدة';
$lang['reset_password_validation_new_password_confirm_label'] = 'تأكيد كلمة المرور الجديدة';

// Activation Email
$lang['email_activate_heading']    = 'تفعيل حساب المستخدم %s';
$lang['email_activate_subheading'] = 'رجاء، اضغط الرابط ل %s.';
$lang['email_activate_link']       = 'تفعيل الحساب';

// Forgot Password Email
$lang['email_forgot_password_heading']    = 'إعادة تعيين كلمة المرور ل %s';
$lang['email_forgot_password_subheading'] = 'رجاء، اضغط هذا الرابط ل %s.';
$lang['email_forgot_password_link']       = 'إعادة تعيين كلمة المرور';

// New Password Email
$lang['email_new_password_heading']    = 'كلمة مرور جديدة ل %s';
$lang['email_new_password_subheading'] = 'تم تهيئة كلمة المرور ل: %s';

