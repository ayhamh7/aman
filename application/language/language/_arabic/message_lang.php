<?php 
	$lang = array(
		// global
		'global.add' => 'إضافة',
		'global.edit' => 'تعديل',
		'global.delete' => 'حذف',
		'global.copy' => 'نسخ',
		'global.view' => 'عرض',
		'global.editData' => 'تعديل البيانات',
		'global.close' => 'إغلاق',
		'global.print' => 'طباعة',
		'global.save' => 'حفظ',
		'global.search' => 'بحث',
		'global.done' => 'تم الانتهاء',
		'global.accept'=>'قبول',
		'global.reject'=>'رفض',
		'global.conseration'=>'استشارة',
		'global.messageDelete' => 'هل أنت متأكد من الحذف، هذه العملية لايمكن التراجع عنها',
		'global.porto'=>'طباعة روتوغرافيور',
		'global.flexo'=>' طباعة فليكسوغرافيك',
		'global.porto2'=>'rotogravure printing',
		'global.flexo2'=>'flexographic printing',
'draft.seen'=>'تمت المشاهدة',
'global.send'=>'إرسال',
'part_of'=>'جزءاً من الـ',
'Cylinder'=>'سيلندرات الطباعة',
'global.assign' =>'اسناد',
'global.restore'=>'استعادة',
'global.copynote'=>'نسخ ملاحظة المدير الفني',
'global.finish'=>'إنهاء',
'contracts.finished'=>'العقود المنفذة',
'admin.email'=>'البريد الالكتروني لاسترجاع الداتا بيز',
'admin.active'=>'عمل البرنامج',
'agreement.colorstring'=>'لون',
'priceoffer.date'   =>'تــاريــخ الــعـرض',
'priceoffer.enddate'=>'تاريخ نهاية العرض',
'Reports'=>'التقارير',
"new price offer"=>"عروض الأسعار الجديدة",
"draft price offer"=>"مسودات عروض الأسعار",
"accepted price offer"=>"العروض المقبولة",
		//Template File
		'template.equation'=>'القيمة في المعادلات',
		'template.nameInEng'=>'الاسم باللغة الإنكليزية',
		'template.nameInAra'=>'الاسم باللغة العربية',
		'template.multipleOrderMsg'=>'إمكانية اضافة أكثر من شكل للطلبية في نفس العقد',
		'template.id'=>'#',
		'template.Actions'=>'العمليات',
'template.number'=>'الرقم',
	         'assadi.members'=>"ممثلي شركة الاسدي",
		//Materials
		'materials'=>'أنواع الخامات',

		//Products
		'products'=>'المنتجات المعبئة',

		//Strapeless
		'strapless'=>"انواع المسكات",

		//banton
		'bantonColor'=>'بانتون ألوان الطباعة',

		//directions
		'directions'=>"الجهات",

		//respects
		'respects'=>"الاحترامات",

		//Color
		'color'=>'الطباعة',
		'colorOre'=>'ألوان الخامات',

		//RequestForm
		'RequestForm'=>'شكل الطلبية',

		//RequestUsed
		'RequestUsed'=>'استعمال الطلبية',

		//PrintingFace
		'PrintingFace'=>'أوحه الطباعة',

		//PrintingType
		'PrintingType'=>'نوع الطباعة',

		//company
		'LayersNumber'=>'عدد الطبقات',

		'company'=>'الشركات',

		'company.name'=>'اسم الشركة',
		'company.mobile'=>'رقم الموبايل',
		'company.address'=>'العنوان',
		'company.email'=>'البريد الالكتروني',
		'company.website'=>'رابط الموقع',
		'company.phone'=>'رقم الهاتف',
		'company.fax'=>'الفاكس',
		'company.assadi'=>'شركة الأسدي لطباعة وتجارة المواد البلاستيكية',
		'company.view'=>'عرض بيانات الشركات',
		'company.edit'=>'تعديل بيانات الشركة',
		'company.workers'=>'ممثلي الشركة',

		//Category
		'category'=>'التصنيفات',
		'category.view'=>'عرض الأصناف',
		'category.viewFeatures'=>'عرض الصفات',
		'category.TypeChoice'=>'إمكانية اختيار اتجاه التعبئة',
		'category.StraplessChoice'=>'إمكانية اختيار لون المسكة',
		'category.StraplessColor'=>'إمكانية اختيار نوع المسكة',
		'category.StraplessSoflaih' => 'إمكانية اختيار نوع السوفليه',
		'category.soflaih_up' => 'علوية',
		'category.soflaih_below' => 'قاعدية',
                'category.soflaih_side' => 'جانبية',
		'category.copy'=>"نسخ",

		//units
		'units'=>'الواحدات',

		//Currency
		'Currency'=>'العملات',

		//ExtendSpecification
		'ExtendSpecification'=>'المواصفات التكميلية',

		//Info
		'info.title'=>'معلومات شركة الأسدي',
		'info.save'=>'حفظ التعديلات',
		'info.alert'=>'يمكنك يعديل المعلومات وحفظها',

		//Agreement
		'agreement.letter_star'=>'الحرف *',
		'agreement.number_star'=>'الرقم *',

		//Memeber
		'member'=>'ممثلي الشركات',
		'member.add'=>'إضافة ممثل جديد للشركة',
		'member.view'=>'استعراض ممثلي الشركة',
		'member.name'=>'الاسم',

		//Attr
		'Attr'=>'الصفات',
		'Attr.view'=>'عرض الصفات',

		//drafts
		'drafts'=>'المسودات',
		'drafts.add'=>'اكتب مسودة العقد',
		'drafts.draft'=>'المسودة',
		'drafts.date'=>'التاريخ',
		'drafts.addeduser'=>'المستخدم',
		//Agreements
		'agreement.number'=> 'الرقم',
		'agreement.agreementNumber'=> 'رقم العـقــد',
		'agreement.agreementOrderName'=> 'اسم الطلبية',
		'agreement.quantity'=> 'الكمية',
		'agreement.createdDate'=> 'تاريخ الـعـقـد',
		'agreement.deleiverDate'=> 'تاريخ التسليم',
		'agreement.note' => 'ملاحظة',
		'agreement.copy' => 'نسخ',
		'agreement.viewNotes' => 'عرض الملاحظات',
		'agreement.viewDraft' => 'عرض المسودة',
		'agreement.addNotes' => 'إضافة الملاحظات',
		'agreement.writeNote'=>'اكتب الملاحظات',
		'agreement.msg1'=>'هل أنت متأكد من قبول العقد',
		'agreement.msg2'=>'هل أنت متأكد من رفض العقد',
		'agreement.msg3'=>'هل أنت متأكد من استشارة العقد',
		'agreement.msg4'=>'هل انت متأكد من العملية',

		//agreement_spec
		'agreement.specType'=>'نوع المواصفات',
		'agreement.matbo'=>'مطبوع',
		'agreement.sadah'=>'سادة',
		'agreement.layers'=>'الطبقات',
		'agreement.layersMsg'=>'إضافة حقل عدد الطبقات',
		'agreement.type'=>'النوع',
		'agreement.Materialtype'=>'نوع الخام',
		'agreement.oreTypeMsg'=>'إضافة حقل نوع الخام',
		'agreement.inColor'=>'اللون من الداخل',
		'agreement.oreColorMsg'=>'إضافة ألوان الخامات',
		'agreement.outColor'=>'اللون من الخارج',
		'agreement.thickness'=>'السماكة',
		'agreement.micron'=>'ميكرون',
'agreement.OrderInfo2'=>"معلومات الطلبية الأساسية",
		'agreement.OrderInfo'=>'معلومات الطلبية',
		'agreement.basic'=>'الأساسية',
		'agreement.choseCompany'=>'اختر الشركة',
		'agreement.choseCompany.msg'=>'لايوجد شركات لاختيارها',
		'agreement.choseCompany.msg1'=>'إضافة شركة جديدة',
		'agreement.companyBy' => 'ممثلة بـ',
		'agreement.choseCompany.msg2'=>'يرجى إضافة ممثل للشركة',
		'agreementView.title'=>'عـقـد فـنـي',
		'agreementView.material'=>'مواصفات الخام',
		'agreementView.print'=>'الطباعة',
		'agreementView.printType'=>'نوع الطباعة',
		'agreementView.color'=>'اللون',
		'agreementView.BantonColorNum'=>'رقم البانتون',
		'agreementView.roll'=>'مواصفات الرول',
		'agreementView.kess'=>'مواصفات الكيس',
		'agreementView.agProduct' => 'مواصفات المنتج النهائي',
		'agreementView.quantity'=>'الكمية',
		'agreementView.extras'=>'المواصفات التكميلية',

		'agreementView.printingLength'=>"طول الطباعة",
		'agreement.choseMsg1'=>'يرجى إضافة حقل شكل الطلبية',
		'agreement.choseMsg2'=>'يرجى إضافة حقل نوع الطلبية',
		'agreement.choseMsg3'=>'يرجى إضافة حقل المنتجات الخام',
		'agreement.choseMsg4' =>'يرجى إضافة حقل الواحدات',
		'agreement.printingInfo'=>'معلومات الطباعة',
		'agreement.MaterialDesc'=>'مواصفات الخام',
		'agreement.MaterialDescType'=>'نوع المواصفات',
		'agreement.printingColors'=>'ألوان الطباعة',
		'agreement.printingInfo'=>'معلومات الطباعة',
		'agreement.colorNumbaer'=> 'عدد الألوان',
		'agreement.taly'=>'طلي اللحام على البارد',
		'agreement.changedColors'=>'الألوان المتغيرة',
		'agreement.addchangedColors'=>'إضافة لون متغير جديد',
		'agreement.details'=>'المواصفات',
		'agreement.specialaiztions'=>'التصنيفات',
		'agreement.Extendspecialaiztions'=>'المواصفات التكميلية',
		'agreement.specialaiztions'=>'التصنيفات',
		'agreement.addNewColor'=>'إضافة لون جديد',
		'agreement.WarningMsg'=>'لا يمكنك ترك هذا الحقل فارغا',
		'agreement.multipleColors'=>'الكيس لديه أكثر من لون',
                'agreement.generalNotes'=>'مـلاحـظـات عـامــة',

		//editableTable

		'agreement.table.specType'=>'تفاصيل التصنيف',
		'agreement.table.straplessType'=>'نوع المسكة',
		'agreement.table.choose'=>'---حدد خيار---',
		'agreement.table.straplessMsg'=>'لم يتم إدخال أنواع مسكات بعد',
		'agreement.table.straplessColor'=>'لون المسكة',
		'agreement.table.colorMsg'=>'لم يتم إدخال ألوان المسكات',
		'agreement.table.fillType'=>'اتجاه التعبئة',
		'agreement.table.fillType.top'=>'من أعلى',
		'agreement.table.fillType.down'=>'من أسفل',
                'agreement.table.fillType.behind'=>'من الجانب',
		'agreement.table.soflaihType' => 'نوع السوفليه',
		
		//FinancialAgreement
		'financial.title'=>'عـقـد مـالـي',
		'financial.number'=>'رقم العقد',
		'financial.firstTeam'=>'الفريق الأول',
		'financial.companyMember'=>'ممثل الشركة',
		'financial.secondTeam'=>'الفريق الثاني',
		'financial.stayedIn'=>'ومقرها في ',
		'financial.singlePrice'=>' لكل 1',
		'financial.currencyMsg'=>'يرجى إضافة حقل الواحدات',
		'financial.wageMsg'=>'يرجى إضافة حقل الاجور',
		'financial.unitMsg'=>'يرجى إضافة حقل العملات',
		'financial.only'=>'فقط ',
		'financial.notother'=>'لاغير',
		'financial.designCost'=>'أجـــور التــصــمـيـم',
		'financial.printKlesh'=>'كليشات الطباعة',
		'financial.printCilender'=>'سليندرات الطباعة',
		'financial.totalCost'=>'الـقـيـمــة الـكـلــيــة',
		'financial.value'=>'قيمة',
		'financial.dicount'=>"يتم حسم قيمة ",
		'financial.dicount2'=>"بعد استجرار كمية ",
		'fin.value'=>'قيمة الدفعة',

                'fin.date'=>'تاريخ الدفعة',
                'fin.number'=>'رقم الدفعة',
                'fin.notes'=>'ملاحظات',
		//Ajax pages
		'changedColor.title'=>'الوان الطباعة المتغيرة',
		'changedColor.colorNum'=>'عدد الألوان',
		'cards_number.tableCount'=>'عدد الجداول',
		'cards_number.orderName'=>'اسم اطلبية',

		'cards_generated.categoryType'=>'نوع التصنيف',
		'cards_generated.Msg1'=>'يرجى إضافة حقل نوع التصنيف',
		'cards_generated.Msg2'=>'اختر تصنيف من فضلك',

		'cards.categoryNum'=>'عدد التصنيفات',

		'agreement.printingLength' =>"طول الطباعة",

		//Wages
		'wage'=>"ادارة الأجور",

		//sidebar
		'contracts'=>'إدارة العقود',
		'contracts.drafts'=>'عرض المسودات',
		'contracts.addNew'=>'اضافة عقد جديد',
		'contracts.viewAll'=>'العقود الفنية الجديدة',
		'contracts.financial'=>'العقود المالية الجديدة',
		'contracts.archived'=>'العقود الفنية الجارية',
                'Financialcontracts.archived'=>'العقود المالية الجارية',
'contracts.rejected' => 'العقود الملغاة',
		'home'=>'الرئيسية',

		'priceOffers'=>'عروض الأسعار',
		'priceOffers.add'=>'اضافة عرض سعر',
		'priceOffers.view'=>'عرض عروض الاسعار',
		'priceoffer.title'=>"عرض سعر",
		'users.managment'=>"إدارة المستخدمين",
		"users.view"=>"عرض المستخدمين",
		"users.add"=>"اضافة مستخدم",

		'public.administration'=>'الإدارة العامة',
		"public.packagedproducts"=>"أنواع المنتجات المعبئة",
		"public.filmstypes"=>"أنواع الخامات ",
		"public.handletypes"=>"أنواع المسكات",
		"public.printingPantone"=>"بانتون طباعة",
		"public.PrintingColors"=>"ألوان الطباعة ",
		"public.filmsColors"=>"ألوان الخامات",
		"public.printingForms"=>"أشكال الطلبية",
		"public.orderuse"=>"استخدام الطلبية",
		"public.PrintingFaces"=>"أوحه الطباعة",
		"public.PrintingTypes"=>"نوع الطباعة",
		"public.LayersNumber"=>"إدارة عدد الطبقات",
		"public.Companies"=>"الشركات",
		"public.classifications"=>"التصنيفات",
		"public.currencies"=>"العملات",
		"public.ComplementarySpecifications"=>"المواصفات التكميلية",
		"public.units"=>"الوحدات",
		"public.AssadiInformation"=>"معلومات الاسدي",
		"public.Cont.num"=>"رقم العقد",
		"public.wage"=>"الأجور",
		"banton"=>"البانتون",
		"colors"=>"الألوان",
		"quantity"=>"الكمية",
		"new contracts"=>"العقود الجديدة",
                "new contracts secretaria"=>"العقود الجديدة للسكرتيرة",
                "new contracts Exc"=>"العقود الجديدة للمدير التنفيذي",
                "new contracts Tech"=>"العقود الجديدة للمدير الفني",
                "new contracts buy"=>"العقود الجديدة لمدير المبيعات",
		"consultation"=>"الاستشارات",
		"notes from technical to Executive"=>"الملاحظات من المدير الفني إلى المدير التنفيذي",
		'agreement.respects.msg1'=>'يرجلى اضافة حقل الاحترامات',
		'roles' => 'صلاحيات',
		'signOut' => 'تسجيل الخروج',
		'and' =>'و',
		'mailBox.messageBody' => 'محتوى الرسالة',
		'mailBox.sendMsg' => 'إرسال الرسالة',
		'mailBox.saveDraft' => 'حفظ كمسودة',
		'mailBox.writeMsg' => 'اكتب الرسالة هنا',
		'mailBox.cc' => 'المعنيين',
		'mailBox.to' => 'المستلمين',
		'mailBox.MsgTitle' => 'عنوان الرسالة',
		'mailBox.newMsg' => 'رسالة جديدة',
		'mailBox.new' => 'الأحدث',
		'mailBox.last' => 'الأقدم',
		'mailBox.msg' => 'الرسالة',
		'mailBox.sender' => 'المرسل',
		'mailBox.msgDate' => 'تاريخ الرسالة',
		'mailBox.archive' => 'الأرشيف',
		'mailBox.draft' => 'المسودات',
		'mailBox.sentMail' => 'البريد المرسل',
		'mailBox.Inbox' => 'البريد',
		'mailBox.InboxTitle' => 'البريد الداخلي',

		// global
		'draft_get' => 'عرض المسودات',
		'draft/draft_post' => 'إضافة مسودة',
		'draft/draft_get_put' => 'تعديل المسودة',

		'agreament/addAgreament_get' => 'استعراض صفحة إضافة عقد',
		'member/member_post' => 'إضافة ممثل شركة',
		'LayersNumber/LayersNumber_post' => 'إضافة عدد طبقات',
		'Color/color_post' => 'إضافة لون جديد',
		'agreament/Agreament/Agreament_post' => 'إضافة عقد جديد',
		'agreament_get' => 'استعراض العقود الفنية',
		'agreament/changeStatus_post' => 'تغيير حالة العقد',
		'agreament/addNote_post' => 'إضافة ملاحظة للعقد',
		'agreament/FinancialAgreements_get' => 'استعراض العقود المالية / عقد مالي',
		'agreament/rejected_get' => 'استعراض العقود  الملغاة',
		'agreament/archive_get' => 'العقود الفنية الجارية',
		'agreament/FinancialArchive_get' => 'استعراض العقود المالية الجارية',
		'agreament/finished_get' => 'استعراض العقود المنتهية',
		'agreament/priceOffer_get' => 'استعراض عروض الأسعار',
		'agreament/priceOffer_post' => 'إضافة عرض سعر',
		'Agreament/priceOfferView_get' => 'استعراض عرض سعر',
		'agreament/priceOffers_get' => 'استعراض عروض الأسعار',

		'product_get' => 'عرض إدارة المنتجات المعبئة',
		'product/product_post' => 'إضافة نوع لأحد المنتجات المعبئة',
		'product/product_get' => 'عرض بيانات أحد المنتجات المعبئة',
		'product/product_delete' => 'حذف أحد المنتجات المعبئة',

		'material_get' => 'عرض إدارة أنواع الخامات',
		'material/material_post' => 'إضافة نوع لأحد أنواع الخامات',
		'material/material_get' => 'عرض بيانات أحد أنواع الخامات',
		'material/material_delete' => 'حذف أحد أنواع الخامات',

		'strapless_get' => 'عرض إدارة أنواع المسكات',
		'strapless/strapless_post' => 'إضافة نوع لأحد أنواع المسكات',
		'strapless/strapless_get' => 'عرض بيانات أحد أنواع المسكات',
		'strapless/strapless_delete' => 'حذف أحد أنواع المسكات',

		'bantonColor_get' => 'عرض إدارة بانتون ألوان الطباعة',
		'bantonColor/bantonColor_post' => 'إضافة نوع لأحد بانتون ألوان الطباعة',
		'bantonColor/bantonColor_get' => 'عرض بيانات أحد بانتون ألوان الطباعة',
		'bantonColor/bantonColor_delete' => 'حذف أحد أنواع بانتون ألوان الطباعة',

		'color_get' => 'عرض إدارة ألوان الطباعة',
		'color/color_post' => 'إضافة نوع لأحد ألوان الطباعة',
		'color/color_get' => 'عرض بيانات أحد ألوان الطباعة',
		'color/color_delete' => 'حذف أحد أنواع ألوان الطباعة',

		'oreColor_get' => 'عرض إدارة ألوان الخامات',
		'oreColor/oreColor_post' => 'إضافة نوع لأحد ألوان الخامات',
		'oreColor/oreColor_get' => 'عرض بيانات أحد ألوان الخامات',
		'oreColor/oreColor_delete' => 'حذف أحد أنواع ألوان الخامات',

		'RequestForm_get' => 'عرض إدارة أشكال الطلبية',
		'RequestForm/RequestForm_post' => 'إضافة نوع لأحد أشكال الطلبية',
		'RequestForm/RequestForm_get' => 'عرض بيانات أحد أشكال الطلبية',
		'RequestForm/RequestForm_delete' => 'حذف أحد أنواع أشكال الطلبية',

		'RequestUsed_get' => 'عرض إدارة استخدام الطلبية',
		'RequestUsed/RequestUsed_post' => 'إضافة نوع لأحد استخدام الطلبية',
		'RequestUsed/RequestUsed_get' => 'عرض بيانات أحد استخدام الطلبية',
		'RequestUsed/RequestUsed_delete' => 'حذف أحد أنواع استخدام الطلبية',


		'PrintingFace_get' => 'عرض إدارة أوجه الطباعة',
		'PrintingFace/PrintingFace_post' => 'إضافة نوع لأحد أوجه الطباعة',
		'PrintingFace/PrintingFace_get' => 'عرض بيانات أحد أوجه الطباعة',
		'PrintingFace/PrintingFace_delete' => 'حذف أحد أنواع أوجه الطباعة',

		'PrintingType_get' => 'عرض إدارة نوع الطباعة',
		'PrintingType/PrintingType_post' => 'إضافة نوع لأحد نوع الطباعة',
		'PrintingType/PrintingType_get' => 'عرض بيانات أحد نوع الطباعة',
		'PrintingType/PrintingType_delete' => 'حذف أحد أنواع نوع الطباعة',

		'LayersNumber_get' => 'عرض إدارة عدد الطبقات',
		'LayersNumber/LayersNumber_post' => 'إضافة نوع لأحد عدد الطبقات',
		'LayersNumber/LayersNumber_get' => 'عرض بيانات أحد عدد الطبقات',
		'LayersNumber/LayersNumber_delete' => 'حذف أحد أنواع عدد الطبقات',

		'Company_get' => 'عرض إدارة الشركات',
		'Company/Company_post' => 'إضافة نوع لأحد الشركات',
		'Company/Company_get' => 'عرض بيانات أحد الشركات',
		'Company/Company_delete' => 'حذف أحد أنواع الشركات',

		'Currency_get' => 'عرض إدارة العملات',
		'Currency/Currency_post' => 'إضافة نوع لأحد العملات',
		'Currency/Currency_get' => 'عرض بيانات أحد العملات',
		'Currency/Currency_delete' => 'حذف أحد أنواع العملات',

		'ExtendSpecification_get' => 'عرض إدارة المواصفات التكميلية',
		'ExtendSpecification/ExtendSpecification_post' => 'إضافة نوع لأحد المواصفات التكميلية',
		'ExtendSpecification/ExtendSpecification_get' => 'عرض بيانات أحد المواصفات التكميلية',
		'ExtendSpecification/ExtendSpecification_delete' => 'حذف أحد أنواع المواصفات التكميلية',

		'Unit_get' => 'عرض إدارة الوحدات',
		'Unit/Unit_post' => 'إضافة نوع لأحد الوحدات',
		'Unit/Unit_get' => 'عرض بيانات أحد الوحدات',
		'Unit/Unit_delete' => 'حذف أحد أنواع الوحدات',

		'Info_get' => 'عرض إدارة معلومات شركة الأسدي',
		'Info/Info_post' => 'إضافة نوع لأحد معلومات شركة الأسدي',
		'Info/Info_get' => 'عرض بيانات أحد معلومات شركة الأسدي',
		'Info/Info_delete' => 'حذف أحد أنواع معلومات شركة الأسدي',

		'Info_get' => 'عرض إدارة معلومات شركة الأسدي',
		'info/info_get' => 'إضافة نوع لأحد معلومات شركة الأسدي',
		'Info/Info_get' => 'عرض بيانات أحد معلومات شركة الأسدي',
		'Info/Info_delete' => 'حذف أحد أنواع معلومات شركة الأسدي',

		'Wage_get' => 'عرض إدارة الأجور',
		'Wage/Wage_post' => 'إضافة نوع لأحد الأجور',
		'Wage/Wage_get' => 'عرض بيانات أحد الأجور',
		'Wage/Wage_delete' => 'حذف أحد أنواع الأجور',

		'Direction_get' => 'عرض إدارة الجهات',
		'Direction/Direction_post' => 'إضافة نوع لأحد الجهات',
		'Direction/Direction_get' => 'عرض بيانات أحد الجهات',
		'Direction/Direction_delete' => 'حذف أحد أنواع الجهات',

		'Respect_get' => 'عرض إدارة الاحترامات',
		'Respect/Respect_post' => 'إضافة نوع لأحد الاحترامات',
		'Respect/Respect_get' => 'عرض بيانات أحد الاحترامات',
		'Respect/Respect_delete' => 'حذف أحد أنواع الاحترامات',


		'Agreament_number_get' => 'عرض إدارة رقم العقد',
		'Agreament_number/Agreament_number_post' => 'إضافة نوع لأحد رقم العقد',
		'Agreament_number/Agreament_number_get' => 'عرض بيانات أحد رقم العقد',
		'Agreament_number/Agreament_number_delete' => 'حذف أحد أنواع رقم العقد',

		//categories
		//Asadi worker
		'member_get' => 'عرض إدارة الممثلين',
		'member/member_post' => 'إضافة نوع لأحد الممثلين',
		'member/member_get' => 'عرض بيانات أحد الممثلين',
		'member/member_delete' => 'حذف أحد أنواع الممثلين',

		'Category_get' => 'عرض إدارة التصنيفات',
		'Category/Category_post' => 'إضافة نوع لأحد التصنيفات',
		'Category/Category_get' => 'عرض بيانات أحد التصنيفات',
		'Category/Category_delete' => 'حذف أحد أنواع التصنيفات',
		'Category/copyCategory_get' => 'نسخ بيانات تصنيف لإنشاء تصنيف جديد',


		'Attr_get' => 'عرض إدارة مواصفات التصنيف',
		'attr/attr_post' => 'إضافة صفة جديدة إلى التصنيف',
		'attr/attr_get' => 'عرض بيانات أحد صفات التصنيف',
		'attr/attr_delete' => 'حذف أحد صفات التصنيف',	

		'log.uri' => 'الرابط',
		'log.operation' => 'العملية',
		'log.operationType' => 'نوع العملية',
		'log.username'=>'اسم المستخدم',
		'log.time'=>'الوقت',
		'log.params'=>'القيم المدخلة',
		'log.ip'=>'عنوان الip الذي تمت منه العملية',
		'log.action'=>'العمليات',

		'get'=>'عرض',
		'post'=>'إضافة / تعديل',
		'put'=>'تعديل',
		'delete'=>'حذف'
		);
?>
