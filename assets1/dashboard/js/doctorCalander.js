
var base_url = 'http://localhost/dorian/';

$(document).ready(function(){
  calanderByDoc();
});

function calanderByDoc() {
  var doc_id = $('#calanderByDoc').val();
  var url = base_url + 'calanderByDoc/' + doc_id;
  $.ajax({
    type: "GET",
    url: url,
    success: function(data) {
      data = JSON.parse(data);
      console.log('data', data);
      $('#myCalander').fullCalendar('destroy');
      initCalander(data);
    }
  });
}

function initCalander(res){
  var started;
  var categoryClass;
  var calendar = $('#myCalander').fullCalendar({
    defaultView : 'agendaWeek',
    header : {
      left: 'prev,next today',
      center: 'title',
      right : ''
    },
    selectable: false,
    selectHelper: false,
    eventClick: function(calEvent, jsEvent, view) {
        console.log('calEvent', calEvent);
      $('#fc_edit').click();
      $('#title2').val(calEvent.title);
      $('#lb-start_at').html(moment(calEvent.start).format('YYYY/MM/DD HH:mm'));
      $('#lb-end_at').html(moment(calEvent.end).format('YYYY/MM/DD HH:mm'));
      $('#patientName').html(calEvent.patient_name);
      $('#patientProfile').attr('href', 'profile/patient' + calEvent.patient_id);
      categoryClass = $("#event_type").val();
      calendar.fullCalendar('unselect');
    },
    events : res.length > 0 ? $.map(res, function (obj){
      return {
        title : obj.title,
        start : new Date(obj.start_at),
        end : new Date (obj.end_at),
        patient_id : obj.patient_id,
        doctor_id : obj.doctor_id,
        patient_name : obj.first_name + ' ' + obj.last_name,
        id: obj.schedule_id 
      }
    }) : []
  });
};