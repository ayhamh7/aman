
var base_url = 'http://localhost/dorian/';

$(document).ready(function(){
  var url = base_url + 'schedule/index_get' ;
  $.ajax({
    type: "GET",
    url: url,
    success: function(data) {
      data = JSON.parse(data);
      initCalander(data);
    }
  });

});


function calanderByDep() {
  var dep_id = $('#calanderByDep').val();
  var url = base_url + 'calanderByDep/' + dep_id;
  $.ajax({
    type: "GET",
    url: url,
    success: function(data) {
      data = JSON.parse(data);
      console.log('data', data);
      $('#myCalander').fullCalendar('destroy');
      initCalander(data);
    }
  });
}

function calanderByDoc() {
  var doc_id = $('#calanderByDoc').val();
  var url = base_url + 'calanderByDoc/' + doc_id;
  $.ajax({
    type: "GET",
    url: url,
    success: function(data) {
      data = JSON.parse(data);
      console.log('data', data);
      $('#myCalander').fullCalendar('destroy');
      initCalander(data);
    }
  });

}

function bookDate(data){
  $.ajax({
    type: "POST",
    url: base_url + 'schedule/book',
    data : data,
    success: function(msg) {
      msg = JSON.parse(msg);
      new PNotify({
        title : 'success',
        text: msg,
        type: 'success'
      });
    },
    error : function (msg){
      msg = JSON.parse(msg);
      new PNotify({
        title : 'Error',
        text: msg.statusText,
        type: 'error'
      });
      location.reload();
    }
  });
}


function updateBook(data){
  $.ajax({
    type: "PUT",
    url: base_url + 'schedule/book',
    data : data,
    success: function(msg) {
      msg = JSON.parse(msg);
      new PNotify({
        title : 'Success',
        text: msg,
        type: 'success'
      });
    },
    error : function (msg){
      msg = JSON.parse(msg);
      new PNotify({
        title : 'error',
        text: msg.statusText,
        type: 'error'
      });
      location.reload();
    }
  });
}

function initCalander(res){
  var started;
  var categoryClass;
  var calendar = $('#myCalander').fullCalendar({
    defaultView : 'agendaWeek',
    header : {
      left: 'prev,next today',
      center: 'title',
      right : ''
    },
    selectable: true,
    selectHelper: true,
    select: function(start, end, allDay) {
      $('#lb-start_at').html(moment(start).format('YYYY/MM/DD HH:mm'));
      $('#lb-end_at').html(moment(end).format('YYYY/MM/DD HH:mm'));

      $('#fc_create').click();
      started = start;
      ended = end
      $(".antosubmit").on("click", function() {
        var title = $("#title").val();
        if (end) {
          ended = end
        }
        categoryClass = $("#event_type").val();
        if (title) {
          var data = {
            start_at : moment(start).format('YYYY/MM/DD HH:mm'),
            end_at : moment(end).format('YYYY/MM/DD HH:mm'),
            patient_id :  $('#patient').val(),
            doctor_id : $('#patient_doc').val(),
            title : title
          };
          var id = bookDate(data);
          calendar.fullCalendar('renderEvent', {
            title: title,
            start: started,
            end: end,
            patient_id : data.patient_id,
            doctor_id : data.doctor_id,
            id : id 
          },
                true // make the event "stick"
                );
        }
        $('#title').val('');
        calendar.fullCalendar('unselect');
        $('.antoclose').click();
        return true;
      });
    },
    eventClick: function(calEvent, jsEvent, view) {
        console.log('calEvent', calEvent);
      $('#fc_edit').click();
      $('#title2').val(calEvent.title);
      $('#lb-start_at2').html(moment(calEvent.start).format('YYYY/MM/DD HH:mm'));
      $('#lb-end_at2').html(moment(calEvent.end).format('YYYY/MM/DD HH:mm'));
      categoryClass = $("#event_type").val();
      $(".antosubmit2").on("click", function() {
        calEvent.title = $("#title2").val();
        var data = {
          start_at : calEvent.start,
          end_at : calEvent.end,
          patient_id :  $('#patient2').val(),
          doctor_id : $('#doctor_id2').val(),
          title : $('#title2').val(),
          id : event.schedule_id
        };
        updateBook(data);
        calendar.fullCalendar('updateEvent', calEvent);
        $('.antoclose2').click();
      });
      calendar.fullCalendar('unselect');
    },
    editable: true,
    eventDrop: function(event, delta) {
      var data = {
        start_at : moment(event.start).format('YYYY/MM/DD HH:mm'),
        end_at : moment(event.end).format('YYYY/MM/DD HH:mm'),
        patient_id :  event.patient_id,
        doctor_id : event.doctor_id,
        title : event.title,
        id : event.schedule_id
      }; 
      updateBook(data);
    },
    events : res.length > 0 ? $.map(res, function (obj){
      return {
        title : obj.title,
        start : new Date(obj.start_at),
        end : new Date (obj.end_at),
        patient_id : obj.patient_id,
        doctor_id : obj.doctor_id,
        id: obj.schedule_id 
      }
    }) : []
  });
};